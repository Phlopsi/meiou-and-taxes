name="M&T Indian Units and Ships DLC Support"
path="mod/MEIOUandTaxes_rep_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesRP.jpg"
supported_version="1.21.*.*"
