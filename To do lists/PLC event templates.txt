Krak�w academy:
	Event:
		Trigger:
			Somewhere around the historical date 1364
		Options:
			Fund it from royal treasury
				~1500 ducats - gain univ in Krak�w
			Fund it from royal salt mines
				income from Wieliczka & Bohnia halved - gain univ in Krak�w
				Allows to re-fund it later with money via decision, which removes the modifier.
			Ignore:
				prestige loss
		

POL-LIT union:
	Starting event:
		Trigger:
			Pol or Lit: female ruler or female regency
		MTTH modifier:
			RM
			High relations.
		Options:
			create PU for player/stronger nation, weighted towards POL
	Mission:
		Restore PU instead of vassalization

PLC formation:
	Decision - Propose Polish Lithuanian union:
		Allow:
			Lithuania/Poland vassal/PU
			At least 20% integration.
			
	Event - Unification Sejm Started:
		Trigger:
			Union proposed
	Event - Lithuanian nobles response:
		Options:
			Agree (the more likely the bigger relations/integration factor):
				PLC formed
			Require concessions (middle way):
				
			Oppose (if disliked):
				trigger
			

13YW:
	Starting event
		Is it needed?
		Mission better?
	Peace of Throrn
		Immediate:
			set flag PeaceofTHorn, disallowing vassalisation mission
		Options:
			Annex them:
				gives claims/gaining control over the whole TO lands, major relations hit and CB for emperor/pope
			Pomerelia must be Polish again:
				historical solution - royal&ducal prussia, small relations hit for the pope, medium for emperor
			We just want the Vistula Estuary:
				Annexes everything TO-owned west of Vistula
			
				
Livonian Order:
	Livonian Order asks for protection:
		Trigger:
			losing a war against orthodox country, reciever: neigbouring catholic, based on prestige
		Options:
			Accept their fealty and demand secularisation:
				historical solution - create KUR, rest annexed with gaining control/claims
			We need a strong ally:
				create LIV as ally (or vassal?), opinion boost
			It's their problem:
				does nothing

Hussite Wars - possibility of PU with BOH #historyczne powody
	Hussites ask for help in war
		send some trops for support rebelion: #event has continuation
			+hussites gets some more rebels
			- lost relation with BOH, PAP, HRE, catholic neighbours
			- lost manpower, 
			- lost cash
			- some catholic rebel (small one) 
		abandon this idea (maybe help fighting against them) #event chain ends
	
	Statute of Wielu� (continuation, final event in the chain)
		Be convinced by the pope to outlaw hussitism in Poland and cease all support for the heretics:
			- Improved relations with BOH&HRE&PAP
			- hussite minorities removed
			- hussite revolt in owned provinces with minorities (mainly hussite, perhaps also orthodox with a small probability).
	
	Hussites lost war 
		- lost prestige
		- some pope punishment
		- legitmacy lost
		
	Hussites win war
		+Polish king gain throne of BOH
		- BOH became HUSSITE
		- catholic rebels in Poland
		- event worst relation with CATHOLICS
		
		
				