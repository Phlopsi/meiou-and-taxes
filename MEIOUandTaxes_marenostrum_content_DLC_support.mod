name="M&T Mare Nostrum Content DLC Support"
path="mod/MEIOUandTaxes_marenostrum_content_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesMN.jpg"
supported_version="1.21.*.*"
