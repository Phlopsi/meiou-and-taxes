name="M&T Purple Phoenix DLC Support"
path="mod/MEIOUandTaxes_pp_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesPP.jpg"
supported_version="1.21.*.*"
