name="M&T Native Americans I Unit DLC Support"
path="mod/MEIOUandTaxes_naupa_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesNAN.jpg"
supported_version="1.21.*.*"
