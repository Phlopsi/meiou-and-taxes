###########################################################
# Events for the Indian Estate
#
# written by impspy
###########################################################
#Punjab
country_event = {
	id = indian_estate_disaster.1
	title = indian_estate_disaster.1.t
	desc = indian_estate_disaster.1.d
	picture = NOBLE_ESTATE_TAKE_OVER_eventPicture
	
	major = yes
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			release_all_subjects = yes
			if = {
				limit = {
					any_owned_province = { estate_punjabi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_punjabi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_punjabi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = PTA
					cede_province = PTA
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_sindhi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_sindhi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_sindhi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = SND
					cede_province = SND
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_delhi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_delhi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_delhi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = DLH
					cede_province = DLH
				}
			}
			if = {
				limit = {
					is_year = 1600
					any_owned_province = { estate_awadhi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_awadhi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_awadhi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = ODH
					cede_province = ODH
				}
			}
			if = {
				limit = {
					NOT = { is_year = 1600 }
					any_owned_province = { estate_awadhi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_awadhi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_awadhi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = AHM
					cede_province = AHM
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_bihari_trigger = yes }
					NOT = {
						capital_scope = {
							estate_bihari_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_bihari_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = TRT
					cede_province = TRT
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_bengali_trigger = yes }
					NOT = {
						capital_scope = {
							estate_bengali_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_bengali_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BNG
					cede_province = BNG
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_gujarati_trigger = yes }
					NOT = {
						capital_scope = {
							estate_gujarati_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_gujarati_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = GUJ
					cede_province = GUJ
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_mlw_rajput_trigger = yes }
					NOT = {
						capital_scope = {
							estate_mlw_rajput_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_mlw_rajput_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MLW
					cede_province = MLW
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_orissan_trigger = yes }
					NOT = {
						capital_scope = {
							estate_orissan_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_orissan_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = ORI
					cede_province = ORI
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_telegu_trigger = yes }
					NOT = {
						capital_scope = {
							estate_telegu_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_telegu_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = GOC
					cede_province = GOC
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_dakani_trigger = yes }
					NOT = {
						capital_scope = {
							estate_dakani_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						province_group = berar_group
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BRR
					cede_province = BRR
				}
				every_owned_province = {
					limit = {
						estate_dakani_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BAH
					cede_province = BAH
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_karnata_trigger = yes }
					NOT = {
						capital_scope = {
							estate_karnata_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_karnata_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = VIJ
					cede_province = VIJ
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_mysorean_trigger = yes }
					NOT = {
						capital_scope = {
							estate_mysorean_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_mysorean_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MYS
					cede_province = MYS
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_mysorean_trigger = yes }
					NOT = {
						capital_scope = {
							estate_mysorean_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_mysorean_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MYS
					cede_province = MYS
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_malabari_trigger = yes }
					NOT = {
						capital_scope = {
							estate_malabari_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_malabari_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = ZAM
					cede_province = ZAM
				}
			}
			if = {
				limit = {
					religion_group = muslim
					any_owned_province = { estate_carnatic_trigger = yes }
					NOT = {
						capital_scope = {
							estate_carnatic_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_carnatic_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = KRK
					cede_province = KRK
				}
			}
			if = {
				limit = {
					NOT = { religion_group = muslim }
					any_owned_province = { estate_carnatic_trigger = yes }
					NOT = {
						capital_scope = {
							estate_carnatic_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_carnatic_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = GNG
					cede_province = GNG
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_madurai_trigger = yes }
					NOT = {
						capital_scope = {
							estate_madurai_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_madurai_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MAD
					cede_province = MAD
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_ceylon_trigger = yes }
					NOT = {
						capital_scope = {
							estate_ceylon_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_ceylon_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = KTH
					cede_province = KTH
				}
			}
			if = {
				limit = {
					NOT = { is_year = 1650 }
					any_owned_province = { estate_marathas_trigger = yes }
					NOT = {
						capital_scope = {
							estate_marathas_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						province_group = bijapur_group
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BIJ
					cede_province = BIJ
				}
				every_owned_province = {
					limit = {
						province_group = ahmadnagar_group
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BAS
					cede_province = BAS
				}
			}
			if = {
				limit = {
					is_year = 1650
					any_owned_province = { estate_marathas_trigger = yes }
					NOT = {
						capital_scope = {
							estate_marathas_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_marathas_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MAR
					cede_province = MAR
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_jharkhandi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_jharkhandi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_jharkhandi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = NGP
					cede_province = NGP
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_sambalpuri_trigger = yes }
					NOT = {
						capital_scope = {
							estate_sambalpuri_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_sambalpuri_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BST
					cede_province = BST
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_gondi_trigger = yes }
					NOT = {
						capital_scope = {
							estate_gondi_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_gondi_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = GDW
					cede_province = GDW
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_bodo_trigger = yes }
					NOT = {
						capital_scope = {
							estate_bodo_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						estate_bodo_trigger = yes
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = ASS
					cede_province = ASS
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_rajput_trigger = yes }
					NOT = {
						capital_scope = {
							estate_rajput_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						province_id = 2718
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = JSL
					cede_province = JSL
				}
				every_owned_province = {
					limit = {
						province_id = 519
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = NAG
					cede_province = NAG
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 2719
							province_id = 2720
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BIK
					cede_province = BIK
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 512
							province_id = 514
							province_id = 3122
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MAW
					cede_province = MAW
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 512
							province_id = 514
							province_id = 3122
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MAW
					cede_province = MAW
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 518
							province_id = 525
							province_id = 2707
							province_id = 2708
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MEW
					cede_province = MEW
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 2706
							province_id = 2631
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = JAU
					cede_province = JAU
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 520
							province_id = 3117
							province_id = 528
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = HDT
					cede_province = HDT
				}
			}
			if = {
				limit = {
					any_owned_province = { estate_himalayan_trigger = yes }
					NOT = {
						capital_scope = {
							estate_himalayan_trigger = yes
						}
					}
				}
				every_owned_province = {
					limit = {
						province_id = 523
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = KMN
					cede_province = KMN
				}
				every_owned_province = {
					limit = {
						province_id = 562
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = SKK
					cede_province = SKK
				}
				every_owned_province = {
					limit = {
						province_id = 565
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = BHU
					cede_province = BHU
				}
				every_owned_province = {
					limit = {
						province_id = 2701
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = GRW
					cede_province = GRW
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 3110
							province_id = 3113
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = MND
					cede_province = MND
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 2700
							province_id = 3112
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = KGR
					cede_province = KGR
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 509
							province_id = 3107
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = LDK
					cede_province = LDK
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 2115
							province_id = 3105
							province_id = 2116
							province_id = 2226
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = KSH
					cede_province = KSH
				}
				every_owned_province = {
					limit = {
						OR = {
							province_id = 554
							province_id = 3198
							province_id = 2691
							province_id = 4124
							province_id = 557
							province_id = 4125
							province_id = 2692
						}
					}
					remove_core = ROOT
					remove_claim = ROOT
					add_core = NPL
					cede_province = NPL
				}
			}
		}
	}
	
	option = {
		name = indian_estate_disaster.1.a #The end
		end_disaster = estate_nobility_disaster_india		
		custom_tooltip = indian_collapse			
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_NOBILITY_DECLINES
			influence = -50
			duration = 5475
		}	
	}	
}

# Nobility brought to heel
country_event = {
	id = indian_estate_disaster.2
	title = indian_estate_disaster.2.t
	desc = indian_estate_disaster.2.d
	picture = NOBLE_ESTATE_eventPicture
	
	major = yes
	is_triggered_only = yes
	
	option = {
		name = indian_estate_disaster.2.a #Excellent
		end_disaster = estate_nobility_disaster_india
		hidden_effect = {
			add_estate_loyalty = {
				estate = estate_nobles
				loyalty = 10
			}
			add_estate_influence_modifier = {
				estate = estate_nobles
				desc = EST_VAL_NOBILITY_DECLINES
				influence = -50
				duration = 5475
			}
		}	
	}
}