country_decisions = {
	remove_wrath_of_god = {
		potential = {
			has_country_modifier = wrath_of_god
		}
		allow = {
			stability = 3
			num_of_cities = 10
			NOT = { num_of_loans = 1 }
			is_bankrupt = no
			NOT = { inflation = 10 }
			is_at_war = no
			ADM = 2
			DIP = 2
			MIL = 2
			OR = {
				ADM = 4
				advisor = statesman
				adm_tech = 25
				innovativeness_ideas = 2
				spy_ideas = 2
				economic_ideas = 2
				administrative_ideas = 2
				}
			OR = {
				MIL = 4
				advisor = army_organiser
				mil_tech = 25
				aristocracy_ideas = 2
				plutocracy_ideas = 2
				leadership_ideas = 2
				logistic_ideas = 2
				quality_ideas = 2
				quantity_ideas = 2
				}
			OR = {
				#FB war_exhaustion = 0
				NOT = { war_exhaustion = 1 }	#FB
				prestige = 50
				}
			adm_power = 400
			dip_power = 100
			mil_power = 100
		}
		effect = {
			remove_country_modifier = wrath_of_god
			add_adm_power = -400
			add_dip_power = -100
			add_mil_power = -100
			swap_free_idea_group = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
}