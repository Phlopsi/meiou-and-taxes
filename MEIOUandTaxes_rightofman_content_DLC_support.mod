name="M&T Right of Man Content DLC Support"
path="mod/MEIOUandTaxes_rightofman_content_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesRoM.jpg"
supported_version="1.21.*.*"
