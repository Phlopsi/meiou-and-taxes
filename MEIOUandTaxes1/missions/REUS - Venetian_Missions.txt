# Vanatian Missions

conquer_friuli = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		NOT = { owns = 111 }	# Friuli
		111 = {
			NOT = { owner = { alliance_with = ROOT } }
		}
		NOT = { has_country_modifier = venetian_terra_firma }
		NOT = { has_country_flag = conquered_fruili }
		111 = {
			OR = {
				is_capital = no
				owner = {
					NOT = { num_of_cities = 3 }
				}
			}
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 111
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 111
	}
	abort_effect = {
		remove_claim = 111
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_fruili
		add_country_modifier = {
			name = "venetian_terra_firma"
			duration = 3650
		}
		if = {
			limit = {
				111 = { NOT = { is_core = ROOT } }
			}
			111 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


subjugate_aquileia = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_vassalization }
		AQU = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = AQU }
			AQU = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		AQU = { vassal_of = VEN }
	}
	chance = {
		factor = 500
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = AQU value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = AQU
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AQU
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = AQU
			}
		}
	}
}


conquer_brescia = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = venetian_terra_firma }
		NOT = { has_country_flag = conquered_brescia }
		1345 = {
			NOT = { owner = { alliance_with = ROOT } }
			NOT = { owned_by = ROOT }
			any_neighbor_province = { owned_by = ROOT }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1345
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 1345
	}
	abort_effect = {
		remove_claim = 1345
	}
	effect = {
		add_prestige = 10
		set_country_flag = conquered_brescia
		add_country_modifier = {
			name = "venetian_terra_firma"
			duration = 3650
		}
		if = {
			limit = {
				1345 = { NOT = { is_core = ROOT } }
			}
			1345 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


conquer_verona = {
	
	type = country

	category = MIL
	ai_mission = yes

	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		NOT = { owns = 108 }	# Verona
		108 = {
			NOT = { owner = { alliance_with = ROOT } }
		}
		NOT = { has_country_modifier = venetian_terra_firma }
		NOT = { has_country_flag = conquered_verona }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 108
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 108
	}
	abort_effect = {
		remove_claim = 108
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_verona
		add_country_modifier = {
			name = "venetian_terra_firma"
			duration = 3650
		}
		if = {
			limit = {
				108 = { NOT = { is_core = ROOT } }
			}
			108 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


conquer_istria = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		owns = 112
		NOT = { owns = 130 }	# Istria
		NOT = { has_country_modifier = mediterranean_ambition }
		NOT = { has_country_flag = conquered_istria }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 130
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 130
	}
	abort_effect = {
		remove_claim = 130
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_istria
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		if = {
			limit = {
				130 = { NOT = { is_core = ROOT } }
			}
			130 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


conquer_cyprus = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		exists = TUR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = mediterranean_ambition }
		NOT = { has_country_flag = conquered_cyprus }
		321 = {								# Lefcosi
			NOT = { owned_by = ROOT }
			NOT = { owned_by = TUR }
		}
		2750 = {							# Famagusta
			NOT = { owned_by = ROOT }
			NOT = { owned_by = TUR }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			321 = { owned_by = TUR }
			2750 = { owned_by = TUR }
		}
	}
	success = {
		NOT = { war_with = TUR }
		owns = 321
		owns = 2750
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 321
		add_claim = 2750
	}
	abort_effect = {
		remove_claim = 321
		remove_claim = 2750
	}
	effect = {
		add_prestige = 10
		set_country_flag = conquered_cyprus
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		if = {
			limit = {
				321 = { NOT = { is_core = ROOT } }
			}
			321 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				2750 = { NOT = { is_core = ROOT } }
			}
			2750 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


retake_crete = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		exists = TUR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = mediterranean_ambition }
		NOT = { has_country_flag = crete_retaken }
		TUR = { owns = 163 }		# Kriti
		TUR = { owns = 2749 }		# Candia
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = TUR }
		owns = 163
		owns = 2749
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 163
		add_claim = 2749
	}
	abort_effect = {
		remove_claim = 163
		remove_claim = 2749
	}
	effect = {
		add_prestige = 10
		set_country_flag = crete_retaken
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		if = {
			limit = {
				163 = { NOT = { is_core = ROOT } }
			}
			163 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				2749 = { NOT = { is_core = ROOT } }
			}
			2749 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


monopolize_italian_cot = {
	
	type = country

	category = DIP
	
	allow = {
		tag = VEN
		NOT = {
			112 = {				# Gulf of Venice
				is_strongest_trade_power = VEN
			}
			101  = {				# Cote d'Azur
				is_strongest_trade_power = VEN
			}
		}
		NOT = { has_country_flag = monopolize_italian_cot }
	}
	abort = {
	}
	success = {
		112 = {			# Gulf of Venice
			is_strongest_trade_power = VEN
		}
		101 = {				# Cote d'Azur
			is_strongest_trade_power = VEN
		}	
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = national_trade_policy
		}	
	}
	effect = {
		add_treasury = 60
		add_dip_power = 60
		set_country_flag = monopolize_italian_cot
		add_country_modifier = {
			name = "merchant_society"
			duration = 3650
		}
	}
}


disrupt_genoese_trade = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = VEN
		exists = GEN
		GEN = {	
			has_country_flag = black_sea_free_trade
			owns = 1298				# Tana
			owns = 1299				# Tmutarkan
			owns = 2573				# Kerch
			owns = 101				# Genoa
		}
	}
	abort = {
		GEN = {
			NOT ={
				has_country_flag = black_sea_free_trade
			}
		}
	}
	success = {
		OR = {
			controls = 1298
			controls = 1299
			controls = 2573
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = national_trade_policy
		}
		modifier = {
			factor = 2
			VEN = { faction_in_power = mr_traders }
		}
	}
	effect = {
		add_treasury = 300
		1914 = {
			add_trade_modifier = {
				who = ROOT
				duration = 3650
				power = 5
				key = genoese_trade_concession
			}
		}
	}
}

conquer_kotor = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		owns = 112
		NOT = { owns = 138 }	# Kotor
		NOT = { has_country_modifier = mediterranean_ambition }
		NOT = { has_country_flag = conquered_kotor }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 138
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 2
			mil = 4
		}	
		modifier = {
			factor = 1.5
			RAG = { vassal_of = VEN }
		}
		modifier = {
			factor = 2
			VEN = { faction_in_power = mr_traders }
		}
	}
	immediate = {
		add_claim = 138
	}
	abort_effect = {
		remove_claim = 138
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_kotor
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		if = {
			limit = {
				138 = { NOT = { is_core = ROOT } }
			}
			138 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}

conquer_durres = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = VEN
		is_free_or_tributary_trigger = yes
		owns = 112
		NOT = { owns = 2373 }	# Durres
		NOT = { has_country_modifier = mediterranean_ambition }
		NOT = { has_country_flag = conquered_durres }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 2373
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 1.5
			VEN = { faction_in_power = mr_traders }
		}	
	}
	immediate = {
		add_claim = 2373
	}
	abort_effect = {
		remove_claim = 2373
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_durres
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		if = {
			limit = {
				2373 = { NOT = { is_core = ROOT } }
			}
			2373 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}
