# English Missions

conquer_ireland = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		region = ireland_region
		NOT = { owned_by = ROOT }
	}
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		is_free_or_tributary_trigger = yes
		mil = 4
		is_year = 1500
		owns = 373				# The Pale
		NOT = { has_country_modifier = british_ambition }
		NOT = { ireland_region = { type = all owned_by = ROOT } }
		}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			mil = 5
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_adm_power = 100
		add_country_modifier = {
			name = "british_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


conquer_scotland = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		OR = {
			area = highlands_area
			area = lowlands_area
		}
		owned_by = SCO
	}
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		is_free_or_tributary_trigger = yes
		mil = 4
		is_year = 1603
		SCO = {
			is_free_or_tributary_trigger = yes
			OR = {
				highlands_area = { owned_by = SCO }
				lowlands_area = { owned_by = SCO }
			}
		}
		NOT = { has_opinion = { who = SCO value = 50 } }
		NOT = { has_country_modifier = british_ambition }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			SCO = { is_subject_of = ROOT }
			any_target_province = {
				NOT = { owned_by = ROOT }
				NOT = { owned_by = SCO }
			}
		}
	}
	success = {
		NOT = { exists = SCO }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 5
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "british_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


vassalize_scotland = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		is_free_or_tributary_trigger = yes
		is_year = 1357
		exists = SCO
		SCO = {
			is_free_or_tributary_trigger = yes
			NOT = { num_of_cities = ROOT }
		}
		NOT = { has_global_flag = hundred_year_war }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SCO }
			SCO = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		SCO = { vassal_of = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
		modifier = {
			factor = 2
			dip = 5
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = SCO
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = SCO
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "military_vassalization"
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = SCO
			}
		}
	}
}


england_discovers_north_america = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		has_idea = quest_for_the_new_world
		num_of_ports = 3
		num_of_heavy_ship = 1
		east_america_superregion = { range = ROOT }
		NOT = { east_america_superregion = { has_discovered = ROOT } }
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		east_america_superregion = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_ports = 10
		}
		modifier = {
			factor = 2
			num_of_ports = 20
		}
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


english_colony_in_north_america = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_colonists = 1
		num_of_ports = 3
		NOT = { has_country_modifier = colonial_enthusiasm }
		east_america_superregion = {
			has_discovered = ROOT
			is_empty = yes
			range = ROOT
		}
		NOT = { east_america_superregion = { country_or_vassal_holds = ROOT } }
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { east_america_superregion = { is_empty = yes } }
				NOT = { east_america_superregion = { country_or_vassal_holds = ROOT } }
			}
		}
	}
	success = {
		east_america_superregion = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


england_discovers_the_carribean = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_ports = 3
		has_idea = quest_for_the_new_world
		east_america_superregion = { has_discovered = ROOT }
		NOT = { has_country_modifier = colonial_enthusiasm }
		carribeans_region = { range = ROOT }
		NOT = { carribeans_region = { has_discovered = ROOT } }
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		carribeans_region = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}	
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 730
		}
	}
}


english_colony_in_the_carribean = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_colonists = 1
		num_of_ports = 3
		NOT = { has_country_modifier = colonial_enthusiasm }
		carribeans_region = {
			has_discovered = ROOT
			is_empty = yes
			range = ROOT
		}
		NOT = { carribeans_region = { country_or_vassal_holds = ROOT } }
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { carribeans_region = { is_empty = yes } }
				NOT = { carribeans_region = { country_or_vassal_holds = ROOT } }
			}
		}
	}
	success = {
		carribeans_region = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
		modifier = {
			factor = 0.5
			any_subject_country = {
				carribeans_region = { owned_by = PREV }
			}
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 730
		}
	}
}


england_discovers_india = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_ports = 3
		has_idea = quest_for_the_new_world
		east_america_superregion = { has_discovered = ROOT }
		carribeans_region = { has_discovered = ROOT } 
		indian_coast_group = { range = ROOT }
		NOT = { indian_coast_group = { has_discovered = ROOT } }
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			NOT = {	has_idea = quest_for_the_new_world }
		}
	}
	success = {
		indian_coast_group = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}	
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 730
		}
	}
}


conquer_indian_coastal_province = {
	
	type = country
	ai_mission = yes

	category = MIL
	
	allow = {
		always = no #Replaced by Generic Mission with weights
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_ports = 3
		indian_coast_group = { has_discovered = ROOT }
		NOT = { indian_coast_group = { country_or_vassal_holds = ROOT } }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		indian_coast_group = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 5
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 5
			has_idea = colonial_ventures
		}
	}
	immediate = {
		indian_coast_group = {
			limit = { has_discovered = ROOT }
			add_claim = ROOT
		}
	}
	abort_effect = {
		indian_coast_group = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_treasury = 50
	}
}
 

england_discovers_australia = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_ports = 3
		has_idea = quest_for_the_new_world
		east_america_superregion = { has_discovered = ROOT }
		indian_coast_group = { has_discovered = ROOT }
		southern_australia_area = { range = ROOT }
		NOT = { southern_australia_area = { has_discovered = ROOT } }
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			NOT = { has_idea = quest_for_the_new_world }
		}
	}
	success = {
		southern_australia_area = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}	
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 730
		}
	}
}


english_colony_in_australia = {
	
	type = country

	category = DIP
	
	allow = {
		has_global_flag = south_wales_open
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		num_of_colonists = 1
		num_of_ports = 3
		NOT = { has_country_modifier = colonial_enthusiasm }
		southern_australia_area = {
			has_discovered = ROOT
			is_empty = yes
			range = ROOT
		}
		NOT = { southern_australia_area = { country_or_vassal_holds = ROOT } }
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { southern_australia_area = { is_empty = yes } }
				NOT = { southern_australia_area = { country_or_vassal_holds = ROOT } }
			}
		}
	}
	success = {
		southern_australia_area = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


establish_trade_in_indian_cot = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		has_discovered = 529
		has_discovered = 538
		has_discovered = 561
		NOT = {
			529 = {		# Konkan
				is_strongest_trade_power = ROOT
			}
			538 = {	# Malabar
				is_strongest_trade_power = ROOT
			}
			561 = {	# Bengal
				is_strongest_trade_power = ROOT
			}
		}
		NOT = { has_country_flag = establish_trade_in_indian_cot }
		NOT = { has_country_modifier = east_india_trade_rush }
	}
	abort = {
	}
	success = {
		OR = {
			529 = {		# Konkan
				is_strongest_trade_power = ROOT
			}
			538 = {	# Malabar
				is_strongest_trade_power = ROOT
			}
			561 = {	# Bengal
				is_strongest_trade_power = ROOT
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = shrewd_commerce_practise
		}
	}
	effect = {
		add_treasury = 40
		add_dip_power = 40
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		set_country_flag = establish_trade_in_indian_cot
	}
}


establish_trade_in_american_cot = {
	
	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = { has_global_flag = hundred_year_war }
		east_america_superregion = {
			has_discovered = ROOT
		}
		NOT = {
			965 = {		# New York
				OR = {
					is_strongest_trade_power = ENG
					is_strongest_trade_power = GBR
				}
			}
		}
		NOT = { has_country_flag = establish_trade_in_american_cot }
	}
	abort = {
	}
	success = {
			965 = {		# New York
			OR = {
				is_strongest_trade_power = ENG
				is_strongest_trade_power = GBR
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = shrewd_commerce_practise
		}
	}
	effect = {
		add_treasury = 40
		add_dip_power = 40
		set_country_flag = establish_trade_in_american_cot
	}
}

monopolize_british_cot = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = ENG
			tag = GBR
		}
		NOT = {
			236 = {		# London
				OR = {
					is_strongest_trade_power = ENG
					is_strongest_trade_power = GBR
				}
			}
		}
		OR = {
			ENG = { controls = 236 }
			GBR = { controls = 236 }
		}
		NOT = { has_country_flag = monopolize_british_cot }
	}
	abort = {
	}
	success = {
		236 = {		# London
			OR = {
				is_strongest_trade_power = ENG
				is_strongest_trade_power = GBR
			}
		}	
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = national_trade_policy
		}	
	}
	effect = {
		add_treasury = 40
		add_dip_power = 40
		set_country_flag = monopolize_british_cot
	}
}

occupy_paris = {
	
	type = country

	category = MIL
	
	allow = {
		tag = ENG
		exists = FRA
		war_with = FRA
		NOT = { is_year = 1475 }
		ouest_france_region = { owned_by = ROOT }
		FRA = { controls = 183 }
		183 = { is_capital = yes }
		NOT = { has_country_flag = has_occupied_paris }
		has_global_flag = hundred_year_war
	}
	abort = {
		OR = {
			NOT = { war_with = FRA }
			any_ally = { controls = 183 }
		}
	}
	success = {
		controls = 183
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			FRA = {
				NOT = { mil = 2 }
			}
		}
	}
	effect = {
		ouest_france_region = { add_claim = ROOT }
		add_prestige = 5
		set_country_flag = has_occupied_paris
	}
}


conquer_normandie_and_caux = {
	
	type = country
	ai_mission = yes

	category = MIL
	
	allow = {
		tag = ENG
		exists = FRA
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { is_year = 1475 }
		NOT = { has_country_modifier = military_victory }
		has_global_flag = hundred_year_war
		ouest_france_region = { owned_by = ROOT }
		FRA = {
			owns_or_vassal_of = 167	# Caux
			owns_or_vassal_of = 168	# Normandie
			owns_or_vassal_of = 2240	# Cotentin
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns_or_vassal_of = 167
		owns_or_vassal_of = 168
		owns_or_vassal_of = 2240
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = FRA value = -100 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = FRA value = -200 } }
		}
	}
	immediate = {
		add_claim = 167
		add_claim = 168
		add_claim = 2240
	}
	abort_effect = {
		remove_claim = 167
		remove_claim = 168
		remove_claim = 2240
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		if = {
			limit = {
				167 = { NOT = { is_core = ROOT } }
			}
			167 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				168 = { NOT = { is_core = ROOT } }
			}
			168 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				2240 = { NOT = { is_core = ROOT } }
			}
			2240 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


conquer_aquitaine = {
	
	type = country
	ai_mission = yes

	category = MIL
	
	allow = {
		tag = ENG
		exists = FRA
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { is_year = 1475 }
		NOT = { has_country_modifier = military_victory }
		has_global_flag = hundred_year_war
		ouest_france_region = { owned_by = ROOT }
		FRA = {
			owns_or_vassal_of = 2239	# Saintonge 
			owns_or_vassal_of = 176	# Gascogne
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { ouest_france_region = { owned_by = ROOT } }
		}
	}
	success = {
		owns_or_vassal_of = 2239
		owns_or_vassal_of = 176
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = FRA value = -100 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = FRA value = -200 } }
		}
	}
	immediate = {
		add_claim = 2239
		add_claim = 176
	}
	abort_effect = {
		remove_claim = 2239
		remove_claim = 176
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		if = {
			limit = {
				2239 = { NOT = { is_core = ROOT } }
			}
			2239 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				176 = { NOT = { is_core = ROOT } }
			}
			176 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


vassalize_france = {
	
	type = country

	category = MIL
	
	allow = {
		tag = ENG
		exists = FRA
		is_free_or_tributary_trigger = yes
		NOT = { is_year = 1475 }
		has_global_flag = hundred_year_war
		ouest_france_region = {
			owned_by = ROOT
			is_core = ROOT
		}
		FRA = {
			is_free_or_tributary_trigger = yes
			NOT = { num_of_infantry = ROOT }
			NOT = { num_of_cavalry = ROOT }
			NOT = { num_of_cities = ROOT }
		}
		NOT = { truce_with = FRA }
		NOT = { war_with = FRA }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = FRA }
			FRA = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		FRA = { junior_union_with = ENG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_restore_personal_union
			months = 120
			target = FRA
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_restore_personal_union
			target = FRA
		}
	}
	effect = {
		add_prestige = 10
		hidden_effect = {
			remove_casus_belli = {
				type = cb_restore_personal_union
				target = FRA
			}
		}
	}
}
