# Emperor Missions

improved_relations_with_electors = {
	
	type = elector_countries

	category = DIP

	allow = {
		FROM = {
			is_emperor = yes
			NOT = { has_country_modifier = foreign_contacts }
		}
		is_elector = yes
		is_emperor = no
		has_opinion = { who = FROM value = 0 }
		NOT = { has_opinion = { who = FROM value = 50 } }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		FROM = { is_emperor = no }
	}
	success = {
		has_opinion = { who = FROM value = 100 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.05
			NOT = { has_opinion = { who = FROM value = 0 } }
		}
		modifier = {
			factor = 1.07
			preferred_emperor = FROM
		}
		modifier = {
			factor = 1.04
			num_of_cities = 4
		}	
		modifier = {
			factor = 1.20
			religion = FROM
		}			
		modifier = {
			factor = 1.15
			primary_culture = FROM
		}
		modifier = {
			factor = 1.05
			dynasty = FROM
		}
		modifier = {
			factor = 1.02
			is_neighbor_of = FROM
		}	
	}
	effect = {
		FROM = { 
			add_adm_power = 10
			add_legitimacy = 5
			add_imperial_influence = 5
			add_country_modifier = {
				name = "foreign_contacts"
				duration = 365
			}
		}
	}
}


become_emperor = {
	
	type = country

	category = DIP

	allow = {
		is_emperor = no
		OR = {
			is_elector = yes
			capital_scope = {
				is_part_of_hre = yes
			}
		}
		any_known_country = {
			is_elector = yes
			preferred_emperor = FROM
		}
		is_free_or_tributary_trigger = yes
		is_lesser_in_union = no
		government = monarchy
		num_of_cities = 6
		OR = {
			NOT = { has_country_flag = empire_reclaimed }
			had_country_flag = { flag = empire_reclaimed days = 7300 }
		}
	}
	abort = {
		OR = {
			NOT = {
				OR = {
					is_elector = yes
					capital_scope = {
						is_part_of_hre = yes
					}
				}
			}		
			is_subject_other_than_tributary_trigger = yes
			is_lesser_in_union = yes
			NOT = { government = monarchy }
		}
	}
	success = {
		is_emperor = yes
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.25
			num_of_cities = 8
		}
		modifier = {
			factor = 1.50
			num_of_cities = 12
		}		
		modifier = {
			factor = 1.10
			culture_group = low_germanic
		}
		modifier = {
			factor = 1.10
			
		}
		modifier = {
			factor = 1.10
			culture_group = high_germanic
		}
	}
	effect = {
		add_stability_1 = yes
		add_imperial_influence = 10
		add_country_modifier = {
			name = "succesful_bid_for_imperial_crown"
			duration = 3650
		}
	}
}


convince_elector = {
	
	type = elector_countries

	category = DIP

	allow = {
		FROM = {
			OR = {
				culture_group = low_germanic
				
				culture_group = high_germanic
				capital_scope = { is_part_of_hre = yes }
				NOT = { has_country_modifier = foreign_contacts }
			}
			government = monarchy
			religion_group = christian
			num_of_cities = 4
			is_female = no
		}
		has_opinion = { who = FROM value = 50 }
		NOT = { preferred_emperor = FROM }
		NOT = { preferred_emperor = ROOT }
		NOT = { num_of_cities = 6 }
		is_free_or_tributary_trigger = yes
		ai = yes
	}
	abort = {
		OR = {
			ai = no
			is_elector = no
			is_subject_other_than_tributary_trigger = yes
			num_of_cities = 6
			NOT = {
				FROM = {
					OR = {
						culture_group = low_germanic
						
						culture_group = high_germanic
						capital_scope = { is_part_of_hre = yes }
						NOT = { has_country_modifier = foreign_contacts }
					}
					government = monarchy
					religion_group = christian
					num_of_cities = 4
				}
			}
			FROM = { is_female = yes allows_female_emperor = no}
		}
	}
	success = {
		preferred_emperor = FROM
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.07
			has_opinion = { who = FROM value = 50 }
		}
		modifier = {
			factor = 1.1
			religion = FROM
		}			
		modifier = {
			factor = 1.1
			primary_culture = FROM
		}
		modifier = {
			factor = 1.1
			dynasty = FROM
		}
		modifier = {
			factor = 1.1
			is_neighbor_of = FROM
		}	
	}
	effect = {
		FROM = { 
			add_prestige = 25
			add_legitimacy = 10
			add_dip_power = 50
			add_country_modifier = {
				name = "foreign_contacts"
				duration = 365
			}
		}
	}
}
