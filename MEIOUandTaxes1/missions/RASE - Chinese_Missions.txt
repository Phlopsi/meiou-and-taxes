# Chinese Missions

annex_dai_viet = {
	
	type = country

	category = MIL
	
	allow = {
		faction_in_power = temples
		tag = MNG
		is_free_or_tributary_trigger = yes
		mil = 4
		is_lesser_in_union = no
		exists = DAI
		NOT = { alliance_with = DAI }
		NOT = { has_country_modifier = military_victory }
		DAI = {
			is_neighbor_of = ROOT
			religion_group = ROOT
			NOT = { num_of_cities = ROOT }
			owns = 2395		# Hanoi
		}
	}
	abort = {
		OR = {
			NOT = { faction_in_power = temples }
			NOT = { exists = DAI }
			is_subject_other_than_tributary_trigger = yes
			is_lesser_in_union = yes
			DAI = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = DAI }
		owns = 2395
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = DAI value = 0 } }
		}
	}
	immediate = {
		add_claim = 2395
	}
	abort_effect = {
		remove_claim = 2395
	}
	effect = {
		add_army_tradition = 10
		add_stability_1 = yes
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}


defend_korea = {

	type = country
	
	category = MIL
	
	allow = {
		tag = MNG
		exists = JOS
		exists = JAP
		is_free_or_tributary_trigger = yes
		is_lesser_in_union = no
		KOR = {
			war_with = JAP
			is_neighbor_of = ROOT
		}
		war_with = JAP
	}
	abort = {
		OR = {
			NOT = { war_with = JAP }
			is_subject_other_than_tributary_trigger = yes
			is_lesser_in_union = yes	
		}
	}
	success = {
		NOT = { war_with = JAP }
		NOT = { korea_region = { owned_by = JAP } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		korea_region = { add_claim = ROOT }
	}
}


colonize_taiwan = {
	
	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = MNG
			tag = MCH
		}
		OR = {
			has_factions = no
			faction_in_power = enuchs
		}
		owns = 695						# Nanjing
		2305 = { is_empty = yes base_tax = 1 }
		num_of_colonists = 1
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = { has_country_flag = colonized_taiwan_CHI }
	}
	abort = {
		OR = {
			AND = { 
				NOT = { faction_in_power = enuchs }
				has_factions = yes
			}
			AND = {
				2305 = { is_empty = no }
				NOT = { owns = 2305 }
			}
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		owns = 2305
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
	}
	effect = {
		add_prestige = 3
		set_country_flag = colonized_taiwan_CHI
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


colonize_deren = {
	
	type = country
	
	category = DIP

	allow = {
		OR = {
			tag = MNG
			tag = MCH
		}
		1044 = {
			is_empty = yes
			base_tax = 1
			has_discovered = ROOT
		}
		OR = {
			has_factions = no
			faction_in_power = enuchs
		}
		num_of_colonists = 1
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
	}
	abort = {
		OR = {
			NOT = { 
				OR = {
					has_factions = no
					faction_in_power = enuchs
				}
			}
			AND = {
				1044 = { is_empty = no }
				NOT = { owns = 1044 }
			}
			NOT = { num_of_ports = 1 }
		}	
	}
	success = {
		owns = 1044
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
	}
	effect = {
		add_prestige = 3
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


china_discovers_india = {
	
	type = country
	
	category = DIP

	allow = {
		tag = MNG
		has_idea = quest_for_the_new_world
		NOT = { indian_coast_group = { has_discovered = ROOT } }
		num_of_ports = 1
	}
	abort = {
		NOT = {	has_idea = quest_for_the_new_world }
		NOT = { num_of_ports = 1 }
	}
	success = {
		indian_coast_group = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}	
	}
	effect = {
		add_treasury = 50
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


start_the_conquest_of_ming_china = {

	type = country
	
	category = MIL
	
	allow = {
		tag = MCH
		exists = MNG
		NOT = { war_with = MNG }
		NOT = { owns = 720 }		# Onggirat
		NOT = { owns = 2248 }		# Liaoxi
		NOT = { owns = 2249 }		# Liaodong
		is_lesser_in_union = no
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_victory }
	}
	abort = {
		OR = {
			NOT = { exists = MNG }
			is_lesser_in_union = yes
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		NOT = { war_with = MNG }
		OR = {
			owns = 720
			owns = 2248
			owns = 2249
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		add_claim = 720
		add_claim = 2248
		add_claim = 2249
	}
	abort_effect = {
		remove_claim = 720
		remove_claim = 2248
		remove_claim = 2249
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		if = {
			limit = {
				720 = { NOT = { is_core = ROOT } }
			}
			720 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				2248 = { NOT = { is_core = ROOT } }
			}
			2248 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				2249 = { NOT = { is_core = ROOT } }
			}
			2249 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}

