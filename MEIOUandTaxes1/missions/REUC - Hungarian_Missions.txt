# Hungary Missions

hungary_austria_relations = {
	
	type = country

	category = DIP
	
	allow = {
		tag = HUN
		exists = HAB
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { war_with = HAB }
		NOT = { marriage_with = HAB }
		HAB = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			war_with = HAB
			HAB = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = HAB
		HAB = { has_opinion = { who = HUN value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HAB value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HAB value = -100 } }
		}
	}
	effect = {
		add_stability_1 = yes
		add_country_modifier = {
				name = "foreign_contacts"
				duration = 3650
			}
	}
}


hungary_transylvania_relations = {
	
	type = country

	category = DIP
	
	allow = {
		tag = HUN
		exists = TRA
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { war_with = TRA }
		NOT = { marriage_with = TRA }
		TRA = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			war_with = TRA
			TRA = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = TRA
		TRA = { has_opinion = { who = HUN value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TRA value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TRA value = -100 } }
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
				name = "foreign_contacts"
				duration = 3650
			}
	}
}


vassalize_transylvania_hun = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = HUN
		exists = TRA
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_vassalization }
		TRA = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = TRA }
			TRA = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		TRA = { vassal_of = HUN }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TRA value = 100 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TRA value = 200 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = TRA
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = TRA
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = TRA
			}
		}
	}
}


conquer_the_banat = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = HUN
		exists = TRA
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = drove_back_the_turk }
		NOT = { has_opinion = { who = TRA value = 0 } }
		TRA = {
			is_free_or_tributary_trigger = yes
			owns = 1404		# Banat
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1404
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 1404
	}
	abort_effect = {
		remove_claim = 1404
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
		if = {
			limit = {
				1404 = { NOT = { is_core = ROOT } }
			}
			1404 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


retake_croatia = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = HUN
		exists = TUR
		is_free_or_tributary_trigger = yes
		NOT = { alliance_with = TUR }
		NOT = { has_country_modifier = drove_back_the_turk }
		mil = 4
		TUR = {
			is_neighbor_of = ROOT
			owns = 131				# Croatia
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 131
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 131
	}
	abort_effect = {
		remove_claim = 131
	}
	effect = {
		add_army_tradition = 25
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
		add_stability_1 = yes
		if = {
			limit = {
				131 = { NOT = { is_core = ROOT } }
			}
			131 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}
