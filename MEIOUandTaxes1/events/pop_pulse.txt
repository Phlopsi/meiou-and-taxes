# Author: Phlopsi
namespace = pop_pulse

country_event = {
	id = pop_pulse.004
	title = no_localization
	desc = no_localization
	picture = MEIOU_AND_TAXES_eventPicture

	hidden = yes
	is_triggered_only = yes

	trigger = {
		always = yes
	}

	immediate = {
		# Checks provinces to add siege malus
		country_event = { id = POP_sieges.001 }
		country_event = { id = POP_Estates.900 } ### Custom tooltip trigger tickers (phew!  Say that 3 times fast!)
	#	OTT ={ country_event = { id = POP_shifting_modifiers.006 } }
		
		every_country = {
		  country_event = {
		    id = POP_Global_AI_Decisions.018
		  }
		}
		every_country = { 
			limit = {
				exists = yes 
			}
		#	if = {
		#		limit = {
		#			has_country_modifier = GN_below_30_loyalty
		#			NOT = { has_country_flag = already_hit_with_GN_loyalty }
		#		}
		#		set_country_flag = already_hit_with_GN_loyalty
		#		country_event = {
		#			id = POP_Estates_Events.121
		#		}
		#	}
		#	if = {
		#		limit = {
		#			has_country_modifier = LN_below_30_loyalty
		#			NOT = { has_country_flag = already_hit_with_LN_loyalty }
		#		}
		#		set_country_flag = already_hit_with_LN_loyalty
		#		country_event = {
		#			id = POP_Estates_Events.122
		#		}
		#	}
		#	if = {
		#		limit = {
		#			has_country_modifier = BG_below_30_loyalty
		#			NOT = { has_country_flag = already_hit_with_BG_loyalty }
		#		}
		#		set_country_flag = already_hit_with_BG_loyalty
		#		country_event = {
		#			id = POP_Estates_Events.123
		#		}
		#	}
			
			
			
			
			
			if = {
				limit = {
					has_country_flag = GN_revokation_pain_signal
				}
				random_list = { 
					20 = { set_country_flag = GN_revokation_pain_1	}
					20 = { set_country_flag = GN_revokation_pain_2	}
					20 = { set_country_flag = GN_revokation_pain_3	}
					20 = { set_country_flag = GN_revokation_pain_4	}
					20 = { set_country_flag = GN_revokation_pain_5	}
				}
				if = {
					limit = { 
						has_country_flag = GN_revokation_pain_1
					}	
					country_event = { 
						id = POP_Estates.601 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = GN_revokation_pain_2
					}	
					country_event = { 
						id = POP_Estates.602 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = GN_revokation_pain_3
					}	
					country_event = { 
						id = POP_Estates.603 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = GN_revokation_pain_4
					}	
					country_event = { 
						id = POP_Estates.604 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = GN_revokation_pain_5
					}	
					country_event = { 
						id = POP_Estates.605 days = 1 
					}
				}
			}
			if = {
				limit = {
					has_country_flag = LN_revokation_pain_signal
				}
				random_list = { 
					20 = { set_country_flag = LN_revokation_pain_1	}
					20 = { set_country_flag = LN_revokation_pain_2	}
					20 = { set_country_flag = LN_revokation_pain_3	}
					20 = { set_country_flag = LN_revokation_pain_4	}
					20 = { set_country_flag = LN_revokation_pain_5	}
				}
				if = {
					limit = { 
						has_country_flag = LN_revokation_pain_1
					}	
					country_event = { 
						id = POP_Estates.701 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = LN_revokation_pain_2
					}	
					country_event = { 
						id = POP_Estates.702 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = LN_revokation_pain_3
					}	
					country_event = { 
						id = POP_Estates.703 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = LN_revokation_pain_4
					}	
					country_event = { 
						id = POP_Estates.704 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = LN_revokation_pain_5
					}	
					country_event = { 
						id = POP_Estates.705 days = 1 
					}
				}
			}
			if = {
				limit = {
					has_country_flag = BG_revokation_pain_signal
				}
				random_list = { 
					20 = { set_country_flag = BG_revokation_pain_1	}
					20 = { set_country_flag = BG_revokation_pain_2	}
					20 = { set_country_flag = BG_revokation_pain_3	}
					20 = { set_country_flag = BG_revokation_pain_4	}
					20 = { set_country_flag = BG_revokation_pain_5	}
				}
				if = {
					limit = { 
						has_country_flag = BG_revokation_pain_1
					}	
					country_event = { 
						id = POP_Estates.801 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = BG_revokation_pain_2
					}	
					country_event = { 
						id = POP_Estates.802 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = BG_revokation_pain_3
					}	
					country_event = { 
						id = POP_Estates.803 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = BG_revokation_pain_4
					}	
					country_event = { 
						id = POP_Estates.804 days = 1 
					}
				}
				if = {
					limit = { 
						has_country_flag = BG_revokation_pain_5
					}	
					country_event = { 
						id = POP_Estates.805 days = 1 
					}
				}
			}
		}
	}

	option = {
		name = no_localization
	}
}
