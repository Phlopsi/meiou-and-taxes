namespace = prisonner_of_war


province_event = {
	id = prisonner_of_war.001
	title = "prisonner_of_war.001.name"
	desc = "prisonner_of_war.001.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		owner = {
			is_at_war = yes
		}
		has_ruler_leader_from = FROM
		NOT = { any_country = { has_country_flag = ruler_prisonner_of_war } }
	}
	
	immediate = {
		hidden_effect = {
			1 = {
				change_variable = { which = "prisonner_of_war_001_fired" value = 1 }
			}
		}
	}

	option = {
		name = "prisonner_of_war.001.opta"
		if = {
			limit = {
				FROM = { NOT = { mil_tech = 10 } tag = BLO }
			}
			random_list = {
				75 = { }
				25 = { BLO = { kill_ruler = yes } MNF = { country_event = { id = flavor_bri.14 days = 0 } } }
			}
		}
		if = {
			limit = {
				FROM = { tag = CAS }
				CAS = { has_disaster = castilian_civil_war }
			}
			random_list = {
				75 = { }
				25 = { CAS = { country_event = { id = prisonner_of_war.002 days = 0 } save_event_target_as = faction_leader_captured } ENR = { country_event = { id = castilian_civil_war.31 days = 5 } save_event_target_as = faction_leader_success } }
			}
		}
		if = {
			limit = {
				FROM = { tag = ENR }
				CAS = { has_disaster = castilian_civil_war }
			}
			random_list = {
				10 = { ENR = { country_event = { id = prisonner_of_war.002 days = 0 } save_event_target_as = faction_leader_captured } CAS = { country_event = { id = castilian_civil_war.31 days = 5 } save_event_target_as = faction_leader_success } }
				90 = { }
			}
		}
		if = {
			limit = {
				FROM = { NOT = { mil_tech = 10 } NOT = { tag = BLO } }
			}
			random_list = {
				95 = { }
				5 = { FROM = { country_event = { id = prisonner_of_war.002 days = 0 } } }
			}
		}
		if = {
			limit = {
				FROM = { mil_tech = 10 NOT = { mil_tech = 20 } }
			}
			random_list = {
				96 = { }
				4 = { FROM = { country_event = { id = prisonner_of_war.002 days = 0 } } }
			}
		}
		if = {
			limit = {
				FROM = { mil_tech = 20 NOT = { mil_tech = 30 } }
			}
			random_list = {
				97 = { }
				3 = { FROM = { country_event = { id = prisonner_of_war.002 days = 0 } } }
			}
		}
		if = {
			limit = {
				FROM = { mil_tech = 40 NOT = { mil_tech = 50 } }
			}
			random_list = {
				98 = { }
				2 = { FROM = { country_event = { id = prisonner_of_war.002 days = 0 } } }
			}
		}
		if = {
			limit = {
				FROM = { mil_tech = 50 }
			}
			random_list = {
				99 = { }
				1 = { FROM = { country_event = { id = prisonner_of_war.002 days = 0 } } }
			}
		}
	}
}

country_event = {
	id = prisonner_of_war.002
	title = "prisonner_of_war.002.name"
	desc = "prisonner_of_war.002.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			1 = {
				change_variable = { which = "prisonner_of_war_002_fired" value = 1 }
			}
		}
	}
	
	option = {
		name = "prisonner_of_war.002.opta"
		hidden_effect = {
			set_country_flag = ruler_prisonner_of_war
			exile_heir_as = heir_prisonner_of_war_ROOT
			exile_ruler_as = ruler_prisonner_of_war_ROOT
		}
		define_ruler = { 
			regency = yes
			ADM = 0
			DIP = 0
			MIL = 0
		}
		add_war_exhaustion = 10
	}
}

country_event = {
	id = prisonner_of_war.100
	title = "prisonner_of_war.100.name"
	desc = "prisonner_of_war.100.desc"
	picture = CONQUEST_eventPicture
	
	trigger = {
		has_country_flag = ruler_prisonner_of_war
		OR = {
			had_country_flag = { flag = ruler_prisonner_of_war days = 1825 }
			ruler_age = 45
		}
		is_at_war = yes
	}
	
	mean_time_to_happen = {
		months = 120

		modifier = {
			ruler_age = 50
			factor = 1.1
		}
		modifier = {
			ruler_age = 55
			factor = 1.2
		}
		modifier = {
			ruler_age = 60
			factor = 1.3
		}
		modifier = {
			ruler_age = 65
			factor = 1.5
		}
		modifier = {
			ruler_age = 75
			factor = 2.0
		}
	}
	
	option = {
		name = "prisonner_of_war.100.opta"
		set_heir = heir_prisonner_of_war_ROOT
		clr_country_flag = ruler_prisonner_of_war
		add_war_exhaustion = 10
	}
}

country_event =  {
	id = prisonner_of_war.101
	title = "prisonner_of_war.101.name"
	desc = "prisonner_of_war.101.desc"
	picture = CONQUEST_eventPicture
	
	# is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = ruler_prisonner_of_war
        OR = {
            AND = {
                NOT = { has_country_flag = jean_prisonner_of_war }
                is_at_war = no
            }
            AND = {
                has_country_flag = jean_prisonner_of_war
                NOT = { war_with = ENG }
            }
        }
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "prisonner_of_war.101.opta"
		if = {
			limit = {
				has_regency = yes
			}
			country_event = { id = prisonner_of_war.102 days = 0 }
		}
		if = {
			limit = {
				OR = { has_regency = no has_consort_regency = yes }
			}
			country_event = { id = prisonner_of_war.103 days = 0 }
		}
	}
}

country_event =  {
	id = prisonner_of_war.102
	title = "prisonner_of_war.102.name"
	desc = "prisonner_of_war.102.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "prisonner_of_war.102.opta"
		set_ruler = ruler_prisonner_of_war_ROOT
		set_heir = heir_prisonner_of_war_ROOT
		clr_country_flag = ruler_prisonner_of_war
		if = {
			limit = {
				tag = FRA
				has_country_flag = king_jean_released
			}
			country_event = { id = on_new_monarch.2 days = 1 }
			clr_country_flag = king_jean_released
		}
	}
}

country_event =  {
	id = prisonner_of_war.103
	title = "prisonner_of_war.103.name"
	desc = "prisonner_of_war.103.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "prisonner_of_war.103.opta"
		set_ruler = ruler_prisonner_of_war_ROOT
		set_heir = heir_prisonner_of_war_ROOT
		clr_country_flag = ruler_prisonner_of_war
		random_list = {
			50 = { country_event = { id = prisonner_of_war.104 days = 0 } }
			50 = { country_event = { id = prisonner_of_war.105 days = 0 } }
		}
	}
}

country_event =  {
	id = prisonner_of_war.104
	title = "prisonner_of_war.104.name"
	desc = "prisonner_of_war.104.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "prisonner_of_war.104.opta"
	}
}

country_event =  {
	id = prisonner_of_war.105
	title = "prisonner_of_war.105.name"
	desc = "prisonner_of_war.105.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "prisonner_of_war.105.opta"
		capital_scope = { spawn_rebels = { type = pretender_rebels size = 2 } }
	}
}

country_event =  {
	id = prisonner_of_war.501
	title = "prisonner_of_war.501.name"
	desc = "prisonner_of_war.501.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "prisonner_of_war.501.opta" # Aquitaine
		ai_chance = { 
			factor = 90 
		}
		FRA = { country_event = { id = prisonner_of_war.511 days = 0 } }
	}
	option = {
		name = "prisonner_of_war.501.optb" # Aquitaine and Normandie
		ai_chance = { 
			factor = 10 
		}
		FRA = { country_event = { id = prisonner_of_war.521 days = 0 } }
	}
	option = {
		name = "prisonner_of_war.501.optc" # No negociation
		ai_chance = { 
			factor = 0 
		}
		FRA = { country_event = { id = prisonner_of_war.531 days = 0 } }
	}
}

country_event =  {
	id = prisonner_of_war.511
	title = "prisonner_of_war.511.name"
	desc = "prisonner_of_war.511.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = negociating_release_jean
	}
	
	option = {
		name = "prisonner_of_war.511.opta"
		ai_chance = { 
			factor = 90 
		}
		every_province = {
			limit = {
				province_group = aquitaine_group
				OR = {
					owned_by = ROOT
					owner = { vassal_of = ROOT }
					owner = { junior_union_with = ROOT }
				}
			}
			cede_province = GUY
		}
		# white_peace = ENG
		set_country_flag = king_jean_released
		country_event = { id = prisonner_of_war.102 days = 0 }
	}
	option = {
		name = "prisonner_of_war.501.optc" # No negociation
		ai_chance = { 
			factor = 10 
		}
	}
}

country_event =  {
	id = prisonner_of_war.521
	title = "prisonner_of_war.521.name"
	desc = "prisonner_of_war.521.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = negociating_release_jean
	}
	
	option = {
		name = "prisonner_of_war.521.opta"
		ai_chance = { 
			factor = 90 
		}
		every_province = {
			limit = {
				province_group = aquitaine_group
				OR = {
					owned_by = ROOT
					owner = { vassal_of = ROOT }
					owner = { junior_union_with = ROOT }
				}
			}
			cede_province = GUY
			add_core = ROOT
		}
		every_owned_province = {
			limit = {
				area = normandy_area
			}
			cede_province = ENG
		}
		# white_peace = ENG
		set_country_flag = king_jean_released
		country_event = { id = prisonner_of_war.102 days = 0 }
	}
	option = {
		name = "prisonner_of_war.501.optc" # No negociation
		ai_chance = { 
			factor = 10 
		}
	}
}

country_event =  {
	id = prisonner_of_war.531
	title = "prisonner_of_war.531.name"
	desc = "prisonner_of_war.531.desc"
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = negociating_release_jean
	}
	
	option = {
		name = "prisonner_of_war.531.opta" # No negociation
	}
}
