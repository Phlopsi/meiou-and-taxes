namespace = white_horde_collapse

#Start event
country_event = {
	id = white_horde_collapse.1
	title = "white_horde_collapse.1.t"
	desc = "white_horde_collapse.1.d"
	picture = ASSASSINATION_eventPicture
	
	major = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.OHNO"
		kill_ruler = yes
		if = {
			limit = {
				BLU = { vassal_of = ROOT }
			}
			free_vassal = BLU
		}
		hidden_effect = {
			clr_country_flag = white_horde_murdered_ruler
			set_country_flag = white_horde_collapse_ongoing
			add_legitimacy = -50
			define_heir = {
				name = "Nawruz Beg"
				dynasty = ROOT
				age = 20
				ADM = 1
				DIP = 1
				MIL = 1
			}
		}
	}
}

country_event = {
	id = white_horde_collapse.2
	title = "white_horde_collapse.2.t"
	desc = "white_horde_collapse.2.d"
	picture = ASSASSINATION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_heir = yes
		heir_age = 15
		OR = {
			AND = {
				NOT = { legitimacy = 75 }
				heir_claim = 75
			}
			AND = {
				NOT = { legitimacy = 50 }
				heir_claim = 50
			}
			AND = {
				NOT = { legitimacy = 25 }
				heir_claim = 25
			}
		}
		NOT = { has_country_modifier = puppet_khanate }
		has_regency = no
	}
	
	option = {
		name = "OPT.OHNO"
		kill_ruler = yes
		hidden_effect = {
			add_legitimacy = -50
		}
	}
}

country_event = {
	id = white_horde_collapse.3
	title = "white_horde_collapse.3.t"
	desc = "white_horde_collapse.3.d"
	picture = CIVIL_WAR_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		has_country_modifier = genghis_khanate
	}
	
	option = {
		name = "OPT.BASTARD"
		define_ruler = {
			name = "Mamai"
			dynasty = "Kiyat"
			ADM = 3
			DIP = 2
			MIL = 4
		}
		define_ruler_to_general = {
			fire = 4
			shock = 3
			manuever = 3
			siege = 1
		}
		hidden_effect = {
			remove_country_modifier = genghis_khanate
			add_country_modifier = {
				name = puppet_khanate
				duration = -1
			}
			kill_heir = yes
		}
	}
}

country_event = {
	id = white_horde_collapse.4
	title = "white_horde_collapse.4.t"
	desc = "white_horde_collapse.4.d"
	picture = CIVIL_WAR_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		OR = {
			has_regency = yes
			NOT = { legitimacy = 60 }
		}
		has_country_modifier = genghis_khanate
	}
	
	option = {
		name = "OPT.BASTARD"
		define_ruler = {
			dynasty = ROOT
			ADM = 1
			DIP = 1
			MIL = 1
		}
		hidden_effect = {
			add_legitimacy = -75
			kill_heir = yes
		}
	}
}

country_event = {
	id = white_horde_collapse.5
	title = "white_horde_collapse.5.t"
	desc = "white_horde_collapse.5.d"
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "OPT.VERYWELL"
		hidden_effect = {
			clr_country_flag = white_horde_collapse_ongoing
		}
	}
}

country_event = {
	id = white_horde_collapse.6
	title = "white_horde_collapse.6.t"
	desc = "white_horde_collapse.6.d"
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "OPT.VERYWELL"
	}
}
