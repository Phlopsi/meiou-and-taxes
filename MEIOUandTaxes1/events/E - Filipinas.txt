### Phiilippinese Events


### Rise of Maguindanao
country_event = {
	id = orient_pearl.1
	title = orient_pearl.1.t
	desc = orient_pearl.1.d
	picture = RELIGIOUS_TURMOIL_eventPicture
	fire_only_once = yes

	trigger = {
		is_year = 1450
		capital_scope = {
			OR = {
				region = malaya_region
				region = sumatra_region
				region = borneo_region
			}
		}
		religion = sunni
		any_province = {
			area = mindanao_area
			is_empty = yes
		}
	}

	mean_time_to_happen = {
		months = 720
	}
	
	option = { 
		name = orient_pearl.1.a
		random_province = {
			limit = {
				area = mindanao_area
				is_empty = yes
			}
			add_core = MGD
			cede_province = MGD
		}
		hidden_effect = {
			every_known_country = {
				limit = {
					NOT = { tag = ROOT }
					capital_scope = {
						superregion = southeast_asia_superregion
					}
				}
				country_event = { id = orient_pearl.2 days = 0 }
			}
		}
	}
}

country_event = {
	id = orient_pearl.2
	title = orient_pearl.2.t
	desc = orient_pearl.2.d
	picture = RELIGIOUS_TURMOIL_eventPicture
	fire_only_once = yes
	is_triggered_only = yes
	
	option = { 
		name = orient_pearl.2.a
	}
	option = { 
		name = orient_pearl.2.b
	}
}