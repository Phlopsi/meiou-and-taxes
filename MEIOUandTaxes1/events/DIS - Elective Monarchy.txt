

#Succession crises copied from dharper's work
country_event = {
	id = meiouelective_monarchy.101
	title = meiouelective_monarchy.101.t
	desc = meiouelective_monarchy.101.d
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes
	
	immediate = { 
		hidden_effect = {
			if = {
				limit = {
					NOT = { tag = BOH }
					BOH = {
						is_neighbor_of = ROOT
						government = monarchy
						is_female = no
						is_lesser_in_union = no
					}
				}
				random = {
					chance = 30
					BOH = { country_event = { id = meiouelective_monarchy.102 days = 1 } }
					 add_country_modifier = {
						name = bohemia_elected
						duration = 100
						hidden = yes
					}					
				}
			}
			if = {
				limit = {
					NOT = {
						has_country_modifier = bohemia_elected
					}
					NOT = { tag = HUN }
					HUN = {
						is_neighbor_of = ROOT
						government = monarchy
						is_female = no
						is_lesser_in_union = no
					}
				}
				random = {
					chance = 30
					HUN = { country_event = { id = meiouelective_monarchy.102 days = 1 } }
					 add_country_modifier = {
						name = hungary_elected
						duration = 100
						hidden = yes
					}					
				}
			}
			if = {
				limit = {
					NOT = {
						has_country_modifier = bohemia_elected
						has_country_modifier = hungary_elected
					}
					NOT = { tag = HAB }
					HAB = {
						is_neighbor_of = ROOT
						government = monarchy
						is_female = no
						is_lesser_in_union = no
					}
				}
				random = {
					chance = 15
					HAB = { country_event = { id = meiouelective_monarchy.102 days = 1 } }
					 add_country_modifier = {
						name = austria_elected
						duration = 100
						hidden = yes
					}					
				}
			}
			if = {
				limit = {
					NOT = {
						has_country_modifier = bohemia_elected
						has_country_modifier = hungary_elected
						has_country_modifier = austria_elected
					}
					NOT = { tag = POL }
					POL = {
						is_neighbor_of = ROOT
						government = monarchy
						is_female = no
						is_lesser_in_union = no
					}
				}
				random = {
					chance = 25
					POL = { country_event = { id = meiouelective_monarchy.102 days = 1 } }
					 add_country_modifier = {
						name = poland_elected
						duration = 100
						hidden = yes
					}					
				}
			}
			if = {
				limit = {
					NOT = {
						has_country_modifier = bohemia_elected
						has_country_modifier = hungary_elected
						has_country_modifier = austria_elected
						has_country_modifier = poland_elected
					}
				}
				#country_event = { id = meiouelective_monarchy.XXX days = 1 } #local pretender rises
			}
		}
	}	
	
	option = {
		name = meiouelective_monarchy.101.a #oh dear
		exile_ruler_as = weak_heir_exiled
		set_country_flag = weak_heir_exiled
	}	
}

# Crown offered
country_event = {
	id = meiouelective_monarchy.102
	title = meiouelective_monarchy.102.t
	desc = meiouelective_monarchy.102.d
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes
	
	option = {		# We accept
		name = meiouelective_monarchy.102.a
		every_country = {
			limit = {
				senior_union_with = FROM
			}
			break_union = FROM
		}
		create_union = FROM
		set_country_flag = overlord_contested_kingdom	
		FROM = { country_event = { id = meiouelective_monarchy.103 days = 1 } } #weak heir become a rebel stack
		#ai_chance = { factor = 60 }
	}
	#option = {		# We decline
	#	name = meiouelective_monarchy.102.b
	#	FROM = { country_event = { id = meiouelective_monarchy.XXX days = 1 } } #local pretender rises
	#	ai_chance = { factor = 40 }
	#}
}

# foreign rule
country_event = {
	id = meiouelective_monarchy.103
	title = meiouelective_monarchy.103.t
	desc = meiouelective_monarchy.103.d
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes
	
	option = {		
		name = meiouelective_monarchy.103.a
		random_owned_province = {
			limit = {
				NOT = { units_in_province = 1 }
			}
			spawn_rebels = {
				type = pretender_rebels
				size = 10
				win = yes
			}
		}
	}
}

# end of first chain
country_event = {
	id = meiouelective_monarchy.104
	title = meiouelective_monarchy.104.t
	desc = meiouelective_monarchy.104.d
	picture = LIBERUM_VETO_eventPicture
	
	trigger = {
		has_disaster = elective_succession_crisis
		is_lesser_in_union = yes
		overlord = {
			has_country_flag = overlord_contested_kingdom
		}
		NOT = {
			num_of_rebel_armies = 1
		}
		
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {		
		name = meiouelective_monarchy.104.a
		clr_country_flag = elective_crises_start
		end_disaster = elective_succession_crisis
		overlord = {
			clr_country_flag = overlord_contested_kingdom
#			country_event = { id = meiouelective_monarchy.105 days = 1 }
		}
	}
}

country_event = {
	id = meiouelective_monarchy.105
	title = meiouelective_monarchy.104.t
	desc = meiouelective_monarchy.104.d
	picture = LIBERUM_VETO_eventPicture
	
	trigger = {
		has_disaster = elective_succession_crisis
		had_country_flag = {
			 flag = elective_crises_start
			 days = 3650
		}
		overlord = {
			has_country_flag = overlord_contested_kingdom
		}
		NOT = {
			num_of_rebel_armies = 1
		}
		
	}
	
	mean_time_to_happen = { months = 120 }
	
	option = {		
		name = meiouelective_monarchy.104.a
		clr_country_flag = elective_crises_start
		end_disaster = elective_succession_crisis
	}
}
