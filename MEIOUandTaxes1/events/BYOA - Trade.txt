##############################################
#                                            #
#     Trade.1 Bi-Yearly On Action Events     #
#                                            #
##############################################


# Respect our flag
country_event = {
	id = trade.1
	title = trade.1.t
	desc = trade.1.d
	picture = NAVAL_MILITARY_eventPicture
	
	is_triggered_only = yes
	trigger = {
		num_of_merchants = 3
		capital_scope = {
			has_trader = root
			NOT = { is_strongest_trade_power = root }
		}
		
		has_idea_group = naval_ideas
	}
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			trade_ideas = 1
		}
		modifier = {
			factor = 3
			trade_ideas = 3
		}
		modifier = {
			factor = 5
			trade_ideas = 7
		}
	}

	option = {
		name = trade.1.a
		add_prestige = -15
		capital_scope = {
			strongest_trade_power = { 
				reverse_add_casus_belli = {
					target = ROOT
					type = cb_trade_war_triggered
					months = 60
				}
			}
		}
	}
	option = {
		name = trade.1.b
		random_list = { 30 = {} 40 = { subtract_variable = { which = stability_points value = 50 } } 30 = { subtract_stability_1 = yes } }
	}
}

# This cannot stand
country_event = {
	id = trade.2
	title = trade.2.t
	desc = trade.2.d
	picture = NAVAL_MILITARY_eventPicture
	
	is_triggered_only = yes
	trigger = {
		num_of_merchants = 3
		num_of_light_ship = 1
		capital_scope = {
			has_trader = root
			has_port = yes
			NOT = { is_strongest_trade_power = root }
			NOT = { 
				has_trade_modifier = {
					who = ROOT
					name = pirate_hunting
				}
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			trade_ideas = 1
		}
		modifier = {
			factor = 3
			trade_ideas = 3
		}
		modifier = {
			factor = 5
			trade_ideas = 7
		}
	}

	option = {
		name = trade.2.a
		capital_scope = {
			add_trade_modifier = {
				who = root
				duration = 1825
				power = -10
				key = pirate_hunting
			}
		}
		add_dip_power = -75
	}
	option = {
		name = trade.2.b
		random_list = { 30 = {} 40 = { subtract_variable = { which = stability_points value = 50 } } 30 = { subtract_stability_1 = yes } }
	}
}

# Expulsion of Merchants
country_event = {
	id = trade.3
	title = trade.3.t
	desc = trade.3.d
	picture = MERCHANTS_TALKING_eventPicture
	
	is_triggered_only = yes

	trigger = {
		num_of_merchants = 3
		any_active_trade_node = {
			NOT = { most_province_trade_power = { tag = ROOT } }
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = trade.3.a
		ai_chance = { factor = 100 }
		random_active_trade_node = {
			limit = {
				NOT = { most_province_trade_power = { tag = ROOT } }
			}
			most_province_trade_power = {
				add_opinion = {
					who = root
					modifier = merchants_too_succesful
				}
			}
			add_trade_modifier = {
				who = root
				duration = 365
				power = 5
				key = merchants_too_succesful
			}
		}
	}
	option = {
		name = trade.3.b
		random_active_trade_node = {
			limit = {
				NOT = { most_province_trade_power = { tag = ROOT } }
			}
			most_province_trade_power = {
				add_opinion = {
					who = root
					modifier = merchants_standing_down
				}
			}
			add_trade_modifier = {
				who = root
				duration = 365
				power = -5
				key = merchants_standing_down
			}
		}		
	}
}

# Trade Crisis
country_event = {
	id = trade.4
	title = trade.4.t
	desc = trade.4.d
	picture = MERCHANTS_TALKING_eventPicture
	
	is_triggered_only = yes

	trigger = {
		trade_income_percentage = 0.33
		NOT = { has_country_modifier = minor_trade_crisis }
		NOT = { has_country_modifier = severe_trade_crisis }
	}

	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			NOT = { adm = 3 }
		}
		modifier = {
			factor = 1.5
			trade_income_percentage = 0.5
		}
	}

	option = {
		name = trade.4.a
		add_country_modifier = {
			name = "severe_trade_crisis"
			duration = 365
		}
	}
	option = {
		name = trade.4.b
		add_dip_power = -50
		add_country_modifier = {
			name = "minor_trade_crisis"
			duration = 365
		}
	}
}

# Merchant Bankrupt!
country_event = {
	id = trade.5
	title = trade.5.t
	desc = trade.5.d
	picture = MERCHANTS_TALKING_eventPicture
	
	is_triggered_only = yes

	trigger = {
		num_of_merchants = 3
		NOT = { luck = yes }
		any_active_trade_node = {
			NOT = { strongest_trade_power = { tag = ROOT } }
		}
	}

	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			NOT = { trade_ideas = 3 }
		}
		modifier = {
			factor = 1.5
			NOT = { trade_ideas = 7 }
		}		
	}

	option = {
		name = trade.5.a
		random_active_trade_node = {
			limit = {
				NOT = { strongest_trade_power = { tag = ROOT } }
			}
			recall_merchant = ROOT
			add_trade_modifier = {
				who = ROOT
				duration = 180
				power = -15
				key = merchant_bankrupt
			}
		}
	}
	option = {
		name = trade.5.b
		add_dip_power = -20
	}
	
	option = {
		name = trade.5.c
		random_active_trade_node = {
			limit = {
				NOT = { strongest_trade_power = { tag = ROOT } }
			}
			add_trade_node_income = -20
		}
	}
}

# Merchant Shipwrecked!
country_event = {
	id = trade.6
	title = trade.6.t
	desc = trade.6.d
	picture = MERCHANTS_TALKING_eventPicture
	
	is_triggered_only = yes

	trigger = {
		num_of_merchants = 3
		NOT = { luck = yes }
		any_active_trade_node = {
			is_sea = yes
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = trade.6.a
		random_active_trade_node = {
			limit = {
				is_sea = yes
			}
			add_trade_node_income = -10
		}		
	}
}

