
# Tribal Succession Crisis
country_event = {
	id = tribal_succession.1
	title = tribal_succession.1.t
	desc = tribal_succession.1.d
	picture = KING_SICK_IN_BED_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government = steppe_horde
		OR = {
			NOT = { legitimacy = 50 }
			has_regency = yes
		}
	}
	
	option = {
		name = tribal_succession.1.a
		trigger = {
			NOT = { legitimacy = 50 }
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = pretender_organizing }
			}
			add_province_modifier = {
				name = "pretender_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = pretender_organizing }
			}
			add_province_modifier = {
				name = "pretender_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		every_owned_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		add_ruler_modifier = {
			name = "tribal_succession_crisis"
		}
		custom_tooltip = tribal_crisis_can_end_when

	}
	
	option = {
		name = tribal_succession.1.b
		trigger = {
			has_regency = yes
		}
		define_ruler = {
			dip = 3
			mil = 3
			adm = 3
		}
		add_legitimacy = -70
	}
}

# Succession Secured!
country_event = {
	id = tribal_succession.2
	title = tribal_succession.2.t
	desc = tribal_succession.2.d
	picture = NEW_HEIR_eventPicture
	
	trigger = {
		government = steppe_horde
		has_ruler_modifier = tribal_succession_crisis
		NOT = {
			any_owned_province = {
				has_province_modifier = pretender_organizing
			}
		}
		OR = { has_regency = no has_consort_regency = yes }
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {
		name = tribal_succession.1.a
		remove_country_modifier = tribal_succession_crisis
		add_stability_1 = yes
		add_legitimacy = 25
	}
}
	