########################################
# Events for Mali
#
# written by Henrik Lohmander
########################################

# Rise of Fulo
province_event = {
	id = flavor_mal.2
	title = flavor_mal.2.t
	desc = flavor_mal.2.d
	picture = HORDE_ON_HORSEBACK_eventPicture
	fire_only_once = yes

	trigger = {
		province_id = 2906
		NOT = { exists = FLO }
		is_year = 1470
		tekrur_area = {
			is_empty = yes
		}
	}

	mean_time_to_happen = {
		years = 15
	}
	
	option = {
		name = flavor_mal.2.a
		random_province = {
			limit = {
				OR = {
					area = tekrur_area
					province_id = 348	#Tichitt
					province_id = 1111	#Kars el Barka
					province_id = 2774	#Futa Tooro
					province_id = 2906	#Kantor
					province_id = 2912	#Bambuk
					province_id = 2965	#Galam
					province_id = 2966	#Futa Jallon
				}
				is_empty = yes
			}
			# add_base_tax = 5
			# add_base_production = 5
			# add_base_manpower = 3
			
			cede_province = FLO
			add_core = FLO
			owner = {
				release = FLO
			}
		}
		every_province = {
			limit = {
				OR = {
					area = tekrur_area
					province_id = 348	#Tichitt
					province_id = 1111	#Kars el Barka
					province_id = 2774	#Futa Tooro
					province_id = 2906	#Kantor
					province_id = 2912	#Bambuk
					province_id = 2965	#Galam
					province_id = 2966	#Futa Jallon
				}
			}
			add_core = FLO
			owner = {
				country_event = { id = flavor_mal.3 }
			}
		}
		every_province = {
			limit = {
				OR = {
					area = tekrur_area
					province_id = 348	#Tichitt
					province_id = 1111	#Kars el Barka
					province_id = 2774	#Futa Tooro
					province_id = 2906	#Kantor
					province_id = 2912	#Bambuk
					province_id = 2965	#Galam
					province_id = 2966	#Futa Jallon
				}
				is_empty = no
			}
			add_province_modifier = {
				name = "mal_fulani_migration"
				duration = 3650
			}
			change_culture = fulani
		}
		every_province = {
			limit = {
				OR = {
					area = tekrur_area
					province_id = 348	#Tichitt
					province_id = 1111	#Kars el Barka
					province_id = 2774	#Futa Tooro
					province_id = 2906	#Kantor
					province_id = 2912	#Bambuk
					province_id = 2965	#Galam
					province_id = 2966	#Futa Jallon
				}
				is_empty = yes
			}
			cede_province = FLO
			# add_base_tax = 5
			# add_base_production = 5
			# add_base_manpower = 3
		}
		FLO = {
			add_absolutism = -30
			
			define_ruler = {
				name = "Koli Tengella"
				dynasty = "Denianke"
				MIL = 4
				claim = 100
			}
			capital_scope = {
				cavalry = FLO
			}
			every_owned_province = {
				infantry = FLO
			}
		}
	}
}

# Event to send to other province owners of the area when Fulo is formed.
country_event = {
	id = flavor_mal.3
	title = flavor_mal.2.t
	desc = flavor_mal.2.d
	picture = HORDE_ON_HORSEBACK_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { owns = 2906 }
	}

	option = { 
		name = flavor_nub.2.a
		tooltip = {
			every_province = {
				limit = {
					OR = {
						area = tekrur_area
						province_id = 348	#Tichitt
						province_id = 1111	#Kars el Barka
						province_id = 2774	#Futa Tooro
						province_id = 2906	#Kantor
						province_id = 2912	#Bambuk
						province_id = 2965	#Galam
						province_id = 2966	#Futa Jallon
					}
				}
				add_core = FLO
			}
		}
	}
}


# The Wangara and the Portuguese
country_event = {
	id = flavor_mal.4
	title = flavor_mal.4.t
	desc = flavor_mal.4.d
	picture = ECONOMY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MAL
		exists = POR
		is_year = 1455 #ie after the initial, more violent encounters.
		any_owned_province = {
			west_africa_region_trigger = yes
			has_discovered = POR
		}
	}
	
	mean_time_to_happen = {
		months = 180
	}

	option = {
		name = flavor_mal.4.a
		add_country_modifier = {
			name = mal_portuguese_traders
			duration = -1
		}
	}
}

#Portuguese Traders Meddle in Local Politics.
country_event = {
	id = flavor_mal.5
	title = flavor_mal.5.t
	desc = flavor_mal.5.d
	picture = ACCUSATION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MAL
		exists = POR
		is_year = 1465
		has_country_modifier = mal_portuguese_traders
		any_owned_province = {
			west_africa_region_trigger = yes
		}
	}
	
	mean_time_to_happen = {
		months = 300
	}

	option = {
		name = flavor_mal.5.a	#Act decisively to limit Portuguese influence
		add_country_modifier = {
			name = mal_wangaras_upset
			duration = -1
		}
		remove_country_modifier = mal_portuguese_traders
		POR = {
			add_opinion = { who = MAL modifier = trade_conflict }
		}
	}
	
	option = {
		name = flavor_mal.5.b	#The problem lies with the disloyal chiefs
		add_stability_1 = yes
		every_owned_province = {
			limit = {
				OR = {
					west_africa_region_trigger = yes
					province_id = 1111
					province_id = 2770
					province_id = 2906
				}
	 		}
			add_province_modifier = {
				name = "mal_portuguese_meddling"
				duration = 3650
			}
		}
	}
}

#Increasing Islamisation of the Aristocracy
country_event = {
	id = flavor_mal.6
	title = flavor_mal.6.t
	desc = flavor_mal.6.d
	picture = RELIGIOUS_TURMOIL_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MAL
		religion_group = muslim
	}
	
	mean_time_to_happen = {
		months = 450
	}

	option = {
		name = flavor_mal.6.a	#Convert our people!
		add_piety = 0.15
		add_country_modifier = {
			name = mal_islamisation_of_the_aristocracy
			duration = -1
		}
	}
	
	option = {
		name = flavor_mal.6.b	#Let us not rock the boat.
		add_stability_1 = yes
		add_piety = -0.15
	}
}

#Unsafe Roads
country_event = {
	id = flavor_mal.7
	title = flavor_mal.7.t
	desc = flavor_mal.7.d
	picture = OVEREXTENSION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MAL
		is_year = 1480
		NOT = { is_year = 1550  }
		OR = {
			is_neighbor_of = FLO
			any_owned_province = {
				has_province_modifier = mal_fulani_migration
			}
		}
	}
	
	mean_time_to_happen = {
		months = 210
	}

	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					is_capital = no
					any_neighbor_province = {
						is_capital = no
					}
				}
				set_province_flag = mali_roads_unsafe
			}
			random_owned_province = {
				limit = {
					is_capital = no
					any_neighbor_province = {
						is_capital = no
					}
					NOT = { has_province_flag = mali_roads_unsafe }
				}
				set_province_flag = mali_roads_unsafe
			}
			random_owned_province = {
				limit = {
					is_capital = no
					any_neighbor_province = {
						is_capital = no
					}
					NOT = { has_province_flag = mali_roads_unsafe }
				}
				set_province_flag = mali_roads_unsafe
			}
			random_owned_province = {
				limit = {
					is_capital = no
					any_neighbor_province = {
						is_capital = no
					}
					NOT = { has_province_flag = mali_roads_unsafe }
				}
				set_province_flag = mali_roads_unsafe
			}
			random_owned_province = {
				limit = {
					is_capital = no
					any_neighbor_province = {
						is_capital = no
					}
					NOT = { has_province_flag = mali_roads_unsafe }
				}
				set_province_flag = mali_roads_unsafe
			}
		}
	}
	
	option = {
		name = flavor_mal.7.a
		ai_chance = { factor = 40 }
		every_owned_province = {
			limit = {
				has_province_flag = mali_roads_unsafe
			}
			add_local_autonomy = 60
			add_province_modifier = {
				name = "mal_unsafe_roads"
				duration = 365
			}
			clr_province_flag = mali_roads_unsafe
		}
	}
	
	option = {
		name = flavor_mal.7.b
		ai_chance = { factor = 20 }
		add_adm_power = -100
		every_owned_province = {
			limit = {
				has_province_flag = mali_roads_unsafe
			}
			add_province_modifier = {
				name = "mal_unsafe_roads"
				duration = 915
			}
			clr_province_flag = mali_roads_unsafe
		}
	}
	
	option = {
		name = flavor_mal.7.c
		ai_chance = { factor = 40 }
		every_owned_province = {
			limit = {
				has_province_flag = mali_roads_unsafe
			}
			add_province_modifier = {
				name = "mal_unsafe_roads"
				duration = 3650
			}
			clr_province_flag = mali_roads_unsafe
		}
	}
}

#Rise of Kaabu
country_event = {
	id = flavor_mal.8
	title = flavor_mal.8.t
	desc = flavor_mal.8.d
	picture = CIVIL_WAR_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MAL
		NOT = { exists = KAA }
		is_year = 1475
		religion_group = muslim
		any_owned_province = {
			is_core = KAA
			culture = senegambian
			religion = animism
			is_capital = no
		}
	}
	
	mean_time_to_happen = {
		months = 450
		
		modifier = {
			factor = 0.7
			any_owned_province = {
				is_core = KAA
				has_province_modifier = mal_unsafe_roads
			}
		}
		modifier = {
			factor = 0.7
			any_owned_province = {
				is_core = KAA
				has_province_modifier = mal_portuguese_meddling
			}
		}
		modifier = {
			factor = 0.7
			has_country_modifier = mal_islamisation_of_the_aristocracy
		}
	}
	
	option = {
		name = flavor_mal.8.a
		ai_chance = { factor = 90 }
		every_owned_province = {
			limit = {
				is_core = KAA
				culture = senegambian
				religion = animism
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	
	option = {
		name = flavor_mal.8.b
		ai_chance = { factor = 10 }
		release = KAA
		KAA = {
			define_ruler = {
				name = "Sama Koli"
				dynasty = "Jeenung"
			}
		}
	}

}

########################################
# Events for Mali
#
# written by Gilles Gaudray
########################################

# The Songhai Rebellion
country_event = {
	id = flavor_mal.11
	title = flavor_mal.11.t
	desc = flavor_mal.11.d
	picture = AFRICAN_WARRIORS_eventPicture
	
	trigger = {
		tag = MAL
		NOT = { exists = SON }
		num_of_cities = 15
		any_owned_province = {
			area = dendi_area
		}
	}
	
	mean_time_to_happen = {
		months = 600
		modifier = {
			factor = 0.95
			NOT = { stability = 2 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 0.80
			any_owned_province = {
				area = dendi_area
				culture = songhai
			}
		}
	}
	
	immediate = {
		every_owned_province = {
			limit = {
				area = dendi_area
			}
			change_culture = songhai
			add_core = SON
		}
	}
	option = {
		name = flavor_mal.11.a
		ai_chance = {
			factor = 100
		}
		every_owned_province = {
			limit = {
				area = dendi_area
			}
			cede_province = SON
		}
	}
	option = {
		name = flavor_mal.11.b
		ai_chance = {
			factor = 0
		}
		random_owned_province = {
			limit = {
				area = dendi_area
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 7300
			}
			add_unrest = 20
		}
	}
}

# flavor_mal.12.t: "Malian Authority Under Threat"
# flavor_mal.12.d: "Throughout the 14th and early 15th centuries, our grasp on our outlying territories has grown weaker and weaker, notably resulting in Songhai renouncing any claim to loyalty to us. Now, people closer to home are demanding a weaker Malian state while people to the west in Jolof demand independence from us altogether. We can try to put down this challenge to our authority, but it will come at great cost."
# flavor_mal.12.a: "Mali's decline ends here!"
# flavor_mal.12.b: "We can make some concessions..."
