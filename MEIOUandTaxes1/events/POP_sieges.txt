namespace = POP_sieges

### By Demian

country_event = {  
	id = POP_sieges.001
	title = POP_sieges.001.t
	desc = POP_sieges.001.d
	picture = REFORM_eventPicture
	is_triggered_only = yes
	hidden = no
	
	immediate = { 
		every_province = {
			limit = { 
				is_city = yes 
			}	
			if = {
				limit = {
					check_variable = { which = disease_hit_recently value = 1 }	
					has_siege = no 
				}
				subtract_variable = { which = disease_hit_recently	value = 2 }
			}
			if = {
				limit = {
					has_siege = yes 
					fort_level = 1 
				}
				if = { 
					limit = {
						NOT = { check_variable = { which = disease_hit_recently value = 0 }	}
					}
					set_variable = { which = disease_hit_recently	value = 0 }
				}
				set_variable = { which = units_sieging 			value = 0 }
				if = {
					limit = {
						num_of_units_in_province = { 	amount = 1 }
						NOT = { num_of_units_in_province = { 	amount = 10 } }
					}
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 1	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 2	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 3	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 4	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 5	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 6	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 7	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 8	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 9	}
						}
						change_variable = { which = units_sieging 	value = 1 }
					} } } } } } } } } 
				}
				if = {
					limit = {
						num_of_units_in_province = { 			amount = 10 }
						NOT = { num_of_units_in_province = { 	amount = 50 } }
					}
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 10 }	
							NOT = { num_of_units_in_province = { 	amount = 20 } }
						}	
						set_variable = { which = units_sieging		value = 10 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 11 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 12 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 13 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 14 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 15 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 16 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 17 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 18 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 19 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } } 
					}	
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 20 }	
							NOT = { num_of_units_in_province = { 	amount = 30 } }
						}	
						set_variable = { which = units_sieging		value = 20 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 21 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 22 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 23 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 24 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 25 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 26 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 27 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 28 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 29 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } } 
					}	
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 30 }	
							NOT = { num_of_units_in_province = { 	amount = 40 } }
						}	
						set_variable = { which = units_sieging		value = 30 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 31 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 32 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 33 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 34 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 35 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 36 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 37 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 38 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 39 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } }
					}	
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 40 }	
							NOT = { num_of_units_in_province = { 	amount = 50 } }
						}	
						set_variable = { which = units_sieging		value = 40 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 41 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 42 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 43 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 44 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 45 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 46 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 47 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 48 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 49 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } } 
					}		
				}		
				if = {
					limit = {
						num_of_units_in_province = { 			amount = 50 }
						NOT = { num_of_units_in_province = { 	amount = 100 } }
					}
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 50 }	
							NOT = { num_of_units_in_province = { 	amount = 60 } }
						}	
						set_variable = { which = units_sieging		value = 50 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 51 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 52 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 53 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 54 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 55 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 56 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 57 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 58 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 59 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } }
					}	
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 60 }	
							NOT = { num_of_units_in_province = { 	amount = 70 } }
						}	
						set_variable = { which = units_sieging		value = 60 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 61 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 62 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 63 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 64 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 65 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 66 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 67 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 68 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 69 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } }
					}
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 70 }	
							NOT = { num_of_units_in_province = { 	amount = 80 } }
						}	
						set_variable = { which = units_sieging		value = 70 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 71 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 72 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 73 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 74 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 75 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 76 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 77 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 78 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 79 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } }
					}	
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 80 }	
							NOT = { num_of_units_in_province = { 	amount = 90 } }
						}	
						set_variable = { which = units_sieging		value = 80 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 81 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 82 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 83 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 84 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 85 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 86 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 87 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 88 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 89 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } }
					}		
					if = {
						limit = { 
							num_of_units_in_province = { 			amount = 90 }	
							NOT = { num_of_units_in_province = { 	amount = 100 } }
						}	
						set_variable = { which = units_sieging		value = 90 }	
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 91 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 92 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 93 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 94 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 95 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 96 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 97 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 98 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						
						if = {
							limit = { 
								num_of_units_in_province = {  amount = 99 }
							}
							change_variable = { which = units_sieging 	value = 1 }
						} } } } } } } } }
					}	
				}		
				if = {
					limit = { 
						num_of_units_in_province = { 			amount = 100 }	
					}		
					set_variable = { which = units_sieging 	value = 100 }
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 110 }
						}
						change_variable = { which = units_sieging 	value = 10 }
				
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 120 }
						}
						change_variable = { which = units_sieging 	value = 10 }
						
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 130 }
						}
						change_variable = { which = units_sieging 	value = 10 }
								
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 140 }
						}
						change_variable = { which = units_sieging 	value = 10 }
					
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 150 }
						}
						change_variable = { which = units_sieging 	value = 10 }
					
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 160 }
						}
						change_variable = { which = units_sieging 	value = 10 }
					
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 170 }
						}
						change_variable = { which = units_sieging 	value = 10 }
					
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 180 }
						}
						change_variable = { which = units_sieging 	value = 10 }
					
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 190 }
						}
						change_variable = { which = units_sieging 	value = 10 }
						
					if = {
						limit = { 
							num_of_units_in_province = {  amount = 200 }
						}
						change_variable = { which = units_sieging 	value = 10 }
				
					} } } } } } } } } }
				}
							
				set_variable = { which = siege_garrison value = 1 }
				if = {
					limit = {	
						garrison = 1001
					}
					set_variable = { which = siege_garrison value = 2 }
					if = {
						limit = {	
							garrison = 2001
						}
						set_variable = { which = siege_garrison value = 3 }
						if = {
							limit = {	
								garrison = 3001
							}
							set_variable = { which = siege_garrison value = 4 }
							if = {
								limit = {	
									garrison = 4001
								}
								set_variable = { which = siege_garrison value = 5 }
								if = {
									limit = {	
										garrison = 5001
									}
									set_variable = { which = siege_garrison value = 6 }
									if = {
										limit = {	
											garrison = 6001
										}
										set_variable = { which = siege_garrison value = 7 }
										if = {
											limit = {	
												garrison = 7001
											}
											set_variable = { which = siege_garrison value = 8 }
											if = {
												limit = {	
													garrison = 8001
												}
												set_variable = { which = siege_garrison value = 9 }	
												if = {
													limit = {	
														garrison = 9001
													}
													set_variable = { which = siege_garrison value = 10 }		
													if = {
														limit = {	
															garrison = 10001
														}
														set_variable = { which = siege_garrison value = 11 }
														if = {
															limit = {	
																garrison = 11001
															}
															set_variable = { which = siege_garrison value = 12 }		
																if = {
																	limit = {	
																		garrison = 12001
																	}
																	set_variable = { which = siege_garrison value = 13 }	
																	if = {
																		limit = {	
																			garrison = 13001
																		}
																		set_variable = { which = siege_garrison value = 14 }		
					} } } } } } } } } } } } 
				}
				#units_sieging/siege_garrison=siege_disparity
				set_variable = { which = siege_disparity 		value = 0 }
				set_variable = { which = siege_disparity 		which = units_sieging }
				if = {
					limit = {
						is_variable_equal = {
							which = siege_garrison
							value = 0
						}
					}

					# log = "<ERROR><B3137180><THIS:[This.GetName]><PREV:[Prev.GetName]><ROOT:[Root.GetName]><FROM:[From.GetName]> Division by zero!"

					else = {
						divide_variable = {
							which = siege_disparity
							which = siege_garrison
						}
					}
				}
				if = {
					limit = {
						NOT = { has_province_flag = under_siege_penalty_assigned }
					}
					assign_siege_modifier = yes	
				}
				if = {
					limit = {
						check_variable = { which = siege_disparity	value = 4 }
						NOT = { check_variable = { which = siege_disparity	value = 8 } }
						NOT = { has_province_modifier = garrison_outnumbered }
					}
					assign_siege_modifier = yes
				}
				if = {
					limit = {
						check_variable = { which = siege_disparity	value = 8 }
						NOT = { check_variable = { which = siege_disparity	value = 16 } }
						NOT = { has_province_modifier = garrison_significantly_outnumbered }
					}
					assign_siege_modifier = yes
				}
				if = {
					limit = {
						check_variable = { which = siege_disparity	value = 16 }
						NOT = { check_variable = { which = siege_disparity	value = 32 } }
						NOT = { has_province_modifier = garrison_tremendously_outnumbered }
					}
					assign_siege_modifier = yes
				}
				if = {
					limit = {
						check_variable = { which = siege_disparity	value = 32 }
						NOT = { check_variable = { which = siege_disparity	value = 64 } }
						NOT = { has_province_modifier = garrison_massively_outnumbered }
					}
					assign_siege_modifier = yes
				}
				if = {
					limit = {
						check_variable = { which = siege_disparity	value = 64 }
						NOT = { has_province_modifier = garrison_overwhelmed }
					}
					assign_siege_modifier = yes
				}			
				set_variable = { which = siege_necessary_threshold 			value = 0.1 }
				change_variable = { which = siege_necessary_threshold 		which = siege_garrison }
				multiply_variable = { which = siege_necessary_threshold		value = 2 }
				subtract_variable = { which = disease_hit_recently			value = 0.5 }
				if = {
					limit = {
						check_variable = { which = units_sieging 					which = siege_necessary_threshold } 
						NOT = { check_variable = { which = disease_hit_recently 	value = 20 } }
					}
					province_event = { 
						id = POP_sieges.003
					}
				}
			}	
		}
	}
	
	option = { 
		name = "POP_sieges.001.a"
		
	
	}
}

province_event = {  
	id = POP_sieges.002
	title = POP_sieges.002.t
	desc = POP_sieges.002.d
	picture = REFORM_eventPicture
	is_triggered_only = no
	hidden = yes
	
	trigger = { 
		has_province_flag = under_siege_penalty_assigned
		has_siege = no
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	immediate = { 
		remove_province_modifier = garrison_outnumbered
		remove_province_modifier = garrison_significantly_outnumbered
		remove_province_modifier = garrison_tremendously_outnumbered
		remove_province_modifier = garrison_massively_outnumbered
		remove_province_modifier = garrison_overwhelmed
		clr_province_flag = under_siege_penalty_assigned
	}
	
	option = { 
		name = "POP_sieges.002.a"
		
	
	}
}

province_event = {  
	id = POP_sieges.003
	title = POP_sieges.003.t
	desc = POP_sieges.003.d
	picture = REFORM_eventPicture
	is_triggered_only = yes
	hidden = yes
	
	trigger = { 
		
	}
	
	immediate = { 
		random_list = {
			75 = {  }
			15 = { add_province_modifier = { name = mild_disease_outbreak			duration = 12 } change_variable = { which = disease_hit_recently	value = 5 } }
			5 = {  add_province_modifier = { name = moderate_disease_outbreak		duration = 12 } change_variable = { which = disease_hit_recently	value = 10 } }
			5 = {  add_province_modifier = { name = severe_disease_outbreak			duration = 12 } change_variable = { which = disease_hit_recently	value = 15 } }
		}
	}
	
	option = { 
		name = "POP_sieges.003.a"
		
	
	}
}

province_event = {  
	id = POP_sieges.004
	title = POP_sieges.004.t
	desc = POP_sieges.004.d
	picture = REFORM_eventPicture
	is_triggered_only = yes
	hidden = yes
	
	trigger = { 
		
	}
	
	immediate = { 
		every_province = {
			set_variable = { which = garrison_size 	value = 0 }
			export_to_variable = { 
				which = garrison_size
				value = garrison_size
				who = ROOT
			}
		}
	}
	
	option = { 
		name = "POP_sieges.004.a"
		
	
	}
}