namespace = red_turban

country_event = {

	id = red_turban.1
	title = "red_turban.1.t"
	desc = "red_turban.1.d"
	picture = DIPLOMACY_eventPicture

	trigger = {
		OR = {
			primary_culture = wuhan
			primary_culture = hanyu
			primary_culture = hakka
			primary_culture = yueyu
			primary_culture = minyu
			primary_culture = lanyin
		}
		is_free_or_tributary_trigger = yes
		AND = {
			NOT = { num_of_cities = 20 }
			any_neighbor_country = {
				OR = {
					primary_culture = wuhan
					primary_culture = hanyu
					primary_culture = hakka
					primary_culture = yueyu
					primary_culture = minyu
					primary_culture = lanyin
				}
				ai = yes
				num_of_cities = 100
				legitimacy = 70
			}
		}
		ai = yes
	}

	mean_time_to_happen = {
		months = 10
	}

	option = {
		name = "OPT.ISTHATSO"
		if = {
			limit = {
				NOT = { num_of_cities = 20 }
				any_neighbor_country = {
					OR = {
						primary_culture = wuhan
						primary_culture = hanyu
						primary_culture = hakka
						primary_culture = yueyu
						primary_culture = minyu
						primary_culture = lanyin
					}
					num_of_cities = 100
					legitimacy = 70
				}
			}
			random_neighbor_country = {
				limit = {
					OR = {
						primary_culture = wuhan
						primary_culture = hanyu
						primary_culture = hakka
						primary_culture = yueyu
						primary_culture = minyu
						primary_culture = lanyin
					}
					num_of_cities = 100
					legitimacy = 70
				}
				country_event = { id = red_turban.2 days = 0 }
			}
		}
	}
}

country_event = {

	id = red_turban.2
	title = "red_turban.2.t"
	desc = "red_turban.2.d"
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = "OPT.EXCELLENT"
		inherit = FROM
	}
}

#### Yuan loses Southwest #### 41
country_event = {
	id = red_turban.3
	
	title = "red_turban.3.t"
	desc = "red_turban.3.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = YUA
		NOT = { has_country_flag = yuan_loses_south }
		NOT = { 
			num_of_owned_provinces_with = {
				value = 30
				chinese_region_trigger = yes
			}
		}
	}
	
	mean_time_to_happen = {
		months = 6
	}
	
	option = {
		name = "red_turban.3a"
		every_province = {
			limit = {
				superregion = southwest_china_superregion
				is_core = YUA
			}
			remove_core = YUA
		}
		add_prestige = -15
		add_legitimacy = -5	
		set_country_flag = yuan_loses_south
	}
}
#### Yuan loses East  #### 51
country_event = {
	id = red_turban.4
	
	title = "red_turban.4.t"
	desc = "red_turban.4.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = YUA
		is_year = 1359		
		NOT = { has_country_flag = yuan_loses_wuhan }
		NOT = { 
			num_of_owned_provinces_with = {
				value = 20
				chinese_region_trigger = yes
			}
		}
	}

	mean_time_to_happen = { months = 1 }	
	option = {
		name = "red_turban.3a"
		every_province = {
			limit = {
				superregion = east_china_superregion
				is_core = YUA
			}
			remove_core = YUA
		}
		free_vassal = CTA			
		add_prestige = -15
		add_legitimacy = -5	
		set_country_flag = yuan_loses_wuhan
	}
}
#### Yuan loses North #### 47
country_event = {
	id = red_turban.5
	
	title = "red_turban.5.t"
	desc = "red_turban.5.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = YUA
		NOT = { has_country_flag = yuan_loses_hanyu }
		NOT = { 
			num_of_owned_provinces_with = {
				value = 5
				chinese_region_trigger = yes
			}
		}
	}
	
		mean_time_to_happen = { months = 1 }
	
	option = {
		name = "red_turban.3a"
		every_province = {
			limit = {
				superregion = north_china_superregion
				is_core = YUA
				NOT = { culture_group = altaic }
			}
			remove_core = YUA
		}
		add_prestige = -15
		add_legitimacy = -5	
		set_country_flag = yuan_loses_hanyu
	}
}
#### Yuan degenerates ####
country_event = {
	id = red_turban.6
	
	title = "red_turban.6.t"
	desc = "red_turban.6.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = YUA
		is_year = 1365
	}
	
	option = {
		name = "red_turban.3a"
		change_government = steppe_horde
		change_religion = tengri_pagan_reformed
		set_capital = 3273
		yua_degeneration = yes
		
		free_vassal = QIN
		free_vassal = LNG
		free_vassal = CYU		
		every_owned_province = {
			limit = {
				OR = {
					region = korea_region
					region = manchuria_region
				}
				NOT = { area = khorchin_area }
				NOT = { is_core = MXI }
			}
			add_core = MXI
		}
		release_vassal = MXI
		hidden_effect = {
			every_owned_province = {
				limit = {
					is_core = MXI
				}
				cede_province = MXI
			}
		}
		create_subject = {
            who = MXI
            subject_type = tributary_state
        }
		MXI = {
			define_ruler = {
				name = "Naghacu"
				dynasty = "Jalayirid"
				mil = 4
				adm = 2
				dip = 3
				age = 55
			}
			infantry = capital
			infantry = capital
			infantry = capital
			infantry = capital
			infantry = capital
			cavalry = capital
			cavalry = capital
			cavalry = capital
			cavalry = capital
		}
		if = {
			limit = {
				CSE = { vassal_of = YUA }
			}
			CSE = { country_event = { id = red_turban.15 } }
		}
	}
}
#### Yuan dissolves ####

country_event = {
	id = red_turban.7
	
	title = "red_turban.7.t"
	desc = "red_turban.7.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		tag = YUA
		NOT = { num_of_cities = 50 }
		OR = {
			MXI = { 
				exists = yes
				NOT = { is_subject_of = YUA }
			}
			NOT = { owns = 3273 }
		}
	}
		
	immediate = {
		hidden_effect = {
			every_province = {
				limit = {
					is_core = YUA
				}
				remove_core = YUA
			}
			every_province = {
				limit = {
					province_group = khalka_group
					culture = mongol
				}
				change_culture = khalkas
				add_core = KHA
			}
			every_owned_province = {
				limit = {
					province_group = tumed_group
					culture = mongol
				}
				change_culture = tumed
				add_core = TMD
			}
			every_owned_province = {
				limit = {
					province_group = chakhar_group
					culture = mongol
				}
				change_culture = chahar
				add_core = CHH
			}
			every_owned_province = {
				limit = {
					OR = {
						culture = buryat
						culture = khamnigan
					}
				}
				add_core = BRT
			}
			every_province = {
				limit = {
					culture = mongol
					OR = {
						area = khorchin_area
						owned_by = MXI
					}
				}
				change_culture = uriankhai
				add_core = MXI
			}
			4196 = {
				if = {
					limit ={
						owned_by = ROOT
					}
					cede_province = XXX
				}
			}
			random_list = {
				34 = {
					715 = {
						add_core = KHA
						change_culture = khalkas
					}
					3272 = {
						add_core = KHA
						change_culture = khalkas
					}
				}
				33 = {
					715 = {
						add_core = OIR
						change_culture = oirats
					}
					3272 = {
						add_core = OIR
						change_culture = oirats
					}
				}
				33 = {
					715 = {
						add_core = TMD
						change_culture = tumed
					}
					3272 = {
						add_core = TMD
						change_culture = tumed
					}
				}
			}
			if = {
				limit = {
					2743 = { owned_by = ROOT }
				}
				add_core = HMI
			}
		}
	}
	
	option = {
		name = "red_turban.7a"
		release = TMD
		release = CHH
		if = {
			limit = {
				NOT = { exists = BRT }
			}
			release = BRT
			else = {
				every_owned_province = {
					limit = {
						is_core = BRT
					}
					cede_province = BRT
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = OIR }
			}
			release = OIR
			else = {
				every_owned_province = {
					limit = {
						is_core = OIR
					}
					cede_province = OIR
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = MXI }
			}
			release = MXI
			else = {
				every_owned_province = {
					limit = {
						is_core = MXI
					}
					cede_province = MXI
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = HMI }
			}
			release = HMI
		}
		change_tag = KHA
		release_all_subjects = yes
		every_owned_province = {
			limit = {
				culture = mongol
			}
			change_culture = khalkas
		}
	}	
	option = {
		name = "red_turban.7b"
		release = TMD
		release = KHA
		if = {
			limit = {
				NOT = { exists = BRT }
			}
			release = BRT
			else = {
				every_owned_province = {
					limit = {
						is_core = BRT
					}
					cede_province = BRT
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = OIR }
			}
			release = OIR
			else = {
				every_owned_province = {
					limit = {
						is_core = OIR
					}
					cede_province = OIR
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = MXI }
			}
			release = MXI
			else = {
				every_owned_province = {
					limit = {
						is_core = MXI
					}
					cede_province = MXI
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = HMI }
			}
			release = HMI
		}
		change_tag = CHH
		release_all_subjects = yes
		every_owned_province = {
			limit = {
				culture = mongol
			}
			change_culture = chahar
		}
	}
	option = {
		name = "red_turban.7c"
		release = KHA
		release = CHH
		if = {
			limit = {
				NOT = { exists = BRT }
			}
			release = BRT
			else = {
				every_owned_province = {
					limit = {
						is_core = BRT
					}
					cede_province = BRT
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = OIR }
			}
			release = OIR
			else = {
				every_owned_province = {
					limit = {
						is_core = OIR
					}
					cede_province = OIR
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = MXI }
			}
			release = MXI
			else = {
				every_owned_province = {
					limit = {
						is_core = MXI
					}
					cede_province = MXI
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = HMI }
			}
			release = HMI
		}
		change_tag = TMD
		release_all_subjects = yes
		every_owned_province = {
			limit = {
				culture = mongol
			}
			change_culture = tumed
		}
	}
	option = {
		name = "red_turban.7d"
		trigger = {
			NOT = { exists = OIR }
		}
		release = KHA
		release = CHH
		release = TMD
		if = {
			limit = {
				NOT = { exists = BRT }
			}
			release = BRT
			else = {
				every_owned_province = {
					limit = {
						is_core = BRT
					}
					cede_province = BRT
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = MXI }
			}
			release = MXI
			else = {
				every_owned_province = {
					limit = {
						is_core = MXI
					}
					cede_province = MXI
				}
			}
		}
		change_tag = OIR
		release_all_subjects = yes
		every_owned_province = {
			limit = {
				culture = mongol
			}
			change_culture = oirats
		}
	}
	option = {
		name = "red_turban.7e"
		trigger = {
			NOT = { exists = BRT }
		}
		release = KHA
		release = CHH
		release = TMD
		if = {
			limit = {
				NOT = { exists = OIR }
			}
			release = OIR
			else = {
				every_owned_province = {
					limit = {
						is_core = OIR
					}
					cede_province = OIR
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = MXI }
			}
			release = MXI
			else = {
				every_owned_province = {
					limit = {
						is_core = MXI
					}
					cede_province = MXI
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = HMI }
			}
			release = HMI
		}
		change_tag = BRT
		release_all_subjects = yes
	}
	option = {
		name = "red_turban.7f"
		trigger = {
			NOT = { exists = MXI }
		}
		release = KHA
		release = CHH
		release = TMD
		if = {
			limit = {
				NOT = { exists = OIR }
			}
			release = OIR
			else = {
				every_owned_province = {
					limit = {
						is_core = OIR
					}
					cede_province = OIR
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = BRT }
			}
			release = BRT
			else = {
				every_owned_province = {
					limit = {
						is_core = BRT
					}
					cede_province = BRT
				}
			}
		}
		if = {
			limit = {
				NOT = { exists = HMI }
			}
			release = HMI
		}
		change_tag = MXI
		release_all_subjects = yes
	}
}
#### Yuan is successful ####
country_event = {
	id = red_turban.8
	
	title = "red_turban.8.t"
	desc = "red_turban.8.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		has_country_flag = lost_mandate_of_heaven
		tag = YUA
		check_variable = { which = "Demesne_in_the_Mandate_of_Heaven" value = 235 }
	}
		
	option = {
		name = "red_turban.3a"
		clr_country_flag = lost_mandate_of_heaven
		every_subject_country = {
			limit = {
				OR = {
					culture_group = chinese_group
					primary_culture = uriankhai
				}
			}
			YUA = { inherit = PREV }
		}
		north_china_superregion = { limit = { owned_by = ROOT } add_core = ROOT }
		north_china_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
		east_china_superregion = { limit = { owned_by = ROOT } add_core = ROOT }
		east_china_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
		southwest_china_superregion = { limit = { owned_by = ROOT } add_core = ROOT }
		southwest_china_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
		add_faction = temples
		add_faction = enuchs
		add_faction = bureaucrats
	}
}
#### Core cleanup ####
country_event = {
	id = red_turban.9
	
	title = "red_turban.9.t"
	desc = "red_turban.9.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = mandate_of_heaven_claimed
		any_province = {
			chinese_region_trigger = yes
			OR	= {
				owned_by = ROOT
				owner = {
					NOT = {	culture_group = chinese_group }
				}
			}
			any_core_country = {
				OR = {
					culture_group = chinese_group
					culture_group = zuqun
				}
				exists = no
			}
		}
	}
		
	option = {
		name = "red_turban.3a"
		every_province = {
			limit = {
				chinese_region_trigger = yes
			}
			every_core_country = {
				limit = {
					OR = {
						culture_group = chinese_group
						culture_group = zuqun
					}
					exists = no
				}
				remove_core = PREV
			}
			every_core_country = {
				limit = {
					OR = {
						culture_group = chinese_group
						culture_group = zuqun
					}
					exists = no
				}
				remove_core = PREV
			}
			every_core_country = {
				limit = {
					OR = {
						culture_group = chinese_group
						culture_group = zuqun
					}
					exists = no
				}
				remove_core = PREV
			}
		}
	}
}
#### Post-peace core gain ####
country_event = {
	id = red_turban.10
	
	title = "red_turban.10.t"
	desc = "red_turban.10.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = red_turban_reb
		any_owned_province = {
			chinese_region_trigger = yes
			NOT = { is_core = ROOT }
		}
	}
		
	option = {
		name = "red_turban.3a"
		every_owned_province = {
			limit = {
				chinese_region_trigger = yes
				NOT = { is_core = ROOT }
			}
			add_core = ROOT
		}
	}
}
#### Border incident ####
country_event = {
	id = red_turban.11
	
	title = "red_turban.11.t"
	desc = "red_turban.11.d"
	
	picture = SIEGE_eventPicture
	
	hidden = yes
	
	trigger = {
		has_country_flag = red_turban_reb
		ai = yes
		is_at_war = no
		any_neighbor_country = {
			OR = {
				#has_country_flag = red_turban_reb
				#has_country_flag = mandate_of_heaven_claimed
				culture_group = chinese_group
				culture_group = zuqun
			}
			NOT = { truce_with = ROOT }
			NOT = { alliance_with = ROOT }
			#is_free_or_tributary_trigger = yes
			is_at_war = no
			ai = yes
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
		
	option = {
		name = "red_turban.3a"
		random_neighbor_country = {
			limit = {
				OR = {
					#has_country_flag = red_turban_reb
					#has_country_flag = mandate_of_heaven_claimed
					culture_group = chinese_group
					culture_group = zuqun
				}
				NOT = { truce_with = ROOT }
				#is_free_or_tributary_trigger = yes
				is_at_war = no
				ai = yes
			}
			declare_war_with_cb = {
				who = ROOT
				casus_belli = cb_chinese_civil_war
			}
		}
		ai_chance = {
			factor = 90
			
			modifier = {
				factor = 0
				NOT = { treasury = 50 }
			}
			modifier = {
				factor = 0
				num_of_loans = 10
			}
			modifier = {
				factor = 0
				NOT = { manpower = 10 }
			}
		}
	}
	option = {
		name = "red_turban.3b"
	}
}
#### Impass, no longer a rebel ####
country_event = {
	id = red_turban.12
	
	title = "red_turban.12.t"
	desc = "red_turban.12.d"
	
	picture = SIEGE_eventPicture
	
	hidden = yes
	
	trigger = {
		had_country_flag = { flag = red_turban_reb days = 7300 }
	}
	
	mean_time_to_happen = {
		months = 6
	}
		
	option = {
		name = "red_turban.3a"
		clr_country_flag = red_turban_reb
	}
}

#### Change government type ####
country_event = {
	id = red_turban.13
	
	title = "red_turban.13.t"
	desc = "red_turban.13.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
		
	option = {
		name = "red_turban.13.a"
		#
	}
	option = {
		name = "red_turban.13.b"
		trigger = {
			NOT = { government = chinese_monarchy }
		}
		change_government = chinese_monarchy
	}
	option = {
		name = "red_turban.13.c"
		trigger = {
			NOT = { government = chinese_monarchy_2 }
		}
		change_government = chinese_monarchy_2
	}
	option = {
		name = "red_turban.13.d"
		trigger = {
			NOT = { government = chinese_monarchy_3 }
		}
		change_government = chinese_monarchy_3
	}
	option = {
		name = "red_turban.13.e"
		trigger = {
			NOT = { government = chinese_monarchy_4 }
		}
		change_government = chinese_monarchy_4
	}
	option = {
		name = "red_turban.13.f"
		trigger = {
			NOT = { government = chinese_monarchy_5 }
		}
		change_government = chinese_monarchy_5
	}
}

#### Occupation, surrender ####
country_event = {
	id = red_turban.14
	
	title = "red_turban.14.t"
	desc = "red_turban.14.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_in_war = { casus_belli = cb_chinese_civil_war }
		culture_group = chinese_group
		NOT = { war_score = -85 }
		any_known_country = {
			war_with = ROOT
			is_in_war = { casus_belli = cb_chinese_civil_war }
			OR = {
				culture_group = chinese_group
				has_country_flag = barbarian_claimant_china
				has_country_flag = mandate_of_heaven_claimed
			}
		}
	}
		
	option = { #Surrender
		name = "red_turban.14a"
		every_owned_province = {
			remove_core = ROOT
			cede_province = controller
			add_core = controller
		}
		random_known_country = {
			limit = {
				war_with = ROOT
				is_in_war = { casus_belli = cb_chinese_civil_war }
				OR = {
					culture_group = chinese_group
					has_country_flag = barbarian_claimant_china
					has_country_flag = mandate_of_heaven_claimed
				}
			}
			inherit = ROOT
			country_event = { id = red_turban.20 }
		}
		ai_chance = {
			factor = 100
		}
	}
	
	option = { #Fight on - player only
		name = "red_turban.14b"
		ai_chance = {
			factor = 0
		}
	}
}
### Shen subverient to Uriankhai
country_event = {
	id = red_turban.15
	
	title = "red_turban.15.t"
	desc = "red_turban.15.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "red_turban.15a"
		MXI = {
			inherit = CSE
			set_capital = 4035
		}
		ai_chance = {
			factor = 100
		}
	}
	option = {
		name = "red_turban.15b"
		MXI = {
			add_claim = liaoning_area
		}
		ai_chance = {
			factor = 0
		}
	}
}

### Uriankhai surrender
country_event = {
	id = red_turban.16
	
	title = "red_turban.16.t"
	desc = "red_turban.16.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	option = { #Make them our client state
		name = "red_turban.16a"
		vassalize = MXI
		MXI = { country_event = { id = red_turban.17 } }
		ai_chance = {
			factor = 25
		}
	}
	option = { #Integrate them
		name = "red_turban.16b"
		inherit = MXI
		ai_chance = {
			factor = 0
		}
	}
	option = { #Create three guards and allow Jurchen settlement
		name = "red_turban.16c"
		vassalize = MXI
		every_province = {
			limit = {
				area = liaoning_area
			}
			hidden_effect = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
					}
					add_core = ROOT
				}
				if = {
					limit = {
						culture = uriankhai
					}
					change_culture = jurchen
				}
			}
			cede_province = ROOT
		}
		hidden_effect = {
			every_province = {
				limit = {
					area = donggye_area
					culture = uriankhai
				}
				change_culture = jurchen
			}
			every_province = {
				limit = {
					OR = {
						area = khorchin_area
						owned_by = MXI
					}
					culture = mongol
				}
				change_culture = uriankhai
				add_core = MXI
			}
		}
		if = {
			limit = {
				NOT = {
					calc_true_if = {
						all_country = {
							exists = yes
							primary_culture = jurchen
						}
						amount = 7
					}
				}
			}
			if = {
				limit = {
					NOT = { exists = MJZ }
				}
				hidden_effect = {
					every_province = {
						limit = { region = manchuria_region }
						remove_core = MJZ
					}
				}
				MXI = {
					every_owned_province = {
						limit = {
							province_group = jianzhou_group
						}
						remove_core = MXI
						add_core = MJZ
						cede_province = MJZ
						change_culture = jurchen
					}
				}
			}
			if = {
				limit = {
					NOT = { exists = MRC }
				}
				hidden_effect = {
					every_province = {
						limit = { region = manchuria_region }
						remove_core = MRC
					}
				}
				MXI = {
					every_owned_province = {
						limit = {
							province_group = jianzhou_group
						}
						remove_core = MXI
						add_core = MRC
						cede_province = MRC
						change_culture = jurchen
					}
				}
			}
			if = {
				limit = {
					NOT = { exists = MWK }
				}
				hidden_effect = {
					every_province = {
						limit = { region = manchuria_region }
						remove_core = MWK
					}
				}
				MXI = {
					every_owned_province = {
						limit = {
							province_group = jianzhou_group
						}
						remove_core = MXI
						add_core = MWK
						cede_province = MWK
						change_culture = jurchen
					}
				}
			}
			if = {
				limit = {
					NOT = { exists = MHR }
				}
				hidden_effect = {
					every_province = {
						limit = { region = manchuria_region }
						remove_core = MHR
					}
				}
				MXI = {
					every_owned_province = {
						limit = {
							province_group = jianzhou_group
						}
						remove_core = MXI
						add_core = MHR
						cede_province = MHR
						change_culture = jurchen
					}
				}
			}
			if = {
				limit = {
					NOT = { exists = MUD }
				}
				hidden_effect = {
					every_province = {
						limit = { region = manchuria_region }
						remove_core = MUD
					}
				}
				MXI = {
					every_owned_province = {
						limit = {
							province_group = jianzhou_group
						}
						remove_core = MXI
						add_core = MUD
						cede_province = MUD
						change_culture = jurchen
					}
				}
			}
			if = {
				limit = {
					NOT = { exists = MNA }
				}
				hidden_effect = {
					every_province = {
						limit = { region = manchuria_region }
						remove_core = MNA
					}
				}
				MXI = {
					every_owned_province = {
						limit = {
							province_group = jianzhou_group
						}
						remove_core = MXI
						add_core = MNA
						cede_province = MNA
						change_culture = jurchen
					}
				}
			}
			if = {
				limit = {
					NOT = {
						calc_true_if = {
							all_country = {
								exists = yes
								primary_culture = jurchen
							}
							amount = 7
						}
					}
				}
				if = {
					limit = {
						NOT = { exists = MJZ }
					}
					hidden_effect = {
						every_province = {
							limit = { region = manchuria_region }
							remove_core = MJZ
						}
					}
					MXI = {
						every_owned_province = {
							limit = {
								province_group = haixi_group
							}
							remove_core = MXI
							add_core = MJZ
							cede_province = MJZ
							change_culture = jurchen
						}
					}
				}
				if = {
					limit = {
						NOT = { exists = MRC }
					}
					hidden_effect = {
						every_province = {
							limit = { region = manchuria_region }
							remove_core = MRC
						}
					}
					MXI = {
						every_owned_province = {
							limit = {
								province_group = haixi_group
							}
							remove_core = MXI
							add_core = MRC
							cede_province = MRC
							change_culture = jurchen
						}
					}
				}
				if = {
					limit = {
						NOT = { exists = MWK }
					}
					hidden_effect = {
						every_province = {
							limit = { region = manchuria_region }
							remove_core = MWK
						}
					}
					MXI = {
						every_owned_province = {
							limit = {
								province_group = haixi_group
							}
							remove_core = MXI
							add_core = MWK
							cede_province = MWK
							change_culture = jurchen
						}
					}
				}
				if = {
					limit = {
						NOT = { exists = MHR }
					}
					hidden_effect = {
						every_province = {
							limit = { region = manchuria_region }
							remove_core = MHR
						}
					}
					MXI = {
						every_owned_province = {
							limit = {
								province_group = haixi_group
							}
							remove_core = MXI
							add_core = MHR
							cede_province = MHR
							change_culture = jurchen
						}
					}
				}
				if = {
					limit = {
						NOT = { exists = MUD }
					}
					hidden_effect = {
						every_province = {
							limit = { region = manchuria_region }
							remove_core = MUD
						}
					}
					MXI = {
						every_owned_province = {
							limit = {
								province_group = haixi_group
							}
							remove_core = MXI
							add_core = MUD
							cede_province = MUD
							change_culture = jurchen
						}
					}
				}
				if = {
					limit = {
						NOT = { exists = MNA }
					}
					hidden_effect = {
						every_province = {
							limit = { region = manchuria_region }
							remove_core = MNA
						}
					}
					MXI = {
						every_owned_province = {
							limit = {
								province_group = haixi_group
							}
							remove_core = MXI
							add_core = MNA
							cede_province = MNA
							change_culture = jurchen
						}
					}
				}
				else = {
					random_country = {
						limit = {
							primary_culture = jurchen
							NOT = { capital_scope = { province_group = jianzhou_group } }
						}
						country_event = { id = red_turban.18 }
					}
				}
			}
			else = {
				random_country = {
					limit = {
						primary_culture = jurchen
					}
					country_event = { id = red_turban.18 }
				}
			}
		}
		ai_chance = {
			factor = 15
		}
	}
}
### Ming creates Uriankhai a client state
country_event = {
	id = red_turban.17
	
	title = "red_turban.17.t"
	desc = "red_turban.17.d"
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "red_turban.17a"
		change_government = chinese_monarchy_2
	}
}
### Possibility of settlement
country_event = {
	id = red_turban.18
	
	title = "red_turban.18.t"
	desc = "red_turban.18.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = { #Jianzhou
		name = "red_turban.18a"
		trigger = {
			jianzhou_group = {
				owned_by = MXI
			}
		}
		hidden_effect = {
			every_province = {
				limit = { region = manchuria_region }
				remove_core = ROOT
			}
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 4033
					province_id = 1048
					province_id = 1050
				}
			}
			cede_province = XXX
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 3256
					culture = daur
				}
			}
			cede_province = MYR
			change_culture = daur
			add_core = MYR
		}
		random_neighbor_country = {
			limit = {
				primary_culture = jurchen
			}
			every_province = {
				limit = {
					owned_by = ROOT
				}
				cede_province = PREV
				add_core = PREV
			}
		}
		MXI = {
			every_owned_province = {
				limit = {
					province_group = jianzhou_group
				}
				remove_core = MXI
				add_core = ROOT
				cede_province = ROOT
				change_culture = jurchen
			}
		}
		hidden_effect = {
			if = {
				limit = {
					haixi_group = {
						owned_by = MXI
					}
				}
				random_country = {
					limit = {
						NOT = { tag = ROOT }
						primary_culture = jurchen
					}
					country_event = { id = red_turban.19 }
				}
			}
		}
		ai_chance = {
			factor = 100
		}
	}
	option = { #Haixi
		name = "red_turban.18b"
		trigger = {
			haixi_group = {
				owned_by = MXI
			}
		}
		hidden_effect = {
			every_province = {
				limit = { region = manchuria_region }
				remove_core = ROOT
			}
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 4033
					province_id = 1048
					province_id = 1050
				}
			}
			cede_province = XXX
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 3256
					culture = daur
				}
			}
			cede_province = MYR
			change_culture = daur
			add_core = MYR
		}
		random_neighbor_country = {
			limit = {
				primary_culture = jurchen
			}
			every_province = {
				limit = {
					owned_by = ROOT
				}
				cede_province = PREV
				add_core = PREV
			}
		}
		MXI = {
			every_owned_province = {
				limit = {
					province_group = haixi_group
				}
				remove_core = MXI
				add_core = ROOT
				cede_province = ROOT
				change_culture = jurchen
			}
		}
		hidden_effect = {
			if = {
				limit = {
					jianzhou_group = {
						owned_by = MXI
					}
				}
				random_country = {
					limit = {
						NOT = { tag = ROOT }
						primary_culture = jurchen
					}
					country_event = { id = red_turban.19 }
				}
			}
		}
		ai_chance = {
			factor = 100
		}
	}
	option = { #No thanks
		name = "red_turban.18c"
		trigger = {
			NOT = { ai = yes }
		}
		hidden_effect = {
			random_country = {
				limit = {
					NOT = { tag = ROOT }
					primary_culture = jurchen
				}
				country_event = { id = red_turban.19 }
			}
		}
	}
}
### Copy of above
country_event = {
	id = red_turban.19
	
	title = "red_turban.18.t"
	desc = "red_turban.18.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = { #Jianzhou
		name = "red_turban.18a"
		trigger = {
			jianzhou_group = {
				owned_by = MXI
			}
		}
		hidden_effect = {
			every_province = {
				limit = { region = manchuria_region }
				remove_core = ROOT
			}
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 4033
					province_id = 1048
					province_id = 1050
				}
			}
			cede_province = XXX
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 3256
					culture = daur
				}
			}
			cede_province = MYR
			change_culture = daur
			add_core = MYR
		}
		random_neighbor_country = {
			limit = {
				primary_culture = jurchen
			}
			every_province = {
				limit = {
					owned_by = ROOT
				}
				cede_province = PREV
				add_core = PREV
			}
		}
		MXI = {
			every_owned_province = {
				limit = {
					province_group = jianzhou_group
				}
				remove_core = MXI
				add_core = ROOT
				cede_province = ROOT
				change_culture = jurchen
			}
		}
		hidden_effect = {
			if = {
				limit = {
					haixi_group = {
						owned_by = MXI
					}
				}
				random_country = {
					limit = {
						NOT = { tag = ROOT }
						primary_culture = jurchen
					}
					country_event = { id = red_turban.18}
				}
			}
		}
		ai_chance = {
			factor = 100
		}
	}
	option = { #Haixi
		name = "red_turban.18b"
		trigger = {
			haixi_group = {
				owned_by = MXI
			}
		}
		hidden_effect = {
			every_province = {
				limit = { region = manchuria_region }
				remove_core = ROOT
			}
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 4033
					province_id = 1048
					province_id = 1050
				}
			}
			cede_province = XXX
		}
		every_owned_province = {
			limit = {
				OR = {
					province_id = 3256
					culture = daur
				}
			}
			cede_province = MYR
			change_culture = daur
			add_core = MYR
		}
		random_neighbor_country = {
			limit = {
				primary_culture = jurchen
			}
			every_province = {
				limit = {
					owned_by = ROOT
				}
				cede_province = PREV
				add_core = PREV
			}
		}
		MXI = {
			every_owned_province = {
				limit = {
					province_group = haixi_group
				}
				remove_core = MXI
				add_core = ROOT
				cede_province = ROOT
				change_culture = jurchen
			}
		}
		hidden_effect = {
			if = {
				limit = {
					jianzhou_group = {
						owned_by = MXI
					}
				}
				random_country = {
					limit = {
						NOT = { tag = ROOT }
						primary_culture = jurchen
					}
					country_event = { id = red_turban.18 }
				}
			}
		}
		ai_chance = {
			factor = 100
		}
	}
	option = { #No thanks
		name = "red_turban.18c"
		trigger = {
			NOT = { ai = yes }
		}
		hidden_effect = {
			random_country = {
				limit = {
					NOT = { tag = ROOT }
					primary_culture = jurchen
				}
				country_event = { id = red_turban.18 }
			}
		}
	}
}
