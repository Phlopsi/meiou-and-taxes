
#stability down
country_event = {
	id = stability.1
	title = "stability.1.t"
	desc = "stability.1.d"
	picture = PALAIS_MAZARIN_eventPicture

	hidden = yes
	
	is_triggered_only = yes
	
	option = {
		name = "stability.1.a"
		set_variable = { which = "national_stability" value = -3 }
		if = {
			limit = {
				stability = -2 
			}
			change_variable = { which = "national_stability" value = 1 }
			if = {
				limit = {
					stability = -1 
				}
				change_variable = { which = "national_stability" value = 1 }
				if = {
					limit = {
						stability = 0 
					}
					change_variable = { which = "national_stability" value = 1 }
					if = {
						limit = {
							stability = 1 
						}
						change_variable = { which = "national_stability" value = 1 }
						if = {
							limit = {
								stability = 2 
							}
							change_variable = { which = "national_stability" value = 1 }
							if = {
								limit = {
									stability = 3 
								}
								change_variable = { which = "national_stability" value = 1 }
							}
						}
					}
				}
			}
		}
	}
}

#stability up
country_event = {
	id = stability.2
	title = "stability.2.t"
	desc = "stability.2.d"
	picture = PALAIS_MAZARIN_eventPicture

	hidden = yes
	
	trigger = {
		OR = {
			AND = {
				stability = 3
				NOT = { check_variable = { which = "national_stability" value = 3 } }
			}
			AND = {
				stability = 2
				NOT = { check_variable = { which = "national_stability" value = 2 } }
			}
			AND = {
				stability = 1
				NOT = { check_variable = { which = "national_stability" value = 1 } }
			}
			AND = {
				stability = 0
				NOT = { check_variable = { which = "national_stability" value = 0 } }
			}
			AND = {
				stability = -1
				NOT = { check_variable = { which = "national_stability" value = -1 } }
			}
			AND = {
				stability = -2
				NOT = { check_variable = { which = "national_stability" value = -2 } }
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "stability.2.a"
		add_country_modifier = {
			name = "stability_cooldown"
			duration = 3650
			desc = "STABILITY_COOLDOWN"
		}
		set_variable = { which = "national_stability" value = -3 }
		if = {
			limit = {
				stability = -2 
			}
			change_variable = { which = "national_stability" value = 1 }
			if = {
				limit = {
					stability = -1 
				}
				change_variable = { which = "national_stability" value = 1 }
				if = {
					limit = {
						stability = 0 
					}
					change_variable = { which = "national_stability" value = 1 }
					if = {
						limit = {
							stability = 1 
						}
						change_variable = { which = "national_stability" value = 1 }
						if = {
							limit = {
								stability = 2 
							}
							change_variable = { which = "national_stability" value = 1 }
							if = {
								limit = {
									stability = 3 
								}
								change_variable = { which = "national_stability" value = 1 }
							}
						}
					}
				}
			}
		}
	}
}