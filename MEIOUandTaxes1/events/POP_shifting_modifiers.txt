namespace = POP_shifting_modifiers

### By KJ and Demian

country_event = {
    id = POP_shifting_modifiers.001
    title = "POP_shifting_modifiers.001.t"
    desc = "POP_shifting_modifiers.001.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
    #is_triggered_only = yes
    hidden = no
 
    trigger = {
       has_country_flag = center_of_universe
       NOT = { has_country_modifier = modifiers_shifted }
	   is_year = 1366
	}
    
    mean_time_to_happen = {
		months = 1
	}
 
    immediate = {
        add_country_modifier = {
			name = modifiers_shifted
			duration = 3650
			hidden = yes
		} 
		every_province = { 
			limit = { 
				is_city = yes
			}
			remove_province_modifier = preeminent_region_city
			remove_province_modifier = preeminent_sub_continent_city
			remove_province_modifier = preeminent_continent_city
			if = { 
				limit = { 
					base_production = 5 
				}
				set_variable = { 	which = biggest_region_city 	value = 0 }		
				set_variable = { 	which = biggest_region_city 	which = urban_population }
				change_variable = { which = biggest_region_city 	which = urban_population_growing }
			}
		}
		every_province = { 
			limit = { 
				has_province_flag = regional_food_center 
			}
			set_variable = { 	which = biggest_region_city 		  value = 0 }		
			change_variable = { which = biggest_region_city 		  which = urban_population }
			change_variable = { which = biggest_region_city 		  which = urban_population_growing }
			set_variable = {    which = biggest_region_city_food      value = 0 }
			set_variable = {    which = biggest_region_city_food      which = biggest_region_city } ### This is for the province that is actually the food center
			every_province = {
				limit = {
					region = PREV 
					base_production = 5
					NOT = { has_province_flag = regional_food_center }
				}
				if = {
					limit = { 
						check_variable = { which = biggest_region_city which = PREV } 
					}
					set_province_flag = regional_candidate 
					PREV = { set_variable = { which = biggest_region_city which = PREV } }
				}
			}
			every_province = { 
				limit = { 
					region = PREV
					has_province_flag = regional_candidate
					NOT = { has_province_flag = regional_food_center }
				}
				if = { 
					limit = { 
						check_variable = { which = biggest_region_city which = PREV }
					}
					add_permanent_province_modifier = {
						name = preeminent_region_city
						duration = -1
					}
				}
			}
			if = { 
				limit = { 
					base_production = 5 
					check_variable = { which = biggest_region_city_food    which = biggest_region_city } 
				} 
				add_permanent_province_modifier = {
					name = preeminent_region_city
					duration = -1
				}
			}
			set_variable = { which = biggest_region_city_food		 value = 0 }
		#	every_province = {
		#		limit = {
		#			has_province_flag = pending_regional_city 
		#		}
		#		clr_province_flag = pending_regional_city
		#		remove_province_modifier = preeminent_region_city
		#	}
		}
		country_event = { 
			id = POP_shifting_modifiers.002
		}
	}
	
	option = {
        name = "POP_shifting_modifiers.001.a"
        ai_chance = { factor = 100 }
		
		
	}	
}
	
country_event = {
    id = POP_shifting_modifiers.002
    title = "POP_shifting_modifiers.002.t"
    desc = "POP_shifting_modifiers.002.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
    is_triggered_only = yes
    hidden = yes
 
     
    immediate = {
        every_province = { 
			limit = { 
				base_production = 10
			}
			set_variable = { 	which = biggest_sub_city 	value = 0 }		
			set_variable = { 	which = biggest_sub_city 	which = urban_population }
			change_variable = { which = biggest_sub_city 	which = urban_population_growing }
		}
		every_province = { 
			limit = { 
				has_province_flag = sub_continent_food_center 
			}
			set_variable = { 	which = biggest_sub_city 		   value = 0 }
			change_variable = { which = biggest_sub_city 		   which = urban_population }
			change_variable = { which = biggest_sub_city 		   which = urban_population_growing }
			set_variable = { 	which = biggest_sub_city_food      value = 0 }
			set_variable = { 	which = biggest_sub_city_food      which = biggest_sub_city }
			every_province = {
				limit = {
					has_province_flag = part_of_@PREV
					base_production = 10
					NOT = { has_province_flag = sub_continent_food_center }
				}
				if = {
					limit = { 
						check_variable = { which = biggest_sub_city which = PREV } 
					}
					set_province_flag = sub_cont_candidate 
					PREV = { set_variable = { which = biggest_sub_city which = PREV } }
				}
			}
			every_province = { 
				limit = { 
					has_province_flag = part_of_@PREV
					has_province_flag = sub_cont_candidate
				}
				if = { 
					limit = { 
						check_variable = { which = biggest_sub_city which = PREV }
					}
					add_permanent_province_modifier = {
						name = preeminent_sub_continent_city
						duration = -1
					}
				}
			}
			if = { 
				limit = { 
					base_production = 10
					check_variable = { which = biggest_sub_city_food    which = biggest_sub_city } 
				} 
				add_permanent_province_modifier = {
					name = preeminent_sub_continent_city
					duration = -1
				}
			}
			set_variable = { which = biggest_sub_city_food 				 value = 0 }
		}
		country_event = { 
			id = POP_shifting_modifiers.003
		}
	}
	
	option = {
        name = "POP_shifting_modifiers.002.a"
        ai_chance = { factor = 100 }
		
		
	}	
}
	
country_event = {
    id = POP_shifting_modifiers.003
    title = "POP_shifting_modifiers.003.t"
    desc = "POP_shifting_modifiers.003.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
    is_triggered_only = yes
    hidden = yes
 
    immediate = {
        every_province = { 
			limit = { 
				base_production = 15
			}
			set_variable = { 	which = biggest_continent_city 	value = 0 }		
			set_variable = { 	which = biggest_continent_city 	which = urban_population }
			change_variable = { which = biggest_continent_city 	which = urban_population_growing }
		}
		every_province = { 
			limit = { 
				has_province_flag = continent_food_center 
			}
			set_variable = { 	which = biggest_continent_city 		 value = 0 }	
			change_variable = { which = biggest_continent_city 		 which = urban_population }
			change_variable = { which = biggest_continent_city 		 which = urban_population_growing }
			set_variable = { 	which = biggest_continent_city_food  value = 0 }
			set_variable = { 	which = biggest_continent_city_food  which = biggest_continent_city }
			every_province = {
				limit = {
					has_province_flag = part_cont_of_@PREV 
					base_production = 15
					NOT = { has_province_flag = continent_food_center }
				}
				if = {
					limit = { 
						check_variable = { which = biggest_continent_city which = PREV } 
					}
					set_province_flag = continent_candidate 
					PREV = { set_variable = { which = biggest_continent_city which = PREV } }
				}
			}
			every_province = { 
				limit = { 
					has_province_flag = part_cont_of_@PREV
					has_province_flag = continent_candidate
				}
				if = { 
					limit = { 
						check_variable = { which = biggest_continent_city which = PREV }
					}
					add_permanent_province_modifier = {
						name = preeminent_continent_city
						duration = -1
					}
				}
			}
			if = { 
				limit = { 
					base_production = 15
					check_variable = { which = biggest_continent_city_food    which = biggest_continent_city } 
				} 
				add_permanent_province_modifier = {
					name = preeminent_continent_city
					duration = -1
				}
			}
			set_variable = { which = biggest_continent_city_food     value = 0 }
		}
		every_province = { 
			limit = { 
				is_city = yes
			}	
			clr_province_flag = regional_candidate
			clr_province_flag = sub_cont_candidate
			clr_province_flag = continent_candidate
			if = {
				limit = {
					has_province_modifier = preeminent_continent_city
				}
				remove_province_modifier = preeminent_region_city
				remove_province_modifier = preeminent_sub_continent_city
			}
			if = { 
				limit = { 
					has_province_modifier = preeminent_sub_continent_city
				}
				remove_province_modifier = preeminent_region_city
			}
			set_variable = { which = biggest_region_city 			 value = 0 }
			set_variable = { which = biggest_sub_city 				 value = 0 }
			set_variable = { which = biggest_continent_city		     value = 0 }
		}
	}
	
	option = {
        name = "POP_shifting_modifiers.003.a"
        ai_chance = { factor = 100 }
		

	}	
}	
	
	
	
country_event = {
    id = POP_shifting_modifiers.004
    title = "POP_shifting_modifiers.004.t"
    desc = "POP_shifting_modifiers.004.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
 
    immediate = {
		every_province = { 
			limit = { 
				has_province_flag = continent_food_center 
			}
			set_variable = { which = continent_population 		value = 0 }
			set_variable = { which = continent_num_of_country 	value = 0 }
			set_province_flag = DERP
			every_province = {
				limit = {
					is_capital = yes
					has_province_flag = part_cont_of_@PREV  
				}
				set_province_flag = DERP_DERP
				set_variable = { which = country_total_population 				value = 0 }
				PREV = { change_variable = { which = continent_num_of_country 	value = 1 } }
				owner = {
					PREV = { change_variable = { which = country_total_population	which = PREV } }
				}
				set_variable = { which = continent_population 				value = 0 }
				set_variable = { which = continent_population				which = country_total_population }
				PREV = { change_variable = { which = continent_population	which = PREV } }
			}	
			set_variable = { which = continent_country_average_population		value = 0 }
			set_variable = { which = continent_country_average_population		which = continent_population }
			if = {
				limit = {
					is_variable_equal = {
						which = continent_num_of_country
						value = 0
					}
				}

				# log = "<ERROR><A9FAA870><THIS:[This.GetName]><PREV:[Prev.GetName]><ROOT:[Root.GetName]><FROM:[From.GetName]> Division by zero!"

				else = {
					divide_variable = {
						which = continent_country_average_population
						which = continent_num_of_country
					}
				}
			}
				
		}	
	}
   
   
   
    option = {
        name = "POP_shifting_modifiers.004.a"
        ai_chance = { factor = 100 }
		
		
	}
}

country_event = { 
    id = POP_shifting_modifiers.005
    title = "POP_shifting_modifiers.005.t"
    desc = "POP_shifting_modifiers.005.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = yes
	
	trigger = {
		is_at_war = no 
		NOT = { num_of_loans = 2 }
		is_year = 1358
		ai = yes
	}
	
	immediate = {
		if = {
			limit = {
				NOT = { check_variable = { which = OTT_AGGRESSION 	value = 0.1 } }
			}
			set_variable = { which = OTT_AGGRESSION		value = 0 }
		}
		set_variable = { which = ottoman_army_strength 		value = 0 }
		export_to_variable = {
			which = ottoman_army_strength
			value = army_size
		}
		
		every_country = {
			limit = {
				is_neighbor_of = ROOT 
			}
			set_variable = { 	  which = ottoman_army_strength 		value = 0 }
			set_variable = { 	  which = ottoman_army_strength 		which = PREV }
			set_variable = { 	  which = ottoman_weak_opponent 		value = 0 }
			export_to_variable = {
				which = ottoman_weak_opponent
				value = army_size
			}
			multiply_variable = { which = ottoman_weak_opponent			value = 1.25 }
		}
	#	if = {
	#		limit = {
	#			exists = MAM 
	#		}
	#		random_country = { 
	#			limit = {
	#				tag = MAM
	#				is_neighbor_of = ROOT 
	#				NOT = { truce_with = ROOT } 	
	#				is_free_or_tributary_trigger = yes
	#				check_variable = { which = ottoman_army_strength		which = ottoman_weak_opponent }
	#				NOT = { has_country_flag = has_been_guaranteed }
	#				NOT = { num_of_allies = 1 } 
	#			}
	#			ROOT = { 
	#				declare_war_with_cb = {
	#					who = PREV 
	#					casus_belli = cb_rum_sultanate_predator
	#				}
	#			}
	#			ROOT = {
	#			#	add_country_modifier = {
	#			#		name = attacked_recently 
	#			#		duration = 1825
	#			#		hidden = yes 
	#			#	}
	#				change_variable = { which = OTT_AGGRESSION 	value = 1 }
	#			}
	#		}
	#	}
		if = {
			limit = {
				NOT = { num_of_revolts = 1 }
			}	
			if = {
				limit = { 
					NOT = { check_variable = { which = total_upper_class_country 	value = 10 } }
				}
				random_country = {
					limit = {
						OR = {
							AND = {
								tag = BUL
								is_neighbor_of = ROOT
							}
							tag = BYZ
							AND = {	
								is_neighbor_of = ROOT 
								NOT = { num_of_allies = 1 }  
								NOT = { vassal = 1 }
								is_free_or_tributary_trigger = yes
								check_variable = { which = ottoman_army_strength		which = ottoman_weak_opponent }
								NOT = { has_country_flag = has_been_guaranteed }
								NOT = { check_variable = { which = total_upper_class_country 	value = 2 } }
								NOT = { tag = KAR }
								is_vassal = no
							}
						}
						NOT = { truce_with = ROOT }
					}
					clr_country_flag = has_been_guaranteed
					ROOT = { 
						declare_war_with_cb = {
							who = PREV 
							casus_belli = cb_sense_weakness
						}
					}
					ROOT = {
					#	add_country_modifier = {
					#		name = attacked_recently 
					#		duration = 1825
					#		hidden = yes 
					#	}
						change_variable = { which = OTT_AGGRESSION 	value = 1 }
					}
				}
			}
		#	if = {
		#		limit = { 
		#			check_variable = { which = total_upper_class_country 	value = 10 }
		#			NOT = { check_variable = { which = total_upper_class_country 	value = 30 } }
		#		}
		#		random_country = {
		#			limit = {
		#				is_neighbor_of = ROOT 
		#				NOT = { truce_with = ROOT } 
		#				NOT = { num_of_allies = 1 }  
		#				is_free_or_tributary_trigger = yes
		#				check_variable = { which = ottoman_army_strength		which = ottoman_weak_opponent }
		#				NOT = { has_country_flag = has_been_guaranteed }
		#				NOT = { check_variable = { which = total_upper_class_country 	value = 3 } }
		#			}
		#			clr_country_flag = has_been_guaranteed
		#			ROOT = { 
		#				declare_war_with_cb = {
		#					who = PREV 
		#					casus_belli = cb_sense_weakness
		#				}
		#			}
		#			ROOT = {
		#			#	add_country_modifier = {
		#			#		name = attacked_recently 
		#			#		duration = 1000
		#			#		hidden = yes 
		#			#	}
		#				change_variable = { which = OTT_AGGRESSION 	value = 1 }
		#			}
		#		}
		#	}
		#	if = {
		#		limit = { 
		#			check_variable = { which = total_upper_class_country 	value = 30 }
		#		}
		#		random_country = {
		#			limit = {
		#				is_neighbor_of = ROOT 
		#				NOT = { truce_with = ROOT } 
		#				NOT = { num_of_allies = 1 }  
		#				is_free_or_tributary_trigger = yes
		#				check_variable = { which = ottoman_army_strength		which = ottoman_weak_opponent }
		#				NOT = { has_country_flag = has_been_guaranteed }
		#				NOT = { check_variable = { which = total_upper_class_country 	value = 5 } }
		#			}
		#			clr_country_flag = has_been_guaranteed
		#			ROOT = { 
		#				declare_war_with_cb = {
		#					who = PREV 
		#					casus_belli = cb_sense_weakness
		#				}
		#				change_variable = { which = declared_with_rum_demian_aggression		value = 1 }
		#					
		#			}
		#			ROOT = {
		#			#	add_country_modifier = {
		#			#		name = attacked_recently 
		#			#		duration = 1000
		#			#		hidden = yes 
		#			#	}
		#				change_variable = { which = OTT_AGGRESSION 	value = 1 }
		#			}
		#		}
		#	}
		}
	}
	
    option = {
        name = "POP_shifting_modifiers.005.a"
        ai_chance = { factor = 100 }
		
		
	}
}	

country_event = { 
    id = POP_shifting_modifiers.006
    title = "POP_shifting_modifiers.006.t"
    desc = "POP_shifting_modifiers.006.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = yes
	
	trigger = {
		is_year = 1358
		NOT = { is_year = 1363 }
	}
	
	immediate = {
		OTT = { country_event = { id = POP_shifting_modifiers.005 } }
	}
	
    option = {
        name = "POP_shifting_modifiers.006.a"
        ai_chance = { factor = 100 }
		
		
	}
}	
		
country_event = { 
    id = POP_shifting_modifiers.007
    title = "POP_shifting_modifiers.007.t"
    desc = "POP_shifting_modifiers.007.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
	
	trigger = {
		
	}
	
	immediate = {
		
		set_variable = { which = stability_cost_modifier 	value = 0 }
		export_to_variable = {
			which = stability_cost_modifier
			value = modifier:stability_cost_modifier
		}
		if = {
			limit = {
				check_variable = { which = stability_cost_modifier	value = 0.01 } 
			}
			set_variable = { 	  which = stability_cost_modifier_invert 		value = 0 }
			set_variable = { 	  which = stability_cost_modifier_invert 		which = stability_cost_modifier }
			change_variable = {   which = stability_cost_modifier				value = 1 }
			subtract_variable = { which = stability_cost_modifier				which = stability_cost_modifier_invert }		
			subtract_variable = { which = stability_cost_modifier				which = stability_cost_modifier_invert }
			else = { 
				multiply_variable = { which = stability_cost_modifier 	value = -1 }
				change_variable = { which = stability_cost_modifier		value = 1 }
			}
		}
	}
	
    option = {
        name = "POP_shifting_modifiers.007.a"
        ai_chance = { factor = 100 }
		
		
	}
}	

country_event = { 
    id = POP_shifting_modifiers.008
    title = "POP_shifting_modifiers.008.t"
    desc = "POP_shifting_modifiers.008.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
	
	trigger = {
		
	}
	
	immediate = {
		
		add_country_modifier = {
			name = stability_murder
			duration = -1 
		}
	}
	
    option = {
        name = "POP_shifting_modifiers.008.a"
        ai_chance = { factor = 100 }
		
		
	}
}	