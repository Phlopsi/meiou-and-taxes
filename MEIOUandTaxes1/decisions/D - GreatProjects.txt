#We need a demian ticker style system for this, when the time comes.  A constant malus with a construction fund, I think.

country_decisions = {

	construct_kiel_canal = {
		major = yes
		potential = {
			OR = {
				ai = no
				AND = {
					treasury = 14000
					owns = 1252
				}
			}
			has_discovered = 1252
			1252 = {
				range = ROOT
				NOT = { has_great_project = kiel_canal }
				NOT = { has_construction = canal }
			}
			adm_tech = 20
		}
		allow = {
			owns = 1252
			adm_tech = 25
			treasury = 12000
		}
		effect = {
			add_treasury = -12000
			1252 = {	# Holstein
				add_great_project = kiel_canal
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	construct_suez_canal = {
		major = yes
		potential = {
			OR = {
				ai = no
				AND = {
					treasury = 14000
					owns = 359
				}
			}
			has_discovered = 359
			359 = {
				range = ROOT
				NOT = { has_great_project = suez_canal }
				NOT = { has_construction = canal }
			}
			adm_tech = 15
		}
		allow = {
			owns = 359
			adm_tech = 20
			treasury = 12000
		}
		effect = {
			add_treasury = -12000
			359 = {	# Dumyat
				add_great_project = suez_canal
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	construct_panama_canal = {
		major = yes
		potential = {
			is_random_new_world = no
			OR = {
				ai = no
				AND = {
					treasury = 14000
					owns = 835
				}
			}
			has_discovered = 835
			835 = {
				range = ROOT
				NOT = { has_great_project = panama_canal }
				NOT = { has_construction = canal }
			}
			adm_tech = 45
		}
		allow = {
			owns = 835
			adm_tech = 50
			treasury = 12000
		}
		effect = {
			add_treasury = -12000
			835 = {
				add_great_project = panama_canal
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	construct_forbidden_city_kaifeng = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 701
			}
			has_discovered = 701
			701 = {
				range = ROOT
				NOT = { has_great_project = forbidden_city_kaifeng }
				NOT = { has_province_modifier = forbidden_city_kaifeng }
				NOT = { has_construction = great_project }
			}
			708 = {
				NOT = { has_great_project = forbidden_city_beijing }
				NOT = { has_province_modifier = forbidden_city_beijing }
				NOT = { has_construction = great_project }
			}
			2121 = {
				NOT = { has_great_project = forbidden_city_guangzhou }
				NOT = { has_province_modifier = forbidden_city_guangzhou }
				NOT = { has_construction = great_project }
			}
			2150 = {
				NOT = { has_great_project = forbidden_city_nanjing }
				NOT = { has_province_modifier = forbidden_city_nanjing }
				NOT = { has_construction = great_project }
			}
			2252 = {
				NOT = { has_great_project = forbidden_city_xian }
				NOT = { has_province_modifier = forbidden_city_xian }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			capital = 701
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				701 = {
					add_great_project = forbidden_city_kaifeng
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				701 = {
					add_permanent_province_modifier = { name = forbidden_city_kaifeng duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_beijing = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 708
			}
			has_discovered = 708
			701 = {
				range = ROOT
				NOT = { has_great_project = forbidden_city_kaifeng }
				NOT = { has_province_modifier = forbidden_city_kaifeng }
				NOT = { has_construction = great_project }
			}
			708 = {
				NOT = { has_great_project = forbidden_city_beijing }
				NOT = { has_province_modifier = forbidden_city_beijing }
				NOT = { has_construction = great_project }
			}
			2121 = {
				NOT = { has_great_project = forbidden_city_guangzhou }
				NOT = { has_province_modifier = forbidden_city_guangzhou }
				NOT = { has_construction = great_project }
			}
			2150 = {
				NOT = { has_great_project = forbidden_city_nanjing }
				NOT = { has_province_modifier = forbidden_city_nanjing }
				NOT = { has_construction = great_project }
			}
			2252 = {
				NOT = { has_great_project = forbidden_city_xian }
				NOT = { has_province_modifier = forbidden_city_xian }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			capital = 708
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				708 = {
					add_great_project = forbidden_city_beijing
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				708 = {
					add_permanent_province_modifier = { name = forbidden_city_beijing duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_guangzhou = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 2121
			}
			has_discovered = 2121
			701 = {
				range = ROOT
				NOT = { has_great_project = forbidden_city_kaifeng }
				NOT = { has_province_modifier = forbidden_city_kaifeng }
				NOT = { has_construction = great_project }
			}
			708 = {
				NOT = { has_great_project = forbidden_city_beijing }
				NOT = { has_province_modifier = forbidden_city_beijing }
				NOT = { has_construction = great_project }
			}
			2121 = {
				NOT = { has_great_project = forbidden_city_guangzhou }
				NOT = { has_province_modifier = forbidden_city_guangzhou }
				NOT = { has_construction = great_project }
			}
			2150 = {
				NOT = { has_great_project = forbidden_city_nanjing }
				NOT = { has_province_modifier = forbidden_city_nanjing }
				NOT = { has_construction = great_project }
			}
			2252 = {
				NOT = { has_great_project = forbidden_city_xian }
				NOT = { has_province_modifier = forbidden_city_xian }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			capital = 2121
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				2121 = {
					add_great_project = forbidden_city_guangzhou
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				2121 = {
					add_permanent_province_modifier = { name = forbidden_city_guangzhou duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_nanjing = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 2150
			}
			has_discovered = 2150
			701 = {
				range = ROOT
				NOT = { has_great_project = forbidden_city_kaifeng }
				NOT = { has_province_modifier = forbidden_city_kaifeng }
				NOT = { has_construction = great_project }
			}
			708 = {
				NOT = { has_great_project = forbidden_city_beijing }
				NOT = { has_province_modifier = forbidden_city_beijing }
				NOT = { has_construction = great_project }
			}
			2121 = {
				NOT = { has_great_project = forbidden_city_guangzhou }
				NOT = { has_province_modifier = forbidden_city_guangzhou }
				NOT = { has_construction = great_project }
			}
			2150 = {
				NOT = { has_great_project = forbidden_city_nanjing }
				NOT = { has_province_modifier = forbidden_city_nanjing }
				NOT = { has_construction = great_project }
			}
			2252 = {
				NOT = { has_great_project = forbidden_city_xian }
				NOT = { has_province_modifier = forbidden_city_xian }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			capital = 2150
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				2150 = {
					add_great_project = forbidden_city_nanjing
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				2150 = {
					add_permanent_province_modifier = { name = forbidden_city_nanjing duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_xian = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 2252
			}
			has_discovered = 2252
			701 = {
				range = ROOT
				NOT = { has_great_project = forbidden_city_kaifeng }
				NOT = { has_province_modifier = forbidden_city_kaifeng }
				NOT = { has_construction = great_project }
			}
			708 = {
				NOT = { has_great_project = forbidden_city_beijing }
				NOT = { has_province_modifier = forbidden_city_beijing }
				NOT = { has_construction = great_project }
			}
			2121 = {
				NOT = { has_great_project = forbidden_city_guangzhou }
				NOT = { has_province_modifier = forbidden_city_guangzhou }
				NOT = { has_construction = great_project }
			}
			2150 = {
				NOT = { has_great_project = forbidden_city_nanjing }
				NOT = { has_province_modifier = forbidden_city_nanjing }
				NOT = { has_construction = great_project }
			}
			2252 = {
				NOT = { has_great_project = forbidden_city_xian }
				NOT = { has_province_modifier = forbidden_city_xian }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			capital = 2252
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				2252 = {
					add_great_project = forbidden_city_xian
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				2252 = {
					add_permanent_province_modifier = { name = forbidden_city_xian duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_hagia_sophia_minarets = {
		major = yes
		potential = {
			religion = sunni
			any_owned_province = { region = east_balkan_region }
			OR = {
				ai = no
				owns = 1402
			}
			has_discovered = 1402
			1402 = {
				range = ROOT
				NOT = { has_great_project = hagia_sophia_minarets }
				NOT = { has_province_modifier = hagia_sophia_minarets }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			capital = 1402
			is_at_war = no
			OR = {
				statesman = 2
				theologian = 2
				adm = 3
			}
			stability = 1
			treasury = 600
			adm_power = 50
		}
		effect = {
			add_treasury = -500
			add_adm_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				1402 = {
					add_great_project = hagia_sophia_minarets
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				1402 = {
					add_permanent_province_modifier = { name = hagia_sophia_minarets duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_moscow = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 295
			}
			has_discovered = 295
			295 = {
				range = ROOT
				NOT = { has_great_project = kremlin_moscow }
				NOT = { has_province_modifier = kremlin_moscow }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			#capital = 295
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 1000
			mil_power = 50
		}
		effect = {
			#add_treasury = -900
			add_mil_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				295 = {
					add_great_project = kremlin_moscow
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				295 = {
					add_permanent_province_modifier = { name = kremlin_moscow duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_astrakhan = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 1293
			}
			has_discovered = 1293
			1293 = {
				range = ROOT
				NOT = { has_great_project = kremlin_astrakhan }
				NOT = { has_province_modifier = kremlin_astrakhan }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			#capital = 1293
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 1000
			mil_power = 50
		}
		effect = {
			add_treasury = -900
			add_mil_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				1293 = {
					add_great_project = kremlin_astrakhan
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				1293 = {
					add_permanent_province_modifier = { name = kremlin_astrakhan duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_novgorod = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 309
			}
			has_discovered = 309
			309 = {
				range = ROOT
				NOT = { has_great_project = kremlin_novgorod }
				NOT = { has_province_modifier = kremlin_novgorod }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			#capital = 309
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 1000
			mil_power = 50
		}
		effect = {
			add_treasury = -900
			add_mil_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				309 = {
					add_great_project = kremlin_novgorod
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				309 = {
					add_permanent_province_modifier = { name = kremlin_novgorod duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_kazan = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 1307
			}
			has_discovered = 1307
			1307 = {
				range = ROOT
				NOT = { has_great_project = kremlin_kazan }
				NOT = { has_province_modifier = kremlin_kazan }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			#capital = 1307
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 1000
			mil_power = 50
		}
		effect = {
			add_treasury = -900
			add_mil_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				1307 = {
					add_great_project = kremlin_kazan
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				1307 = {
					add_permanent_province_modifier = { name = kremlin_kazan duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	#construct_kremlin_nizhny = {
	#	major = yes
	#	potential = {
	#		culture_group = east_slavic
	#		OR = {
	#			ai = no
	#			owns = 1292
	#		}
	#		has_discovered = 1292
	#		1292 = {
	#			range = ROOT
	#			NOT = { has_great_project = kremlin_nizhny }
	#			NOT = { has_province_modifier = kremlin_nizhny }
	#			NOT = { has_construction = great_project }
	#		}
	#	}
	#	allow = {
	#		capital = 1292
	#		is_at_war = no
	#		OR = {
	#			fortification_expert = 2
	#			army_organiser = 2
	#			mil = 3
	#		}
	#		stability = 1
	#		treasury = 1000
	#		mil_power = 50
	#	}
	#	effect = {
	#		add_treasury = -900
	#		add_mil_power = -50
	#		1292 = {
	#			add_permanent_province_modifier = { name = kremlin_nizhny duration = -1 }
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#	ai_importance = 1000
	#}

	construct_kremlin_smolensk = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 293
			}
			has_discovered = 293
			293 = {
				range = ROOT
				NOT = { has_great_project = kremlin_smolensk }
				NOT = { has_province_modifier = kremlin_smolensk }
				NOT = { has_construction = great_project }
			}
		}
		allow = {
			#capital = 293
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 1000
			mil_power = 50
		}
		effect = {
			add_treasury = -900
			add_mil_power = -50
			if = {
				limit = {
					has_dlc = "National Monuments II"
				}
				293 = {
					add_great_project = kremlin_smolensk
				}
			}
			if = {
				limit = {
					NOT = { has_dlc = "National Monuments II" }
				}
				293 = {
					add_permanent_province_modifier = { name = kremlin_smolensk duration = -1 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	#construct_kremlin_pskov = {
	#	major = yes
	#	potential = {
	#		culture_group = east_slavic
	#		OR = {
	#			ai = no
	#			owns = 274
	#		}
	#		has_discovered = 274
	#		274 = {
	#			range = ROOT
	#			NOT = { has_great_project = kremlin_pskov }
	#			NOT = { has_province_modifier = kremlin_pskov }
	#			NOT = { has_construction = great_project }
	#		}
	#	}
	#	allow = {
	#		capital = 274
	#		is_at_war = no
	#		OR = {
	#			fortification_expert = 2
	#			army_organiser = 2
	#			mil = 3
	#		}
	#		stability = 1
	#		treasury = 1000
	#		mil_power = 50
	#	}
	#	effect = {
	#		add_treasury = -900
	#		add_mil_power = -50
	#		274 = {
	#			add_permanent_province_modifier = { name = kremlin_pskov duration = -1 }
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#	ai_importance = 1000
	#}

	cancel_kiel_canal = {
		major = yes
		potential = {
			ai = no
			owns = 1252
			1252 = {
				has_construction = canal
			}
		}
		allow = {
			1252 = {
				has_construction = canal
			}
		}
		effect = {
			1252 = {	# Holstein
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	cancel_suez_canal = {
		major = yes
		potential = {
			ai = no
			owns = 359
			359 = {
				has_construction = canal
			}
		}
		allow = {
			359 = {
				has_construction = canal
			}
		}
		effect = {
			359 = {	# Diamientia
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	cancel_panama_canal = {
		major = yes
		potential = {
			ai = no
			owns = 835
			835 = {
				has_construction = canal
			}
		}
		allow = {
			835 = {
				has_construction = canal
			}
		}
		effect = {
			835 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_forbidden_city_kaifeng = {
		major = yes
		potential = {
			ai = no
			owns = 701
			701 = {
				has_construction = canal
			}
		}
		allow = {
			701 = {
				has_construction = canal
			}
		}
		effect = {
			701 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_forbidden_city_beijing = {
		major = yes
		potential = {
			ai = no
			owns = 708
			708 = {
				has_construction = canal
			}
		}
		allow = {
			708 = {
				has_construction = canal
			}
		}
		effect = {
			708 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_forbidden_city_guangzhou = {
		major = yes
		potential = {
			ai = no
			owns = 2121
			2121 = {
				has_construction = canal
			}
		}
		allow = {
			2121 = {
				has_construction = canal
			}
		}
		effect = {
			2121 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_forbidden_city_nanjing = {
		major = yes
		potential = {
			ai = no
			owns = 2150
			2150 = {
				has_construction = canal
			}
		}
		allow = {
			2150 = {
				has_construction = canal
			}
		}
		effect = {
			2150 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_forbidden_city_xian = {
		major = yes
		potential = {
			ai = no
			owns = 2252
			2252 = {
				has_construction = canal
			}
		}
		allow = {
			2252 = {
				has_construction = canal
			}
		}
		effect = {
			2252 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_hagia_sophia_minarets = {
		major = yes
		potential = {
			ai = no
			owns = 1402
			1402 = {
				has_construction = canal
			}
		}
		allow = {
			1402 = {
				has_construction = canal
			}
		}
		effect = {
			1402 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_moscow = {
		major = yes
		potential = {
			ai = no
			owns = 701
			701 = {
				has_construction = canal
			}
		}
		allow = {
			701 = {
				has_construction = canal
			}
		}
		effect = {
			701 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_astrakhan = {
		major = yes
		potential = {
			ai = no
			owns = 1293
			1293 = {
				has_construction = canal
			}
		}
		allow = {
			1293 = {
				has_construction = canal
			}
		}
		effect = {
			1293 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_novgorod = {
		major = yes
		potential = {
			ai = no
			owns = 309
			309 = {
				has_construction = canal
			}
		}
		allow = {
			309 = {
				has_construction = canal
			}
		}
		effect = {
			309 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_kazan = {
		major = yes
		potential = {
			ai = no
			owns = 1307
			1307 = {
				has_construction = canal
			}
		}
		allow = {
			1307 = {
				has_construction = canal
			}
		}
		effect = {
			1307 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_nizhny = {
		major = yes
		potential = {
			ai = no
			owns = 1292
			1292 = {
				has_construction = canal
			}
		}
		allow = {
			1292 = {
				has_construction = canal
			}
		}
		effect = {
			1292 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_smolensk = {
		major = yes
		potential = {
			ai = no
			owns = 293
			293 = {
				has_construction = canal
			}
		}
		allow = {
			293 = {
				has_construction = canal
			}
		}
		effect = {
			293 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	cancel_kremlin_pskov = {
		major = yes
		potential = {
			ai = no
			owns = 274
			274 = {
				has_construction = canal
			}
		}
		allow = {
			274 = {
				has_construction = canal
			}
		}
		effect = {
			274 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

}
