country_decisions = {

	open_christian_age_of_exploration = {
		potential = {
			NOT = { has_country_flag = age_of_exploration }
			religion_group = christian
			num_of_ports = 1
			always = no
		}
		allow = {
			OR = {
				full_idea_group = naval_ideas
				ai = yes
			}
			OR = {
				AND = {
					OR = { tag = POR tag = GBR tag = NED }
					dip_tech = 14
				}
				AND = {
					culture_group = iberian
					NOT = { andalucia_region = { type = all owner = { religion_group = muslim } } }
					dip_tech = 14
				}
				AND = {
					OR = { tag = ENG  tag = FRA }
					NOT = { has_global_flag = hundred_year_war }
					dip_tech = 14
				}
					dip_tech = 20
			}
			num_of_ports = 2
			is_at_war = no
			
		}
		effect = {
			set_country_flag = age_of_exploration
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	open_other_age_of_exploration = {
		potential = {
			NOT = { has_country_flag = age_of_exploration }
			NOT = { religion_group = christian }
			num_of_ports = 1
			always = no
		}
		allow = {
			full_idea_group = naval_ideas
			dip_tech = 20
			num_of_ports = 2
			is_at_war = no
		}
		effect = {
			set_country_flag = age_of_exploration
		}
		ai_will_do = {
			factor = 0
		}
		ai_importance = 0
	}
	
	open_early_age_of_exploration = {
		potential = {
			religion_group = christian
			num_of_ports = 1
			owns = 230
			OR = {
				tag = POR
				culture_group = iberian
			}
			NOT = { num_of_explorers = 1 }
			NOT = { has_idea_group = exploration_ideas }
			NOT = { has_country_flag = early_exploration }
		}
		allow = {
			OR = {
				full_idea_group = naval_ideas
				ai = yes
			}
			OR = {
				AND = {
					tag = POR
					dip_tech = 12
				}
				AND = {
					culture_group = iberian
					dip_tech = 14
				}
			}
			num_of_ports = 2
		}
		effect = {
			add_country_modifier = {
				name = "early_exploration"
				duration = -1
			}
			add_prestige = 1
			set_country_flag = early_exploration
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

}
