########################################
#                                      #
#      RestoreCordoba.txt      #
#                                      #
########################################
#
# Generic decision for N. African countires
#
########################################

country_decisions = {

	andalucian_caliphate = {
		major = yes
		potential = {
			NOT = { has_country_flag = al_andalus_claimed }
			OR = {
				culture_group = maghreb
				culture_group = berber_group
				primary_culture = andalucian
			}
			NOT = { culture = tuareg }
			is_tribal = no
			religion_group = muslim
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Almohad" value = 20 }
				check_variable = { which = "Cores_on_Almohad" value = 20 }
			}
			owns = 223   # Granada
			owns = 341   # Tunis
			owns = 343   # Fes
			owns = 347   # Marrakech
			owns = 350   # Al Djazair
			owns = 225   # Cordoba
		}
		effect = {
			andalucia_region = { limit = { owned_by = ROOT } add_core = ROOT }
			andalucia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			west_maghreb_region = { limit = { owned_by = ROOT } add_core = ROOT }
			west_maghreb_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			central_maghreb_region = { limit = { owned_by = ROOT } add_core = ROOT }
			central_maghreb_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			east_maghreb_region = { limit = { owned_by = ROOT } add_core = ROOT }
			east_maghreb_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			toledo_area = { limit = { owned_by = ROOT } add_core = ROOT }
			toledo_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			alentejo_area = { limit = { owned_by = ROOT } add_core = ROOT }
			alentejo_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
	        add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
	        set_country_flag = al_andalus_claimed
			#berber_group_union_effect = yes
			add_accepted_culture = guanche
			add_accepted_culture = rifain
			add_accepted_culture = tamazight
			add_accepted_culture = chleuh
			add_accepted_culture = berber
			add_accepted_culture = kabyle
			add_accepted_culture = tuareg
			#maghreb_union_effect = yes
			add_accepted_culture = maure
			add_accepted_culture = fassi
			add_accepted_culture = algerian
			add_accepted_culture = tunisian
			add_accepted_culture = libyan
			add_accepted_culture = hassaniya
			
			add_accepted_culture = andalucian
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = ALM_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 400
	}

}

