########################################
#                                      #
#      Eastern Cultural.txt      #
#                                      #
########################################
#
# ported from Trin's Oriental mod by impspy
#
########################################

country_decisions = {
	introduce_heir_oriental_mod = {
		potential = {
			government = monarchy
			has_heir = yes
			heir_age = 15
			not = {
				heir_age = 18
				has_ruler_modifier = heir_introduced
			}
		}
		allow = {
			years_of_income = 0.1
			dip_power = 20
		}
		effect = {
			add_ruler_modifier = {
				name = "heir_introduced"
				duration = 1460
			}
			add_heir_claim = 10
			add_prestige = 5
			add_years_of_income = -0.1
			add_dip_power = -20
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	develop_diwani_script_oriental_mod = {
		potential = {
			religion = sunni
			government = monarchy
			not = {
				has_country_modifier = diwani_script
			}
		}
		allow = {
			adm_power = 30
			artist = 2
		}
		effect = {
			add_legitimacy = 30
			add_adm_power = -30
		}
		ai_will_do = {
			factor = 1
		}
	}
	

	support_development_of_hindusthani_music_oriental_mod = {
		potential = {
			OR = {
				culture_group = hindusthani
				culture_group = eastern_aryan
				culture_group = central_indian
				culture_group = pahari_group
				culture_group = rajput
				primary_culture = marathi
			}
			NOT = {
				has_country_modifier = hindusthani_music
				has_country_modifier = carnatic_music
			}
		}
		allow = {
			OR = {
				artist = 2
				has_idea_group = culture_ideas
			}
			adm_power = 30
			dip_power = 10
			years_of_income = 0.4
		}
		effect = {
			add_country_modifier = {
				name = hindusthani_music
				duration = 3650
			}
			add_adm_power = -30
			add_dip_power = -10
			add_years_of_income = -0.2
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	support_development_of_carnatic_music_oriental_mod = {
		potential = {
			OR = {
				culture_group = dravidian
				primary_culture = kannada
				primary_culture = telegu
				primary_culture = tuluva
			}
			NOT = {
				has_country_modifier = hindusthani_music
				has_country_modifier = carnatic_music
			}
		}
		allow = {
			OR = {
				artist = 2
				has_idea_group = culture_ideas
			}
			adm_power = 30
			dip_power = 10
			years_of_income = 0.4
		}
		effect = {
			add_country_modifier = {
				name = carnatic_music
				duration = 3650
			}
			add_adm_power = -30
			add_dip_power = -10
			add_years_of_income = -0.4
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	patronize_chisti_shrine_oriental_mod = {
		potential = {
			capital_scope = { indian_region_trigger = yes }
			is_year = 1500
			OR = {
				religion = sunni
				religion = shiite
			}
			NOT = {
				is_year = 1600
				has_country_modifier = chisti_shrines
			}
			OR = {
				prestige = 0.25
				has_idea_group = culture_ideas				#Patronage
				has_idea_group = humanist_ideas		#Syncretism
				has_country_flag = theology_ideas					#State controlled Church
			}
		}
		allow = {
			adm_power = 10
			years_of_income = 0.35
		}
		effect = {
			add_country_modifier = {
				name = chisti_shrines
				duration = 3650
			}
			add_adm_power = -10
			add_years_of_income = -0.35
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	weighing_ceremony_oriental_mod = {
		potential = {
			capital_scope = { indian_region_trigger = yes }
			OR = {
				AND = { government = despotic_monarchy has_country_modifier = title_6 }
				AND = { government = feudal_monarchy has_country_modifier = title_6  }
				AND = { government = administrative_monarchy has_country_modifier = title_6  }
				AND = { government = constitutional_monarchy has_country_modifier = title_6  }
				AND = { government = indian_monarchy has_country_modifier = title_6  }
				AND = { government = rajput_monarchy has_country_modifier = title_6  }
				
				government = absolute_monarchy
				
				government = enlightened_despotism
			}
			not = {
				has_country_modifier = weighing_ceremony
			}
		}
		allow = {
			adm_power = 10
			years_of_income = 0.1
		}
		effect = {
			add_country_modifier = {
				name = "weighing_ceremony"
				duration = 3650
			}
			add_legitimacy = 15
			add_adm_power = -10
			add_years_of_income = -0.1
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	expand_royal_elephant_stables_oriental_mod = {
		potential = {
			capital_scope = { indian_region_trigger = yes }
			government = monarchy
			OR = {
				east_bengal_region = { owned_by = ROOT }
				west_bengal_region = { owned_by = ROOT }
				deccan_region = { owned_by = ROOT }
				madura_area = { owned_by = ROOT }
				coromandel_area = { owned_by = ROOT }
				south_carnatic_area = { owned_by = ROOT }
				andhra_area = { owned_by = ROOT }
				godavari_area = { owned_by = ROOT }
				nepalese_area = { owned_by = ROOT }
				owns = 2210 #
				owns = 555 #
				owns = 2555 #
				owns = 522 #
				owns = 521 #
			}
			not = {
				has_country_modifier = royal_elephants
			}
		}
		allow = {
			adm_power = 10
			years_of_income = 0.1
		}
		effect = {
			add_country_modifier = {
				name = "royal_elephants"
				duration = 3600
			}
			add_adm_power = -10
			add_years_of_income = -0.1
		}
		ai_will_do = {
			factor = 1
		}
	}
	#Dakani decisions add later
	
}
