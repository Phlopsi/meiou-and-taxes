country_decisions = {

	move_capital_to_delhi = {
		potential = {
			tag = MUG
			NOT = { capital = 2163 }
			NOT = { has_country_flag = relocated_capital_delhi }
		}
		allow = {
			owns = 2163
			is_at_war = no
			prestige = 0
			stability = 0
		}
		effect = {
			set_country_flag = relocated_capital_delhi
			add_prestige = 100
			add_stability_1 = yes
			change_unit_type = indian
			set_capital = 2163			
			add_accepted_culture = kanauji
			2163 = {
			 	# add_base_tax = 2
			 	# add_base_manpower = 1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

}
