bookmark =
{
	name = "TGE1530_NAME"
	desc = "TGE1530_DESC"
	date = 1530.8.13

	country = TUR
	country = PER
	country = MUG
	country = TRT
	country = GUJ
	country = BUK
	country = ADA
	country = MOR

	easy_country = TUR
	easy_country = PER
	easy_country = MUG

	effect = {
		1 = { set_global_flag = f_game_start }
		SWE = { add_stability = 4 }
		DAN = { add_stability = 4 }
		TUR = { add_stability = 4 }
		MUG = { add_stability = 4 }
		FRA = { add_stability = 4 }
		MOS = { add_stability = 4 }
		ENG = { add_stability = 4 }
		HAB = { add_stability = 4 }
		AHM = { add_stability = 4 }
		TAU = { add_stability = 4 }
		GUJ = { add_stability = 4 }
		SPA = { add_stability = 4 }
		POR = { add_stability = 4 }
		PER = { add_stability = 2 }
		MOR = { add_stability = 2 }
		TOK = { add_stability = 2 }
	}
}
