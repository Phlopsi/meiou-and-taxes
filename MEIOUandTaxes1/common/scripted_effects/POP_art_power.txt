set_art_power = {
			set_variable = { which = art_power_from_upper_class value = 0 }
			set_variable = { which = art_power_from_education value = 0 }
			set_variable = { which = art_power_from_buildings value = 0 }
			set_variable = { which = art_power_from_court value = 0 }
			set_variable = { which = art_power_percentage value = 0 }
			
			set_variable = { which = art_power			value = 0 }
			
			# Art buildings make upper class pop produce art power
			set_variable = { which = art_power_from_upper_class which = upper_class_urban_weight } # Upper class Weight
			
			if = {
				limit = {
					NOT = { 
						has_building = art_corporation
						has_building = fine_arts_academy
						has_building = opera 
					}
				}
				multiply_variable = { which = art_power_from_upper_class value = 0.01 }
			}
			
			set_variable = { which = art_power_percentage value = 1 }
			
			if = { limit = { has_building = art_corporation } multiply_variable = { which = art_power_from_upper_class value = 0.025 } change_variable = { which = art_power_from_buildings value = 0.75 } change_variable = { which = art_power_percentage value = 0.10 } }
			if = { limit = { has_building = fine_arts_academy } multiply_variable = { which = art_power_from_upper_class value = 0.03 } change_variable = { which = art_power_from_buildings value = 1.5 } change_variable = { which = art_power_percentage value = 0.15 } }
			if = { limit = { has_building = opera } multiply_variable = { which = art_power_from_upper_class value = 0.035 } change_variable = { which = art_power_from_buildings value = 3 } change_variable = { which = art_power_percentage value = 0.20 } }
			
			if = { limit = { banking = 100 } multiply_variable = { which = art_power_from_upper_class value = 1.1 } }
			if = { limit = { renaissance = 100 } multiply_variable = { which = art_power_from_upper_class value = 1.1 } }
			if = { limit = { casual_literacy = 100 } multiply_variable = { which = art_power_from_upper_class value = 1.1 } }
			if = { limit = { owner = { has_idea = mecenas } }  multiply_variable = { which = art_power_from_upper_class value = 1.25 } }
			
			# Educated upper class pop also produce art power
			set_variable = { which = art_power_from_education which = university_education_multiplier }
			divide_variable = { which = art_power_from_education value = 2 }

			if = { limit = { has_building = great_temple } change_variable = { which = art_power_from_buildings value = 0.5 } }
						
			if = {
				limit = {
					has_province_flag = university_present
				}
				change_variable = { which = art_power_percentage which = university_rank_factor }
			}
			
			if = {
				limit = {
					OR = {
						has_province_modifier = minor_center_of_trade
						has_province_modifier = important_center_of_trade
						has_province_modifier = major_center_of_trade
						has_province_modifier = dominant_center_of_trade
						has_province_modifier = minor_center_of_production
						has_province_modifier = important_center_of_production
						has_province_modifier = major_center_of_production
						has_province_modifier = dominant_center_of_production
					}
				}
				change_variable = { which = art_power_percentage value = 0.10 } #production or trade center always attracts a bit of artits from surrounding
			}
			
			# Art power from court
			
			if = {
				limit = { 
					OR = {
						is_capital = yes
						has_province_flag = has_regional_capital
					}
				}
					if = {
						limit = { owner = { has_country_modifier = court_level_1 } }
						set_variable = { which = art_power_from_court value = 0.5 } change_variable = { which = art_power_percentage value = 0.05 }
					}
					if = {
						limit = { owner = { has_country_modifier = court_level_2 } }
						set_variable = { which = art_power_from_court value = 1 }  change_variable = { which = art_power_percentage value = 0.15 } 
					}
					if = {
						limit = { owner = { has_country_modifier = court_level_3 } }
						set_variable = { which = art_power_from_court value = 2 }  change_variable = { which = art_power_percentage value = 0.30 } # Basic Court
					}
					if = {
						limit = { owner = { has_country_modifier = court_level_4 } }
						set_variable = { which = art_power_from_court value = 2 }  change_variable = { which = art_power_percentage value = 0.45 } 
					}
					if = {
						limit = { owner = { has_country_modifier = court_level_5 } }
						set_variable = { which = art_power_from_court value = 3 }  change_variable = { which = art_power_percentage value = 0.60 } 
					}
					if = {
						limit = { owner = { has_country_modifier = court_level_6 } }
						set_variable = { which = art_power_from_court value = 4 }  change_variable = { which = art_power_percentage value = 0.75 } 
					}
					
					if = {
						limit = { owner = { NOT = { prestige = 0 } } }
						multiply_variable = { which = art_power_from_court value = 0.75 } 
					}
					if = {
						limit = { owner = { prestige = 25 } }
						change_variable = { which = art_power_from_court value = 0.25 }  multiply_variable = { which = art_power_from_court value = 1.10 } 
					}
					if = {
						limit = { owner = { prestige = 50 } }
						change_variable = { which = art_power_from_court value = 0.25 }  multiply_variable = { which = art_power_from_court value = 1.10 } 
					}
					if = {
						limit = { owner = { prestige = 75 } }
						change_variable = { which = art_power_from_court value = 0.25 }  multiply_variable = { which = art_power_from_court value = 1.10 } 
					}
					
					if = { limit = { owner = { has_idea = patron_of_art } }  multiply_variable = { which = art_power_from_court value = 1.25 } } 
					if = { limit = { owner = { has_idea = state_propaganda } }  multiply_variable = { which = art_power_from_court value = 1.25 } }
					
					if = { limit = { has_province_flag = has_regional_capital }  multiply_variable = { which = art_power_from_court value = 0.75 } } #Regional cap is not as good as capital
				}
			
			#Global percentage addition with ideas
			
			if = { limit = { has_province_flag = renaissance_ideas } change_variable = { which = art_power_percentage value = 0.05 } }
			if = { limit = { owner = { has_idea = monuments } }  change_variable = { which = art_power_percentage value = 0.025 } }
			if = { limit = { owner = { has_idea = architecture_studies } }  change_variable = { which = art_power_percentage value = 0.025 } }
			if = { limit = { owner = { has_idea = religious_art } }  change_variable = { which = art_power_percentage value = 0.10 } }
			if = { limit = { owner = { has_idea_group = asceticism_ideas } }  subtract_variable = { which = art_power_percentage value = 0.10 } }
			if = { limit = { owner = { AND = { religion_group = muslim piety = 0 } } }  subtract_variable = { which = art_power_percentage value = 0.25 } }
			
			if = { limit = { owner = { ruler_has_personality = free_thinker_personality } }  multiply_variable = { which = art_power_from_court value = 1.10 } }
			if = { limit = { owner = { ruler_has_personality = architectural_visionary_personality } }  multiply_variable = { which = art_power_from_court value = 1.20 } }
			if = { limit = { owner = { ruler_has_personality = scholar_personality } }  multiply_variable = { which = art_power_from_education value = 1.20 } }
			if = { limit = { owner = { ruler_has_personality = greedy_personality } }  multiply_variable = { which = art_power_from_court value = 0.75 } }
			if = { limit = { owner = { ruler_has_personality = fanatical_priest_personality } }  multiply_variable = { which = art_power_from_court value = 0.5 } change_variable = { which = art_power_percentage value = -0.25 } }
			if = { limit = { owner = { ruler_has_personality = master_theologian_personality } } multiply_variable = { which = art_power_from_education value = 1.20 } }
			if = { limit = { owner = { ruler_has_personality = generous_personality } }  multiply_variable = { which = art_power_from_court value = 1.20 } }
						
			set_variable = { which = art_power value = 0 }
			change_variable = { which = art_power which = art_power_from_court }
			change_variable = { which = art_power which = art_power_from_upper_class }
			change_variable = { which = art_power which = art_power_from_education }
			change_variable = { which = art_power which = art_power_from_buildings }
			
			if = {
				limit = {
					NOT = { check_variable = { which = art_power_percentage value = 0.8 } }
				}
				set_variable = { which = art_power_percentage value = 0.8 }
			}
			
			multiply_variable = { which = art_power which = art_power_percentage }
}

art_center_dist_calc = {
				random_country = {
					limit = {
						owns = 1244
					}
					capital_scope = { save_global_event_target_as = previous_capital_2 }
					
					set_capital = PREV
					
					export_to_variable = {
						which = art_center_dist
						value = capital_distance
						who = event_target:owner_1240
					}
					
					PREV = { set_variable = { which = art_center_dist which = PREV } }
					
					set_capital = event_target:previous_capital_2
					
					clear_global_event_target = previous_capital_2
				}
				
				multiply_variable = { which = art_center_dist value = 0.4 }
				
				if = {
					limit = {
						PREV = { has_province_modifier = important_center_of_art }
					}
					divide_variable = { which = art_center_dist value = 1.5 }
				}
				if = {
					limit = {
						has_province_modifier = median_center_of_art
					}
					divide_variable = { which = art_center_dist value = 1.5 }
				}
				if = {
					limit = {
						has_province_modifier = important_center_of_art
					}
					divide_variable = { which = art_center_dist value = 3 }
				}
				
				if = {
					limit = {
						NOT = { area = PREV }
					}
					multiply_variable = { which = art_center_dist value = 2 }
				}
				if = {
					limit = {
						area = PREV
						
						NOT = { region = PREV }
					}
					multiply_variable = { which = art_center_dist value = 3 }
				}
				if = {
					limit = {
						area = PREV
						region = PREV
						
						NOT = { has_province_flag = part_of_@event_target:sc_food_center }
					}
					multiply_variable = { which = art_center_dist value = 5 }
				}
				if = {
					limit = {
						area = PREV
						region = PREV
						has_province_flag = part_of_@event_target:sc_food_center
						
						NOT = { has_province_flag = part_cont_of_@event_target:cont_food_center }
					}
					multiply_variable = { which = art_center_dist value = 8 }
				}
				
				if = {
					limit = {
						owned_by = PREV
					}
					divide_variable = { which = art_center_dist value = 2 }
				}
				if = {
					limit = {
						OR = {
							religion = PREV
							culture_group = PREV
						}
					}
					divide_variable = { which = art_center_dist value = 1.5 }
				}
}