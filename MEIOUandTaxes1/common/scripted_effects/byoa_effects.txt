census_and_ce_calc_effect = {
	custom_tooltip = calcul_byoa.tt
	#hidden_effect = {
	#	country_event = { id = comm_effic.1 days = 5 random = 15 }
	#}
}

all_byoa_calc_effect = {
	hidden_effect = {
		if = {
			limit = {
				owns = 1244
			}
			country_event = { id = calcul_byoa.1 days = 1 }
			country_event = { id = calcul_byoa.2 days = 10 }
		}
	}
}

indepotent_byoa_calc_effect = {
	custom_tooltip = calcul_byoa.tt2
	hidden_effect = {
		country_event = { id = calcul_byoa.1 days = 1 }
	}
}

assumilation_process_effect = {
	##############################
	# Vassal-Overlord Relationship
	##############################
	every_subject_country = {
		limit = {
			is_subject = yes NOT = { is_subject_of_type = tributary_state }
			check_variable = { which = Years_as_Vassal value = 1 }
		}

		set_variable = { which = Assimilation_Process value = 10 }
		
		# Factors Preventing Integration
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				culture_group = japanese
				government = japanese_monarchy
				NOT = { government_rank = 5 }
			}
			divide_variable = { which = Assimilation_Process value = 0.5 }
		}
			
		# Size of Vassal
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				NOT = { total_development = 51 }
			}
			divide_variable = { which = Assimilation_Process value = 2.5 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 51 NOT = { total_development = 101 }
			}
			divide_variable = { which = Assimilation_Process value = 2.0 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 101 NOT = { total_development = 201 }
			}
			divide_variable = { which = Assimilation_Process value = 1.5 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 201 NOT = { total_development = 301 }
			}
			divide_variable = { which = Assimilation_Process value = 1.2 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 301 NOT = { total_development = 401 }
			}
			divide_variable = { which = Assimilation_Process value = 1.0 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 401 NOT = { total_development = 501 }
			}
			divide_variable = { which = Assimilation_Process value = 0.8 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 501 NOT = { total_development = 601 }
			}
			divide_variable = { which = Assimilation_Process value = 0.6 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				total_development = 601
			}
			divide_variable = { which = Assimilation_Process value = 0.5 }
		}
			
		set_variable = { which = Years_as_Subject which = Assimilation_Process }
		multiply_variable = { which = Years_as_Subject value = 10 }
		# Centralization of Overlord
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 10 } }
			}
			divide_variable = { which = Assimilation_Process value = 0.1 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 20 } absolutism = 10 }
			}
			divide_variable = { which = Assimilation_Process value = 0.5 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 30 } absolutism = 20 }
			}
			divide_variable = { which = Assimilation_Process value = 0.7 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 40 } absolutism = 30 }
			}
			divide_variable = { which = Assimilation_Process value = 0.8 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 50 } absolutism = 40 }
			}
			divide_variable = { which = Assimilation_Process value = 0.9 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 60 } absolutism = 50 }
			}
			divide_variable = { which = Assimilation_Process value = 1.0 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 70 } absolutism = 60 }
			}
			divide_variable = { which = Assimilation_Process value = 1.1 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 80 } absolutism = 70 }
			}
			divide_variable = { which = Assimilation_Process value = 1.2 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 90 } absolutism = 80 }
			}
			divide_variable = { which = Assimilation_Process value = 1.3 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { absolutism = 100 } absolutism = 90 }
			}
			divide_variable = { which = Assimilation_Process value = 1.5 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { absolutism = 100 }
			}
			divide_variable = { which = Assimilation_Process value = 2.0 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				has_country_flag = curtailed_nobility
			}
			divide_variable = { which = Assimilation_Process value = 1.10 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { reverse_has_opinion_modifier = { who = PREV modifier = curtailed_nobility_refused } }
			}
			divide_variable = { which = Assimilation_Process value = 0.80 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { has_active_policy = one_currency_act }
			}
			divide_variable = { which = Assimilation_Process value = 1.10 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { has_active_policy = vassal_integration_act }
			}
			divide_variable = { which = Assimilation_Process value = 1.10 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { has_active_policy = full_sovereignty_act }
			}
			divide_variable = { which = Assimilation_Process value = 1.10 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				has_country_modifier = reduced_tribute_vassal
			}
			divide_variable = { which = Assimilation_Process value = 1.05 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				has_country_modifier = increased_tribute_vassal
			}
			divide_variable = { which = Assimilation_Process value = 0.95 }
		}
		
		# Cultures and Religions
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { primary_culture = PREV }
			}
			divide_variable = { which = Assimilation_Process value = 1.05 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { culture_group = PREV }
			}
			divide_variable = { which = Assimilation_Process value = 1.05 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { religion = PREV } }
			}
			divide_variable = { which = Assimilation_Process value = 0.9 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { NOT = { religion_group = PREV } }
			}
			divide_variable = { which = Assimilation_Process value = 0.5 }
		}
		
		# Overlord's Tech Level
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { adm_tech = 10 }
			}
			divide_variable = { which = Assimilation_Process value = 1.1 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { adm_tech = 20 }
			}
			divide_variable = { which = Assimilation_Process value = 1.1 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { adm_tech = 30 }
			}
			divide_variable = { which = Assimilation_Process value = 1.1 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { adm_tech = 40 }
			}
			divide_variable = { which = Assimilation_Process value = 1.1 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { adm_tech = 50 }
			}
			divide_variable = { which = Assimilation_Process value = 1.1 }
	}
		
		# Overlord's Government
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { government = republic }
			}
			divide_variable = { which = Assimilation_Process value = 0.5 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { government = theocratic_government }
			}
			divide_variable = { which = Assimilation_Process value = 0.2 }
		}
		if = {
			limit = {
				is_subject = yes NOT = { is_subject_of_type = tributary_state }
				overlord = { government = monastic_order_government }
			}
			divide_variable = { which = Assimilation_Process value = 0.3 }
		}
		
		# Overlord's ideas
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = 0.05  } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = 0.10 } } } } divide_variable = { which = Assimilation_Process value = 0.95 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.10 }                                                                                           } } divide_variable = { which = Assimilation_Process value = 0.90 } }
			
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.05 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = 0     } } } } divide_variable = { which = Assimilation_Process value = 1.05 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.10 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.05 } } } } divide_variable = { which = Assimilation_Process value = 1.10 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.15 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.10 } } } } divide_variable = { which = Assimilation_Process value = 1.15 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.20 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.15 } } } } divide_variable = { which = Assimilation_Process value = 1.20 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.25 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.20 } } } } divide_variable = { which = Assimilation_Process value = 1.25 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.30 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.25 } } } } divide_variable = { which = Assimilation_Process value = 1.30 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.35 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.30 } } } } divide_variable = { which = Assimilation_Process value = 1.35 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.40 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.35 } } } } divide_variable = { which = Assimilation_Process value = 1.40 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.45 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.40 } } } } divide_variable = { which = Assimilation_Process value = 1.45 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.50 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.45 } } } } divide_variable = { which = Assimilation_Process value = 1.50 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.55 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.50 } } } } divide_variable = { which = Assimilation_Process value = 1.55 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.60 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.55 } } } } divide_variable = { which = Assimilation_Process value = 1.60 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.65 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.60 } } } } divide_variable = { which = Assimilation_Process value = 1.65 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.70 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.65 } } } } divide_variable = { which = Assimilation_Process value = 1.70 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.75 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.70 } } } } divide_variable = { which = Assimilation_Process value = 1.75 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.80 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.75 } } } } divide_variable = { which = Assimilation_Process value = 1.80 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.85 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.80 } } } } divide_variable = { which = Assimilation_Process value = 1.85 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.90 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.85 } } } } divide_variable = { which = Assimilation_Process value = 1.90 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.95 } NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.90 } } } } divide_variable = { which = Assimilation_Process value = 1.95 } }
		if = { limit = { is_subject = yes NOT = { is_subject_of_type = tributary_state } overlord = {                                                                                  NOT = { has_global_modifier_value = { which = diplomatic_annexation_cost value = -0.95 } } } } divide_variable = { which = Assimilation_Process value = 2.00 } }
		
		# End of Calculations
		multiply_variable = { which = Assimilation_Process value = 10 }
		set_variable = { which = Remaining_Years which = Assimilation_Process }
		subtract_variable = { which = Remaining_Years which = Years_as_Vassal }
		
		# Overlord has become a subject nation
		if = {
			limit = {
				overlord = {
					is_subject = yes NOT = { is_subject_of_type = tributary_state }
					NOT = { has_country_flag = overlord_subject }
				}
			}
			every_country = {
				limit = {
					overlord_of = PREV
				}
				country_event = { id = subject_integration.201 }
			}
		}
		if = {
			limit = {
				#is_free_or_tributary_trigger = yes
				OR = {
					is_subject = no
					is_subject_of_type = tributary_state
				}
				has_country_flag = overlord_subject
			}
			clr_country_flag = overlord_subject
		}
		
		# End of Calculations
		set_variable = { which = Integration_Factor which = Years_as_Vassal }
		multiply_variable = { which = Integration_Factor value = 100 }
		if = {
			limit = {
				is_variable_equal = {
					which = Assimilation_Process
					value = 0
				}
			}

			# log = "<ERROR><AD90DE82><THIS:[This.GetName]><PREV:[Prev.GetName]><ROOT:[Root.GetName]><FROM:[From.GetName]> Division by zero!"

			else = {
				divide_variable = {
					which = Integration_Factor
					which = Assimilation_Process
				}
			}
		}
	}
}

hre_cleanup_effect = {
	HAW = {
		set_in_empire = no
		any_owned_province = { set_in_empire = no }
	}
	TXX = {
		set_in_empire = no
		any_owned_province = { set_in_empire = no }
	}
}
