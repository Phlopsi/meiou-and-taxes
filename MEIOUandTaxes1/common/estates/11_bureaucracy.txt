estate_bureaucracy =
{
	icon = 8

	color = { 200 0 50 }

	# Is estate kept in control of province on conquest?
	keep_on_conquest = no

	# They want to control at least this % of the country's non-overseas development, if # of home provinecs is above set amount
	min_territory = 0
	min_provinces_to_want_territory = 999

	# If true, country will get estate
	trigger = {
		estate_bureaucracy_trigger = yes
	}

	# If true, province can be granted to estate
	province_trigger = {

	}

	# Min autonomy in estate provinces
	min_autonomy = 0

	# Estate cancels out the following multiplicative local autonomy effects:

	base_influence = 0
	influence_modifier = {
		desc = bureaucracy_influence_50
		trigger = {
			has_country_flag = bureaucracy_influence_50
		}
    	influence = 50
	}
	
	influence_modifier = {
		desc = bureaucracy_influence_25
		trigger = {
			has_country_flag = bureaucracy_influence_25
		}
    	influence = 25
	}
	
	influence_modifier = {
		desc = bureaucracy_influence_13
		trigger = {
			has_country_flag = bureaucracy_influence_13
		}
    	influence = 13
	}
	
	influence_modifier = {
		desc = bureaucracy_influence_6
		trigger = {
			has_country_flag = bureaucracy_influence_6
		}
    	influence = 6
	}
	
	influence_modifier = {
		desc = bureaucracy_influence_3
		trigger = {
			has_country_flag = bureaucracy_influence_3
		}
    	influence = 3
	}
	
	influence_modifier = {
		desc = bureaucracy_influence_2
		trigger = {
			has_country_flag = bureaucracy_influence_2
		}
    	influence = 2
	}
	
	influence_modifier = {
		desc = bureaucracy_influence_1
		trigger = {
			has_country_flag = bureaucracy_influence_1
		}
    	influence = 1
	}
	
	
	# Special interactions
	interaction = {
		name = "ASSESS_NATIONAL_STABILITY"
		icon = 20
		years_between_use = 0

		trigger = {

		}

		effect = {
			country_event = {
				id = POP_Estates_Interactions.301
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	interaction = {
		name = "COURT"
		icon = 29
		years_between_use = 0

		trigger = {

		}

		effect = {
			country_event = {
				id = court.100
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
#	interaction = {
#		name = "STATE_CORRUPTION"
#		icon = 6
#		years_between_use = 0
#
#		trigger = {
#
#		}
#
#		effect = {
#			country_event = {
#				id = POP_Estates_Interactions.310
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
	
	interaction = {
		name = "EDUCATION"
		icon = 25
		years_between_use = 0

		trigger = {

		}

		effect = {
			country_event = {
				id = POP_Estates_Interactions.302
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
#	interaction = {
#		name = "THE_COLONIES"
#		icon = 16
#		years_between_use = 0
#
#		trigger = {
#
#		}
#
#		effect = {
#			country_event = {
#				id = POP_Estates_Interactions.308
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}

#	interaction = {
#		name = "NATIONAL_DEMOGRAPHICS"
#		icon = 21
#		years_between_use = 0
#
#		trigger = {
#
#		}
#
#		effect = {
#			country_event = {
#				id = POP_Estates_Interactions.309
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}

	interaction = {
		name = "PLAGUE_MEASURES"
		icon = 24
		years_between_use = 0

		potential = {
			OR = {
				has_global_flag = plague_begun
				AND = {
					has_country_flag = new_world_country
					has_country_flag = new_world_plagues_known
				}
			}
		}

		trigger = {
			OR = {
				has_global_flag = plague_begun
				AND = {
					has_country_flag = new_world_country
					has_country_flag = new_world_plagues_known
				}
			}
		}

		effect = {
			if = { limit = { has_country_flag = new_world_country } 
				hidden_effect = { update_native_plagues_display_information = yes }
				country_event = {
					id = POP_Colonial.400
				}
			}
			if = { limit = { NOT = { has_country_flag = new_world_country } } 
				country_event = {
					id = POP_Estates_Interactions.307
				}
			}
		}
		ai_will_do = {
			factor = 100
			modifier = {
				factor = 0
				OR = { #Unless this is a native country fighting against New World Plagues keep deactivated for now
					has_country_modifier = plague_measures_ai_pause #Also don't let ai spam the decision all the time
					NOT = { has_country_flag = new_world_country }
					NOT = { has_country_flag = new_world_plagues_known }
				}
			}
		}
	}

	interaction = {
		name = "NATIONAL_TIMBER"
		icon = 27
		years_between_use = 0

		potential = {
			num_of_ports = 1
		}
		trigger = {
			num_of_ports = 1
		}

		effect = {
			country_event = {
				id = POP_Estates_Interactions.4000
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	interaction = {
		name = "SEE_TOP_CITY"
		icon = 26
		years_between_use = 0

		effect = {
			country_event = {
				id = POP_Estates_Interactions.5001
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	interaction = {
		name = "Dispatch Runners"
		icon = 28
		years_between_use = 0

		trigger = {
			NOT = { has_global_flag = runner_still_running }
		}

		effect = {
			country_event = {
				id = POP_Travel_Time.006
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
}
