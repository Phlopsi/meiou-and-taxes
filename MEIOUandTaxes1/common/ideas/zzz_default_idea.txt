# This file contains the default idea group in order to make sure it is loaded last.


default_ideas = {
	start = {
		stability_cost_modifier = -0.05
		advisor_cost = -0.1
	}
	
	bonus = {
		global_tax_modifier = 0.10
	}
	
	free = yes
	
	increase_discipline = {
		discipline = 0.025
	}
	increase_taxation = {
		global_tax_modifier = 0.05
	}	
	cheaper_mercenaries = {
		mercenary_cost = -0.10
	}
	increased_trade_power = {
		global_trade_power = 0.05
	}
	increased_manpower = {
		global_manpower_modifier = 0.05
	}
	increased_global_foreign_trade_power = {
		global_foreign_trade_power = 0.05
	}
	increased_production_income = {
		production_efficiency = 0.05
	}
}
