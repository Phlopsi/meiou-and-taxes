parliamentary_reform = {
#Reform to clear rotten boroughs

	category = 4

	allow = {
		any_owned_province = {
			has_province_modifier = rotten_borough
		}
	}
	
	effect = {
		add_stability_1 = yes
		every_owned_province = {
			limit = { has_province_modifier = rotten_borough }
			remove_province_modifier = rotten_borough
		}
	}
	
	modifier = {
		legitimacy = 0.5
		republican_tradition = 0.5
	}

	chance = {
		factor = 1000 #Always available if needed.
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			any_owned_province = {
				has_province_modifier = rotten_borough
			}
		}
	}
}