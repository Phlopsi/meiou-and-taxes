backing_war_effort = {

	category = 1

	allow = {
		is_at_war = yes
	}
	
	effect = {
		add_stability_1 = yes
	}
	
	modifier = {
		war_exhaustion = -0.05
		manpower_recovery_speed = 0.1
	}

	ai_will_do = {
		factor = 1
	}
}