#Clerical Marriage
married_pastors_aspect = { #Protestant, Reformed, Calixtine, Eastern Rite
	effect = { 
		missionaries = 0.25
		}
	ai_will_do = { factor = 0 }
	}
celibate_priests_aspect = { #Gnostic, Catholic
	effect = { 
		prestige = 0.15
		}
	ai_will_do = { factor = 0 }
	}
#Marriage and Divorce
legal_divorce_aspect = { #Protestant
	effect = { }
	ai_will_do = { factor = 0 }
	}
limited_divorce_aspect = { #Reformed, Calixtine, Catholic, Eastern Rite
	effect = { 
		legitimacy = 0.10
		}
	ai_will_do = { factor = 0 }
	}
civil_unions_aspect = { #Gnostic
	effect = { 
		legitimacy = -0.10
		heir_chance = 0.10
		}
	ai_will_do = { factor = 0 }
	}
#Scriptural Interpretation
biblical_inerrancy_aspect = { #Reformed, Calixtine, Reformed (Congregationalist)
	effect = { 
		tolerance_own = 0.25
		tolerance_heretic = -0.25
		}
	ai_will_do = { factor = 0 }
	}
individual_interpretation_aspect = { #Protestant (Lutheran, Methodist), Gnostic
	effect = { 
		tolerance_heretic = 0.25
#		church_power = 10
		}
	ai_will_do = { factor = 0 }
	}
guided_interpretation_aspect = { #Protestant (Anglican), Catholic, Eastern Rite
	effect = { 
		stability_cost_modifier = -0.10
		}
	ai_will_do = { factor = 0 }
	}
#Baptism
infant_baptism_aspect = { #Protestant, Calixtine, Reformed (Calvinist, Congregationalist), Catholic, Eastern Rite
	effect = { } #Default
	ai_will_do = { factor = 0 }
	}
adult_baptism_aspect = { #Reformed (Baptist), Gnostic
	effect = { 
		tolerance_own = 0.25
		global_missionary_strength = -0.0025
		}
	ai_will_do = { factor = 0 }
	}
#Free Will
free_will_aspect = { #Reformed (Baptist, Congregationalist), Protestant, Catholic, Eastern Rite, Calixtine (Lollard), Gnostic*
	effect = { } #Default
	ai_will_do = { factor = 0 }
	}
predestination_aspect = { #Reformed (Calvinist, Presbyterian), Calixtine (Taborite)
	effect = { 
		tolerance_own = 0.25
		global_missionary_strength = -0.0025
		}
	ai_will_do = { factor = 0 }
	}
#Ordination of Clergy
appointed_bishops_aspect = { #Protestant, Catholic, Eastern Rite
	effect = { 
		}
	ai_will_do = { factor = 0 }
	}
elected_ministers_aspect = { #Reformed (Presbyterian, Congregationalist, sort of Calvinist)
	effect = { 
		global_missionary_strength = 0.0025
		stability_cost_modifier = 0.05
		}
	ai_will_do = { factor = 0 }
	}
lay_priesthood_aspect = { #Gnostic, Calixtine
	effect = { 
		missionaries = 0.25
		stability_cost_modifier = 0.10
		}
	ai_will_do = { factor = 0 }
	}
#Membership Requirements
enforced_membership_aspect = { #Protestant (Lutheran, Anglican), Reformed (Calvinist), Catholic, Eastern Rite, Calixtine*, Gnostic*
	effect = { } #Default
	ai_will_do = { factor = 0 }
	}
free_membership_aspect = { #Protestant (Methodist), Reformed (Baptist, Presbyterian, Congregationalist, Quaker)
	effect = { 
		global_missionary_strength = -0.0025
		tolerance_own = 0.25
		}
	ai_will_do = { factor = 0 }
	}
