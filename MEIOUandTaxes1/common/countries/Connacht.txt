# Country name see file name.

graphical_culture = westerngfx

color = { 97  126  37 }

historical_idea_groups = {
	naval_ideas
	administrative_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	logistic_ideas
	spy_ideas
	#exploration_ideas
	innovativeness_ideas
	quality_ideas
}

historical_units = { #Celtic group
	western_spearman_infantry
	western_hobelar_cavalry
	western_longbow_infantry
	unique_ENG_yeoman_infantry
	western_mountedarcher_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_charge_infantry
	unique_SCO_galloglaigh_infantry
	western_freeshooter_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_irregular_infantry
	western_gallop_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_platoon_infantry
	western_bayonet_infantry
	western_carabinier_cavalry
	western_rifled_infantry
	western_hunter_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}
	
leader_names = {
	FitzGerald Butler Burke Dempsey "D'Arcy" FitzPatrick Joyce
}

monarch_names = {
	"Turlough #2" = 80
	"Conchobhar #2" = 60
	"Tadhg #2" = 40
	"Donnchad #7" = 20
	"Muireadhach #1" = 20
	"Diarmaid #3" = 1
	"Mathghamhain #0" = 20
	"Ádhamh #0" = 1
	"Ailill #0" = 1
	"Brígh #0" = 1
	"Brónach #0" = 1
	"Cadwgwan #0" = 1
	"Caoimhín #0" = 1
	"Cinnéidigh #0" = 1
	"Deaglán #0" = 1
	"Dougal #0" = 1
	"Fionnlagh #0" = 1
	"Garbhán #0" = 1
	"Lachtna #0" = 1
	"Lorccán #0" = 1
	"Mainchín #0" = 1
	"Mathúin #0" = 1
	"Meallán #0" = 1
	"Naomhán #0" = 1
	"Niall #0" = 1
	"Odhrán #0" = 1
	"Pádraig #0" = 1
	"Quinn #0" = 1
	"Riain #0" = 1
	"Ríoghnán #0" = 1
	"Rórdan #0" = 1
	"Ruaidhrí #0" = 1
	"Ruarc #0" = 1
	"Séaghdhá #0" = 1
	"Seanán #0" = 1
	"Sechnall #0" = 1
	"Tighearnach #0" = 1
	"Tuathal #0" = 1
	"Uilliam #0" = 1
	"Uinseann #0" = 1
	
	"Aifric #0" = -1
	"Fionola #0" = -1
	"Raghnailt #0" = -1
	"Saraid #0" = -1
	"Órfhlaith #0" = -1
	"Fedelma #0" = -1
	"Neassa #0" = -1
	"Rosheen #0" = -1
	"Siofra #0" = -1
	"Ceridwen #0" = -1
	"Elain #0" = -1
	"Gwenyth #0" = -1
	"Megan #0" = -1
}

ship_names = {
	Athlone
	Ballina
	Ballinrobe
	Ballyhaunis
	Boyle
	Castlebar
	Galway
	Connemara
	Mayo
	Roscommon	
}
