# Rebels who took over two provinces during the Sengoku

graphical_culture = asiangfx

color = { 211  220  178 }

historical_idea_groups = {
	logistic_ideas
	diplomatic_ideas
	popular_religion_ideas
	trade_ideas
	administrative_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
}

#Japanese group
historical_units = {
	asian_warrior_monk_infantry
	asian_horse_archer_cavalry
	asian_bushi_cavalry
	asian_shashu_no_ashigaru_infantry
	asian_samurai_cavalry
	asian_samurai_infantry
	asian_yarigumi_infantry
	asian_late_samurai_cavalry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_horse_guard_cavalry
	asian_new_guard_infantry
	asian_volley_infantry
	asian_armeblanche_cavalry
	asian_bayonet_infantry
	asian_lighthussar_cavalry
	asian_drill_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
	}

monarch_names = {
	"Kakunyo #1" = 50 
	"Zonkaku #1" = 50
	"Rennyo #1" = 50 #8th abbot
	"Jitsunyo #0" = 50 #9th abbot
	"Kennyo #0" = 50 #11th abbot
	"Kyonyo #0" = 40 #12th and 22nd abbot
	"Junnyo #0" = 50
	"Shonyo #0" = 50
	"Sennyo #0" = 50
	"Zennyo #1" = 50
	"Koson #0" = 50
	
	"Ako #0" = -1
	"Asahi #0" = -1
	"Aya #0" = -1
	"Harukiri #0" = -1
	"Inuwaka #0" = -1
	"Itoito #0" = -1
	"Itsuitsu #0" = -1
	"Koneneme #0" = -1
	"Mitsu #0" = -1
	"Narime #0" = -1
	"Sakami #0" = -1
	"Shiro #0" = -1
	"Tatsuko #0" = -1
	"Tomiko #0" = -1
	"Toyome #0" = -1
	"Yamabukime #0" = -1
	}

leader_names = {
	Akamatsu Akechi Ashikaga Amago Kyogoku Rokkaku Asakura Asano Aso Chosokabe Date Hachisuka
	Hatakeyama Hojo Hosokawa Ichijo Keda Imagawa Isshiki Ito Jinbo Kikuchi Kono Maeda
	Matsunaga Mimura Miyoshi Mogami Mori Nabeshima Nagao Oda Ogasawara Otomo Ouchi
	Ryuzoji Sagara Saito Sanada Satake Satomi Shiba Shibata Shimazu Shoni Takeda Toki
	Tokugawa Matsudaira Toyotomi Tsutsui Uesugi Uragami Yamana Yamauchi
}

ship_names = {
	"Mutsu Maru" "Dewa Maru" "Shimotsuke Maru"
	"Hitachi Maru"
	"Kazusa Maru" "Shimousa Maru" "Musashi Maru" "Kozuke Maru" "Izu Maru" "Echigo Maru" "Ettchu Maru"
	"Kaga Maru" "Noto Maru" "Suruga Maru" "Totomi Maru" "Joshu Maru" "Enshu Maru" "Bushu Maru" "Esshu Maru"
	"Mikawa Maru" "Ushu Maru" "Soushu Maru" "Boushu Maru" "Kai Maru" "Shinshu Maru" "Koshu Maru"
	"Owari Maru" "Bishu Maru" "Mino Maru" "Echizen Maru" "Ise Maru" "Shima Maru" "Omi Maru" 
	"Goushu Maru" "Yamashiro Maru" "Wakasa Maru" "Tanba Maru" "Tango Maru" "Yamato Maru" "Kii Maru"
	"Kishu Maru" "Iga Maru" "Awa Maru" "Sanuki Maru" "Tosa Maru"
	"Iyo Maru" "Harima Maru" "Banshu Maru"
	"Hoki Maru" "Inaba Maru" "Bizen Maru" "Mimasaka Maru" "Binshu Maru"
	"Bingo Maru" "Bittchu Maru" "Aki Maru" "Izumo Maru" "Iwami Maru" "Suou Maru" "Nagato Maru" "Bungo Maru"
	"Buzen Maru" "Chikuzen Maru"
	"Chikugo Maru" "Hizen Maru" "Higo Maru" "Chikushu Maru" "Hyuga Maru" "Osumi Maru" "Satsuma Maru" "Sasshu Maru"
}

army_names = {
	"$PROVINCE$ Ikki"
}

fleet_names = {
	"$PROVINCE$ Suigun" "$PROVINCE$ Sendan" "$PROVINCE$ Tai"
}
