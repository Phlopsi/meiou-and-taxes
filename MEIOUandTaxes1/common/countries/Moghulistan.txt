#ChagataiKhanate.txt
#2011-jun-30 FB	resequenced historical_idea_groups as per DW June 29

graphical_culture = muslimgfx

color = { 37  112  49 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = { #Indian Sultanate group
	indian_spearmen_infantry
	indian_arab_cavalry
	indian_bow_infantry
	indian_mamluk_cavalry
	indian_jagirdar_infantry
	indian_rajput_camelry
	indian_dakhili_infantry
	indian_kshatriya_cavalry
	indian_banduqchi_infantry
	indian_zamindar_cavalry
	indian_sepoy_musketeer_infantry
	indian_musketeer_elephantry
	indian_matchlockmen_infantry
	indian_heavy_cavalry
	indian_rocketeer_infantry
	indian_pindari_cavalry
	indian_westernized_infantry
	indian_uhlan_cavalry
	indian_mass_infantry
	indian_lighthussar_cavalry
	indian_drill_infantry
	indian_hunter_cavalry
	indian_columnar_infantry
	indian_lancer_cavalry
	indian_breech_infantry
}

monarch_names = {
	"Nusrat #0" = 20
	"Sayyid Khidr #0" = 20
	"Daulat Khan #0" = 20
	"Khidr Khan #0" = 20
	"Mubarrak Shah #1" = 30
	"Mohammed Shah #3" = 40
	"Aladdin Alam Shah #0" = 10
	"Bahl�l #0" = 20
	"Saikander #0" = 20
	"Ibrahim #1" = 20
	"Sher #0" = 20
	"Islam Shah #0" = 20
	"Shams-I-Jahan #0" = 20
	"Muhammad #0" = 20
	"Naqsh-I-Jahan #0" = 20
	"Awais #0" = 20
	"Shir Muhammad #0" = 20
	"Esen Buqa #0" = 20
	"Y�suf #0" = 20
	"Mahmud #0" = 20
	"Mans�r #0" = 20
	"'Abd ar-Rash�d #0" = 20
	"'Abd al-Kar�m #0" = 20
	"'Abd al-Lat�f #0" = 20
	"'Abd All�h #0" = 20
	"Ilbars #0" = 20
	"Ism�'�l #0" = 20
	"Muhammad Am�n #0" = 20
	"Galdan #0" = 20
	"Chagatai #1" = 10
	"Alghu #1" = 20
	"Mubarak #1" = 20
	"Yes� #0" = 20
	"Qara #0" = 20
	"Baraq #0" = 20
	"Neg�bei #0" = 20
	
	"Jahanara #0" = -1
	"Raushan #0" = -1
	"Sirhindi  #0" = -1
	"Zinat #0" = -1
	"Fakr al-Nisa #0" = -1
	"Nur Bai #0" = -1
}

leader_names = {
	Altanqai Boketei Tegusteni Qadancher Tolui-gene
	Bayardai Yekedai "Suren Chinua" "Altan Unegen"
	"Boke Qorchi" "Arigh Erdene" "Bayan Vachir" "Delger Dash"
}

ship_names = {
	"Alghu Khan"
	"Baraq Khan"
	Chagatai
	Orghina
	"Genghis Khan"
	"Mubarak Shah"
	"Yes�nto'a"
	Qipchaq
	"Jagatai Khan" "Amo Darya"
	"Syr Darya" "�gedai Khan"
	G�y�k "M�ngke Khan" "Qubilai Khan"
	Duwa "Qazan Khan" "Tughlugh Timur"
	Irtysh "Amir Husayn" "Timur i Leng"
	Samarqand
}
