#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 99  16  27 }

historical_idea_groups = {
	administrative_ideas
	popular_religion_ideas
	trade_ideas
	quantity_ideas
	leadership_ideas
	merchant_marine_ideas
	economic_ideas
	logistic_ideas
}

historical_units = { #Manchu group
	asian_mixed_crossbow_infantry
	asian_fire_lance_cavalry
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_handgunner_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_wagon_infantry
	asian_dragoon_cavalry
	asian_charge_infantry
	asian_regular_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_hunter_cavalry
	asian_impulse_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Bombogor #0" = 30
	"Guiguar #0" = 30
	"Bardaci #0" = 30
	"Mersentei #0" = 30
	"Hailanca #0" = 30
	"Dolongga #0" = 30
	"Degulai #0" = 30
	"Gantimur #0" = 30
	"Bukuri Yongson #0" = 5
	"Dudu Mengtemu #0" = 5
	"Cungsan #0" = 5
	"Tolo #0" = 5
	"Sibeoci Fiyanggu #0" = 5	
	"Fuman #0" = 5
	"Giocangga #0" = 5
	"Taksi #0" = 5
	"Dorgon #0" = 5
	"Hooge #0" = 5
	"Fulin #0" = 5	
	"Fanca #0" = 5
	"Hoogle #0" = 5
	"Yinzhi #0" = 5
	"Yinreng #0" = 5
	"Yinti #0" = 5
	"Daisan #0" = 5
	"Abatai #0" = 5
	"Ajige #0" = 5
	"Dodo #0" = 5
	"Yinxiang #0" = 5
	"Yinsi #0" = 5
	"Yintang #0" = 5
	"Hongshi #0" = 5
	"Hongzhou #0" = 5
	"Yongqi #0" = 5
	"Ahadan #0" = 1
	"Aihana #0" = 1
	"Aisingga #0" = 1
	"Ajida #0" = 1
	"Adali #0" = 1
	"Ajigen #0" = 1
	"Akdun #0" = 1
	"Alan #0" = 1
	"Alin #0" = 1
	"Anabu #0" = 1
	"Anaku #0" = 1
	"Arsun #0" = 1
	"Arsalan #0" = 1
	"Ashai #0" = 1
	"Asu #0" = 1
	"Ayanda #0" = 1
	"Badai #0" = 1
	"Baha #0" = 1
	"Bahabu #0" = 1
	"Bahai #0" = 1
	"Baise #0" = 1
	"Bajar #0" = 1
	"Baksan #0" = 1
	"Balu #0" = 1
	"Baran #0" = 1
	"Basan #0" = 1
	"Bohoto #0" = 1	
	"Boji #0" = 1
	"Bootai #0" = 1
	"Boro #0" = 1
	"Buka #0" = 1
	"Busuku #0" = 1
	"Buyali Mergen #0" = 1
	"Buyandai Efu #0" = 1
	"Cagan #0" = 1
	"Caida #0" = 1
	"Canggadai #0" = 1
	"Carki #0" = 1
	"Ceke #0" = 1
	"Centai #0" = 1
	"Cihala #0" = 1
	"Colhon #0" = 1
	"Coki #0" = 1
	"Coohar #0" = 1
	"Cukuaturu #0" = 1
	"Dai #0" = 1
	"Daihan #0" = 1
	"Dalan #0" = 1
	"Dari #0" = 1
	"Dasu #0" = 1
	"Defu #0" = 1
	"Doha #0" = 1
	"Doholon #0" = 1
	"Doola #0" = 1
	"Duici #0" = 1
	"Dulba #0" = 1
	"Dulei #0" = 1
	"Dungga #0" = 1
	"Ebilun #0" = 1
	"Elbihe #0" = 1
	"Faka #0" = 1
	"Fakjin #0" = 1
	"Fangge #0" = 1
	"Fatha #0" = 1
	"Fatan #0" = 1
	"Fiyada #0" = 1
	"Fiyangga #0" = 1
	"Fiyahan #0" = 1
	"Fiyasha #0" = 1
	"Gabula #0" = 1
	"Gabula Baturu #0" = 1
	"Gadahun #0" = 1
	"Gaha #0" = 1
	"Gajitai #0" = 1
	"Ganjuhan #0" = 1
	"Garin #0" = 1
	"Gasha #0" = 1
	"Gebungge #0" = 1
	"Giohoto #0" = 1
	"Gisun #0" = 1
	"Goho #0" = 1
	"Gohon #0" = 1
	"Goko #0" = 1
	"Gungge #0" = 1
	"Hada #0" = 1
	"Haidu #0" = 1
	"Halhan #0" = 1
	"Hari #0" = 1
	"Hasiri #0" = 1
	"Hatan #0" = 1
	"Hecen #0" = 1
	"Hedu #0" = 1
	"Hele #0" = 1
	"Hethe #0" = 1
	"Hetu #0" = 1
	"Heye #0" = 1
	"Hida #0" = 1
	"Hife #0" = 1
	"Hife Baks #0" = 1
	"Hirgen #0" = 1
	"Hiribu #0" = 1
	"Hohori #0" = 1 
	"Hohori Efu #0" = 1
	"Hokton #0" = 1
	"Holo #0" = 1
	"Homin #0" = 1
	"Hontoho #0" = 1
	"Horontai #0" = 1
	"Hose #0" = 1
	"Hoto #0" = 1
	"Huhu #0" = 1
	"Huju #0" = 1
	"Hutu #0" = 1
	"Huba #0" = 1
	"Huban #0" = 1
	"Hulan #0" = 1
	"Huri #0" = 1
	"Huse #0" = 1
	"Ibahan #0" = 1
	"Icangga #0" = 1
	"Icengge #0" = 1
	"Ilha #0" = 1
	"Ilibu #0" = 1
	"Indahun #0" = 1
	"Isan #0" = 1
	"Isangga #0" = 1
	"Isha #0" = 1
	"Isu #0" = 1
	"Itu #0" = 1
	"Jingji #0" = 1
	"Jiratai #0" = 1
	"Jolo #0" = 1
	"Jorho #0" = 1
	"Jortai #0" = 1
	"Juce #0" = 1
	"Jucengge Baturu #0" = 1
	"Jumara #0" = 1
	"Juntai #0" = 1
	"Kamtu #0" = 1
	"Kacilan #0" = 1
	"Kara #0" = 1
	"Kenje #0" = 1
	"Kesi #0" = 1
	"Kesibu #0" = 1
	"Kilin #0" = 1
	"Kuri #0" = 1
	"Labi #0" = 1
	"Ladu #0" = 1
	"Lahari #0" = 1
	"Laju #0" = 1
	"Lamun #0" = 1
	"Lenggeri #0" = 1
	"Lobi #0" = 1
	"Lomi #0" = 1
	"Lunggu #0" = 1
	"Mahu #0" = 1
	"Maikan #0" = 1
	"Maise #0" = 1
	"Maitu #0" = 1
	"Majan #0" = 1
	"Mala #0" = 1
	"Malukan #0" = 1
	"Malahi #0" = 1
	"Manggai #0" = 1
	"Malu #0" = 1
	"Manggiya #0" = 1
	"Masan #0" = 1
	"Megu #0" = 1
	"Minggan #0" = 1
	"Minggatu #0" = 1
	"Monggo #0" = 1
	"Mucen #0" = 1
	"Muheliyen #0" = 1
	"Muhu #0" = 1
	"Mujan #0" = 1
	"Mulu #0" = 1
	"Murca #0" = 1
	"Muri #0" = 1
	"Musen #0" = 1
	"Musi #0" = 1
	"Namu #0" = 1
	"Nacin #0" = 1
	"Nikan #0" = 1
	"Nere #0" = 1
	"Nikan Baturu #0" = 1
	"Nionio #0" = 1
	"Nisiha #0" = 1
	"Niyada #0" = 1
	"Musu #0" = 1
	"Ocir #0" = 1
	"Ofono #0" = 1
	"Oha #0" = 1
	"Olbo #0" = 1
	"Okson #0" = 1
	"Olon #0" = 1
	"Orhodai #0" = 1
	"Ongoro #0" = 1
	"Ordo #0" = 1
	"Sabdangga #0" = 1
	"Sabi #0" = 1
	"Sadun #0" = 1
	"Saha #0" = 1
	"Sahada #0" = 1
	"Sahaliyan #0" = 1
	"Sahana #0" = 1
	"Saibigan #0" = 1
	"Saise #0" = 1
	"Samha #0" = 1
	"Sanggabu #0" = 1
	"Sehe #0" = 1
	"Sele #0" = 1
	"Selmin #0" = 1
	"Singgeri #0" = 1
	"Sijintai #0" = 1
	"Sintai #0" = 1
	"Silan #0" = 1
	"Siteku #0" = 1
	"Solho #0" = 1
	"Solon #0" = 1
	"Songgotu Baturu #0" = 1
	"Sonin Baksi #0" = 1
	"Sonin Baturu #0" = 1
	"Suburi #0" = 1
	"Sube #0" = 1
	"Sudan #0" = 1
	"Suhe #0" = 1
	"Suhecen #0" = 1
	"Suiha #0" = 1
	"Suilan #0" = 1
	"Songkoro Baturu #0" = 1
	"Sunggina #0" = 1
	"Sunta #0" = 1
	"Suya #0" = 1
	"Sahun #0" = 1
	"Sari #0" = 1
	"Sunjaci #0" = 1
	"Taktu Baturu #0" = 1
	"Talman #0" = 1
	"Tangguha #0" = 1
	"Tantu #0" = 1
	"To #0" = 1
	"Toro #0" = 1
	"Tubihe #0" = 1
	"Tulhun #0" = 1
	"Tumen #0" = 1
	"Tura #0" = 1
	"Turi #0" = 1
	"Tusa #0" = 1
	"Wahun #0" = 1
	"Wajima #0" = 1
	"Wase #0" = 1
	"Wehe #0" = 1	
	"Weihe #0" = 1
	"Ukada #0" = 1
	"Uku #0" = 1
	"Ukuri #0" = 1
	"Ule #0" = 1
	"Ulgiyaci #0" = 1
	"Ulhu #0" = 1
	"Uli #0" = 1
	"Umudu #0" = 1
	"Unca #0" = 1
	"Unduri #0" = 1
	"Usun #0" = 1
	"USaku #0" = 1
	"Yabu #0" = 1
	"Yala #0" = 1
	"Yaki #0" = 1
	"Yasha #0" = 1
	"Yentai #0" = 1
	"Yodan #0" = 1
	"Yolo #0" = 1
	"Yoro #0" = 1
	"Yuwamboo #0" = 1
	
	"Hejing #0" = -5
	"Hejia #0" = -5
	"Hexiao #0" = -5
	"Zhuangjing #0" = -5
	"Fen #0" = -1
	"Jian #0" = -1
	"Wen #0" = -1
}

leader_names = {
	Amura "Ayan gioro" Asuri "Aha gioro" Akjan Aisin 
	Borjigin Bayot Bayara Boheri Be Bugirgen Besut Biru Buyaci Bohori Busai
	Bulca Busa Buhi Bira Botis Borjik Baige Besu Bari
	Carala Cakcin Cygiya Cemgun Cui 
	Daigiya Donggo Dunggiya Dagiya Dorgun Dedure Dukta Duktari
	Elge Esuri Ertut Fuca Foimo Fiya Fujuri
	Guwalgiya Gioro Giorca "Gioro Hala" Giorcan Gejile Guwalca Gorlo "Giranggi Warka"
	Gerungioro Gin Gioroi Gorben Gocir Giri Gour Gijile Guwanggiyara
	Heseri Hitara Hunggoco Hunggo Hoiho Hurhasu Hasara Hulhu "Huk Sahari" Hunggiri
	Hinggiyan Hushari Hinggiya Han Hurha Hunglo Hoggo Huya Huilo
	Menu Ilara Itari "Iork Ule" Iokuri "Irgen Gioro" "Irgen Baturu" Ituma Ira
	Joogiya Jalari Jakuta Janggiya Jangci Jerde Jathe "Jash Uri" Jebe
	Jugiya Jusu Jinggeri Jaruca Julgen Juge Juru Jot Jodomu 
	Ki Kerde Kuyala Kocili Karla Kardasu Kubulu
	Ligiya Laibu Ledi Lumburi Lei "Lak Ule" Lubui
	Magiya Monggori Mu Murcan Monggo Meng Menggoro Murca 
	Muljire Manja Morci Monggoro Muyan
	Niohuru Nara Ningguta Nimaca Namduru Nikiri Nisa Niowanggiya Noyan
	Nahata Nimaha Nata Naya Namdolu Omoto Oji
	Sumuru Sakda "Sirin gioro" Sioheri Sartu Semira Sikteri Saca Samargi Sakdaholo 
	Sanggiya Sitara Sunggiya Sugiya Samara Suigiya Sikteli Sakca Socara Sahajiri
	Socor Saimire Sorgi Sogiya Sorgiya Sigiya Saimire Surtu Seheri Sadara Suru Sikda
	Tatara Tunggiya Tajagiorca Tokoro Tsanggiya Tomo Tooli Taicuru Tuseri Tomu Tuktan 
	Tunggi Tumen Tolote Taicuru Untahe Ujala Urgucan Unuhucin Usin Ulu Usu Uya Undu
	Urguca Ujaku Unggiot Uncehen Unggirgin Ujaku Urda Ukuri Ulingga Uksun Usujan Usila Use
	Wanggiya Weikutu Wei Weigute Were Wase Wangjab Wali Yonoto Yanja Yanggiya Yeher Yanji
	Yukuri Yunde Yur Yasu
}

ship_names = {
	Jurgen
	Heseri
	Niohuru
	Fuca
	Hun
	Jecen
	Donggo
	Neyen
	Jusheri
	Wanggiya
	Ilan
	Huye
}

army_names = {
	"$PROVINCE$ Jalan" 
}