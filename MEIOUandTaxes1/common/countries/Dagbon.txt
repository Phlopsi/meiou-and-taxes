#Country Name: Please see filename.

graphical_culture = africangfx

color = { 171 160 31 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	leadership_ideas
	spy_ideas
	diplomatic_ideas
	economic_ideas
	administrative_ideas
	quantity_ideas
}

historical_units = { #Pastoralists and Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Sapilem #0" = 40
	"Nyambre #0" = 20
	"Salma #0" = 20
	"Korongo #0" = 20
	"Koom #0" = 20
	"Yamba Sorgo #0" = 20
	"Tigre #0" = 20
	"Dulugu #0" = 20
	"Sawadogo #0" = 20
	"Karfo #0" = 20
	"Baongo #0" = 20
	"Kutu #0" = 20
	"Sanum #0" = 20
	"Wobgo #0" = 20
	"Mazi #0" = 20
	"Sigiri #0" = 20
	"Saaga #0" = 20
	"Kugri #0" = 20
	"Yadega #0" = 20
	"Yolomfaogona #0" = 20
	"Kourita #0" = 20
	"Geda #0" = 20
	"Wobgho #0" = 20
	"Kango #0" = 20
	"Kaongo #0" = 20
	"Tuguri #0" = 1
	"Korogo #0" = 1
	"Ragongo #0" = 1
	"Nyambe Moogo #0" = 1
	"Totebaldbo #0" = 1
	"Yemde #0" = 1
	"Woboga #0" = 1
	"Piiyo #0" = 1
	"Bulli #0" = 1
	"Wedraogo #0" = 1
	"Ligidi #0" = 1
	"Kobga #0" = 1
	"Gigma #0" = 1
	"Dramani #0" = 1
	
	"Adaeze #0" = -1
	"Chidi #0" = -1
	"Poko #0" = -1
	"Kenga #0" = -1
}

leader_names = {
	Addy
	Afrifa
	Sadami
	Sabah
	Zakari
	Offei
	Donkor
	Akoto
	Sribor
	Moyoyo
}

ship_names = {
	Nyame "Asase Yaa" Bia Tano
	Dwoada Benada Wukuada Yawoada
	Fiada Memeneda Kwasiada
}
