# Khalkha.txt
# Tag : KHA
# Tech : steppestech
#2011-jun-30 FB	resequenced historical_idea_groups

graphical_culture = asiangfx

color = { 108  153  65 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Dayan #0" = 20
	"Darayisung #0" = 20
	"Mulon #0" = 20
	"Manduulun #0" = 20
	"Sayn #0" = 20
	"Bodi #0" = 20
	"Buyan #0" = 20
	"Ligdan #0" = 20
	"Tayisung #0" = 20
	"Adai #0" = 20
	"Oyiradai #0" = 20
	"Delbeg #0" = 20
	"�ljei #0" = 20
	"G�n #0" = 20
	"�r�g #0" = 15
	"Arigaba #1" = 10
	"Ayurparibhadra #1" = 10
	"Biligt� #1" = 10
	"Elbeg #1" = 10
	"G�y�k #1" = 10
	"Irinchibal #1" = 10
	"Jorightu #1" = 10
	"Kublai #1" = 10
	"M�ngke #1" = 10
	"�gedei #1" = 10
	"Qayshan #1" = 10
	"Qoshila #1" = 10
	"Suddhipala #1" = 10
	"Temujin #1" = 10
	"Tem�r #1" = 10
	"Toghan #1" = 10
	"Tolui #1" = 10
	"Toq #1" = 10
	"Uskhal #1" = 10
	"Yes�n #1" = 10
	"Ariq #0" = 1
	"Batu #0" = 1
	"Eljigidei #0" = 1
	"Joichi #0" = 1

	"Heying" = -1
	"Chunmei" = -1
	"Tianhui" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
}

leader_names = {
	Orsolgene Chagan-cher "Tegus Burilgi" "Quara Temur" "Arigh Arslan"
	"Batu Unegen" "Koke Gal" Nasantai Ulaganqan Qyugungge Maidari
	Delgergene Subeqai
}

ship_names = {
	"Ariq Boke"
	Chengzong
	G�y�k
	Hoelun Hulagu
	Kublai Kiyad K�l�g
	Mongke
	Olkunut �gedei
	Qabul Qaidu
	"Sorghaghtani Beki"
	Tolui
	Yesugei
}
