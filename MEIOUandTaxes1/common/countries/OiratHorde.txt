# OiratHorde.txt
# Tag : OIR
# Tech : steppestech
#2011-jun-30 FB	resequenced historical_idea_groups

graphical_culture = asiangfx

color = { 204  184  177 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Batula #1" = 40
	"Toghan #1" = 40
	"Galdan #0" = 40
	"Amursana #0" = 20
	"Erdeni #0" = 20
	"Choghtu #0" = 20
	"Dawa #0" = 20
	"Esen #1" = 20
	"G�shi #0" = 20
	"Lha-bzang #0" = 20
	"Khara Khula #0" = 20
	"Kho #0" = 20
	"Sengge #0" = 20
	"Tseten #0" = 20
	"Tsewang #0" = 20
	"Kundelung #0" = 15
	"Lobzang #0" = 15
	"Sutai #0" = 15
	"Tsobasa #0" = 15
	"Ubasi #0" = 15
	"Quduqa #0" = 10
	"Toghto #0" = 10
	"Altai #0" = 1
	"B�ki #0" = 1
	"Bayan #0" = 1
	"Dayan #0" = 1
	"Hami #0" = 1
	"Molon #0" = 1
	"Oori #0" = 1
	"Orl�k #0" = 1
	"Sechen #0" = 1
	"Radi #0" = 1
	"Talman #0" = 1
	"Todo #0" = 1
	"Tumen #0" = 1
	"Tumu #0" = 1
	"Ubashi #0" = 1
	"Ulan #0" = 1
	"Wehe #0" = 1
	"Yelu #0" = 1
	"Zaya #0" = 1

	"Heying" = -1
	"Chunmei" = -1
	"Tianhui" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
}

leader_names = {
	Jasray
	Gonbat
	Hattori
	Olzvoi
	Bira
	Batmunkh
	Ganbold
	Saihan
	Ochirbal
	Davaa
	Choros
	Torghud
	Khoid
	Dorbed
}

ship_names = {
	Bargut Buzav Kerait Naiman "D�rvn ��rd"
	Khoshut Olo Dzungar Torgut Dorbot
	Amdo "Altan Khan" Khoshot Ol�ts G�shi
}
