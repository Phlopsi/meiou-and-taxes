#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 191  110  62 }

historical_idea_groups = {
	logistic_ideas
	diplomatic_ideas
	popular_religion_ideas
	trade_ideas
	administrative_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
}

#Tibetan group
historical_units = {
	asian_warrior_monk_infantry
	asian_horse_archer_cavalry
	#complete guesses follow
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_repeating_crossbow_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_banner_infantry
	asian_forbidden_soldier_cavalry
	asian_new_guard_infantry
	asian_platoon_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_lighthussar_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Changchub Gyaltsen #1" = 20
	"Shakya Gyaltsen #1" = 20
	"Drakpa Changchub #1" = 20
	"Sonam Drakpa #1" = 20
	"Drakpa Gyaltsen #1" = 20
	"Drakpa Jungne #1" = 20
	"Kunga Lekpa #1" = 20
	"Ngagi Wangpo #1" = 20
	"Tashi Drakpa #0" = 20
	"Drowai Gonpo #0" = 20
	"Drakpa Gyaltsen #0" = 20
	"Nampar Gyalwa #0" = 20
	"Namgyal Palzang #0" = 20
	"Lozang Gyatso #0" = 20
	"Yeshey Gyatso #0" = 20
	"Kelzang Gyatso #0" = 20
	"Palden Yeshe #0" = 20
	"Jamphel Gyatso #0" = 20
	"Choekyi Nyima #0" = 20
	"Lungtok Gyatso #0" = 20
	"Tenpai Nyima #0" = 20
	"Tsultrim Gyatso #0" = 20
	"Gedun Drub #1" = 20
	"Gedun Gyatso #1" = 20
	"Sonam Gyatso #1" = 20
	"Yonten Gyatso #1" = 20
	"Tseten #0" = 1
	"Thutob Namgyal #0" = 1
	"Lhawang Dorje #0" = 1
	"Tensung #0" = 1
	"Phuntsok Namgyal #0" = 1
	"Tenkyong #0" = 1
	"Norzang #0" = 1
	"Kunzang #0" = 1
	"Donyo Dorje #0" = 1
	"Ngawang Namgyal #0" = 1
	"Tseten Dorje #0" = 1
	"Jigme Drakpa #0" = 1

	"Damo Nyetuma #0" = -1
	"Kunga Pemo #0" = -1
	"Kunga Lhanzi #0" = -1
	"Dondrub Dolma #0" = -1
	"Namkha Kyi #0" = -1
	"Kamala #0" = -1
	"Indira #0" = -1
	"Hoelun #0" = -1
	"Tamülün #0" = -1
	"Sorghaghtani #0" = -1
}

leader_names = {
	Rinpungpa
	Tsangpa
	Phagmodrupa
	Kharpa
	Khoshut
	Sakya
	Lhasa
	Shigatse
	Gyantse
	Qamdo
	Shiquanhe
	Nagchu
	Bamda
	Rutog
	Nyingchi
	Nedong
	Coqen
	Barkam
	Sakya
	Gartse
	Pelbar
	Lhatse
	Tingri
}

ship_names = {
	Barkam
	Dartsendo
	Gyantse Gartse
	Lhasa Lhotse
	Makalu 
	Nagqu Nyingchi Nedong
	Pelbar
	Qamdo
	Shigatse Sakya
	Tinggri
}
