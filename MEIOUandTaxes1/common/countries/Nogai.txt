# Nogai.txt
# Tag : NOG
# Tech : steppestech
#2011-jun-30 FB	resequenced historical_idea_groups

graphical_culture = muslimgfx

color = { 144  181  106 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Yadigai #0" = 10
	"'Isa Beg #0" = 10
	"Mansur #0" = 10
	"Kai Qubad #0" = 10
	"Nur #0" = 10
	"Mustafa #0" = 10
	"Osman #0" = 10
	"Yamguchi #0" = 10
	"Musa Mirza #0" = 10
	"Shigai #0" = 10
	"Seydyak #0" = 10
	"Shig-Mamai #0" = 10
	"Ishey #0" = 10
	"Yusuf #0" = 10
        "Il Murza #0" = 10
        "Isma'il #0" = 10
        "Yunus #0" = 10
        "Tin-Ahmad #0" = 10
        "Mirza Urus #0" = 10
        "Yeni Bey #0" = 10
        "Ishterek #0" = 10
        "Yan Arslan #0" = 10
        "Alba Murza #0" = 10
        "Kana Bey Mirza #0" = 10
        "Djan Mambet Bey #0" = 10

	"Aybanu #0" = -1
	"Alsou #0" = -1
	"Arslanbika #0" = -1
	"Alfia #0" = -1
	"Aida #0" = -1
	"Aziz #0" = -1
	"Agdal #0" = -1
	"Bakir #0" = -1
	"Vazih #0" = -1
	"Walia #0" = -1
	"Gaysha #0" = -1
	"Gulnara #0" = -1
	"Zia #0" = -1
	"Zifa #0" = -1
	"Zakaria #0" = -1
	"Zamira #0" = -1
	"Zainulla #0" = -1
	"Irada #0" = -1
	"Islamia #0" = -1
	"Katib #0" = -1
	"Kirama #0" = -1
	"Lyabiba #0" = -1
	"Lutfulla #0" = -1
	"Maysara #0" = -1
	"Nailya #0" = -1
	"Nasima #0" = -1
	"Rushaniya #0" = -1
	"Sadiq #0" = -1
	"Salih #0" = -1
	"Fazil #0" = -1
	"Farida #0" = -1
	"Hayat #0" = -1
	"Habibullah #0" = -1
	"Shamil #0" = -1
	"Elvira #0" = -1
	"Elmira #0" = -1
	"Yuldus #0" = -1

}

leader_names = {
	"Ulagan Temur" "Bora Chinua" "Suren Unegen" "Qara Erdene"
	"Delger Cheren" Chaganchar Qorchigene Burilgidai Qadantai
	Temurcher Tegus-chi Berkegene Yekechar Batu-tu
}

ship_names = {
	"Kara Nojaj" Edig� Saraycik Yayik
	"Chinggis Khan" "Batu Khan" "Tartar Khan"
	"M�ngke Timur" "Waqqas Bej" Chaka Buri
	"Qara Quchuq" Mangyt
}
