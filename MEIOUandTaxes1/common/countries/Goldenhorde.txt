# GoldenHorde.txt
# Tag : GOL
# Tech : steppestech
#2011-jun-30 FB	resequenced historical_idea_groups as per DW June 29

graphical_culture = muslimgfx

color = { 201  209  177 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Temur Qutlugh #0" = 20
	"Shadi Beg #0" = 20
	"Pulad Khan #0" = 20
	"Tem�r #0" = 20
	"Jalal Al-Din #0" = 20
	"Karim Berdi #0" = 20
	"Kebek #0" = 20
	"Kuchuk Muhammad #0" = 40
	"Mahmud #0" = 80
	"Ahmad #0" = 50
	"Sayid Ahmad #0" = 60
	"Baraq #0" = 30
	"Devlat Berdi #0" = 20
	"Ulug Muhammad #0" = 40
	"Jabbar Berdi #0" = 20

	"Aybanu #0" = -1
	"Alsou #0" = -1
	"Arslanbika #0" = -1
	"Alfia #0" = -1
	"Aida #0" = -1
	"Aziz #0" = -1
	"Agdal #0" = -1
	"Bakir #0" = -1
	"Vazih #0" = -1
	"Walia #0" = -1
	"Gaysha #0" = -1
	"Gulnara #0" = -1
	"Zia #0" = -1
	"Zifa #0" = -1
	"Zakaria #0" = -1
	"Zamira #0" = -1
	"Zainulla #0" = -1
	"Irada #0" = -1
	"Islamia #0" = -1
	"Katib #0" = -1
	"Kirama #0" = -1
	"Lyabiba #0" = -1
	"Lutfulla #0" = -1
	"Maysara #0" = -1
	"Nailya #0" = -1
	"Nasima #0" = -1
	"Rushaniya #0" = -1
	"Sadiq #0" = -1
	"Salih #0" = -1
	"Fazil #0" = -1
	"Farida #0" = -1
	"Hayat #0" = -1
	"Habibullah #0" = -1
	"Shamil #0" = -1
	"Elvira #0" = -1
	"Elmira #0" = -1
	"Yuldus #0" = -1
}

leader_names = {
	"Boke Temur" "Qutlugh Vachir" "Suren Chinua" "Delger Bayar"
	"Shria Gan" "Bora Chinua" Nasange Temur-chi Chinuagene Toluilun
	Qutughqan "Koke Burilgi" Surenchar Bayangge
}

ship_names = {
	Batu Sartaq Ulaghchi Berke "Mengu-Timur" "Tuda-Mengu"
	Talabuga "Nogai Khan" Tokhta "�z Beg" "Tini Beg" "Jani Beg"
	"Berdi Beg" Qulpa "Nawruz Beg" Khidr "Timur Khwaja" Abdallah
	"Muhammad Bolak" "Mamai Khan" Toqtamish "Tem�r Qutlugh" Ediguh
	"Shadi Beg" "Polad Khan" Tem�r "Jalal ad-Din" "Karim Berdi" Kebek
	"Jabbar Berdi" "Ulugh Muhammad" "Dawlat Berdi" Baraq "Sayud Ahmad"
}
