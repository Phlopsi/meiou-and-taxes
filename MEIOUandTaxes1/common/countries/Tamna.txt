#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 108  180  78 }

historical_idea_groups = {
	logistic_ideas
	leadership_ideas
	naval_ideas
	trade_ideas
	spy_ideas
	diplomatic_ideas
	economic_ideas
	quality_ideas
}

#Korean group
historical_units = {
	asian_tobang_infantry
	asian_singi_cavalry
	asian_elite_horse_cavalry
	asian_fire_arrow_rocket_infantry
	asian_yangban_armored_cavalry
	asian_five_commands_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	unique_KOR_soko_reserve_infantry
	asian_samsubyong_infantry
	asian_forbidden_soldier_cavalry
	unique_KOR_new_five_corps_infantry
	asian_regular_infantry
	asian_carabinier_cavalry
	asian_line_infantry
	asian_lighthussar_cavalry
	asian_drill_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Malo #0" = 100
	"Yu #0" = 100
	"Jogi #0" = 100
	"Jeongik #0" = 100
	"Jeok #0" = 100
	"Yeorim #0" = 100
	"Jeonggan #0" = 100
	"Boksu #0" = 100
	"Indan #0" = 100
	"Sujwa #0" = 100
	"Seok #0" = 100
	"Sunryang #0" = 100
	"Sunwon #0" = 100
	"Myeonggeol #0" = 100
	"Singeol #0" = 100
	"Bongrye #0" = 100
	
	"Seohui #0" = -1
	"Sehui #0" = -1
	"Yerin #0" = -1
	"Minhui #0" = -1
	"Sena #0" = -1
	"Sohui #0" = -1
	"Jeonghui #0" = -1
	"Hana #0" = -1
	"Yena #0" = -1
	"Mina #0" = -1
	"Gonghwe #0" = -1
	"Jeonghyeon #0" = -1
	"Inseon #0" = -1
	"Jeongsun #0" = -1
	"Bangja #0" = -1
	"Harin #0" = -1
	"Yongwoo #0" = -1
	"Yeongmi #0" = -1
	"Chaerin #0" = -1
	"Seonhwa #0" = -1
	"Yeonyi #0" = -1
	"Choseon #0" = -1
	"Seonmi #0" = -1
	"Monseon #0" = -1
	"Okjeong #0" = -1
	"Jayeong #0" = -1
	"Yeojeong #0" = -1
	"Soyeon #0" = -1
	"Hye #0" = -1
	"Ryeong #0" = -1
	"Un #0" = -1
	"Rin #0" = -1
	"Ran #0" = -1
	"Eun #0" = -1
	"Cheong #0" = -1
}

leader_names = {
	# Tamna Flavor
	Go Go Go Go Go Go Go Go Go Go
	Yang Yang Yang Yang Yang Bu Bu Bu Bu Bu
	Kim Yi Pak Choi Jeong Ga Gye Go Ra Noh Dam Do Du
	Ryu Ban Ma Mun Seo Son Song Sin Sim An Yang O Yun
	Jang Chung Jeon Cho Chin Han Ha Hong Heo
}

ship_names = {
	Baekdu Baekje 
	Duman Daedeok Dongmyeongseong
	Gayasan Geumgang Geumho Goguryeo "Gyeon Hwon"
	Jirisan Jeongjong
	Kumgangsan
	Nakdong
	Sincheon Silla Sobaek
	Taebaek Taejo 
	Hanseong Haemosu Habaek Hwaeomsa
	Worak
	Yuhwa Yeong 
}
