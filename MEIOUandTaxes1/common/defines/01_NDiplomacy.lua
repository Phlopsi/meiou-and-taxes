
NDefines.NDiplomacy.UNCONDITIONAL_SURRENDER_MONTHS = 1					-- Months before unconditional surrender starts having an effect on Call for Peace. Set to negative values to disable feature.
NDefines.NDiplomacy.DISHONORABLE_PEACE_MONTHS = 36						-- See DISHONORABLE_PEACE_WARSCORE. Set to 0 to entirely disable the feature.
NDefines.NDiplomacy.DISHONORABLE_PEACE_WARSCORE = -20					-- If you have more than this amount of individual warscore, peacing out within DISHONORABLE_PEACE_MONTHS of war start counts as a dishonorable act and incurs a CALL_ALLY_DECLINE_PRESTIGE_PENALTY hit.
NDefines.NDiplomacy.PREPARE_FOR_WAR_COST = 5							-- Cost in favors to ask AI to prepare for war

NDefines.NDiplomacy.FAVOR_GAIN_FOR_HELP = 30							-- Amount of favors gained for helping allies in wars (based on war contribution relative to their power)
NDefines.NDiplomacy.TRUST_PENALTY_FOR_SEPARATE_PEACE = 20				-- Trust penalty for signing a separate peace


NDefines.NDiplomacy.ALLOW_LEADER_DEMAND_TOGGLE = 1						-- Whether or not player is allowed to set if warleader can negotiate for them
NDefines.NDiplomacy.VASSALIZE_BASE_DEVELOPMENT_CAP = 500				-- Countries with more total base tax than this cannot be vassalized
NDefines.NDiplomacy.PEACE_IMPACT_ADM_SCORE = 0.1
NDefines.NDiplomacy.PEACE_IMPACT_DIP_SCORE = 0.1
NDefines.NDiplomacy.PEACE_IMPACT_MIL_SCORE = 0.1
NDefines.NDiplomacy.AUTONOMY_WARSCORE_COST_MODIFIER = 0.40				-- How much autonomy reduces score by (at 1, 50% autonomy = 50% reduction)	
NDefines.NDiplomacy.MAX_PEACE_TREATY_COST = 500							-- in diplo power
NDefines.NDiplomacy.MAX_FREE_CITIES = 15
NDefines.NDiplomacy.HRE_PRINCE_AUTHORITY_THRESHOLD = 80					-- Threshold below which you lose IA, and above which you gain it
NDefines.NDiplomacy.END_OF_CRUSADES = 1870 								-- End of Crusade/Excommunicate actions. AI might also befriend old religious enemies.		# DEI GRATIA CHANGED
NDefines.NDiplomacy.WE_IMPACT_ON_ANNEX_INTEGRATE = -0.075				-- multiplied with current WE
NDefines.NDiplomacy.TRUCE_YEARS = 5 									-- _DDEF_TRUCE_YEARS_; Years of Truce
NDefines.NDiplomacy.SCALED_TRUCE_YEARS = 10								-- Additional years of truce based on % of warscore taken in war (100% warscore = full scaled truce years) { was 0.7 }
NDefines.NDiplomacy.MONARCH_GOV_CHANGE_LEGITIMACY_PENALTY = 0.25		-- Penalty(%) on the legitimacy when changing gov type to the monarchy

NDefines.NDiplomacy.DETECTED_SPY_NETWORK_DAMAGE_MIN = 20
NDefines.NDiplomacy.DETECTED_SPY_NETWORK_DAMAGE_MAX = 70
NDefines.NDiplomacy.SUPPORT_REBELS_EFFECT = 10
NDefines.NDiplomacy.SUPPORT_REBELS_MONEY_FACTOR = 0.5					
NDefines.NDiplomacy.FABRICATE_CLAIM_COST = 40
NDefines.NDiplomacy.CORRUPT_OFFICIALS_COST = 50
NDefines.NDiplomacy.OVEREXTENSION_THRESHOLD = 50.0 						-- at which threshold you can get events

NDefines.NDiplomacy.AE_OTHER_CONTINENT = 0
NDefines.NDiplomacy.AE_DIFFERENT_RELIGION = -0.6
NDefines.NDiplomacy.AE_HRE_INTERNAL = 1.25
NDefines.NDiplomacy.AE_ATTACKER_DEVELOPMENT = 0.07						-- +70% cap (at 1000 development)
NDefines.NDiplomacy.AE_DEFENDER_DEVELOPMENT = 0.05						-- -50% cap (at 1000 development)

NDefines.NDiplomacy.AE_DISTANCE_BASE = 1.20
NDefines.NDiplomacy.AE_PROVINCE_CAP = 35								-- Province development above this will not count for AE

NDefines.NDiplomacy.DIP_PORT_FEES = 0.1									-- DIP_PORT_FEES
NDefines.NDiplomacy.CLAIM_PEACE_COST_DIP_FRACTION = -0.25				-- Fraction of dipcost you pay for cores/claims
NDefines.NDiplomacy.CORE_PEACE_COST_DIP_FRACTION = -0.5					-- Fraction of dipcost you pay for cores
NDefines.NDiplomacy.MONTHS_BEFORE_TOTAL_OCCUPATION = 24					-- Before this many months have passed in the war you cannot gain 100% warscore by just occupying the warleader

NDefines.NDiplomacy.PO_DEMAND_PROVINCES_AE = 0.75							-- _DDEF_PO_DEMAND_PROVINCES_AE = 10, (Per development)  (was 0.55)
NDefines.NDiplomacy.PO_RETURN_CORES_AE = 0.5		 					-- (Per core, only applied if returning cores to vassals of winner)
NDefines.NDiplomacy.PO_FORM_PU_AE = 0.4									-- _DDEF_PO_FORM_PU_AE = 10, (Per development) (were 0.3)
NDefines.NDiplomacy.PO_BECOME_VASSAL_AE = 0.5		 					-- _DDEF_PO_BECOME_VASSAL_AE = 10, (Per development)
NDefines.NDiplomacy.PO_TRANSFER_VASSAL_AE = 0.3 						-- _DDEF_PO_BECOME_VASSAL_AE = 10, (Per development) (was 0.25)

NDefines.NDiplomacy.MIN_INCOME_FOR_SUBSIDIES = 10                 		-- Minimum monthly income for AI to want to spend some on subsidies
NDefines.NDiplomacy.PEACE_COST_DEMAND_PROVINCE = 2						-- Demand a province (scales by province wealth, also used for annex)
NDefines.NDiplomacy.PEACE_COST_CONCEDE_PROVINCE = 1.5					-- Demand colonial area province concession.
NDefines.NDiplomacy.PEACE_COST_BECOME_VASSAL = 1						-- Vassalize a country (scales by province wealth) { was 0.7 }
NDefines.NDiplomacy.PEACE_COST_RETURN_CORE = 1.5							-- Return a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_REVOKE_CORE = 1							-- Revoke a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_RELEASE_ANNEXED = 1.5					-- Release annexed nation (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_RELEASE_VASSAL = 1						-- Release vassal (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_CONVERSION = 0.3							-- scaled with countrysize for forced conversion in peace.
NDefines.NDiplomacy.PEACE_COST_CONCEDE = 5 								-- _DDEF_PEACE_COST_CONCEDE_ Base Peace cost for conceding defeat
NDefines.NDiplomacy.PEACE_COST_DEMAND_NON_OCCUPIED_PROVINCE_MULT = 1.25
NDefines.NDiplomacy.PEACE_COST_DEMAND_CAPITAL_MULT = 1.5				-- { was 1.10 }
NDefines.NDiplomacy.INTEGRATE_UNION_MIN_YEARS = 10
NDefines.NDiplomacy.INTEGRATE_VASSAL_MIN_YEARS = 10
NDefines.NDiplomacy.AGITATE_FOR_LIBERTY_DESIRE = 15						-- Liberty Desire gained due to ongoing agitation.
NDefines.NDiplomacy.AGITATE_FOR_LIBERTY_RATE = 0.5						-- Monthly rate at which Liberty Desire rises towards the maximum during agitation, or otherwise falls towards zero.
NDefines.NDiplomacy.ANNEX_DIP_COST_PER_DEVELOPMENT = 7					-- per development { was 6 }

NDefines.NDiplomacy.MAX_PEACE_TREATY_AE = 100							-- 
NDefines.NDiplomacy.DAYS_TO_DECLARE_WAR = 380							-- Days from start of game before you can DOW anyone

NDefines.NDiplomacy.HRE_VOTE_ENEMY = -200
NDefines.NDiplomacy.HRE_VOTE_LEGUE_ENEMY = -200
NDefines.NDiplomacy.HRE_VOTE_LEAGUE_LEADER = 100
NDefines.NDiplomacy.HRE_VOTE_LEAGUE_LEADER_FRIEND = 200
NDefines.NDiplomacy.HRE_VOTE_HERETIC = -25
NDefines.NDiplomacy.HRE_VOTE_OVERLORD = 50
NDefines.NDiplomacy.HRE_VOTE_VASSAL_ELECTOR = -50
NDefines.NDiplomacy.HRE_VOTE_TOO_SMALL = -50
NDefines.NDiplomacy.HRE_VOTE_BIG_COUNTRY = 50
NDefines.NDiplomacy.HRE_VOTE_VERY_BIG_COUNTRY = 100
NDefines.NDiplomacy.HRE_VOTE_NON_MEMBER = -75
NDefines.NDiplomacy.HRE_VOTE_SAME_CULTURE_GROUP = 5
NDefines.NDiplomacy.HRE_VOTE_ALLIANCE = 30
NDefines.NDiplomacy.HRE_VOTE_ROYAL_MARRIAGE = 10
NDefines.NDiplomacy.HRE_VOTE_CORE_CLAIM = -50

NDefines.NAI.DIPLOMATIC_ACTION_OFFER_CONDOTTIERI_ONLY_NEIGHBORS = 0
