##############################################
#
# rebel_types.txt
#
##############################################
#
# Modified by Gigau
#
# * Changes in MEIOU
#		new rebel types:-
#			fanatics
#			hussites
#		2009-12-18 FB	HT3 changes
#		2010-12-18 FB	DW changes
#		2013-08-17 GG	EUIV changes
#
##############################################
# The five rebel types used by the game engine
##############################################
#
# Valid arguments:
#
# area = [nation/culture/nation_rebel_tag/nation_religion/religion/all]
# government = [monarchy/republic/theocracy/anti/any]
# defection = [nation_rebel_tag/culture/culture_group/religion/any/none]
# independence = [nation_rebel_tag/culture/culture_group/religion/colonial/any/none]
# gfx_type = [culture_province/culture_owner]
#
##############################################
#
# All rebels in province scope
#
# For siege_won_trigger, siege_won_effect
# FROM = owner
# ROOT = province
#
# For Other
# FROM = owner
# ROOT = Homecountry
#