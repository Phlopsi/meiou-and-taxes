##############################################
# Parliamentarian army.
##############################################
parliamentarian_rebels = {

	color = { 120 120 195 }
	area = nation 				# Stays within the nation
	government = republic
	defection = none				# Does not defect
	independence = none				# Does not strive for independence
	unit_transfer = yes
	gfx_type = culture_province	
	
	resilient = yes
	reinforcing = yes
	general = yes
	smart = yes
	
	artillery = 0.1
	infantry = 0.6
	cavalry = 0.3
	
	morale = 1.1
	
	# Possible handle actions
	handle_action_negotiate = no
	handle_action_stability = yes
	handle_action_build_core = yes
	handle_action_send_missionary = yes		
	
	# The rebel type with the highest modifier for this province gets picked
	spawn_chance = {
		factor = 1
		modifier = {
			factor = 0
			has_global_flag = siecle_des_lumieres
		}
		modifier = {
			factor = 2
			is_year = 1700
		}
		modifier = {
			factor = 2
			is_year = 1740
		}
		modifier = {
			factor = 2
			is_year = 1780
		}
		modifier = {
			factor = 0.05
			is_overseas = yes
		}
		modifier = {
			factor = 0.25
			NOT = { is_year = 1600 }
		}
		modifier = {
			factor = 5.0
			owner = {
				has_country_flag = liberalism
			}
		}
		modifier = {
			factor = 5.0
			owner = { has_country_flag = revolution }
		}		
		
		modifier = {
			factor = 0.25
			owner = {
				government = republic
			}
		}
		modifier = {
			factor = 0.1
			owner = {
				NOT = { technology_group = western }
				NOT = { technology_group = eastern }
			}
		}
		modifier = { 
			has_province_modifier = revolutionaries_organizing
			factor = 1.4
		}
	}
 
 	# This is checked for EACH province in the Area of Operations
	movement_evaluation = {
		factor = 1
		modifier = {
			factor = 0.1
			units_in_province = 1
		}
		modifier = {
			factor = 0.001
			controlled_by = REB
		}
		modifier = {
			factor = 1.5
			unrest = 2
		}
		modifier = {
			factor = 1.5
			unrest = 4
		}
		modifier = {
			factor = 1.5
			unrest = 6
		}
	}
 
 	# Province Scope
 	siege_won_trigger = {
 	}
	siege_won_effect = {
	}
	
	# Country scope
	can_negotiate_trigger = {
		always = no
	}
	
	# Country scope
	can_enforce_trigger = {
		always = yes
	}
	
	# Localisation for their demands
	demands_description = "revolutionary_rebels_demand"
	
	# Country Scope	
	demands_enforced_effect = {
		add_prestige = -50
		if = {
			limit = {
				NOT = {
					OR = {
						government = papal_government
						government = steppe_horde
						government = celestial_empire
						government = chinese_monarchy
						government = chinese_monarchy_2
						government = chinese_monarchy_3
						government = chinese_monarchy_4
						government = chinese_monarchy_5
						government = iqta
						government = colonial_government
						government = native_council
					}
				}
			}
			change_government = REB # Special for this effect. Picks preferred rebel government.
		}
		change_religion = REB
		force_converted = yes
		define_ruler = {
			rebel = yes
		}
	}
}