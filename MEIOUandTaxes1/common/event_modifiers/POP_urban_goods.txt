# URBAN TRADE GOODS

# Rank 1 
### Mundane Urban Goods ###
mundane_urban_goods_1 = { 
	trade_value = 0.2
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_2 = { 
	trade_value = 0.4
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_3 = { 
	trade_value = 0.6
	picture = urban_goods_mundane_goods	
}

mundane_urban_goods_4 = { 
	trade_value = 0.8
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_5 = { 
	trade_value = 1
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_6 = { 
	trade_value = 1.2
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_7 = { 
	trade_value = 1.4
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_8 = { 
	trade_value = 1.6
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_9 = { 
	trade_value = 1.8
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_10 = { 
	trade_value = 2
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_12 = { 
	trade_value = 2.4
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_14 = { 
	trade_value = 2.8
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_16 = { 
	trade_value = 3.2
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_18 = { 
	trade_value = 3.6
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_20 = { 
	trade_value = 4
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_22.5 = { 
	trade_value = 4.5
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_25 = { 
	trade_value = 5
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_27.5 = { 
	trade_value = 5.5
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_30 = { 
	trade_value = 6
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_35 = { 
	trade_value = 7
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_40 = { 
	trade_value = 8
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_50 = { 
	trade_value = 10
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_60 = { 
	trade_value = 12
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_70 = { 
	trade_value = 14
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_80 = { 
	trade_value = 16
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_100 = { 
	trade_value = 20
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_120 = { 
	trade_value = 24
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_140 = { 
	trade_value = 28
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_160 = { 
	trade_value = 32
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_180 = { 
	trade_value = 36
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_200 = { 
	trade_value = 40
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_1_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_2_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_3_display = { 
	picture = urban_goods_mundane_goods	
}

mundane_urban_goods_4_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_5_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_6_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_7_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_8_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_9_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_10_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_12_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_14_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_16_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_18_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_20_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_22.5_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_25_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_27.5_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_30_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_35_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_40_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_50_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_60_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_70_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_80_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_100_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_120_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_140_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_160_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_180_display = { 
	picture = urban_goods_mundane_goods
}

mundane_urban_goods_200_display = { 
	picture = urban_goods_mundane_goods
}



# RANK 2

### Linen ###
urban_goods_linen = { ### Price 3
	picture = urban_goods_linen
}

linen_1 = { 
	trade_value = 0.25
	picture = urban_goods_linen
}

linen_2 = { 
	trade_value = 0.5
	picture = urban_goods_linen
}

linen_3 = { 
	trade_value = 0.75
	picture = urban_goods_linen
}

linen_4 = { 
	trade_value = 1
	picture = urban_goods_linen
}

linen_5 = { 
	trade_value = 1.25
	picture = urban_goods_linen
}

linen_6 = { 
	trade_value = 1.5
	picture = urban_goods_linen
}

linen_7 = { 
	trade_value = 1.75
	picture = urban_goods_linen
}

linen_8 = { 
	trade_value = 2
	picture = urban_goods_linen
}

linen_9 = { 
	trade_value = 2.25
	picture = urban_goods_linen
}

linen_10 = { 
	trade_value = 2.5
	picture = urban_goods_linen
}

linen_12 = { 
	trade_value = 3
	picture = urban_goods_linen
}

linen_14 = { 
	trade_value = 3.5
	picture = urban_goods_linen
}

linen_16 = { 
	trade_value = 4
	picture = urban_goods_linen
}

linen_18 = { 
	trade_value = 4.5
	picture = urban_goods_linen
}

linen_20 = { 
	trade_value = 5
	picture = urban_goods_linen
}

linen_22.5 = { 
	trade_value = 5.625
	picture = urban_goods_linen
}

linen_25 = { 
	trade_value = 6.25
	picture = urban_goods_linen
}

linen_27.5 = { 
	trade_value = 6.875
	picture = urban_goods_linen
}

linen_30 = { 
	trade_value = 7.5
	picture = urban_goods_linen
}

linen_35 = { 
	trade_value = 8.75
	picture = urban_goods_linen
}

linen_40 = { 
	trade_value = 10
	picture = urban_goods_linen
}

linen_50 = { 
	trade_value = 12.5
	picture = urban_goods_linen
}

linen_60 = { 
	trade_value = 15
	picture = urban_goods_linen
}

linen_70 = { 
	trade_value = 17.5
	picture = urban_goods_linen
}

linen_80 = { 
	trade_value = 20
	picture = urban_goods_linen
}

linen_100 = { 
	trade_value = 25
	picture = urban_goods_linen
}

linen_120 = { 
	trade_value = 30
	picture = urban_goods_linen
}

linen_140 = { 
	trade_value = 35
	picture = urban_goods_linen
}

linen_160 = { 
	trade_value = 40
	picture = urban_goods_linen
}

linen_180 = { 
	trade_value = 45
	picture = urban_goods_linen
}

linen_200 = { 
	trade_value = 50
	picture = urban_goods_linen
}


urban_goods_metalwork = { ### Price 3.5
	picture = urban_goods_hardware
}

### Metalwork ###
metalwork_1 = { 
	trade_value = 0.25
	picture = urban_goods_metalwork
}

metalwork_2 = { 
	trade_value = 0.5
	picture = urban_goods_metalwork
}

metalwork_3 = { 
	trade_value = 0.75
	picture = urban_goods_metalwork
}

metalwork_4 = { 
	trade_value = 1
	picture = urban_goods_metalwork
}

metalwork_5 = { 
	trade_value = 1.25
	picture = urban_goods_metalwork
}

metalwork_6 = { 
	trade_value = 1.5
	picture = urban_goods_metalwork
}

metalwork_7 = { 
	trade_value = 1.75
	picture = urban_goods_metalwork
}

metalwork_8 = { 
	trade_value = 2
	picture = urban_goods_metalwork
}

metalwork_9 = { 
	trade_value = 2.25
	picture = urban_goods_metalwork
}

metalwork_10 = { 
	trade_value = 2.5
	picture = urban_goods_metalwork
}

metalwork_12 = { 
	trade_value = 3
	picture = urban_goods_metalwork
}

metalwork_14 = { 
	trade_value = 3.5
	picture = urban_goods_metalwork
}

metalwork_16 = { 
	trade_value = 4
	picture = urban_goods_metalwork
}

metalwork_18 = { 
	trade_value = 4.5
	picture = urban_goods_metalwork
}

metalwork_20 = { 
	trade_value = 5
	picture = urban_goods_metalwork
}

metalwork_22.5 = { 
	trade_value = 5.625
	picture = urban_goods_metalwork
}

metalwork_25 = { 
	trade_value = 6.25
	picture = urban_goods_metalwork
}

metalwork_27.5 = { 
	trade_value = 6.875
	picture = urban_goods_metalwork
}

metalwork_30 = { 
	trade_value = 7.5
	picture = urban_goods_metalwork
}

metalwork_35 = { 
	trade_value = 8.75
	picture = urban_goods_metalwork
}

metalwork_40 = { 
	trade_value = 10
	picture = urban_goods_metalwork
}

metalwork_50 = { 
	trade_value = 12.5
	picture = urban_goods_metalwork
}

metalwork_60 = { 
	trade_value = 15
	picture = urban_goods_metalwork
}

metalwork_70 = { 
	trade_value = 17.5
	picture = urban_goods_metalwork
}

metalwork_80 = { 
	trade_value = 20
	picture = urban_goods_metalwork
}

metalwork_100 = { 
	trade_value = 25
	picture = urban_goods_metalwork
}

metalwork_120 = { 
	trade_value = 30
	picture = urban_goods_metalwork
}

metalwork_140 = { 
	trade_value = 35
	picture = urban_goods_metalwork
}

metalwork_160 = { 
	trade_value = 40
	picture = urban_goods_metalwork
}

metalwork_180 = { 
	trade_value = 45
	picture = urban_goods_metalwork
}

metalwork_200 = { 
	trade_value = 50
	picture = urban_goods_metalwork
}

### Cloth ###
urban_goods_cloth = { ### Price 3.5
	picture = urban_goods_cloth
}

cloth_1 = { 
	trade_value = 0.25
	picture = urban_goods_cloth
}

cloth_2 = { 
	trade_value = 0.5
	picture = urban_goods_cloth
}

cloth_3 = { 
	trade_value = 0.75
	picture = urban_goods_cloth
}

cloth_4 = { 
	trade_value = 1
	picture = urban_goods_cloth
}

cloth_5 = { 
	trade_value = 1.25
	picture = urban_goods_cloth
}

cloth_6 = { 
	trade_value = 1.5
	picture = urban_goods_cloth
}

cloth_7 = { 
	trade_value = 1.75
	picture = urban_goods_cloth
}

cloth_8 = { 
	trade_value = 2
	picture = urban_goods_cloth
}

cloth_9 = { 
	trade_value = 2.25
	picture = urban_goods_cloth
}

cloth_10 = { 
	trade_value = 2.5
	picture = urban_goods_cloth
}

cloth_12 = { 
	trade_value = 3
	picture = urban_goods_cloth
}

cloth_14 = { 
	trade_value = 3.5
	picture = urban_goods_cloth
}

cloth_16 = { 
	trade_value = 4
	picture = urban_goods_cloth
}

cloth_18 = { 
	trade_value = 4.5
	picture = urban_goods_cloth
}

cloth_20 = { 
	trade_value = 5
	picture = urban_goods_cloth
}

cloth_22.5 = { 
	trade_value = 5.625
	picture = urban_goods_cloth
}

cloth_25 = { 
	trade_value = 6.25
	picture = urban_goods_cloth
}

cloth_27.5 = { 
	trade_value = 6.875
	picture = urban_goods_cloth
}

cloth_30 = { 
	trade_value = 7.5
	picture = urban_goods_cloth
}

cloth_35 = { 
	trade_value = 8.75
	picture = urban_goods_cloth
}

cloth_40 = { 
	trade_value = 10
	picture = urban_goods_cloth
}

cloth_50 = { 
	trade_value = 12.5
	picture = urban_goods_cloth
}

cloth_60 = { 
	trade_value = 15
	picture = urban_goods_cloth
}

cloth_70 = { 
	trade_value = 17.5
	picture = urban_goods_cloth
}

cloth_80 = { 
	trade_value = 20
	picture = urban_goods_cloth
}

cloth_100 = { 
	trade_value = 25
	picture = urban_goods_cloth
}

cloth_120 = { 
	trade_value = 30
	picture = urban_goods_cloth
}

cloth_140 = { 
	trade_value = 35
	picture = urban_goods_cloth
}

cloth_160 = { 
	trade_value = 40
	picture = urban_goods_cloth
}

cloth_180 = { 
	trade_value = 45
	picture = urban_goods_cloth
}

cloth_200 = { 
	trade_value = 50
	picture = urban_goods_cloth
}
# RANK 3

urban_goods_glassware = { ### Price 4
	picture = urban_goods_glassware
}

glassware_1 = { 
	trade_value = 0.3
	picture = urban_goods_glassware
}

glassware_2 = { 
	trade_value = 0.6
	picture = urban_goods_glassware
}

glassware_3 = { 
	trade_value = 0.9
	picture = urban_goods_glassware
}

glassware_4 = { 
	trade_value = 1.2
	picture = urban_goods_glassware
}

glassware_5 = { 
	trade_value = 1.5
	picture = urban_goods_glassware
}

glassware_6 = { 
	trade_value = 1.8
	picture = urban_goods_glassware
}

glassware_7 = { 
	trade_value = 2.1
	picture = urban_goods_glassware
}

glassware_8 = { 
	trade_value = 2.4
	picture = urban_goods_glassware
}

glassware_9 = { 
	trade_value = 2.7
	picture = urban_goods_glassware
}

glassware_10 = { 
	trade_value = 3
	picture = urban_goods_glassware
}

glassware_12 = { 
	trade_value = 3.6
	picture = urban_goods_glassware
}

glassware_14 = { 
	trade_value = 4.2
	picture = urban_goods_glassware
}

glassware_16 = { 
	trade_value = 4.8
	picture = urban_goods_glassware
}

glassware_18 = { 
	trade_value = 5.4
	picture = urban_goods_glassware
}

glassware_20 = { 
	trade_value = 6
	picture = urban_goods_glassware
}

glassware_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_glassware
}

glassware_25 = { 
	trade_value = 7.5
	picture = urban_goods_glassware
}

glassware_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_glassware
}

glassware_30 = { 
	trade_value = 9
	picture = urban_goods_glassware
}

glassware_35 = { 
	trade_value = 10.5
	picture = urban_goods_glassware
}

glassware_40 = { 
	trade_value = 12
	picture = urban_goods_glassware
}

glassware_50 = { 
	trade_value = 15
	picture = urban_goods_glassware
}

glassware_60 = { 
	trade_value = 18
	picture = urban_goods_glassware
}

glassware_70 = { 
	trade_value = 21
	picture = urban_goods_glassware
}

glassware_80 = { 
	trade_value = 24
	picture = urban_goods_glassware
}

glassware_100 = { 
	trade_value = 30
	picture = urban_goods_glassware
}

glassware_120 = { 
	trade_value = 36
	picture = urban_goods_glassware
}

glassware_140 = { 
	trade_value = 42
	picture = urban_goods_glassware
}

glassware_160 = { 
	trade_value = 48
	picture = urban_goods_glassware
}

glassware_180 = { 
	trade_value = 54
	picture = urban_goods_glassware
}

glassware_200 = { 
	trade_value = 60
	picture = urban_goods_glassware
}

urban_goods_leather = { ### Price 5
	picture = urban_goods_leather
}

leather_1 = { 
	trade_value = 0.3
	picture = urban_goods_leather
}

leather_2 = { 
	trade_value = 0.6
	picture = urban_goods_leather
}

leather_3 = { 
	trade_value = 0.9
	picture = urban_goods_leather
}

leather_4 = { 
	trade_value = 1.2
	picture = urban_goods_leather
}

leather_5 = { 
	trade_value = 1.5
	picture = urban_goods_leather
}

leather_6 = { 
	trade_value = 1.8
	picture = urban_goods_leather
}

leather_7 = { 
	trade_value = 2.1
	picture = urban_goods_leather
}

leather_8 = { 
	trade_value = 2.4
	picture = urban_goods_leather
}

leather_9 = { 
	trade_value = 2.7
	picture = urban_goods_leather
}

leather_10 = { 
	trade_value = 3
	picture = urban_goods_leather
}

leather_12 = { 
	trade_value = 3.6
	picture = urban_goods_leather
}

leather_14 = { 
	trade_value = 4.2
	picture = urban_goods_leather
}

leather_16 = { 
	trade_value = 4.8
	picture = urban_goods_leather
}

leather_18 = { 
	trade_value = 5.4
	picture = urban_goods_leather
}

leather_20 = { 
	trade_value = 6
	picture = urban_goods_leather
}

leather_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_leather
}

leather_25 = { 
	trade_value = 7.5
	picture = urban_goods_leather
}

leather_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_leather
}

leather_30 = { 
	trade_value = 9
	picture = urban_goods_leather
}

leather_35 = { 
	trade_value = 10.5
	picture = urban_goods_leather
}

leather_40 = { 
	trade_value = 12
	picture = urban_goods_leather
}

leather_50 = { 
	trade_value = 15
	picture = urban_goods_leather
}

leather_60 = { 
	trade_value = 18
	picture = urban_goods_leather
}

leather_70 = { 
	trade_value = 21
	picture = urban_goods_leather
}

leather_80 = { 
	trade_value = 24
	picture = urban_goods_leather
}

leather_100 = { 
	trade_value = 30
	picture = urban_goods_leather
}

leather_120 = { 
	trade_value = 36
	picture = urban_goods_leather
}

leather_140 = { 
	trade_value = 42
	picture = urban_goods_leather
}

leather_160 = { 
	trade_value = 48
	picture = urban_goods_leather
}

leather_180 = { 
	trade_value = 54
	picture = urban_goods_leather
}

leather_200 = { 
	trade_value = 60
	picture = urban_goods_leather
}

urban_goods_silk = { ### Price 5
	picture = urban_goods_silk
}

silk_1 = { 
	trade_value = 0.3
	picture = urban_goods_silk
}

silk_2 = { 
	trade_value = 0.6
	picture = urban_goods_silk
}

silk_3 = { 
	trade_value = 0.9
	picture = urban_goods_silk
}

silk_4 = { 
	trade_value = 1.2
	picture = urban_goods_silk
}

silk_5 = { 
	trade_value = 1.5
	picture = urban_goods_silk
}

silk_6 = { 
	trade_value = 1.8
	picture = urban_goods_silk
}

silk_7 = { 
	trade_value = 2.1
	picture = urban_goods_silk
}

silk_8 = { 
	trade_value = 2.4
	picture = urban_goods_silk
}

silk_9 = { 
	trade_value = 2.7
	picture = urban_goods_silk
}

silk_10 = { 
	trade_value = 3
	picture = urban_goods_silk
}

silk_12 = { 
	trade_value = 3.6
	picture = urban_goods_silk
}

silk_14 = { 
	trade_value = 4.2
	picture = urban_goods_silk
}

silk_16 = { 
	trade_value = 4.8
	picture = urban_goods_silk
}

silk_18 = { 
	trade_value = 5.4
	picture = urban_goods_silk
}

silk_20 = { 
	trade_value = 6
	picture = urban_goods_silk
}

silk_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_silk
}

silk_25 = { 
	trade_value = 7.5
	picture = urban_goods_silk
}

silk_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_silk
}

silk_30 = { 
	trade_value = 9
	picture = urban_goods_silk
}

silk_35 = { 
	trade_value = 10.5
	picture = urban_goods_silk
}

silk_40 = { 
	trade_value = 12
	picture = urban_goods_silk
}

silk_50 = { 
	trade_value = 15
	picture = urban_goods_silk
}

silk_60 = { 
	trade_value = 18
	picture = urban_goods_silk
}

silk_70 = { 
	trade_value = 21
	picture = urban_goods_silk
}

silk_80 = { 
	trade_value = 24
	picture = urban_goods_silk
}

silk_100 = { 
	trade_value = 30
	picture = urban_goods_silk
}

silk_120 = { 
	trade_value = 36
	picture = urban_goods_silk
}

silk_140 = { 
	trade_value = 42
	picture = urban_goods_silk
}

silk_160 = { 
	trade_value = 48
	picture = urban_goods_silk
}

silk_180 = { 
	trade_value = 54
	picture = urban_goods_silk
}

silk_200 = { 
	trade_value = 60
	picture = urban_goods_silk
}

urban_goods_steel = { ### Price 5
	picture = urban_goods_steel
}

steel_1 = { 
	trade_value = 0.3
	picture = urban_goods_steel
}

steel_2 = { 
	trade_value = 0.6
	picture = urban_goods_steel
}

steel_3 = { 
	trade_value = 0.9
	picture = urban_goods_steel
}

steel_4 = { 
	trade_value = 1.2
	picture = urban_goods_steel
}

steel_5 = { 
	trade_value = 1.5
	picture = urban_goods_steel
}

steel_6 = { 
	trade_value = 1.8
	picture = urban_goods_steel
}

steel_7 = { 
	trade_value = 2.1
	picture = urban_goods_steel
}

steel_8 = { 
	trade_value = 2.4
	picture = urban_goods_steel
}

steel_9 = { 
	trade_value = 2.7
	picture = urban_goods_steel
}

steel_10 = { 
	trade_value = 3
	picture = urban_goods_steel
}

steel_12 = { 
	trade_value = 3.6
	picture = urban_goods_steel
}

steel_14 = { 
	trade_value = 4.2
	picture = urban_goods_steel
}

steel_16 = { 
	trade_value = 4.8
	picture = urban_goods_steel
}

steel_18 = { 
	trade_value = 5.4
	picture = urban_goods_steel
}

steel_20 = { 
	trade_value = 6
	picture = urban_goods_steel
}

steel_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_steel
}

steel_25 = { 
	trade_value = 7.5
	picture = urban_goods_steel
}

steel_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_steel
}

steel_30 = { 
	trade_value = 9
	picture = urban_goods_steel
}

steel_35 = { 
	trade_value = 10.5
	picture = urban_goods_steel
}

steel_40 = { 
	trade_value = 12
	picture = urban_goods_steel
}

steel_50 = { 
	trade_value = 15
	picture = urban_goods_steel
}

steel_60 = { 
	trade_value = 18
	picture = urban_goods_steel
}

steel_70 = { 
	trade_value = 21
	picture = urban_goods_steel
}

steel_80 = { 
	trade_value = 24
	picture = urban_goods_steel
}

steel_100 = { 
	trade_value = 30
	picture = urban_goods_steel
}

steel_120 = { 
	trade_value = 36
	picture = urban_goods_steel
}

steel_140 = { 
	trade_value = 42
	picture = urban_goods_steel
}

steel_160 = { 
	trade_value = 48
	picture = urban_goods_steel
}

steel_180 = { 
	trade_value = 54
	picture = urban_goods_steel
}

steel_200 = { 
	trade_value = 60
	picture = urban_goods_steel
}

urban_goods_naval_supplies = { ### Price 5
	picture = urban_goods_naval_supplies
}

naval_supplies_1 = { 
	trade_value = 0.3
	picture = urban_goods_naval_supplies
}

naval_supplies_2 = { 
	trade_value = 0.6
	picture = urban_goods_naval_supplies
}

naval_supplies_3 = { 
	trade_value = 0.9
	picture = urban_goods_naval_supplies
}

naval_supplies_4 = { 
	trade_value = 1.2
	picture = urban_goods_naval_supplies
}

naval_supplies_5 = { 
	trade_value = 1.5
	picture = urban_goods_naval_supplies
}

naval_supplies_6 = { 
	trade_value = 1.8
	picture = urban_goods_naval_supplies
}

naval_supplies_7 = { 
	trade_value = 2.1
	picture = urban_goods_naval_supplies
}

naval_supplies_8 = { 
	trade_value = 2.4
	picture = urban_goods_naval_supplies
}

naval_supplies_9 = { 
	trade_value = 2.7
	picture = urban_goods_naval_supplies
}

naval_supplies_10 = { 
	trade_value = 3
	picture = urban_goods_naval_supplies
}

naval_supplies_12 = { 
	trade_value = 3.6
	picture = urban_goods_naval_supplies
}

naval_supplies_14 = { 
	trade_value = 4.2
	picture = urban_goods_naval_supplies
}

naval_supplies_16 = { 
	trade_value = 4.8
	picture = urban_goods_naval_supplies
}

naval_supplies_18 = { 
	trade_value = 5.4
	picture = urban_goods_naval_supplies
}

naval_supplies_20 = { 
	trade_value = 6
	picture = urban_goods_naval_supplies
}

naval_supplies_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_naval_supplies
}

naval_supplies_25 = { 
	trade_value = 7.5
	picture = urban_goods_naval_supplies
}

naval_supplies_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_naval_supplies
}

naval_supplies_30 = { 
	trade_value = 9
	picture = urban_goods_naval_supplies
}

naval_supplies_35 = { 
	trade_value = 10.5
	picture = urban_goods_naval_supplies
}

naval_supplies_40 = { 
	trade_value = 12
	picture = urban_goods_naval_supplies
}

naval_supplies_50 = { 
	trade_value = 15
	picture = urban_goods_naval_supplies
}

naval_supplies_60 = { 
	trade_value = 18
	picture = urban_goods_naval_supplies
}

naval_supplies_70 = { 
	trade_value = 21
	picture = urban_goods_naval_supplies
}

naval_supplies_80 = { 
	trade_value = 24
	picture = urban_goods_naval_supplies
}

naval_supplies_100 = { 
	trade_value = 30
	picture = urban_goods_naval_supplies
}

naval_supplies_120 = { 
	trade_value = 36
	picture = urban_goods_naval_supplies
}

naval_supplies_140 = { 
	trade_value = 42
	picture = urban_goods_naval_supplies
}

naval_supplies_160 = { 
	trade_value = 48
	picture = urban_goods_naval_supplies
}

naval_supplies_180 = { 
	trade_value = 54
	picture = urban_goods_naval_supplies
}

naval_supplies_200 = { 
	trade_value = 60
	picture = urban_goods_naval_supplies
}

urban_goods_paper = { ### Price 5
	picture = urban_goods_paper
}

paper_1 = { 
	trade_value = 0.3
	picture = urban_goods_paper
}

paper_2 = { 
	trade_value = 0.6
	picture = urban_goods_paper
}

paper_3 = { 
	trade_value = 0.9
	picture = urban_goods_paper
}

paper_4 = { 
	trade_value = 1.2
	picture = urban_goods_paper
}

paper_5 = { 
	trade_value = 1.5
	picture = urban_goods_paper
}

paper_6 = { 
	trade_value = 1.8
	picture = urban_goods_paper
}

paper_7 = { 
	trade_value = 2.1
	picture = urban_goods_paper
}

paper_8 = { 
	trade_value = 2.4
	picture = urban_goods_paper
}

paper_9 = { 
	trade_value = 2.7
	picture = urban_goods_paper
}

paper_10 = { 
	trade_value = 3
	picture = urban_goods_paper
}

paper_12 = { 
	trade_value = 3.6
	picture = urban_goods_paper
}

paper_14 = { 
	trade_value = 4.2
	picture = urban_goods_paper
}

paper_16 = { 
	trade_value = 4.8
	picture = urban_goods_paper
}

paper_18 = { 
	trade_value = 5.4
	picture = urban_goods_paper
}

paper_20 = { 
	trade_value = 6
	picture = urban_goods_paper
}

paper_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_paper
}

paper_25 = { 
	trade_value = 7.5
	picture = urban_goods_paper
}

paper_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_paper
}

paper_30 = { 
	trade_value = 9
	picture = urban_goods_paper
}

paper_35 = { 
	trade_value = 10.5
	picture = urban_goods_paper
}

paper_40 = { 
	trade_value = 12
	picture = urban_goods_paper
}

paper_50 = { 
	trade_value = 15
	picture = urban_goods_paper
}

paper_60 = { 
	trade_value = 18
	picture = urban_goods_paper
}

paper_70 = { 
	trade_value = 21
	picture = urban_goods_paper
}

paper_80 = { 
	trade_value = 24
	picture = urban_goods_paper
}

paper_100 = { 
	trade_value = 30
	picture = urban_goods_paper
}

paper_120 = { 
	trade_value = 36
	picture = urban_goods_paper
}

paper_140 = { 
	trade_value = 42
	picture = urban_goods_paper
}

paper_160 = { 
	trade_value = 48
	picture = urban_goods_paper
}

paper_180 = { 
	trade_value = 54
	picture = urban_goods_paper
}

paper_200 = { 
	trade_value = 60
	picture = urban_goods_paper
}

urban_goods_carpet = { ### Price 5
	picture = urban_goods_carpet
}

carpet_1 = { 
	trade_value = 0.3
	picture = urban_goods_carpet
}

carpet_2 = { 
	trade_value = 0.6
	picture = urban_goods_carpet
}

carpet_3 = { 
	trade_value = 0.9
	picture = urban_goods_carpet
}

carpet_4 = { 
	trade_value = 1.2
	picture = urban_goods_carpet
}

carpet_5 = { 
	trade_value = 1.5
	picture = urban_goods_carpet
}

carpet_6 = { 
	trade_value = 1.8
	picture = urban_goods_carpet
}

carpet_7 = { 
	trade_value = 2.1
	picture = urban_goods_carpet
}

carpet_8 = { 
	trade_value = 2.4
	picture = urban_goods_carpet
}

carpet_9 = { 
	trade_value = 2.7
	picture = urban_goods_carpet
}

carpet_10 = { 
	trade_value = 3
	picture = urban_goods_carpet
}

carpet_12 = { 
	trade_value = 3.6
	picture = urban_goods_carpet
}

carpet_14 = { 
	trade_value = 4.2
	picture = urban_goods_carpet
}

carpet_16 = { 
	trade_value = 4.8
	picture = urban_goods_carpet
}

carpet_18 = { 
	trade_value = 5.4
	picture = urban_goods_carpet
}

carpet_20 = { 
	trade_value = 6
	picture = urban_goods_carpet
}

carpet_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_carpet
}

carpet_25 = { 
	trade_value = 7.5
	picture = urban_goods_carpet
}

carpet_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_carpet
}

carpet_30 = { 
	trade_value = 9
	picture = urban_goods_carpet
}

carpet_35 = { 
	trade_value = 10.5
	picture = urban_goods_carpet
}

carpet_40 = { 
	trade_value = 12
	picture = urban_goods_carpet
}

carpet_50 = { 
	trade_value = 15
	picture = urban_goods_carpet
}

carpet_60 = { 
	trade_value = 18
	picture = urban_goods_carpet
}

carpet_70 = { 
	trade_value = 21
	picture = urban_goods_carpet
}

carpet_80 = { 
	trade_value = 24
	picture = urban_goods_carpet
}

carpet_100 = { 
	trade_value = 30
	picture = urban_goods_carpet
}

carpet_120 = { 
	trade_value = 36
	picture = urban_goods_carpet
}

carpet_140 = { 
	trade_value = 42
	picture = urban_goods_carpet
}

carpet_160 = { 
	trade_value = 48
	picture = urban_goods_carpet
}

carpet_180 = { 
	trade_value = 54
	picture = urban_goods_carpet
}

carpet_200 = { 
	trade_value = 60
	picture = urban_goods_carpet
}

urban_goods_chinaware = { ### Price 5 
	picture = urban_goods_chinaware
}

chinaware_1 = { 
	trade_value = 0.3
	picture = urban_goods_chinaware
}

chinaware_2 = { 
	trade_value = 0.6
	picture = urban_goods_chinaware
}

chinaware_3 = { 
	trade_value = 0.9
	picture = urban_goods_chinaware
}

chinaware_4 = { 
	trade_value = 1.2
	picture = urban_goods_chinaware
}

chinaware_5 = { 
	trade_value = 1.5
	picture = urban_goods_chinaware
}

chinaware_6 = { 
	trade_value = 1.8
	picture = urban_goods_chinaware
}

chinaware_7 = { 
	trade_value = 2.1
	picture = urban_goods_chinaware
}

chinaware_8 = { 
	trade_value = 2.4
	picture = urban_goods_chinaware
}

chinaware_9 = { 
	trade_value = 2.7
	picture = urban_goods_chinaware
}

chinaware_10 = { 
	trade_value = 3
	picture = urban_goods_chinaware
}

chinaware_12 = { 
	trade_value = 3.6
	picture = urban_goods_chinaware
}

chinaware_14 = { 
	trade_value = 4.2
	picture = urban_goods_chinaware
}

chinaware_16 = { 
	trade_value = 4.8
	picture = urban_goods_chinaware
}

chinaware_18 = { 
	trade_value = 5.4
	picture = urban_goods_chinaware
}

chinaware_20 = { 
	trade_value = 6
	picture = urban_goods_chinaware
}

chinaware_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_chinaware
}

chinaware_25 = { 
	trade_value = 7.5
	picture = urban_goods_chinaware
}

chinaware_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_chinaware
}

chinaware_30 = { 
	trade_value = 9
	picture = urban_goods_chinaware
}

chinaware_35 = { 
	trade_value = 10.5
	picture = urban_goods_chinaware
}

chinaware_40 = { 
	trade_value = 12
	picture = urban_goods_chinaware
}

chinaware_50 = { 
	trade_value = 15
	picture = urban_goods_chinaware
}

chinaware_60 = { 
	trade_value = 18
	picture = urban_goods_chinaware
}

chinaware_70 = { 
	trade_value = 21
	picture = urban_goods_chinaware
}

chinaware_80 = { 
	trade_value = 24
	picture = urban_goods_chinaware
}

chinaware_100 = { 
	trade_value = 30
	picture = urban_goods_chinaware
}

chinaware_120 = { 
	trade_value = 36
	picture = urban_goods_chinaware
}

chinaware_140 = { 
	trade_value = 42
	picture = urban_goods_chinaware
}

chinaware_160 = { 
	trade_value = 48
	picture = urban_goods_chinaware
}

chinaware_180 = { 
	trade_value = 54
	picture = urban_goods_chinaware
}

chinaware_200 = { 
	trade_value = 60
	picture = urban_goods_chinaware
}

urban_goods_jewelry = { ### Price 5
	picture = urban_goods_jewelry
}

jewelry_1 = { 
	trade_value = 0.3
	picture = urban_goods_jewelry
}

jewelry_2 = { 
	trade_value = 0.6
	picture = urban_goods_jewelry
}

jewelry_3 = { 
	trade_value = 0.9
	picture = urban_goods_jewelry
}

jewelry_4 = { 
	trade_value = 1.2
	picture = urban_goods_jewelry
}

jewelry_5 = { 
	trade_value = 1.5
	picture = urban_goods_jewelry
}

jewelry_6 = { 
	trade_value = 1.8
	picture = urban_goods_jewelry
}

jewelry_7 = { 
	trade_value = 2.1
	picture = urban_goods_jewelry
}

jewelry_8 = { 
	trade_value = 2.4
	picture = urban_goods_jewelry
}

jewelry_9 = { 
	trade_value = 2.7
	picture = urban_goods_jewelry
}

jewelry_10 = { 
	trade_value = 3
	picture = urban_goods_jewelry
}

jewelry_12 = { 
	trade_value = 3.6
	picture = urban_goods_jewelry
}

jewelry_14 = { 
	trade_value = 4.2
	picture = urban_goods_jewelry
}

jewelry_16 = { 
	trade_value = 4.8
	picture = urban_goods_jewelry
}

jewelry_18 = { 
	trade_value = 5.4
	picture = urban_goods_jewelry
}

jewelry_20 = { 
	trade_value = 6
	picture = urban_goods_jewelry
}

jewelry_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_jewelry
}

jewelry_25 = { 
	trade_value = 7.5
	picture = urban_goods_jewelry
}

jewelry_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_jewelry
}

jewelry_30 = { 
	trade_value = 9
	picture = urban_goods_jewelry
}

jewelry_35 = { 
	trade_value = 10.5
	picture = urban_goods_jewelry
}

jewelry_40 = { 
	trade_value = 12
	picture = urban_goods_jewelry
}

jewelry_50 = { 
	trade_value = 15
	picture = urban_goods_jewelry
}

jewelry_60 = { 
	trade_value = 18
	picture = urban_goods_jewelry
}

jewelry_70 = { 
	trade_value = 21
	picture = urban_goods_jewelry
}

jewelry_80 = { 
	trade_value = 24
	picture = urban_goods_jewelry
}

jewelry_100 = { 
	trade_value = 30
	picture = urban_goods_jewelry
}

jewelry_120 = { 
	trade_value = 36
	picture = urban_goods_jewelry
}

jewelry_140 = { 
	trade_value = 42
	picture = urban_goods_jewelry
}

jewelry_160 = { 
	trade_value = 48
	picture = urban_goods_jewelry
}

jewelry_180 = { 
	trade_value = 54
	picture = urban_goods_jewelry
}

jewelry_200 = { 
	trade_value = 60
	picture = urban_goods_jewelry
}
urban_goods_luxury_cloth = { ### Price 5
	picture = urban_goods_luxury_cloth
}

luxury_cloth_1 = { 
	trade_value = 0.3
	picture = urban_goods_luxury_cloth
}

luxury_cloth_2 = { 
	trade_value = 0.6
	picture = urban_goods_luxury_cloth
}

luxury_cloth_3 = { 
	trade_value = 0.9
	picture = urban_goods_luxury_cloth
}

luxury_cloth_4 = { 
	trade_value = 1.2
	picture = urban_goods_luxury_cloth
}

luxury_cloth_5 = { 
	trade_value = 1.5
	picture = urban_goods_luxury_cloth
}

luxury_cloth_6 = { 
	trade_value = 1.8
	picture = urban_goods_luxury_cloth
}

luxury_cloth_7 = { 
	trade_value = 2.1
	picture = urban_goods_luxury_cloth
}

luxury_cloth_8 = { 
	trade_value = 2.4
	picture = urban_goods_luxury_cloth
}

luxury_cloth_9 = { 
	trade_value = 2.7
	picture = urban_goods_luxury_cloth
}

luxury_cloth_10 = { 
	trade_value = 3
	picture = urban_goods_luxury_cloth
}

luxury_cloth_12 = { 
	trade_value = 3.6
	picture = urban_goods_luxury_cloth
}

luxury_cloth_14 = { 
	trade_value = 4.2
	picture = urban_goods_luxury_cloth
}

luxury_cloth_16 = { 
	trade_value = 4.8
	picture = urban_goods_luxury_cloth
}

luxury_cloth_18 = { 
	trade_value = 5.4
	picture = urban_goods_luxury_cloth
}

luxury_cloth_20 = { 
	trade_value = 6
	picture = urban_goods_luxury_cloth
}

luxury_cloth_22.5 = { 
	trade_value = 6.75
	picture = urban_goods_luxury_cloth
}

luxury_cloth_25 = { 
	trade_value = 7.5
	picture = urban_goods_luxury_cloth
}

luxury_cloth_27.5 = { 
	trade_value = 8.25
	picture = urban_goods_luxury_cloth
}

luxury_cloth_30 = { 
	trade_value = 9
	picture = urban_goods_luxury_cloth
}

luxury_cloth_35 = { 
	trade_value = 10.5
	picture = urban_goods_luxury_cloth
}

luxury_cloth_40 = { 
	trade_value = 12
	picture = urban_goods_luxury_cloth
}

luxury_cloth_50 = { 
	trade_value = 15
	picture = urban_goods_luxury_cloth
}

luxury_cloth_60 = { 
	trade_value = 18
	picture = urban_goods_luxury_cloth
}

luxury_cloth_70 = { 
	trade_value = 21
	picture = urban_goods_luxury_cloth
}

luxury_cloth_80 = { 
	trade_value = 24
	picture = urban_goods_luxury_cloth
}

luxury_cloth_100 = { 
	trade_value = 30
	picture = urban_goods_luxury_cloth
}

luxury_cloth_120 = { 
	trade_value = 36
	picture = urban_goods_luxury_cloth
}

luxury_cloth_140 = { 
	trade_value = 42
	picture = urban_goods_luxury_cloth
}

luxury_cloth_160 = { 
	trade_value = 48
	picture = urban_goods_luxury_cloth
}

luxury_cloth_180 = { 
	trade_value = 54
	picture = urban_goods_luxury_cloth
}

luxury_cloth_200 = { 
	trade_value = 60
	picture = urban_goods_luxury_cloth
}

# RANK 3 : Early industrialization goods.

urban_goods_ammunitions = {
	picture = urban_goods_ammunitions
}

urban_goods_arms = {
	picture = urban_goods_arms
}

urban_goods_furniture = {
	picture = urban_goods_furniture
}

urban_goods_hardware = {
	picture = urban_goods_hardware
}

urban_goods_rum = {
	picture = urban_goods_rum
}