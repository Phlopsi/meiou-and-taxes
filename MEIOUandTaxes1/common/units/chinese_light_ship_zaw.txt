#15 - Square-sailed Caravel - Zaw
type = light_ship

hull_size = 22 #60 tonnage
base_cannons = 25 #2-8 cannons per caravel
sail_speed = 8 #top speed 8 knots
trade_power = 3.5

#sailors = 72

sprite_level = 3

trigger = { 
	OR = {
		technology_group = chinese
		technology_group = austranesian
		}
	NOT = { primary_culture = dutch }
	NOT = { culture_group = portuguese }
	NOT = { culture_group = korean_group }
	NOT = { has_country_flag = raised_special_units }
	}


