#10 - Early Caravel - Trading Junk
type = light_ship

hull_size = 20 #50 tons (not larger due to lateen sails)
base_cannons = 23 #12 to 15 cannons
sail_speed = 6 #Average speed of 6 knots (Henry the Navigator)
trade_power = 3

#sailors = 60

sprite_level = 2

trigger = { 
	OR = {
		technology_group = chinese
		technology_group = austranesian
		}
	NOT = { primary_culture = english }
	NOT = { has_country_flag = raised_special_units }
	}


