#45 - Light Hussars

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 6
defensive_morale = 6
offensive_fire = 5
defensive_fire = 3
offensive_shock = 5
defensive_shock = 3

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { tsetse_region_trigger = yes }
	}
	NOT = { has_country_flag = raised_special_units }
}
