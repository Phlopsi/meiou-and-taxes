#30 - Brigantine - Turtle Ship
type = light_ship

hull_size = 37 #180 tons displacement /10
base_cannons = 42 #11 guns per side and 2 in front
sail_speed = 8 #??
trade_power = 5

#sailors = 84

sprite_level = 4

trigger = { 
	culture_group = korean_group
	NOT = { has_country_flag = raised_special_units }
	}
	

