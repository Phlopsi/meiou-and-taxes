#34 - Javelineers

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 6
defensive_morale = 5
offensive_fire = 0
defensive_fire = 1
offensive_shock = 8
defensive_shock = 3

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { tsetse_region_trigger = yes }
	}
	NOT = { has_country_flag = raised_special_units }
}
