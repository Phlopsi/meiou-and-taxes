#Samurai Heavy Cavalry (8)

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 3
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 2
defensive_shock = 3

trigger = {
	any_owned_province = { superregion = japan_superregion }
	NOT = { has_country_flag = raised_special_units }
	}

