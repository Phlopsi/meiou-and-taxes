#0 - Clan Cavalry

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 2
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 1
defensive_shock = 1

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { tsetse_region_trigger = yes }
	}
	NOT = { has_country_flag = raised_special_units }
}
