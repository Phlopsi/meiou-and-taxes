# Wave Cavalry (23)

unit_type = north_american
type = cavalry

maneuver = 2
offensive_morale = 6
defensive_morale = 1
offensive_fire =   2
defensive_fire =   0
offensive_shock =  6
defensive_shock =  2

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}