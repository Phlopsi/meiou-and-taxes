# Ekanda Cavalry (32)

unit_type = austranesian
type = cavalry
maneuver = 2

offensive_morale = 5
defensive_morale = 4
offensive_fire = 4
defensive_fire = 3
offensive_shock = 4
defensive_shock = 2

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}