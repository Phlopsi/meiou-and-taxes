# south_american Medium Hussar (40)

type = cavalry
unit_type = south_american
maneuver = 2

offensive_morale = 4
defensive_morale = 5
offensive_fire = 4
defensive_fire = 3
offensive_shock = 6
defensive_shock = 4

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}