#Light Hussars
#tech level 26

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 5
offensive_fire = 3
defensive_fire = 1
offensive_shock = 4
defensive_shock = 3

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}