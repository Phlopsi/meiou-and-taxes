# Heavy Cavalry (33)

unit_type = indian
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 5
offensive_fire = 1
defensive_fire = 3
offensive_shock = 6
defensive_shock = 4

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}