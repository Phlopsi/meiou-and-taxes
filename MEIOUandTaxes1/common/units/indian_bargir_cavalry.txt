# Bargir Raiders (28)

unit_type = indian
type = cavalry
maneuver = 2

offensive_morale = 7
defensive_morale = 3
offensive_fire = 3
defensive_fire = 2
offensive_shock = 4
defensive_shock = 1

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}