# Western Gallop Cavalry (30)

type = cavalry
unit_type = mesoamerican
maneuver = 2

offensive_morale = 6
defensive_morale = 3
offensive_fire = 2
defensive_fire = 1
offensive_shock = 5
defensive_shock = 4

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}