#Westernized Cavalry (22)

unit_type = mesoamerican
type = cavalry

maneuver = 1	
offensive_morale = 5
defensive_morale = 4	
offensive_fire = 0
defensive_fire = 0
offensive_shock = 5
defensive_shock = 3
trigger = { 
	NOT = { has_country_flag = raised_special_units }
}