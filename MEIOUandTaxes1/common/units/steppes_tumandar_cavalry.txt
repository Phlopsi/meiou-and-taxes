#Tumandar Cavalry (12)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 2
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 4

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}