# Mounted Infantry (23)

unit_type = north_american
type = cavalry

maneuver = 2
offensive_morale = 3
defensive_morale = 3
offensive_fire =   2
defensive_fire =   4
offensive_shock =  3
defensive_shock =  2

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}