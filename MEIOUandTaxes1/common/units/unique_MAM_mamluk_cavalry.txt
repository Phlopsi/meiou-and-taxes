#0 - Amir's Mamluk Cavalry - Mamluks

unit_type = muslim
type = cavalry
maneuver = 2

offensive_morale = 2
defensive_morale = 1 #BONUS
offensive_fire = 0
defensive_fire = 0
offensive_shock = 1
defensive_shock = 3

trigger = {
	primary_culture = egyptian
	NOT = { has_country_flag = raised_special_units }
	}


