#Reiter cavalry
#tech level 31

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 4
defensive_morale = 3
offensive_fire = 4
defensive_fire = 2
offensive_shock = 5
defensive_shock = 3

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}