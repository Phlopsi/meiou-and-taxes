# Rammaha Lancers

unit_type = muslim
type = cavalry
maneuver = 2

#Tech 16
offensive_morale = 4
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 4

trigger = { 
	NOT = { has_country_flag = raised_special_units }
}