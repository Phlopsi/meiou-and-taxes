#55 - Ironclad Battleship (HMS Devastation, 1871)
type = heavy_ship

hull_size = 155 #9330 tons (/40)
base_cannons = 98 #18 cannons, but large ones
sail_speed = 13 

#sailors = 1000

sprite_level = 6

trigger = {
	OR = { 
		technology_group = western
		technology_group = high_american
		technology_group = high_turkishtech
		technology_group = high_muslim
		technology_group = high_chinese
		technology_group = high_eastern
		technology_group = high_indian
		has_idea_group = grand_fleet_ideas
	NOT = { has_country_flag = raised_special_units }
	}
}

