#25 - Horse Guard 

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 4 #BONUS
defensive_morale = 5
offensive_fire = 0
defensive_fire = 1
offensive_shock = 5
defensive_shock = 4

trigger = {
	primary_culture = burmese
	NOT = { has_country_flag = raised_special_units }
	}

