#Reformed Samurai Cavalry (20)

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 3
defensive_morale = 3
offensive_fire = 0
defensive_fire = 1
offensive_shock = 5
defensive_shock = 4

trigger = {
	any_owned_province = { superregion = japan_superregion }
	NOT = { has_country_flag = raised_special_units }
	}

