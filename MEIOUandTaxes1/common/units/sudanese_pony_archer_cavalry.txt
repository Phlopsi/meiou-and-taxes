#16 - Pony Archers

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 4
defensive_morale = 3
offensive_fire = 0
defensive_fire = 2
offensive_shock = 3
defensive_shock = 2

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { tsetse_region_trigger = yes }
	}
	NOT = { has_country_flag = raised_special_units }
}
