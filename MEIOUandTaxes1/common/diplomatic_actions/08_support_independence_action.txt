# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	disallow				Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

# support_inp
support_independence_action = {
	condition = {
		tooltip = INDSUP
		potential = {
			indian_state_trigger = yes
		}
		allow = {
			OR = {
				NOT = { border_distance = { who = FROM distance = 120 } } 
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			FROM = {
				indian_state_trigger = yes
			}
		}
		allow = {	
			FROM = {
				OR = {
					NOT = { border_distance = { who = ROOT distance = 120 } } 
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			culture_group = east_slavic
			technology_group = eastern
			NOT = { has_country_flag = relocated_capital_st_petersburg }
				
		}
		allow = {
			OR = {
				NOT = { border_distance = { who = FROM distance = 120 } } 
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			FROM = {
				culture_group = east_slavic
				technology_group = eastern
				NOT = { has_country_flag = relocated_capital_st_petersburg }
			}			
		}
		allow = {
			FROM = {
				OR = {
					NOT = { border_distance = { who = ROOT distance = 120 } } 
					culture_group = ROOT
				}
			}	
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			amerind_trigger = yes
		}
		allow = {
			OR = {
				NOT = { border_distance = { who = FROM distance = 120 } } 
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			FROM = {
				amerind_trigger = yes
			}
		}
		allow = {	
			FROM = {
				OR = {
					NOT = { border_distance = { who = ROOT distance = 120 } } 
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			african_state_trigger = yes
		}
		allow = {
			OR = {
				NOT = { border_distance = { who = FROM distance = 120 } } 
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			FROM = {
				african_state_trigger = yes
			}
		}
		allow = {	
			FROM = {
				OR = {
					NOT = { border_distance = { who = ROOT distance = 120 } } 
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			mandala_system_state_trigger = yes
		}
		allow = {
			OR = {
				NOT = { border_distance = { who = FROM distance = 120 } } 
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = INDSUP
		potential = {
			FROM = {
				mandala_system_state_trigger = yes
			}
		}
		allow = {	
			FROM = {
				OR = {
					NOT = { border_distance = { who = ROOT distance = 120 } } 
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = AMALGAMATION
		potential = {
			OR = {
				government = amalgamation_government
				FROM = { government = amalgamation_government }
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = PIZARROBROTHERS
		potential = {
			OR = {
				FROM = {
					tag = PIZ
				}
				tag = PIZ
			}
		}
		allow = {
			always = no
		}
	}
}
