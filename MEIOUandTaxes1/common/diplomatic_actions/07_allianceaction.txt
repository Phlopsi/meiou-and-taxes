# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	disallow				Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

# aiiliance
allianceaction = {
	condition = {
		tooltip = COURTSETUP
		potential = {
		}
		allow = {
			has_global_flag = courtsetup
		}
	}
	condition = {
		tooltip = INDIANSTATE
		potential = {
			indian_state_trigger = yes
		}
		allow = {
			FROM = {
				 OR = { 
					is_neighbor_of = ROOT 
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = INDIANSTATE
		potential = {
			FROM = {
				indian_state_trigger = yes
			}
		}
		allow = {
			OR = {
				is_neighbor_of = FROM
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = HREPREWESTPHALIA
		potential = {
			OR = {
				AND = {
					FROM = {
						is_part_of_hre = no
						OR = {
							NOT = { dynasty = ROOT }
							government = theocracy
						}
					}
					AND = {
						is_part_of_hre = yes
						NOT = { capital_scope =  { OR = { superregion = italy_superregion region = provence_region } } }
					}
				}
				AND = {
					FROM = {
						AND = {
							is_part_of_hre = yes
							NOT = { capital_scope =  { OR = { superregion = italy_superregion region = provence_region } } }
						}
						OR = {
							NOT = { dynasty = ROOT }
							government = theocracy
						}
					}
					is_part_of_hre = no
				}
			}
		}
		allow = {
			OR = {
				is_emperor = yes
				FROM = { is_emperor = yes }
				has_global_flag = leagues_peace_of_westphalia
			}
		}
	}
	condition = {
		tooltip = IMPERIALFREECITY
		potential = {
			OR = {
			government = imperial_city
					government = merchant_imperial_city
				}
		}
		allow = {
			FROM = {
				OR = {
				government = imperial_city
						government = merchant_imperial_city
					}
			}
		}
	}
	condition = {
		tooltip = IMPERIALFREECITY
		potential = {
			FROM = {
				OR = {
				government = imperial_city
					government = merchant_imperial_city
				}
			}
		}
		allow = {
				OR = {
			government = imperial_city
					government = merchant_imperial_city
				}
		}
	}
	condition = {
		tooltip = EMPEROROFCHINA
		potential = {
			OR = {
				chinese_imperial_gov_trigger = yes
				FROM = {
					chinese_imperial_gov_trigger = yes
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = TURKISHEMPEROR
		potential = {
			turkish_emperor_trigger = yes
		}
		allow = {
			FROM = {
				inferior_government_trigger = yes
			}
		}
	}
	condition = {
		tooltip = TURKISHEMPEROR
		potential = {
			FROM = {
				turkish_emperor_trigger = yes
			}
		}
		allow = {
			inferior_government_trigger = yes
		}
	}
	condition = {
		tooltip = PERSIANEMPEROR
		potential = {
			culture_group = persian_group
			government_rank = 5
		}
		allow = {
			FROM = {
				inferior_government_trigger = yes
			}
		}
	}
	condition = {
		tooltip = PERSIANEMPEROR
		potential = {
			FROM = {
				culture_group = persian_group
				government_rank = 5
			}
		}
		allow = {
			inferior_government_trigger = yes
		}
	}
	condition = {
		tooltip = IRISHCLAN
		potential = {
			government = irish_monarchy
		}
		allow = {
			FROM = {
				government = irish_monarchy
			}
		}
	}
	condition = {
		tooltip = IRISHCLAN
		potential = {
			FROM = {
				government = irish_monarchy
			}
		}
		allow = {
			government = irish_monarchy
		}
	}
	condition = {
		tooltip = EMPEROROFINDIA
		potential = {
			OR = {
				AND = {
					indian_state_trigger = yes
					government_rank = 6
				}
				AND = {
					FROM = {
						indian_state_trigger = yes
						government_rank = 6
					}
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = RUSSIANPRINCES
		potential = {
			culture_group = east_slavic
			technology_group = eastern
			NOT = { has_country_flag = relocated_capital_st_petersburg }	
		}
		allow = {
			OR = {
				NOT = { border_distance = { who = FROM distance = 120 } } 
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = RUSSIANPRINCES
		potential = {
			FROM = {
				culture_group = east_slavic
				technology_group = eastern
				NOT = { has_country_flag = relocated_capital_st_petersburg }
			}
		}
		allow = {
			FROM = {
				OR = {
					NOT = { border_distance = { who = ROOT distance = 120 } } 
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = AFRICANSTATE
		potential = {
			african_state_trigger = yes
		}
		allow = {
			OR = {
				is_neighbor_of = FROM
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = AFRICANSTATE
		potential = {
			FROM = {
				african_state_trigger = yes
			}
		}
		allow = {
			FROM = {
				OR = {
					is_neighbor_of = ROOT
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = EMPEROROFAFRICA
		potential = {
			OR = {
				AND = {
					african_state_trigger = yes
					government_rank = 6
				}
				AND = {
					FROM = {
						african_state_trigger = yes
						government_rank = 6
					}
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = MANDALASYSTEM
		potential = {
			mandala_system_state_trigger = yes
		}
		allow = {
			OR = {
				is_neighbor_of = FROM
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = MANDALASYSTEM
		potential = {
			FROM = {
				mandala_system_state_trigger = yes
			}
		}
		allow = {
			FROM = {
				OR = {
					is_neighbor_of = ROOT
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = AMALGAMATION
		potential = {
			OR = {
				government = amalgamation_government
				FROM = { government = amalgamation_government }
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = PIZARROBROTHERS
		potential = {
			OR = {
				FROM = {
					tag = PIZ
				}
				tag = PIZ
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = AMERINDSTATE
		potential = {
			amerind_trigger = yes
		}
		allow = {
			OR = {
				is_neighbor_of = FROM
				culture_group = FROM
			}
		}
	}
	condition = {
		tooltip = AMERINDSTATE
		potential = {
			FROM = {
				amerind_trigger = yes
			}
		}
		allow = {
			FROM = {
				OR = {
					is_neighbor_of = ROOT
					culture_group = ROOT
				}
			}
		}
	}
	condition = {
		tooltip = POWERFULMAGNATES
		potential = {
			has_country_modifier = obstacle_magnates	
		}
		allow = {
			NOT = { border_distance = { who = FROM distance = 20 } }
		}
	}
	condition = {
		tooltip = POWERFULMAGNATESFROM
		potential = {
			FROM = {
				has_country_modifier = obstacle_magnates	
			}
		}
		allow = {
			FROM = {
				NOT = { border_distance = { who = ROOT distance = 20 } }
			}
		}
	}
#	condition = {
#		tooltip = BEYLIK
#		potential = {
#			has_country_modifier = turkish_beylik	
#		}
#		allow = {
#			NOT = { border_distance = { who = FROM distance = 20 } }
#		}
#	}
#	condition = {
#		tooltip = BEYLIKFROM
#		potential = {
#			FROM = {
#				has_country_modifier = turkish_beylik	
#			}
#		}
#		allow = {
#			FROM = {
#				NOT = { border_distance = { who = ROOT distance = 20 } }
#			}
#		}
#	}

	#Tech Restrictions
	#0 Ally
	condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 31 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_worst
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_bad
				}
				AND = {
					NOT = { dip_tech = 48 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_worst
				}
			}
			FROM = {
				OR = {
					government = theocracy	
					NOT = { dynasty = ROOT }				
				}
				#ai = yes
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 31 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_worst
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_bad
					}
					AND = {
						NOT = { dip_tech = 48 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_worst
					}
				}
			}
			OR = {
				government = theocracy	
				NOT = { dynasty = FROM }				
			}
			#ai = yes
		}
		allow = {
			always = no
		}
	}
	#End 1 Ally
	#1 Ally
	condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_worst
					num_of_allies = 1
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_bad
					num_of_allies = 1
				}
				AND = {
					NOT = { dip_tech = 31 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_worst
					num_of_allies = 1
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 1
					NOT = { government_rank = 3 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 1
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_bad
					num_of_allies = 1
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
					#ai = yes
				}
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_worst
						num_of_allies = 1
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_bad
						num_of_allies = 1
					}
					AND = {
						NOT = { dip_tech = 31 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_worst
						num_of_allies = 1
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 1
						NOT = { government_rank = 3 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 1
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_bad
						num_of_allies = 1
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				#ai = yes
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 1 Ally
	#2 Ally
		condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 6
					has_country_modifier = court_worst
					num_of_allies = 2
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_bad
					num_of_allies = 2
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_worst
					num_of_allies = 2
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 3
					NOT = { government_rank = 5 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 2
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_bad
					num_of_allies = 2
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_worst
					num_of_allies = 2
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_better
					num_of_allies = 2
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 1
					NOT = { government_rank = 3 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 2
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_bad
					num_of_allies = 2
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
				}
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}

	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 6
						has_country_modifier = court_worst
						num_of_allies = 2
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_bad
						num_of_allies = 2
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_worst
						num_of_allies = 2
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 3
						NOT = { government_rank = 5 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 2
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_bad
						num_of_allies = 2
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_worst
						num_of_allies = 2
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_better
						num_of_allies = 2
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 1
						NOT = { government_rank = 3 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 2
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_bad
						num_of_allies = 2
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 2 Ally
	#3 Ally
		condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 6
					has_country_modifier = court_bad
					num_of_allies = 3
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 6
					has_country_modifier = court_worst
					num_of_allies = 3
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 5
					NOT = { government_rank = 6 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 3
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_bad
					num_of_allies = 3
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_worst
					num_of_allies = 3
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_better
					num_of_allies = 3
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 3
					NOT = { government_rank = 5 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 3
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_bad
					num_of_allies = 3
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_bad
					num_of_allies = 3
				}
				AND = {
					NOT = { dip_tech = 19 } #2
					government_rank = 1 #4
					NOT = { government_rank = 3 }
					has_country_modifier = court_best #2
					num_of_allies = 3
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_better
					num_of_allies = 3
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 1
					NOT = { government_rank = 3 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 3
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
					#ai = yes
				}
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 6
						has_country_modifier = court_bad
						num_of_allies = 3
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 6
						has_country_modifier = court_worst
						num_of_allies = 3
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 5
						NOT = { government_rank = 6 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 3
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_bad
						num_of_allies = 3
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_worst
						num_of_allies = 3
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_better
						num_of_allies = 3
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 3
						NOT = { government_rank = 5 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 3
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_bad
						num_of_allies = 3
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_bad
						num_of_allies = 3
					}
					AND = {
						NOT = { dip_tech = 19 } #2
						government_rank = 1 #4
						NOT = { government_rank = 3 }
						has_country_modifier = court_best #2
						num_of_allies = 3
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_better
						num_of_allies = 3
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 1
						NOT = { government_rank = 3 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 3
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 3 Ally
	#4 Ally
		condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 6
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 4
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 6
					has_country_modifier = court_bad
					num_of_allies = 4
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 6
					has_country_modifier = court_worst
					num_of_allies = 4
				}
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_better
					num_of_allies = 4
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 5
					NOT = { government_rank = 6 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 4
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_bad
					num_of_allies = 4
				}
				AND = {
					NOT = { dip_tech = 19 } #2
					government_rank = 3 #4
					NOT = { government_rank = 5 }
					has_country_modifier = court_best #2
					num_of_allies = 4
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_better
					num_of_allies = 4
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 3
					NOT = { government_rank = 5 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 4
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 } #2
					government_rank = 1 #4
					NOT = { government_rank = 3 }
					has_country_modifier = court_best #2
					num_of_allies = 4
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 1
					NOT = { government_rank = 3 }
					has_country_modifier = court_better
					num_of_allies = 4
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
					#ai = yes
				}
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 6
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 4
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 6
						has_country_modifier = court_bad
						num_of_allies = 4
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 6
						has_country_modifier = court_worst
						num_of_allies = 4
					}
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_better
						num_of_allies = 4
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 5
						NOT = { government_rank = 6 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 4
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_bad
						num_of_allies = 4
					}
					AND = {
						NOT = { dip_tech = 19 } #2
						government_rank = 3 #4
						NOT = { government_rank = 5 }
						has_country_modifier = court_best #2
						num_of_allies = 4
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_better
						num_of_allies = 4
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 3
						NOT = { government_rank = 5 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 4
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 } #2
						government_rank = 1 #4
						NOT = { government_rank = 3 }
						has_country_modifier = court_best #2
						num_of_allies = 4
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 1
						NOT = { government_rank = 3 }
						has_country_modifier = court_better
						num_of_allies = 4
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				#ai = yes
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 4 ally
	#5 Ally
		condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 19 }
					government_rank = 6
					has_country_modifier = court_better
					num_of_allies = 5
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 6
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 5
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 6
					has_country_modifier = court_bad
					num_of_allies = 5
				}
				AND = {
					NOT = { dip_tech = 19 } #2
					government_rank = 5 #4
					NOT = { government_rank = 6 }
					has_country_modifier = court_best #2
					num_of_allies = 5
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_better
					num_of_allies = 5
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 5
					NOT = { government_rank = 6 }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 5
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 3
					NOT = { government_rank = 5 }
					has_country_modifier = court_better
					num_of_allies = 5
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 } #2
					government_rank = 1 #4
					NOT = { government_rank = 3 }
					has_country_modifier = court_best #2
					num_of_allies = 5
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
					#ai = yes
				}
				#ai = yes
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 19 }
						government_rank = 6
						has_country_modifier = court_better
						num_of_allies = 5
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 6
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 5
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 6
						has_country_modifier = court_bad
						num_of_allies = 5
					}
					AND = {
						NOT = { dip_tech = 19 } #2
						government_rank = 5 #4
						NOT = { government_rank = 6 }
						has_country_modifier = court_best #2
						num_of_allies = 5
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_better
						num_of_allies = 5
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 5
						NOT = { government_rank = 6 }
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 5
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 3
						NOT = { government_rank = 5 }
						has_country_modifier = court_better
						num_of_allies = 5
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 } #2
						government_rank = 1 #4
						NOT = { government_rank = 3 }
						has_country_modifier = court_best #2
						num_of_allies = 5
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 5 Ally
	#6 Ally

	condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					NOT = { dip_tech = 19 } #2
					government_rank = 6 #4
					has_country_modifier = court_best #2
					num_of_allies = 6
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 }
					government_rank = 6
					has_country_modifier = court_better
					num_of_allies = 6
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 6
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_bad }
					NOT = { has_country_modifier = court_worst }
					num_of_allies = 6
				}
				AND = {
					dip_tech = 19
					NOT = { dip_tech = 31 } #2
					government_rank = 5 #4
					NOT = { government_rank = 6 }
					has_country_modifier = court_best #2
					num_of_allies = 6
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 5
					NOT = { government_rank = 6 }
					has_country_modifier = court_better
					num_of_allies = 6
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 } #2
					government_rank = 3 #4
					NOT = { government_rank = 5 }
					has_country_modifier = court_best #2
					num_of_allies = 6
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
					#ai = yes
				}
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}

	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						NOT = { dip_tech = 19 } #2
						government_rank = 6 #4
						has_country_modifier = court_best #2
						num_of_allies = 6
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 }
						government_rank = 6
						has_country_modifier = court_better
						num_of_allies = 6
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 6
						NOT = { has_country_modifier = court_best }
						NOT = { has_country_modifier = court_better }
						NOT = { has_country_modifier = court_bad }
						NOT = { has_country_modifier = court_worst }
						num_of_allies = 6
					}
					AND = {
						dip_tech = 19
						NOT = { dip_tech = 31 } #2
						government_rank = 5 #4
						NOT = { government_rank = 6 }
						has_country_modifier = court_best #2
						num_of_allies = 6
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 5
						NOT = { government_rank = 6 }
						has_country_modifier = court_better
						num_of_allies = 6
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 } #2
						government_rank = 3 #4
						NOT = { government_rank = 5 }
						has_country_modifier = court_best #2
						num_of_allies = 6
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}

	#End 6 Ally
	#7 Ally

	condition = {
		tooltip = TECHRESTRICTION
		potential = {
			OR = {
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 }
					government_rank = 6
					has_country_modifier = court_better
					num_of_allies = 7
				}
				AND = {
					dip_tech = 31
					NOT = { dip_tech = 48 } #2
					government_rank = 5 #4
					NOT = { government_rank = 6 }
					has_country_modifier = court_best #2
					num_of_allies = 7
				}
			}
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
					#ai = yes
				}
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				OR = {
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 }
						government_rank = 6
						has_country_modifier = court_better
						num_of_allies = 7
					}
					AND = {
						dip_tech = 31
						NOT = { dip_tech = 48 } #2
						government_rank = 5 #4
						NOT = { government_rank = 6 }
						has_country_modifier = court_best #2
						num_of_allies = 7
					}
				}
			}
			OR = {
				NOT = { dynasty = FROM }
				#ai = yes
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 7 Ally
	#8 Ally

	condition = {
		tooltip = TECHRESTRICTION
		potential = {
			dip_tech = 31
			NOT = { dip_tech = 48 } #2
			government_rank = 6 #4
			has_country_modifier = court_best #2
			num_of_allies = 8
			OR = {
				FROM = {
					NOT = { dynasty = ROOT }
				}
				FROM = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = NUMBEREXCEDED
		potential = {
			FROM = {
				dip_tech = 31
				NOT = { dip_tech = 48 } #2
				government_rank = 6 #4
				has_country_modifier = court_best #2
				num_of_allies = 8
			}
			OR = {
				NOT = { dynasty = FROM }
				#ai = yes
				ROOT = {
					government = theocracy
				}
			}
		}
		allow = {
			always = no
		}
	}
	#End 8 Ally
	condition = {
		tooltip = HORDEGOV
		potential = {
			OR = {
				government = altaic_monarchy	
				government = steppe_horde	
				FROM = { 
					OR = {
						government = altaic_monarchy
						government = steppe_horde
					}	 
				}
			}		
		}
		allow = {
			always = no
		}	
	}
	condition = {
		tooltip = JAPANSHOGUN
		potential = {
			OR = {
				AND = {
					OR = {
						technology_group = turkishtech
						technology_group = chinese
						technology_group = muslim
					}
					capital_scope = {
						AND = { superregion = japan_superregion NOT = { region = korea_region } }		
					}
				}
				FROM = {
					OR = {
						technology_group = turkishtech
						technology_group = chinese
						technology_group = muslim
					}
					capital_scope = {
						AND = { superregion = japan_superregion NOT = { region = korea_region } }		
					}
				}
			}
		}
		allow = {
			OR = {
				culture_group = FROM
				FROM = {
					culture_group = ROOT
				}
				is_neighbor_of = FROM
				FROM = { is_neighbor_of = ROOT }
			}
		}
	}	
	condition = {
		tooltip = THREEPETERS
		potential = {
			OR = {
				tag = ARA
				tag = CAS
			}	
			
			exists = ENR
			
		}
		allow = {
			FROM = {
				OR = {
					tag = POR
					tag = ARA
					tag = CAS
					tag = ENR
				}
			}
		}	
	}
}
