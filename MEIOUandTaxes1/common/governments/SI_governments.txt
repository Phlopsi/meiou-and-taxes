##############################  Serenissima  ##############################
##############################  Governments  ##############################
# by Ignatich

# Special for the Serenissima
venetian_republic = {
	republic = yes

	color = { 200 130 200 }

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 80

	duration = 0 # rule for life.

	republican_name = yes
	royal_marriage = no

	#boost_income = yes
	can_use_trade_post = yes

	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds

	max_states = -10

	rank = {
		1 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		2 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		5 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		6 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
	}
}

#by Marco Dandolo
# for Medici Florence, Pisa, Rimini and others
signoria_monarchy = {
	monarchy = yes

	color = { 40 200 200 }

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 60

	max_states = 3

	rank = {
		1 = {
			production_efficiency = 0.20
			tolerance_heretic = 1
			legitimacy = -0.20

			mercenary_cost = -0.15
			land_maintenance_modifier = 0.10

			global_autonomy = 0.10
			max_absolutism = -10
		}
		2 = {
			production_efficiency = 0.20
			tolerance_heretic = 1
			legitimacy = -0.20

			mercenary_cost = -0.15
			land_maintenance_modifier = 0.10

			global_autonomy = 0.10
			max_absolutism = -10
		}
		3 = {
			production_efficiency = 0.20
			tolerance_heretic = 1
			legitimacy = -0.20

			mercenary_cost = -0.15
			land_maintenance_modifier = 0.10

			global_autonomy = 0.10
			max_absolutism = -10
		}
		4 = {
			production_efficiency = 0.20
			tolerance_heretic = 1
			legitimacy = -0.20

			mercenary_cost = -0.15
			land_maintenance_modifier = 0.10

			global_autonomy = 0.10
			max_absolutism = -10
		}
		5 = {
			production_efficiency = 0.20
			tolerance_heretic = 1
			legitimacy = -0.20

			mercenary_cost = -0.15
			land_maintenance_modifier = 0.10

			global_autonomy = 0.10
			max_absolutism = -10
		}
		6 = {
			production_efficiency = 0.20
			tolerance_heretic = 1
			legitimacy = -0.20

			mercenary_cost = -0.15
			land_maintenance_modifier = 0.10

			global_autonomy = 0.10
			max_absolutism = -10
		}
	}
}

# by Marco Dandolo
# for Siena and other Republics with "consistories"
siena_republic = {
	republic = yes

	color = { 120 150 200 }

	duration = 5

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 40

	republican_name = yes
	royal_marriage = no

	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds

	max_states = -8

	rank = {
		1 = {
			#stuff in factions

			max_absolutism = -10
		}
		2 = {
			#stuff in factions

			max_absolutism = -10
		}
		3 = {
			#stuff in factions

			max_absolutism = -10
		}
		4 = {
			#stuff in factions

			max_absolutism = -10
		}
		5 = {
			#stuff in factions

			max_absolutism = -10
		}
		6 = {
			#stuff in factions

			max_absolutism = -10
		}
	}
}
