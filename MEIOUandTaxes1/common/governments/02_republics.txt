##############################   Republic    ##############################
##############################  Governments  ##############################

noble_republic = {
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	royal_marriage = yes

	color = { 140 180 220 }

	republican_name = yes

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = administrative_republic
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 0
			OR = {
				government = imperial_city
				government = merchant_imperial_city
			}
		}
		modifier = {
			factor = 5
			has_idea = quality_education
			has_idea = finest_of_horses
			has_idea = oak_forests_for_ships
			has_idea = private_to_marshal
			has_idea = naval_drill
			has_idea = massed_battery
		}
	}

	max_states = 1

	rank = {
		1 = {
			land_morale = 0.1
			land_forcelimit_modifier = 0.10
			tolerance_heretic = 1

			global_autonomy = 0.05
			max_absolutism = -10
		}
		2 = {
			land_morale = 0.1
			land_forcelimit_modifier = 0.10
			tolerance_heretic = 1

			global_autonomy = 0.05
			max_absolutism = -10
		}
		3 = {
			land_morale = 0.1
			land_forcelimit_modifier = 0.10
			tolerance_heretic = 1

			global_autonomy = 0.05
			max_absolutism = -10
		}
		4 = {
			land_morale = 0.1
			land_forcelimit_modifier = 0.10
			tolerance_heretic = 1

			global_autonomy = 0.05
			max_absolutism = -10
		}
		5 = {
			land_morale = 0.1
			land_forcelimit_modifier = 0.10
			tolerance_heretic = 1

			global_autonomy = 0.05
			max_absolutism = -10
		}
		6 = {
			land_morale = 0.1
			land_forcelimit_modifier = 0.10
			tolerance_heretic = 1

			global_autonomy = 0.05
			max_absolutism = -10
		}
	}
}

merchant_republic = {
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 40

	color = { 40 130 200 }

	duration = 4

	republican_name = yes
	royal_marriage = no

	#boost_income = yes
	can_use_trade_post = yes

	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = bureaucratic_despotism
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			OR = {
				government = imperial_city
				government = merchant_imperial_city
			}
		}
		modifier = {
			factor = 5
			has_idea = shrewd_commerce_practise
			has_idea = free_trade
			has_idea = merchant_adventures
			has_idea = national_trade_policy
			has_idea = overseas_merchants
			has_idea = trade_manipulation
			has_idea = fast_negotiations
		}
	}

	max_states = -10

	rank = {
		1 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		2 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		5 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		6 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
	}

}

administrative_republic = {
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 105 170 220 }

	duration = 5

	republican_name = yes
	royal_marriage = no

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = republican_dictatorship
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = noble_republic
		}
		modifier = {
			factor = 0
			OR = {
				government = imperial_city
				government = merchant_imperial_city
			}
		}
		modifier = {
			factor = 5
			has_idea = organised_mercenary_payment
			has_idea = benefits_for_mercenaries
			has_idea = organised_mercenary_recruitment
			has_idea = resilient_state
			has_idea = war_cabinet
		}
	}

	max_states = 3

	rank = {
		1 = {
			land_forcelimit_modifier = -0.15
			defensiveness = 0.10

			global_trade_goods_size_modifier = 0.2
			global_tax_modifier = 0.10

			global_autonomy = -0.05
			max_absolutism = -15
		}
		2 = {
			land_forcelimit_modifier = -0.15
			defensiveness = 0.10

			global_trade_goods_size_modifier = 0.2
			global_tax_modifier = 0.10

			global_autonomy = -0.05
			max_absolutism = -15
		}
		3 = {
			land_forcelimit_modifier = -0.15
			defensiveness = 0.10

			global_trade_goods_size_modifier = 0.2
			global_tax_modifier = 0.10

			global_autonomy = -0.05
			max_absolutism = -15
		}
		4 = {
			land_forcelimit_modifier = -0.15
			defensiveness = 0.10

			global_trade_goods_size_modifier = 0.2
			global_tax_modifier = 0.10

			global_autonomy = -0.05
			max_absolutism = -15
		}
		5 = {
			land_forcelimit_modifier = -0.15
			defensiveness = 0.10

			global_trade_goods_size_modifier = 0.2
			global_tax_modifier = 0.10

			global_autonomy = -0.05
			max_absolutism = -15
		}
		6 = {
			land_forcelimit_modifier = -0.15
			defensiveness = 0.10

			global_trade_goods_size_modifier = 0.2
			global_tax_modifier = 0.10

			global_autonomy = -0.05
			max_absolutism = -15
		}
	}
}

crowned_republic = { #Stadtholder
	republic = yes
	duration = 0 # rule for life.
	valid_for_new_country = no
	alid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 125 150 220 }

	republican_name = no
	royal_marriage = yes

	max_states = 1

	rank = {
		1 = {
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3

			global_autonomy = 0.02
		}
		2 = {
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3

			global_autonomy = 0.02
		}
		3 = {
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3

			global_autonomy = 0.02
		}
		4 = {
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3

			global_autonomy = 0.02
		}
		5 = {
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3

			global_autonomy = 0.02
		}
		6 = {
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3

			global_autonomy = 0.02
		}
	}
}

republican_dictatorship = { #Lord Protector
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 15 105 165 }

	republican_name = yes
	royal_marriage = no

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = constitutional_republic
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = administrative_republic
		}
		modifier = {
			factor = 0
			OR = {
				government = imperial_city
				government = merchant_imperial_city
			}
		}
		modifier = {
			factor = 5
			has_idea = national_arsenal
			has_idea = the_old_and_infirm
			has_idea = the_young_can_serve
			has_idea = enforced_service
			has_idea = ships_penny
			has_idea = expanded_supply_trains
			has_idea = mass_army
		}
	}

	max_states = 2

	rank = {
		1 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			global_regiment_cost = -0.10
			stability_cost_modifier = 0.20

			global_autonomy = 0.05
			max_absolutism = 5
		}
		2 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			global_regiment_cost = -0.10
			stability_cost_modifier = 0.20

			global_autonomy = 0.05
			max_absolutism = 5
		}
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			global_regiment_cost = -0.10
			stability_cost_modifier = 0.20

			global_autonomy = 0.05
			max_absolutism = 5
		}
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			global_regiment_cost = -0.10
			stability_cost_modifier = 0.20

			global_autonomy = 0.05
			max_absolutism = 5
		}
		5 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			global_regiment_cost = -0.10
			stability_cost_modifier = 0.20

			global_autonomy = 0.05
			max_absolutism = 5
		}
		6 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			global_regiment_cost = -0.10
			stability_cost_modifier = 0.20

			global_autonomy = 0.05
			max_absolutism = 5
		}
	}
}

constitutional_republic = { #President
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 65 155 220 }

	duration = 4

	republican_name = yes
	royal_marriage = no

	has_parliament = yes

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = bureaucratic_despotism
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = republican_dictatorship
		}
		modifier = {
			factor = 0
			OR = {
				government = imperial_city
				government = merchant_imperial_city
			}
		}
		modifier = {
			factor = 5
			has_idea = foreign_embassies
			has_idea = claim_fabrication
			has_idea = cabinet
			has_idea = adaptability
			has_idea = benign_diplomats
			has_idea = diplomatic_influence
			has_idea = flexible_negotiation
		}
	}

	max_states = 3

	rank = {
		1 = {
			stability_cost_modifier = -0.20
			production_efficiency = 0.2
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1

			global_autonomy = 0.06
			max_absolutism = -10
		}
		2 = {
			stability_cost_modifier = -0.20
			production_efficiency = 0.2
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1


			global_autonomy = 0.06
			max_absolutism = -10
		}
		3 = {
			stability_cost_modifier = -0.20
			production_efficiency = 0.2
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1


			global_autonomy = 0.06
			max_absolutism = -10
		}
		4 = {
			stability_cost_modifier = -0.20
			production_efficiency = 0.2
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1


			global_autonomy = 0.06
			max_absolutism = -10
		}
		5 = {
			stability_cost_modifier = -0.20
			production_efficiency = 0.2
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1


			global_autonomy = 0.06
			max_absolutism = -10
		}
		6 = {
			stability_cost_modifier = -0.20
			production_efficiency = 0.2
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1


			global_autonomy = 0.06
			max_absolutism = -10
		}
	}
}

bureaucratic_despotism = { #First Council
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 25 65 205 }

	royal_marriage = no

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = revolutionary_republic
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = constitutional_republic
		}
		modifier = {
			factor = 0
			OR = {
				government = imperial_city
				government = merchant_imperial_city
			}
		}
		modifier = {
			factor = 5
			has_idea = bureaucracy
			has_idea = organised_construction
			has_idea = smithian_economics
			has_idea = national_bank
			has_idea = debt_and_loans
			has_idea = centralization
			has_idea = nationalistic_enthusiasm
		}
	}

	max_states = 2

	rank = {
		1 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = 0.20

			global_autonomy = -0.10
		}
		2 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = 0.20

			global_autonomy = -0.10
		}
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = 0.20

			global_autonomy = -0.10
		}
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = 0.20

			global_autonomy = -0.10
		}
		5 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = 0.20

			global_autonomy = -0.10
		}
		6 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = 0.20

			global_autonomy = -0.10
		}
	}
}

oligarchic_republic = { # Pisa or Firenze at the start of the game
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 140 200 240 }

	duration = 8

	republican_name = yes
	royal_marriage = no

	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds

	ai_will_do = {
		factor = 0
	}
	ai_importance = 1

	max_states = -10

	#bonus
	rank = {
		1 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		2 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		5 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		6 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
	}
}

merchant_oligarchic_republic = { # Hansa when formed
	republic = yes

	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0

	color = { 220 205 40 }

	duration = 8

	republican_name = yes
	royal_marriage = no

	can_use_trade_post = yes
	can_form_trade_league = yes

	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds

	ai_will_do = {
		factor = 0
	}
	ai_importance = 1

	max_states = -10

	#bonus
	rank = {
		1 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		2 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		5 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
		6 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
		#stuff in factions
		}
	}
}
