# New personalities cannot be added to this file
# Do not change existing personality names!

human = {
	chance = {
		factor = 0
	}
	
	icon = 1
}

ai_capitalist = {
	chance = {
		factor = 100
		modifier = {
			factor = 10
			has_regency = yes
		}
		modifier = {
			factor = 0.5
			NOT = { adm = 1 }
		}			
		modifier = {
			factor = 0.5
			NOT = { adm = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { adm = 3 }
		}	
		modifier = {
			factor = 1.5
			adm = 4
		}		
		modifier = {
			factor = 1.5
			adm = 5
		}		
		modifier = {
			factor = 1.5
			adm = 6
		}
		modifier = {
			factor = 1.1
			administrative_ideas = 1
		}		
		modifier = {
			factor = 1.1
			trade_ideas = 1
		}			
		modifier = {
			factor = 1.1
			economic_ideas = 1
		}
		modifier = {
			factor = 1.1
			plutocracy_ideas = 1
		}
		modifier = {
			factor = 1.25 
			NOT = { total_development = 30 }
		}
		modifier = {
			factor = 2
			government = merchant_republic
		}	
		modifier = {
			factor = 1.5
			tag = MNG
		}
		modifier = {
			factor = 100
			tag = MAM
			luck = no
		}	
		modifier = {
			factor = 3
			OR = {
				tag = ARA
				tag = CAS
				tag = POR
			}
			NOT = { is_year = 1500 }
		}
		modifier = {
			factor = 2
			OR = {
				culture_group = latin
				culture_group = low_germanic
				culture_group = high_germanic
			}
		}
		modifier = {
			factor = 5
			government = japanese_monarchy
			government_rank = 5
			is_year = 1600
		}
	}
	icon = 2
}


ai_diplomat = {
	chance = {
		factor = 100
		modifier = { #DEI GRATIA
			factor = 3
			has_country_flag = reformer_pope
			government = papal_government
			}
		modifier = {
			factor = 0.5
			NOT = { dip = 1 }
		}			
		modifier = {
			factor = 10
			has_regency = yes
		}
		modifier = {
			factor = 0.5
			NOT = { dip = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { dip = 3 }
		}	
		modifier = {
			factor = 1.5
			dip = 4
		}		
		modifier = {
			factor = 1.5
			dip = 5
		}		
		modifier = {
			factor = 1.5
			dip = 6
		}
		modifier = {
			factor = 1.1
			aristocracy_ideas = 1
		}			
		modifier = {
			factor = 1.1
			spy_ideas = 1
		}
		modifier = {
			factor = 1.1
			diplomatic_ideas = 1
		}	
		modifier = {
			factor = 1.1
			innovativeness_ideas = 1
		}		
		modifier = {
			factor = 1.25
			NOT = { total_development = 30 }
		}	
		modifier = {
			factor = 1.5
			is_emperor = yes
			total_development = 50
		}
		modifier = {
			factor = 1.5
			is_emperor = yes
			total_development = 100
		}				
		modifier = {
			factor = 2
			tag = FRA
		}		
		modifier = {
			factor = 3
			tag = BYZ
		}	
		modifier = {
			factor = 1.5
			tag = MNG
		}
		modifier = {
			factor = 1.5
			tag = KOR
		}
		modifier = {
			factor = 3
			OR = {
				tag = ARA
				tag = CAS
				tag = POR
			}
			NOT = { is_year = 1500 }
		}		
		modifier = {
			factor = 3
			tag = ENG
			NOT = {
				num_of_owned_provinces_with = {
					value = 2
					OR = {
						region = east_france_region
						region = provence_region
						region = languedoc_region
						region = ouest_france_region
						region = high_countries_region
					}
				}
			}
		}
		modifier = {
			factor = 2
			OR = {
				culture_group = latin
				tag = BOH
				tag = FLO
				culture_group = low_germanic
				culture_group = high_germanic
			}
		}	
		modifier = {
			factor = 3
			tag = ATJ
		}	
	}
	icon = 3
}

ai_militarist = {
	chance = {
		factor = 100
		modifier = { #DEI GRATIA
			factor = 3
			has_country_flag = corrupt_pope
			government = papal_government
			}
		modifier = {
			factor = 0
			has_regency = yes
		}	
		modifier = {
			factor = 0.5
			government = republic
		}
		modifier = {
			factor = 0.5
			NOT = { mil = 1 }
		}			
		modifier = {
			factor = 0.5
			NOT = { mil = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { mil = 3 }
		}	
		modifier = {
			factor = 1.5
			mil = 4
		}		
		modifier = {
			factor = 2
			mil = 5
		}		
		modifier = {
			factor = 4
			mil = 6
		}
		modifier = {
			factor = 0.5
			is_emperor = yes
			total_development = 50
		}
		modifier = {
			factor = 0.5
			is_emperor = yes
			total_development = 100
		}
		modifier = {
			factor = 1.1
			leadership_ideas = 1
		}			
		modifier = {
			factor = 1.1
			logistic_ideas = 1
		}
		modifier = {
			factor = 1.1
			quality_ideas = 1
		}
		modifier = {
			factor = 1.1
			quantity_ideas = 1
		}			
#		modifier = {
#			factor = 1.1
#			religious_ideas = 1
#		}		
		modifier = {
			factor = 1.25
			any_neighbor_country = {
				is_rival = ROOT
			}
		}
		modifier = {
			factor = 2
			tag = MCH
		}
		modifier = {
			factor = 2
			tag = CZC
		}
		modifier = {
			factor = 3
			tag = INC
		}
		modifier = {
			factor = 10
			OR = {
				government = native_council
				government = steppe_horde
			}
		}
		modifier = {
			factor = 10
			government = japanese_monarchy
			government_rank = 3 
			NOT = { government_rank = 6 }
			is_year = 1500
		}
		modifier = {
			factor = 10
			has_regency = no
			OR = {
				tag = MOS
				tag = RUS
				tag = TUR
				tag = OTT
			}
		}
		modifier = {
			factor = 10
			has_regency = no
			tag = LIT
			OR = {
				is_subject = no
				is_subject_of_type = tributary_state
			}
			OR = {
				religion = baltic_pagan_reformed
				any_core_province = {
					region = ruthenia_region
					NOT = { owned_by = ROOT }
				}
			}
		}
		modifier = {
			factor = 10
			has_regency = no
			tag = MUG
			OR = {
				is_subject = no
				is_subject_of_type = tributary_state
			}
			any_province = {
				province_group = india_charter
				NOT = { owned_by = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			tag = FRA
		}			
		modifier = { # Mod
			factor = 0.5
			tag = MNG
			num_of_cities = 20
		}
		# Mod
		modifier = {
			factor = 2
			tag = TAU
		}
		modifier = {
			factor = 0.5
			tag = AYU
		}		
		modifier = {
			factor = 0.0
			OR = {
				tag = KOR
				tag = JOS
			}
		}		
		modifier = {
			factor = 0.0
			tag = KHM
		}		
		modifier = {
			factor = 0.0
			OR = {
			government = imperial_city
			government = merchant_imperial_city
		}
		}		
		modifier = {
			factor = 0.0
			government = amalgamation_government
		}	
		modifier = {
			factor = 10
			tag = TIM
		}
	}
	icon = 4
}

ai_colonialist = {
	chance = {
		factor = 100	
		modifier = {
			factor = 0
			NOT = { num_of_total_ports = 1 }
			NOT = { any_owned_province = { has_empty_adjacent_province = yes } }
		}
		modifier = {
			factor = 0.5
			has_regency = yes
		}		
		modifier = {
			factor = 0.5
			NOT = { adm = 1 }
		}			
		modifier = {
			factor = 0.5
			NOT = { adm = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { adm = 3 }
		}	
		modifier = {
			factor = 1.5
			adm = 4
		}		
		modifier = {
			factor = 1.5
			adm = 5
		}		
		modifier = {
			factor = 1.5
			adm = 6
		}	
		modifier = {
			factor = 1.1
			trade_ideas = 1
		}			
		modifier = {
			factor = 1.1
			exploration_ideas = 1
		}			
		modifier = {
			factor = 1.1
			merchant_marine_ideas = 1
		}
		modifier = {
			factor = 1.1
			expansion_ideas = 1
		}		
		modifier = {
			factor = 0
			NOT = { num_of_colonists = 1 }
		}
		modifier = {
			factor = 2
			num_of_colonists = 4
		}
		modifier = {
			factor = 2
			num_of_colonists = 7
		}
		modifier = {
			factor = 3
			tag = POR
		}
		modifier = {
			factor = 5
			OR = {
				tag = ARA
				tag = CAS
				tag = SPA
			}
		}
		modifier = {
			factor = 2
			is_colonial_nation = yes
		}
	}
	icon = 5
}

ai_balanced = {
	chance = {
		factor = 100
		modifier = { #DEI GRATIA
			factor = 3
			has_country_flag = secular_pope
			government = papal_government
			}
		modifier = {
			factor = 1.25
			always = yes # higher base chance
		}	
		modifier = {
			factor = 0.5
			tag = MNG
		}	
		modifier = {
			factor = 100
			ai = no
		}
		modifier = {
			factor = 0
			has_regency = yes
		}		
		modifier = {
			factor = 0.5
			NOT = {
				adm = 1
				dip = 1
				mil = 1
			}
		}
		modifier = {
			factor = 0.5
			NOT = {
				adm = 2
				dip = 2
				mil = 2
			}
		}
		modifier = {
			factor = 0.5
			NOT = {
				adm = 3
				dip = 3
				mil = 3
			}
		}	
		modifier = {
			factor = 1.5
			adm = 4
			dip = 4
			mil = 4
		}
		modifier = {
			factor = 1.5
			adm = 5
			dip = 5
			mil = 5
		}		
		modifier = {
			factor = 1.5
			adm = 6
			dip = 6
			mil = 6
		}
	}
	icon = 6
}