#1583 - Ovambo

culture = khoisan
religion = animism
capital = "Ovambo"
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 5
native_ferocity = 2
native_hostileness = 7
trade_goods = unknown # slaves
hre = no

1488.1.1 = { discovered_by = POR } # Bartolomeu Dias
