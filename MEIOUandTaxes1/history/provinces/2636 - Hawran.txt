
owner = MAM
controller = MAM
culture = levantine
religion = sunni
capital = "Adhri'at"
trade_goods = salt
hre = no
base_tax = 12
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1133.1.1 = { town_hall = yes }
1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MAM
	add_core = BHA
}
1500.3.3   = {
	base_tax = 14
	base_production = 2
}
1516.1.1   = { add_core = TUR }
1516.11.8  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = BHA
	controller = BHA
	remove_core = TUR
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}

#1831.1.1 = {
#	controller = EGY
#}
#1833.6.1 = {
#	owner = EGY
#}
