# No previous file for Kolwezi

owner = CKW
controller = CKW
add_core = CKW
culture = chokwe
religion = animism
capital = "Kolwezi"
trade_goods = livestock
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
hre = no
discovered_by = central_african
