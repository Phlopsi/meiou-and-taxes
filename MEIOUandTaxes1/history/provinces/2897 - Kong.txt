# 2897 - Kong

owner = KNG
controller = KNG
culture = akaa
religion = west_african_pagan_reformed
capital = "Kong"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = "ivory_low"
		duration = -1
	}
}
1356.1.1   = {
	add_core = KNG
}
