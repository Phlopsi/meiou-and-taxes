#file name: 1190 - Mzizima (dar es salam)
#LS - african setup alpha 6

owner = KIL
controller = KIL
add_core = KIL
culture = kimgao
religion = sunni
capital = "Kilwa"
base_tax = 5
base_production = 3
base_manpower = 0
trade_goods = slaves
discovered_by = east_african
discovered_by = muslim
discovered_by = indian

hre = no
1300.1.1 = {
	harbour_infrastructure_2 = yes
	merchant_guild = yes
	urban_infrastructure_1 = yes
}
1498.3.16 = { discovered_by = POR } #Vasco Da Gama
1520.1.1  = {
	#owner = POR
	#controller = POR
	#add_core = POR
	base_tax = 7
	
}
1600.1.1  = { discovered_by = TUR }
1763.1.1  = { unrest = 7 }
1784.1.1  = {
	owner = OMA
	controller = OMA
	add_core = OMA
} #Omanis impose direct rule in Kilwa
1856.6.1 = {
	owner = ZAN
	controller = ZAN
   	remove_core = OMA
} # Said's will divided his dominions into two separate principalities, with Thuwaini to become the Sultan of Oman and Majid to become the first Sultan of Zanzibar.

