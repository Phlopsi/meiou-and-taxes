# 2414 - Ekab

owner = CIZ
controller = CIZ
add_core = CIZ
culture = yucatecan
religion = mesoamerican_religion
capital = "Coba"

base_tax = 32
base_production = 2
base_manpower = 2
citysize = 5000
trade_goods = cotton


discovered_by = mesoamerican

hre = no

1000.1.1 = {
	town_hall = yes
	marketplace = yes
	harbour_infrastructure_2 = yes
	temple = yes
}
1517.1.1   = {
	discovered_by = SPA
}
1546.1.1   = {
	owner = SPA
	controller = SPA
	capital = "Cancun"
	citysize = 1000
} #Pedro de Alvanado
1571.1.1   = {
	add_core = SPA
}
1596.1.1   = {
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
