# No previous file for Weskarini

culture = innu
religion = totemism
capital = "Weskarini"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 55
native_ferocity = 1 
native_hostileness = 6

1535.1.1  = { discovered_by = FRA } # Jacques Cartier
1634.7.4  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		citysize = 255
	    } # Founding of Trois-Rivi�res
1639.1.1  = { unrest = 5 } # Settlement created by J�r�me Le Royer, under constant attacks from the Iroquois
1650.1.1  = {  citysize = 1488 trade_goods = fur } # Centre for fur trade
1659.1.1  = { add_core = FRA }
1664.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 1850 }
1701.1.1  = { unrest = 0 } # Peace treaty
1707.5.12  = { discovered_by = GBR }
1750.1.1  = { citysize = 2400 }
1760.1.1  = { controller = GBR } # The government of New France capitulate at Montreal
1763.2.10 = {	owner = GBR
		remove_core = FRA
	    } # Treaty of Paris
1788.2.10  = { add_core = GBR }
1800.1.1  = { citysize = 8000 }
