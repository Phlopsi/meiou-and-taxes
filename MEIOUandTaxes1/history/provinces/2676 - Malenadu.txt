# 2676 - Malenadu

owner = VIJ
controller = VIJ
culture = tuluva
religion = hinduism
capital = "Ikkeri"
trade_goods = lumber #naval_supplies
hre = no
base_tax = 9
base_production = 3
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
add_local_autonomy = 25

1100.1.1 = {
	urban_infrastructure_1 = yes
	workshop = yes
}
1356.1.1 = {
	add_core = VIJ
	add_core = KLN
}
1498.1.1 = { discovered_by = POR }
1511.1.1 = {
	base_tax = 12
}
1530.1.1 = {
	#owner = KLN
	#controller = KLN
	add_core = KLN
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1565.7.1 = {
	owner = KLN
	controller = KLN
	remove_core = VIJ
} #Independent Nayaka after Talikota
1573.1.1 = {
	controller = BIJ
} #Bijapuri expansion
1574.1.1 = {
	controller = KLN
} #Bijapuri expansion
1660.1.1 = {
	owner = BIJ
	controller = BIJ
}
1685.1.1 = {
	controller = MUG
} # Conquered by the Mughal emperor Aurangzeb
1686.1.1 = {
	owner = MYS	#Beaten by the mughals and sold to Mysore
	controller = MYS
 }
1756.1.1 = {
	owner = MAR
	controller = MAR
	add_core = MAR
} # To Marathas
1763.1.1 = {
	owner = MYS	
	controller = MYS
}
