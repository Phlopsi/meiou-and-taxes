# 53 - L�neburg

owner = LUN
controller = LUN
culture = old_saxon
religion = catholic
capital = "L�neburg"
trade_goods = salt
hre = yes
base_tax = 5
base_production = 1
base_manpower = 0
is_city = yes
add_core = LUN
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1000.1.1 = {
	town_hall = yes
	workshop = yes
	local_fortification_1 = yes
}

1500.1.1 = { road_network = yes }
1529.1.1 = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1650.1.1 = { fort_14th = yes }
1705.1.1 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = LUN
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = WES
} # Congress of Vienna, Luneburg is incorporated into the Kingdom of Hannover
