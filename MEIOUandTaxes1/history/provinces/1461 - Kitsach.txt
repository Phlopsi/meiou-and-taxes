# No previous file for Kitsach

culture = apache
religion = totemism
capital = "Kitsach"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5
