# 4072 - Azayi

owner = MOR
controller = MOR
culture = tamazight
religion = sunni
capital = "Kenifra"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = olive
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}

1356.1.1 = {
	add_core = MOR
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1530.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1554.1.1   = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1613.1.1 = { unrest = 0 }
1659.1.1 = { unrest = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { unrest = 3}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
