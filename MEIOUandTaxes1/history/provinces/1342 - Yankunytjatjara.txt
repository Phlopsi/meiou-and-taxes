# 1342 - Yankunytjatkara

culture = aboriginal
religion = polynesian_religion
capital = "Yankunytjatjara"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 0.5
native_hostileness = 1
