# No previous file for Monongahela

culture = shawnee
religion = totemism
capital = "Monogahela"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

450.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "pittsburgh_confluence"
		duration = -1
	}
}

1650.1.1 = { owner = XXX # HUR
	controller = XXX # HUR
	# add_core = XXX # HUR
	is_city = yes
	trade_goods = fur} #Huron flees to the Erie
1656.1.1 = { 	owner = XXX # IRO
	controller = XXX # IRO
	culture = iroquois 
} #Iroquois drive off other natives in Beaver Wars
1671.1.1 = { discovered_by = ENG } # Abraham Wood
1707.5.12 = { discovered_by = GBR }
1754.4.18 = {	owner = FRA
	controller = FRA
	citysize = 400
	culture = francien
	  	religion = catholic
}# Construction of Fort Duquesne
1763.2.10 = {	owner = GBR
	controller = GBR
	culture = english
	religion = protestant
}# Treaty of Paris - ceded to Britain, France gave up its claim
1763.3.1 = { unrest = 6 } # Native discontent with the British rule
1763.10.9 = {	owner = XXX # IRO
	controller = XXX # IRO
	# add_core = XXX # IRO
	culture = iroquois
	religion = totemism }
1768.11.5 = {	owner = GBR
	controller = GBR
	culture = american
	religion = protestant } #Treaty of Fort Stanwix, ceded by Iroquois and soon after colonized }
1774.1.1 = {	citysize = 1000 capital = "Fort Henry" }
1776.7.4 = {	owner = USA
	controller = USA
}# Declaration of independence
1782.11.1 = {	unrest = 0  } # Preliminary articles of peace, the British recognized Amercian independence
1801.7.4 = {	add_core = USA }
