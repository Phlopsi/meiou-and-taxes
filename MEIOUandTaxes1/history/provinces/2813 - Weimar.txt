# 2813 - Weimar

owner = THU
controller = THU
culture = thuringian
religion = catholic
capital = "Weimar"
trade_goods = wheat
hre = yes
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
add_core = THU
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = {
	local_fortification_1 = yes
}

1440.1.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Duchy of Thuringia is inherited by Saxony
1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 4
	base_production = 1
}
1530.1.1   = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1546.1.1   = { fort_14th = yes }
1547.5.19   = { 
	owner = THU
	controller = THU 
	add_core = THU
	remove_core = SAX
}

1572.11.6   = { 
	owner = SWR 
	controller = SWR 
	add_core = SWR 
	remove_core = THU
}

1660.1.1   = {  }
1790.8.1   = { unrest = 5 } # Peasant revolt
1791.1.1   = { unrest = 0 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
