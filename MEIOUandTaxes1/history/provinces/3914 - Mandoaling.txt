# 3914 - Mandoaling

owner = MKP
controller = MKP
add_core = MKP
culture = batak
religion = vajrayana
capital = "Mandoaling"
trade_goods = rice
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1420.1.1   = {
	religion = sunni
}
1501.1.1 = {
	base_tax = 3
}
1515.2.1 = { training_fields = yes }
1524.1.1 = { owner = ATJ controller = ATJ add_core = ATJ }
1600.1.1 = { religion = sunni }
1688.1.1 = { add_core = NED }
1825.1.1 = { owner = NED controller = NED unrest = 2 } # The Dutch gradually gained control
