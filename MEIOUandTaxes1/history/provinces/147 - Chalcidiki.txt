owner = BYZ
controller = BYZ
culture = greek
religion = orthodox
capital = "Thessalonike"
trade_goods = silver # olive
hre = no
base_tax = 5
base_production = 4
base_manpower = 1
is_city = yes
fort_14th = yes

estate = estate_church

discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_glassware
		duration = -1
	}
}

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "macedonia_natural_harbour"
		duration = -1
	}
}

1100.1.1 = {
	merchant_guild = yes
	harbour_infrastructure_2 = yes
	urban_infrastructure_1 = yes
	workshop = yes
	constable = yes
}

1300.1.1 = {
	paved_road_network = yes 
}
1356.1.1   = {
	add_core = BYZ
	add_local_autonomy = 15
}
1390.1.1  = {
	add_core = TUR
	remove_core = OTT
}
1423.1.1 = {
	owner = VEN
	controller = VEN
	add_local_autonomy = -15
} # Ceded to Venice
1430.4.1  = {
	owner = TUR
	controller = TUR
	remove_core = SER
	capital = "Selanik"
} # Captured after a three days siege
1453.5.29 = {
	remove_core = BYZ
} # Constantinople has fallen
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1515.2.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1520.5.5 = {
	base_tax = 9
	base_production = 3
	base_manpower = 1
}
1522.3.20 = {  naval_arsenal = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes fort_14th = no fort_15th = yes
}
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0 }
1566.1.1  = { fort_14th = yes }
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR  } # Murad tries to quell the corruption

1710.1.1  = {  }
1750.1.1  = { add_core = GRE }
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	controller = TUR
}
