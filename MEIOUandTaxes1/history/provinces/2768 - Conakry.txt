# 2768 - Conakry

culture = dyola
religion = west_african_pagan_reformed
capital = "Konakiri"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 50
native_ferocity = 4.5
native_hostileness = 9

discovered_by = sub_saharan

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "conakry_natural_harbour" 
		duration = -1 
		}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1456.1.1   = { discovered_by = POR } #Cadamosto

1885.1.1   = {
	owner = GBR
	controller = GBR
	add_core = GBR
	capital = "Conakry"
	citysize = 200
		trade_goods = fish
}
