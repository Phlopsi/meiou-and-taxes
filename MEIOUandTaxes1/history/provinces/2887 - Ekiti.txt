# 2887 - Ekiti

owner = IFE
controller = IFE
culture = yorumba
religion = west_african_pagan_reformed
capital = "Ekiti"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	owner = OYO
	controller = OYO
	add_core = OYO
	add_core = IFE
}
1520.1.1 = { unrest = 4 } #Benin raids and claimst to loyalty of local Yoruba chiefs
1525.1.1 = { unrest = 0 }
1585.1.1 = { unrest = 4 } #Nupe Raids
1590.1.1 = { unrest = 0 }
1774.1.1 = {
	owner = IFE
	controller = IFE
} #Ife rulers break Oyo domination
