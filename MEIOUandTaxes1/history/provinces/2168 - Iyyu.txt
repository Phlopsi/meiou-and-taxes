# No previous file for Iyyu

culture = cree
religion = totemism
capital = "Iyyu"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 5

1000.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = nelson_eastuary_modifier
		duration = -1
	}
}
1611.1.1 = { discovered_by = ENG } # Henry Hudson
1682.1.1 = {
	owner = ENG
	controller = ENG
	culture = english
	religion = protestant
	citysize = 100
}# Construction of Fort Nelson
1699.1.1 = { discovered_by = FRA } # Pierre le Moyne
1707.1.1 = { add_core = ENG }
1707.5.12 = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1 = { citysize = 880 trade_goods = fur }
1800.1.1 = { citysize = 1144 }
