# 1169 - Lesotho

culture = zulu
religion = animism
capital = "Lesotho"
trade_goods = unknown # slaves
base_tax = 3
base_production = 0
base_manpower = 0
native_size = 7
native_ferocity = 8
native_hostileness = 7
hre = no
