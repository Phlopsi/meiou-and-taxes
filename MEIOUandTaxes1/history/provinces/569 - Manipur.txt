#Province: Manipur
#file name: 569 - Manipur
#MEIOU-FB India 1337+ mod Aug 08

owner = MLB
controller = MLB
culture = meitei
religion = adi_dharam
capital = "Imphal"
trade_goods = rice
base_tax = 9
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = indian
discovered_by = chinese

1356.1.1 = { add_core = MLB }
1511.1.1 = {
	base_tax = 12
}
