# No previous file for Marca Firmana

owner = FRM
controller = FRM
culture = umbrian 
religion = catholic 
capital = "Fermo" 
base_tax = 3
base_production = 3  
base_manpower = 0
is_city = yes    
trade_goods = wine

small_university = yes	# University of Macerata
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no
500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1300.1.1 = {
	urban_infrastructure_1 = yes
	road_network = yes
	workshop = yes
	harbour_infrastructure_1 = yes
	local_fortification_1 = yes
}
1356.1.1 = {
	add_core = FRM
}
1530.1.1 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
	fort_14th = yes 
}
1550.1.1   = {
	owner = PAP
	controller = PAP
	add_core = PAP
} # Camerino falls under direct control of the Papal administration
1805.3.17 = { owner = ITE controller = ITE add_core = ITE } # Treaty of Pressburg
1814.4.11 = { owner = PAP controller = PAP remove_core = ITE } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
