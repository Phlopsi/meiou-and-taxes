# No previous file for Ganganagar

owner = BIK
controller = BIK
culture = jati
religion = hinduism
capital = "Ganganagar"
trade_goods = wool
hre = no
base_tax = 10
base_production = 0
base_manpower = 0

add_local_autonomy = 25
add_core = BIK
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech


1511.1.1 = {
	base_tax = 13
}
1530.1.1 = { 
	add_core = TRT
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1544.1.1 = {
	owner = BNG
	controller = BNG
} # Sur expansion
1545.1.1 = {
	owner = BIK
	controller = BIK
} # Independence regained after death of Sher Shah
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
