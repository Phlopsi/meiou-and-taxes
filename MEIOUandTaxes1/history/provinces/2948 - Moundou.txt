# 2948 - Moundou

owner = KBO
controller = KBO
culture = kanouri		
religion = animism		 
capital = "Moundou"
base_tax = 8
base_production = 0
base_manpower = 0
citysize = 5000
trade_goods = millet
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
	add_core = KBO
}
1520.1.1 = {
	base_tax = 10
}
