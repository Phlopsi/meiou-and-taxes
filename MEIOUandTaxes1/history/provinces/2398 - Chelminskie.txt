# 2398 - Chelmniskiye

owner = TEU
controller = TEU
culture = polish
religion = catholic
capital = "Chelmno"					# (Polish), Kulm (German)
base_tax = 4
base_production = 1
base_manpower = 0
is_city = yes
fort_14th   = yes
trade_goods = lumber
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
hre = no

1300.1.1 = {
	road_network = yes
	marketplace = yes
	constable = yes
}
1356.1.1   = {
	add_core = TEU
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1466.10.19 = {
	owner = POL
	controller = POL
	add_core = POL
	remove_core = TEU
} # Peace treaty, "Peace of Torun"

1490.1.1   = { unrest = 6 } # Uprising led by Mukha
1492.1.1   = { unrest = 0 }
1520.5.5 = {
	base_tax = 6
}
1525.4.10  = {
	#add_core = PRU
}
1530.1.4  = {
	bailiff = yes	
}
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1   = { controller = REB } # Civil war
1589.1.1   = { controller = PLC } # Coronation of Sigismund III

1596.1.1   = { unrest  = 4 } # Religious struggles, union of Brest
1597.1.1   = { unrest = 0 }
1733.1.1   = { controller = REB } # The war of Polish succession
1735.1.1   = { controller = PLC }
1772.8.5 = {
	controller = PRU
	owner = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # First partition
1807.7.9   = {
	owner = POL
	controller = POL
     	remove_core = PRU
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1815.6.9   = {
	owner = PRU
	add_core = PRU
	controller = PRU
} # Congress Poland, under Russian control after the Congress of Vienna
