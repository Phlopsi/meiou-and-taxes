# 4073 - Anfa

owner = MOR
controller = MOR
culture = fassi
religion = sunni
capital = "Anfa"
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no


1088.1.1 = { harbour_infrastructure_1 = yes }

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}

1356.1.1 = {
	add_core = MOR
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1502.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
}
1530.1.1 = {
	add_core = MOR
}
1530.1.2 = {
	remove_core = FEZ
}
1554.1.1   = {
	remove_core = FEZ
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1613.1.1 = { unrest = 0 }
1659.1.1 = { unrest = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { unrest = 3}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
1769.1.1 = {
	owner = MOR
	controller = MOR
	remove_core = POR
}
