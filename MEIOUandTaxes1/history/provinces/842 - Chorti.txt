# 842 - Chorti
# GG - 22/07/2008

owner = QUM
controller = QUM
add_core = QUM
culture = quichean
religion = mesoamerican_religion
capital = "Quirigu�"
base_tax = 5
base_production = 0
base_manpower = 0
trade_goods = cacao
hre = no

discovered_by = mesoamerican

1517.1.1   = {
	discovered_by = SPA
}
1525.1.1 = {
	base_tax = 2
}
1546.1.1   = {
	owner = SPA
	controller = SPA
	capital = "San Gil de Buena Vista"
	citysize = 1000
} #Pedro de Alvanado
1571.1.1   = {
	add_core = SPA
}
1596.1.1   = {
	culture = castillian
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1823.7.10  = {
	owner = CAM
	controller = CAM
	remove_core = MEX
}
