# 2255 - sichuan_area Barkam

owner = YUA
controller = YUA
culture = tibetan
religion = vajrayana
capital = "Songpan"
trade_goods = wool
hre = no
base_tax = 11
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = chinese
discovered_by = steppestech

1225.1.1  = {
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1280.1.1 = { #Warlord state Qin
	add_core = QIN
	add_core = MDO
}
1351.1.1  = {
	owner = CTA
	controller = CTA
    add_core = CTA
	add_core = XIA
}		


1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = QIN
	remove_core = XIA
}
1520.2.2 = {
	base_tax = 14
	base_production = 3
	base_manpower = 1
}
1643.11.1  = {
	owner = DSH
	controller = DSH
	add_core = DSH
}

1645.3.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
	remove_core = DSH
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
