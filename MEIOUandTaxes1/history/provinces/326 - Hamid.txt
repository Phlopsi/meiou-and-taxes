# 326 - Hamid
# MEIOU-GG - Turko-Mongol mod

owner = HMD
controller = HMD
culture = yorouk
religion = sunni
capital = "Burdur"
trade_goods = cotton
hre = no
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1  = {
	add_core = HMD
	add_claim = TKE
	set_province_flag = turkish_name
}
1381.1.1  = {
	owner = OTT
	controller = OTT
	add_core = OTT
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	culture = turkish
	remove_core = OTT
}
1402.1.1  = {
	owner = HMD
	controller = HMD
} # Tamerlane defeat the Ottomans in Angora (Ankara)
1403.1.1  = {
	owner = TUR
	controller = TUR
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = HMD } ###
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1515.1.1 = { training_fields = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1700.1.1  = { fort_14th = yes  }
