# 2713 - Mahi Kantha

owner = MAW
controller = MAW
culture = bhil
religion = hinduism
capital = "Idar"
trade_goods = wool
hre = no
base_tax = 22
base_production = 0
base_manpower = 1
citysize = 7000
add_core = MAW
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = idar_state
		duration = -1
	}
	add_core = IDR
}
1120.1.1 = { textile = yes }
1438.1.1 = {
	owner = MEW
	controller = MEW
} #Conquered by Mewar after Rathore, Sisodiya cooperation breaks down in plots and murder.
1450.1.1   = { citysize = 9000 }
1459.1.1 = {
	owner = GUJ
	controller = GUJ
	add_core = GUJ
}

1485.1.1   = {
	controller = GUJ
}
1485.11.21 = {
	owner = GUJ
	remove_core = MAW
}
1498.1.1   = {
	discovered_by = POR
}
1500.1.1   = { citysize = 10000 }
1511.1.1 = {
	base_tax = 27
	base_production = 1
	town_hall = yes
}
1530.1.1 = {
	add_core = GUJ
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1544.1.1   = {
	owner = BNG
	controller = BNG
} # Sur expansionism
1545.1.1   = {
	owner = GUJ
	controller = GUJ
} # Death of Sher Shah
1550.1.1   = { citysize = 9000 }
1573.6.1   = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by Akbar
1600.1.1   = { citysize = 8000 }
1650.1.1   = { citysize = 12000 }
1700.1.1   = { citysize = 7000 }
1721.1.1   = {
	owner = GAK
	controller = GAK
	add_core = GAK
}
1750.1.1   = { citysize = 8000 }
1800.1.1   = { citysize = 9000 }
