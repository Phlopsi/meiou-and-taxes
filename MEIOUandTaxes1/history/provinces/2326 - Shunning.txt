# 2326 - yunnan_area Jinghong

owner = DLI
controller = DLI
add_core = DLI
culture = shan
religion = buddhism
capital = "Shunning"

base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = tea
hre = no

discovered_by = chinese
discovered_by = steppestech


1253.1.1 = { owner = YUA controller = YUA }
1274.1.1 = { add_core = YUA } 			#creation of yunan province
1350.1.1 = { owner = DLI controller = DLI }
1383.1.1 = { 
	owner = MNG 
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 12
}

1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
