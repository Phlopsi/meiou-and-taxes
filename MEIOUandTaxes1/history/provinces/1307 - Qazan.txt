# 1307 - Qazan

owner = WHI
controller = WHI       
culture = kazani
religion = sunni 
capital = "Bolgar"
base_tax = 12
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = livestock

discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

500.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "kazan_confluence"
		duration = -1
	}
}
600.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_medium"
		duration = -1
	}
}
1000.1.1 = {
	marketplace = yes
	temple = yes
	workshop = yes
	town_hall = yes
}
1356.1.1 = {
	add_core = WHI
	add_core = KAZ
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1438.1.1  = {
	owner = KAZ
	controller = KAZ
}
1444.1.1 = {
	remove_core = GOL
}

1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = {
	base_production = 2
}
1521.1.1 = { base_tax = 15 }
1552.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
	rename_capital = "Qazan"
}
1600.1.1   = {
#	culture = russian
#	religion = orthodox
	remove_core = KAZ
}
