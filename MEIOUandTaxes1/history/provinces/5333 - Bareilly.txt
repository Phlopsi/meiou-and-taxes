# No previous file for Bareilly

owner = AHM
controller = AHM
culture = avadhi
religion = sunni
capital = "Bareilly"
trade_goods = cotton #Calicoes
hre = no
base_tax = 43
base_production = 0
base_manpower = 4
is_city = yes
# 
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
discovered_by = indian


1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1 = {
	#add_core = DLH
	add_core = AHM
	unrest = 6
	#fort_14th = yes
}
1399.1.1 = {
	owner = AHM
	controller = AHM
	unrest = 0
} #guessed date for independance from Delhi Sultanate
1444.1.1 = {
	add_core = DLH
}
1483.1.1  = {
	controller = DLH
	owner = DLH
}
1511.1.1 = {
	base_tax = 55
}
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
} # Battle of Panipat
1528.1.1 = { road_network = yes }
1530.1.2 = { add_core = TRT }
1540.1.1  = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1550.1.1  = {
	capital = "Rampur"
}
1553.1.1  = {
	owner = DLH
	controller = DLH
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23 = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1565.1.1  = {
	controller = REB
	revolt = { type = noble_rebels }
} #Revolt of Uzbek commanders
1566.6.1  = {
	controller = MUG
	revolt = { }
}
1623.1.1  = {
	 culture = pashtun
} #Rohilas starts to settle here

1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1721.1.1  = {
	owner = RMP
	controller = RMP
	remove_core = MUG
} # Rohilkandh independent
1757.1.1  = {
	owner = ODH
	controller = ODH
} # Oudh
1761.1.1  = {
	owner = RMP
	controller = RMP
} # Rohilkandh independent
1794.1.1  = {
	owner = ODH
	controller = ODH
} # Oudh
1801.1.1  = {
	owner = GBR
	controller = GBR
} # Great Britain
