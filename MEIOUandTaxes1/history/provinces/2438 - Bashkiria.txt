# 2437 - Tara

owner = WHI
controller = WHI
culture = bashkir
religion = sunni 
capital = "Ufa"
base_tax = 4
base_production = 0
base_manpower = 0
trade_goods = lumber
is_city = yes
discovered_by = steppestech
discovered_by = eastern
discovered_by = muslim

hre = no

1200.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_low"
		duration = -1
	}
}
1356.1.1 = {
	add_core = WHI
	add_core = KAZ
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1438.1.1  = {
	owner = KAZ
	controller = KAZ
}
1444.1.1 = {
	remove_core = GOL
}

1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1530.1.4  = {
	bailiff = yes	
}
1552.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
}
1600.1.1   = {
#	culture = russian
#	religion = orthodox
	remove_core = KAZ
}
