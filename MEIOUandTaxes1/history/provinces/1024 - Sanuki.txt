# 1024 - Sanuki
# GG/LS - Japanese Civil War

owner = HKW
controller = HKW
culture = shikoku
religion = mahayana #shinbutsu
capital = "Takamatsu"
trade_goods = fish #linen
hre = no
base_tax = 17
base_production = 0
base_manpower = 2
is_city = yes
discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}

1356.1.1 = {
	add_core = HKW
	add_core = MIY
	add_core = CSK
	local_fortification_1 = yes
}
1450.1.1 = {
	owner = MIY
	controller = MIY
}
1501.1.1 = {
	base_tax = 30
	base_production = 1
}
1542.1.1 = { discovered_by = POR }
1550.1.1 = {
	owner = CSK
	controller = CSK
}
1585.1.1 = {
	owner = ODA
	controller = ODA
} # Hashiba (later Toyotomi) Hideyoshi invaded Shikoku with a force of 100,000 men, Chosokabe Motochika keeps only Tosa
1600.9.15  = {
	owner = TGW
	controller = TGW
} # Battle of Sekigahara
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
