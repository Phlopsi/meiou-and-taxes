# 1431 - Dniepr

owner = WHI
controller = WHI
culture = crimean
religion = sunni
capital = "Kyzykermen"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	add_core = WHI
	add_core = CRI
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1427.1.1 = {
	owner = CRI
	controller = CRI
}
1444.1.1 = {
	remove_core = GOL
}
#1449.1.1 = {
#	owner = LIT
#	controller = LIT
#	add_core = LIT
#	remove_core = CRI
#}
#1475.1.1 = {
#	add_core = TUR
#}
1501.1.1 = {
	base_tax = 4
}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1774.7.21 = {
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
}
