# No previous file for Kigoma

owner = UBH
controller = UBH
add_core = UBH
culture = rwandan #Ha
religion = animism
trade_goods = salt
capital = "Ruguru"
hre = no
base_tax = 6
base_production = 0
base_manpower = 0

discovered_by = central_african

1520.1.1 = { base_tax = 8 }