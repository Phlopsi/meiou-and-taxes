# 3821 - Neubandenburg

owner = MST
controller = MST
culture = pommeranian
religion = catholic
hre = yes
base_tax = 4
base_production = 0
trade_goods = wheat#linen
base_manpower = 0
is_city = yes
capital = "Neubrandenburg"
discovered_by = eastern
discovered_by = western
discovered_by = muslim
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
	}
	local_fortification_1 = yes
}
1356.1.1   = {
	add_core = MST
}
1471.7.13 = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1500.1.1 = { road_network = yes }
1530.1.1   = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
