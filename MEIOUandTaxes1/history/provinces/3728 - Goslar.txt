# 3728 - Goslar

owner = GTG
controller = GTG
add_core = GTG
culture = eastphalian
religion = catholic
capital = "Goslar"
trade_goods = lead
base_tax = 4
base_production = 1
base_manpower = 0
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
1100.1.1 = {
	town_hall = yes
	workshop = yes
	local_fortification_1 = yes
}
1200.1.1 = { road_network = yes }
1495.1.1   = { 	
	owner = BRU
	controller = BRU
	add_core = BRU
}

1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 3
	base_production = 2
	base_manpower = 0
}
1529.1.1  = { religion = protestant }
1530.1.4  = {
	bailiff = yes	
}
1805.12.15 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
} # Treaty of Schoenbrunn, ceded to Prussia
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.13 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = WES
} # The kingdom is dissolved
