#1505 - Soyo

owner = LOA
controller = LOA
add_core = LOA
culture = bakongo
religion = animism
capital = "Soyo"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish

discovered_by = sub_saharan
discovered_by = central_african

1472.1.1 = {
	discovered_by = POR
} # Fren�o do P�, add trade post
