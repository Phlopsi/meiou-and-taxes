# 1497 - Timbuktu

owner = MAL
controller = MAL
culture = tuareg
religion = sunni
capital = "Timbuktu"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = millet
small_university = yes #Sankore University
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = HAF
discovered_by = TLE
discovered_by = FEZ

1000.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "timbuktu_confluence"
		duration = -1
	}
}
1100.1.1 = {
	marketplace = yes
	urban_infrastructure_1 = yes
	workshop = yes 
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = MAL
	add_core = TBK
}
1430.1.1   = {
	owner = TBK
	controller = TBK
}
1518.1.1   = {
	owner = SON
	controller = SON
	add_core = SON
	remove_core = MAL
}
1520.1.1 = { base_production = 5 }
1591.4.12  = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SON
} # Battle of Tondibi
1612.1.1   = {
	owner = TBK
	controller = TBK
	remove_core = MOR
} # Moroccans establish the Arma as new independent ruling class

1819.1.1   = {
	owner = MSN
	controller = MSN
	add_core = MSN
	remove_core = SEG
} # The Massina Empire
