# 1276 - Z�lyom

owner = HUN
controller = HUN  
culture = slovak
religion = catholic
capital = "Banska Bistrica"
trade_goods = iron
hre = no
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1515.1.1 = { training_fields = yes }
1520.5.5 = {
	base_tax = 9
	base_production = 0
	base_manpower = 0
}
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1529.10.7 = { bailiff = yes }1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1790.1.1   = {
	fort_15th = no
} # The town, along with the castle, burns down
