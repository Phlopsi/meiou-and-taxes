# 2766 - Faranah

owner = BKE
controller = BKE
culture = mali
religion = west_african_pagan_reformed
capital = "Faranah"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = soudantech
discovered_by = sub_saharan
hre = no
500.1.1 = {
	add_permanent_province_modifier = {
		name = "ivory_low"
		duration = -1
	}
}
1356.1.1   = {
	add_core = BKE
}
1520.1.1 = {
	base_tax = 9
}
