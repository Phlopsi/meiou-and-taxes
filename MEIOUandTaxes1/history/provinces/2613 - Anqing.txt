# 2613 - anhui_area Yingjiang

owner = WUU
controller = WUU
culture = wuhan
religion = confucianism
capital = "Huaining"
trade_goods = cotton
hre = no
base_tax = 47
base_production = 2
base_manpower = 2
is_city = yes
fort_15th = yes
discovered_by = chinese
discovered_by = steppestech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = {
	paved_road_network = yes
	urban_infrastructure_1 = yes
	workshop = yes
	constable = yes
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	owner = WUU
	controller = WUU
	add_core = WUU
}
1366.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = WUU
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 74
	base_production = 3
	base_manpower = 3
}
1644.1.1 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
