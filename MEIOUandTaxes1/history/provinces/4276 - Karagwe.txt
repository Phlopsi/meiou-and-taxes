# No previous file for Karagwe

owner = KRW
controller = KRW
add_core = KRW
culture = ganda
religion = animism
capital = "Karagwe"
trade_goods = wax
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = central_african

1520.1.1 = { base_tax = 11 }