# 3970 - Chikuzen

owner = KKC
controller = KKC
culture = kyushu
religion = mahayana
capital = "Hakata"
trade_goods = fish
hre = no
base_tax = 12
base_production = 2
base_manpower = 1
is_city = yes

discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
}
1000.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "hakata_natural_harbour"
		duration = -1
	}
}
1133.1.1 = {
	merchant_guild = yes
	town_hall = yes
	harbour_infrastructure_2 = yes
}
1356.1.1 = {
	add_core = KKC
	add_core = SHN #home province
}
1359.1.1 = {
	controller = SHN
	owner = SHN
}
1375.1.1 = {
	add_core = IGW
	owner = IGW
	controller = IGW
}
1387.1.1 = {
	owner = SHN
	controller = SHN
	add_claim = OUC
}
1431.1.1 = {
	add_core = OUC
	owner = OUC
	controller = OUC
}
1501.1.1 = {
	base_tax = 21
	base_production = 3
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR 
}
1578.1.1 = {
	owner = SMZ
	controller = SMZ
}
1585.1.1   = {
	religion = catholic
}
1615.1.1   = {
	religion = mahayana
}
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
