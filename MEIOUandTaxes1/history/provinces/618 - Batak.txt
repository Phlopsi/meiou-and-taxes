# 618 - Batak

owner = ARU
controller = ARU
culture = batak
religion = polynesian_religion		#this region began to be Islamified c1300
capital = "Deli"
trade_goods = opium		#tobacco?
hre = no
base_tax = 11
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian


1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_estuary
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "aru_natural_harbour" 
		duration = -1 
		}
}

1133.1.1 = {
	harbour_infrastructure_1 = yes
}


1356.1.1 = {
 	add_core = ARU
}
1480.1.1 = {
	religion = sunni
}
1501.1.1 = {
	base_tax = 13
}
1612.1.1 = {
	owner = ATJ
	controller = ATJ
 	add_core = ATJ
}
1683.1.1 = {
	add_core = NED
}
1825.1.1 = {
	owner = NED
	controller = NED
	unrest = 2
} # The Dutch gradually gained control
