# No previous file for Marmaris

owner = MEN
controller = MEN
culture = greek
religion = sunni
capital = "Marmaris"
trade_goods = wheat
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
add_permanent_claim = BYZ

1200.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = MEN
	set_province_flag = turkish_name
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # Taken over by the Ottomans
1402.1.1  = {
	owner = MEN
	controller = MEN
} # Ottomans defeated by the Seljuk in Ankara
1426.1.1  = {
	owner = TUR
	controller = TUR
}
1451.1.1  = {
	owner = TUR
	controller = TUR
} # Conquered by Mehmed II
1456.1.1  = {
	culture = turkish
	religion =  sunni
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = MEN } ###
1500.3.3   = {
	base_tax = 7
}
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1522.3.20 = {  naval_arsenal = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes fort_14th = yes
}
1530.1.4  = {
	bailiff = yes	
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
