# 3345 - Biak

culture = papuan
religion = polynesian_religion
capital = "Biak"
trade_goods = unknown # fish
hre = no
base_tax = 0 # 2
base_production = 0
base_manpower = 0
native_size = 15
native_ferocity = 4
native_hostileness = 8

1000.1.1   = {
	set_province_flag = papuan_natives
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = uncolonisable_rural_pop_02
		duration = -1
	}
}
1526.1.1 = {
	discovered_by = POR
} # Don Jorge de Mneses
1545.1.1 = {
	discovered_by = SPA
} # Y�igo Ortiz de Retez
