# No previous file for Livorno

owner = PIS
controller = PIS
add_core = PIS
culture = tuscan
religion = catholic 
capital = "Livorno" 
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes							#AdL: was part of the HRE


1300.1.1 = { road_network = yes harbour_infrastructure_1 = yes }

1356.1.1   = {
	add_core = PIS
	add_core = FIR
	local_fortification_1 = yes
}
1399.1.1   = {
	owner = MLO
	controller = MLO
	add_core = MLO
}
1404.1.1   = {
	owner = GEN
	controller = GEN
}
1421.8.28   = {
	owner = FIR
	controller = FIR
}
1494.1.1   = {
	controller = PIS
	owner = PIS
}  # Pisan Republic
1509.1.1   = {
	controller = FIR
	owner = FIR
	bailiff = yes
	remove_core = MLO
}  # Annexed back
1520.5.5 = {
	base_tax = 4
	base_production = 0
	base_manpower = 0
}
1522.3.20 = {  naval_arsenal = yes }
1527.1.1   = {
	controller = SPA
} # War of the League of Cognac
1529.8.3   = {
	controller = FIR
} # Treaty of Cambrai
1530.1.1 = {
	road_network = no paved_road_network = yes 
}
1530.2.27 = {
	hre = no
}
1569.1.1   = {
	owner = TUS
	controller = TUS
	add_core = TUS
	remove_core = FIR
} # Pope Pius V declared Duke Cosimo I de' Medici  Grand Duke of Tuscany
1571.1.1 = {
	
	remove_core = PIS
}
1618.1.1  =  { hre = no }
1801.2.9   = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # The Treaty of LunÚville
1801.3.21  = {
	owner = ETR
	controller = ETR
	add_core = ETR
} # The Kingdom of Etruria
1807.12.10 = {
	owner = FRA
	controller = FRA
	remove_core = ETR
} # Etruria is annexed to France
1814.4.11  = {
	owner = TUS
	controller = TUS
	remove_core = FRA
} # Napoleon abdicates and Tuscany is restored
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
