# 1094 - Gan-garr

culture = aboriginal
religion = polynesian_religion
capital = "Gan-gaarr"
trade_goods = unknown #fish
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 20
native_ferocity = 0.5
native_hostileness = 1

1606.1.1 = {
	discovered_by = NED
} # Dutch navigator Willem Janszoon
1770.8.1 = { discovered_by = GBR } # Cook's 1st voyage
