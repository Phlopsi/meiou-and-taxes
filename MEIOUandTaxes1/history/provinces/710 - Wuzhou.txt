# 710 - guangxi_area Yuzhou

owner = YUA
controller = YUA
culture = zhuang
religion = animism
capital = "Wuzhou"
trade_goods = rice
hre = no
base_tax = 24
base_production = 5
base_manpower = 2
citysize = 53000

discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = {
	urban_infrastructure_2 = yes
	workshop = yes
	marketplace = yes
}
1271.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1274.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1320.1.1 = {
	remove_core = SNG
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
    remove_core = LNG
	remove_core = YUA
}
1400.1.1 = { citysize = 30000 }
1450.1.1 = { citysize = 30000 }
1500.1.1 = { citysize = 30000 }
1514.1.1 = { discovered_by = POR }
1520.2.2 = {
	base_tax = 38
	base_production = 7
	base_manpower = 3
}
1550.1.1 = { citysize = 25000 }
1600.1.1 = { citysize = 25000 }
1650.1.1 = { citysize = 25000 }
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
1700.1.1 = { citysize = 30000 }
1740.1.1 = {  }
1750.1.1 = { citysize = 40000 }
1800.1.1 = { citysize = 50000 }

