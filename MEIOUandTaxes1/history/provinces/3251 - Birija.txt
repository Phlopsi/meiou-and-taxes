# No previous file for Birija

culture = oroqen
religion = tengri_pagan_reformed
capital = "Chumkan"
trade_goods = fur
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 1
native_hostileness = 3
discovered_by = chinese
discovered_by = steppestech

1643.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
#  	religion = orthodox
#  	culture = russian
} # Founded by Pyotr Beketov
1668.1.1 = {
	add_core = RUS
}
1689.8.27 = {
	discovered_by = QNG
	owner = QNG
	controller = QNG
	add_core = QNG
#	culture = manchu
#	religion = confucianism
} # Treaty of Nerchinsk
1858.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
#  	religion = orthodox
#  	culture = russian
} # Treaty of Aigun
