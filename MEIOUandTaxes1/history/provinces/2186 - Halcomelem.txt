# No previous file for Halcomelem

culture = shuswap
religion = totemism
capital = "Halcomelem"
trade_goods = unknown
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 85
native_ferocity = 3
native_hostileness = 4
