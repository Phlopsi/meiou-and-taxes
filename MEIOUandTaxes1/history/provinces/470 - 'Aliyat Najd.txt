# 469 - Al Qasim

owner = ANZ
controller = ANZ
culture = najdi
religion = sunni
capital = "Afif"
trade_goods = wool
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = turkishtech

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = ANZ
}
1500.3.3   = {
	base_tax = 7
}
1744.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
}
1818.9.9 = {
	owner = TUR
	controller = TUR 
} # The end of the Saudi State
