# 12 - Sjaelland
# MEIOU - Gigau

owner = DEN
controller = DEN
culture = danish
religion = catholic
capital = "K�pmann�hafn"
base_tax = 12
base_production = 0
base_manpower = 1
is_city = yes
trade_goods = fish
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
}

1000.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = { 
		name = "sjaelland_natural_harbour" 
		duration = -1 
	}
	add_permanent_province_modifier = {
		name = "herring_province_big"
		duration = -1
	}
}

1119.1.1 = { harbour_infrastructure_2 = yes }
1229.12.17 = {
	marketplace = yes
}
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = DEN
#	add_core = SJA
}
1400.1.1  = { fort_14th = yes } #Unknown date
1401.1.1 = {
	workshop = yes
	town_hall = yes
}
1479.1.1  = {
	small_university = yes
}
1500.3.3 = {
	base_tax = 14
	base_production = 2
	base_manpower = 2
}
1501.1.1 = { road_network = yes }
1502.1.1 = { military_harbour_1 = yes }
1515.1.1 = { training_fields = yes }
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1526.1.1 = { 
	religion = protestant
	#reformation_center = protestant
} #preaching of Hans Tausen
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1531.11.1 = {
	revolt = { type = nationalist_rebels size = 2 }
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = {  }
	controller = DAN
}
1536.1.1  = { religion = protestant }

1556.1.1   = {
	#trade_goods = naval_supplies
}
1670.1.1  = {  }
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
