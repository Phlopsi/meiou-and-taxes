# 3357 - Tanba

owner = NIK
controller = NIK
culture = kansai
religion = mahayana
capital = "Tanba"
trade_goods = lumber #forestry
hre = no
is_city = yes # citysize = 1500

base_tax = 9
base_production = 0
base_manpower = 1

discovered_by = chinese

#One of the Six Old Kilns
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_chinaware
		duration = -1
	}
}
1000.1.1 = { workshop = yes }
1356.1.1 = {
	add_core = NIK #home province
	add_core = HKW #home province
}
1363.1.1 = { #Ashikaga Tadafuyu
	controller = JAP
}
1364.1.1 = {
	add_core = YMN
	owner = YMN
	controller = YMN
}
1392.1.1 = {
	add_core = HKW
	owner = HKW
	controller = HKW
}
1501.1.1 = {
	base_tax = 16
}
1542.1.1 = { discovered_by = POR }
1575.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
