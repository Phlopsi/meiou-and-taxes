# 570 - Ahom

owner = MLB
controller = MLB
culture = naga
religion = adi_dharam
capital = "Dimapur"
trade_goods = rice
base_tax = 3
base_production = 0
base_manpower = 0
citysize = 12000

discovered_by = indian
discovered_by = chinese

1200.1.1 = { road_network = yes }
1356.1.1 = { add_core = MLB }
1511.1.1 = {
	base_production = 1
}
1515.1.1 = { training_fields = yes }
1526.2.1 = { controller = ASS }
1530.1.1 = { owner = ASS add_core = ASS }
1581.1.1 = { add_core = ASS }
1770.1.1 = { unrest = 8 } # Moamoria rebellion
1806.1.1 = { unrest = 0 }
