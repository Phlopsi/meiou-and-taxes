# 1468 - Michoacan

owner = PUR
controller = PUR
add_core = PUR
culture = purepechan
religion = nahuatl
capital = "Patzcuaro"

base_tax = 38
base_production = 2
base_manpower = 2
citysize = 1500
trade_goods = gold


discovered_by = mesoamerican

hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
}
1000.1.1 = {
	workshop = yes
	marketplace = yes
	urban_infrastructure_1 = yes
	constable = yes
}
1450.1.1   = {
	capital = "Tzintzuntzan"
}
1530.1.1 = {
	# owner = PUR
	# controller = PUR
	remove_core = AZT
}
1530.1.1   = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	add_core = SPA
	religion = catholic
	marketplace = yes
	bailiff = yes
	courthouse = yes
} # Francisco V�zquez de Coronado y Luj�n
1576.1.20  = {
	owner = SPA
	controller = SPA
	capital = "Valladolid"
	citysize = 200
	culture = castillian
	religion = catholic
	base_tax = 2
base_production = 2
	trade_goods = livestock
	} # Spanish explorer Crist�bal de O�ate
1601.1.1   = {
	add_core = SPA
	citysize = 1000
}
1650.1.1   = {
	citysize = 2000
}
1700.1.1   = {
	citysize = 5000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 10000
}
1800.1.1   = {
	citysize = 20000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
