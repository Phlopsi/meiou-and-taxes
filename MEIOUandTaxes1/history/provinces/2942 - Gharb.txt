# 2942 - Gharb

owner = FEZ
controller = FEZ
culture = fassi
religion = sunni
capital = "Ksar El Kebir"
base_tax = 9
base_production = 4
base_manpower = 1
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1088.1.1 = {
	harbour_infrastructure_1 = yes
	urban_infrastructure_2 = yes
	workshop = yes
}

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
	local_fortification_1 = yes
}

1356.1.1 = {
	add_core = FEZ
}
1500.3.3 = {
	base_production = 3
	}
1530.1.1 = {
	add_core = MOR
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1667.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = TFL
}
