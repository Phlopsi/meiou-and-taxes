# No previous file for Tuscarora

culture = tuscarora
religion = totemism
capital = "Tuscarora"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 4

1607.1.1  = { discovered_by = ENG } # John Smith
1707.5.12 = { discovered_by = GBR } 
1720.1.1  = {	owner = GBR
		controller = GBR
		citysize = 150
		culture = english
		religion = protestant
	    } # Settlement after the Tuscarora War
1745.1.1  = { add_core = GBR }
1750.1.1  = { citysize = 650 trade_goods = tobacco }
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
		culture = american
	    } # Declaration of independence
1782.11.1 = { remove_core = GBR unrest = 0 } # Preliminary articles of peace, the British recognized American independence
1800.1.1  = { citysize = 1200 }
