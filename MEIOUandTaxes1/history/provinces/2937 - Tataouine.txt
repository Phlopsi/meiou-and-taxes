# 2937 - Tataouine

owner = GHD
controller = GHD  
culture = berber
religion = sunni
capital = "Tataouine"
trade_goods = olive
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = muslim
discovered_by = turkishtech
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
	add_core = GHD
}
1830.1.1 = {
	owner = TUN
	controller = TUN
	add_core = TUN
}
