# 1174 - Uteve

owner = ZIM
controller = ZIM
culture = shona
religion = animism
capital = "Uteve"
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = livestock
hre = no
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = ZIM
}
1493.1.1 = { unrest = 8 } #civil war between Mucombo and Toloa
1495.1.1 = { unrest = 2 } #Toloa wins civil war, Mucombo moves Mutapa center northward
1506.1.1 = { unrest = 7 } #Changamire clans attempt to establish new base in region, gains Torwa support
1512.1.1 = { unrest = 0 } #Changamire finally defeated by Mutapa, Torwa become Mutapa vassals
1520.1.1 = { base_tax = 5 }
1683.1.1 = { unrest = 7 } #Changamire clans launch war to replace Torwa dominance in region
1688.1.1 = { unrest = 0 } #Changamire establish themselves as Rozvi
1691.1.1 = { unrest = 5 } #Torwa try to overthrow the Rozvi
1692.1.1 = { unrest = 0 } #Changamire destroy Torwa
1697.1.1 = { unrest = 5 } #Succession conflict in wake of death of Dombo Chikura
1698.1.1 = { unrest = 0 } #Succession war won by Nechasike
