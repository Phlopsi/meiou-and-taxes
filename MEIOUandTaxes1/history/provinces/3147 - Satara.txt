# No previous file for Satara

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Satara"
trade_goods = cotton
hre = no
base_tax = 18
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim

1100.1.1 = { plantations = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = BIJ
	add_core = BAH
	#fort_14th = yes
}
1490.1.1  = {
	owner = BIJ
	controller = BIJ
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1502.1.1  = { discovered_by = POR }
1511.1.1 = {
	base_tax = 23
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
}
1596.2.1  = { discovered_by = NED } # Cornelis de Houtman
1600.1.1  = { discovered_by = turkishtech discovered_by = ENG discovered_by = FRA }
1659.11.30 = {
	owner = MAR
	controller = MAR
	add_core = MAR
} # battle of Pratapgarh
1676.1.1 = {
	fort_14th = no
	fort_16th = yes
}
1686.1.1  = { controller = MUG } # Aurangzeb
1703.1.1  = { controller = MAR }
1707.5.12 = { discovered_by = GBR }
1818.6.3  = {
	owner = GBR
	controller = GBR
}
