# No previous file for Kokkand

owner = MGH
controller = MGH
culture = chaghatai
religion = sunni
capital = "Kokkand"
trade_goods = wheat
base_tax = 11
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1111.1.1 = {
	set_province_flag = silk_road_town
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "kokkand_silk_road" 
		duration = -1 
		}
	town_hall = yes
	marketplace = yes
	local_fortification_1 = yes
	set_province_flag = mined_goods
	set_province_flag = iron
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MGH
	add_core = KAS
}	
1444.1.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
   	remove_core = MGH
}
1469.8.27 = {
	owner = CHG
	controller = CHG
	remove_core = TIM
}
1501.1.1 = {
	base_tax = 12
	base_production = 2
}
1504.1.1 = {
	owner = SHY
	controller = SHY
	culture=uzbehk
	add_core = SHY
}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
   	remove_core = SHY
}
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
   	remove_core = BUK
}
