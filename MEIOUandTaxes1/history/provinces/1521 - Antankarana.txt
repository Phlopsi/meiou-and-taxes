# 1521 - Antakarana

owner = ANT
controller = ANT
add_core = ANT
culture = vezu
religion = animism
capital = "Antankarana"
is_city = yes
trade_goods = slaves
base_tax = 2
base_production = 0
base_manpower = 0
hre = no

discovered_by = ANT
discovered_by = BAA
discovered_by = BET
discovered_by = BNA
discovered_by = MER
discovered_by = MHF
discovered_by = MNB
discovered_by = SIH
discovered_by = TAM

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "boina_natural_harbour" 
		duration = -1 
		}
}
1500.9.1 = {
	discovered_by = POR
} # Diego Dias
1521.1.1 = {
	base_tax = 3
}
1832.1.1 = {
	owner = MER
	controller = MER
	add_core = MER
}
1885.12.17 = {
	add_core = FRA
}
1897.2.28  = {
	owner = FRA
	controller = FRA
}
