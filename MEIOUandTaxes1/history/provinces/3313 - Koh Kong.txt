# No previous file for Koh Kong

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Koh Kong"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = muslim
discovered_by = indian
hre = no

1867.1.1 = { 			
	owner = FRA
	controller = FRA
}
