#Province: Jammu
#file name: 2226 - Jammu
#MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG - Turko-Mongol mod

owner = KSH
controller = KSH
culture = kashmiri
religion = hinduism
capital = "Jammu"
trade_goods = wool #carpet
hre = no
base_tax = 24
base_production = 1
base_manpower = 2
citysize = 17572
#fort_14th = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_carpet
		duration = -1
	}
}

1000.1.1 = {
	add_permanent_province_modifier = {
		name = jammu_state
		duration = -1
	}
}
1100.1.1 = {
	workshop = yes
	town_hall = yes
}
#1120.1.1 = { textile = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = KSH
}
1400.1.1  = { religion = sunni }
1511.1.1 = {
	base_tax = 33
}
1525.1.1 = {
	controller = TIM owner = TIM add_core = TIM
	revolt = { }
	training_fields = yes
} #Defects to Babur
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
	remove_core = TIM
	bailiff = yes
} # Battle of Panipat & Tag Change

1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1738.1.1  = {
	controller = PER
} # Captured by Persia, Nadir Shah
1739.5.1  = {
	owner = PER
} # Captured by Persia, Nadir Shah
1747.6.1  = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
	unrest = 2
}
1750.1.1  = { citysize = 28760 }
1755.1.1  = {
	owner = KSH
	controller = KSH
} # Governor declares independence
1762.1.1  = {
	owner = DUR
	controller = DUR
} # Revolt beaten
1799.1.1 = {
	owner = PUN
	controller = PUN
}
1849.3.30 = {
	owner = GBR
	controller = GBR
} # End of the Second Anglo-Sikh War

