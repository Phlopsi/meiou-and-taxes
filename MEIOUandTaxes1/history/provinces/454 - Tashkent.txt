# 454 - Tashkent

owner = CHG
controller = CHG
culture = chaghatai
religion = sunni
capital = "Tashkent"
trade_goods = wheat
base_tax = 24
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = KSH
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1000.1.1  = { 
	set_province_flag = silk_road_town
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "tashkent_silk_road" 
		duration = -1 
		}
	town_hall = yes	
	marketplace = yes
	local_fortification_1 = yes
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = CHG
}


1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1 = {
	add_core = SHY
}
1446.1.1 = {
	owner = SHY
	controller = SHY
	culture=uzbehk
	add_core = SHY
	remove_core = TIM
	remove_core = GOL
}
1451.1.1 = { temple = yes }
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1501.1.1 = {
	base_tax = 28
	base_production = 3
	base_manpower = 2
}
1502.1.1 = {
	owner = SHY
	controller = SHY
}
1515.1.1 = { training_fields = yes }
1520.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
1723.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1728.1.1 = { owner = BUK controller = BUK }
1740.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1 = { owner = BUK controller = BUK }
