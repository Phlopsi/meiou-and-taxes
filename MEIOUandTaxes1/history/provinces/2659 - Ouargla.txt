# 2659 - Ouargla

owner = TOG
controller = TOG
culture = berber
religion = ibadi
capital = "Ouargla"
trade_goods = wool
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = MAM
discovered_by = HAF
discovered_by = TLE
discovered_by = FEZ
discovered_by = soudantech
discovered_by = sub_saharan

1100.1.1 = { marketplace = yes }
1356.1.1   = {
	add_core = TOG
}
1530.1.1 = {
	add_permanent_claim = ALG
}
