# 73 - Tirol

owner = TIR
controller = TIR
culture = austrian
religion = catholic
capital = "Innsbruck"
base_tax = 8
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = silver

discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
	local_fortification_1 = yes
}
1200.1.1 = {
	road_network = yes
	town_hall = yes
	workshop = yes
}
1356.1.1   = {
	add_core = TIR
	add_core = BAY
}
1363.1.13  = {
	controller = HAB
	owner = HAB
	remove_core = BAY
}
1379.1.1   = {
	controller = TIR
	owner = TIR
	add_core = TIR
}
1490.1.1   = {
	controller = HAB
	owner = HAB
	add_core = HAB
	remove_core = TIR
	culture = austrian
	fort_14th = yes
}
1495.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1504.1.1   = {
	remove_core = BAV
}
1511.6.23  = {
#	base_manpower = 0.5
	base_manpower = 1.0
} # Landslibell - Tyrolean's only military duty is the defense of their homeland.
1511.7.1   = {
	
} # With the Landslibell came the duty to be prepared for defense -> Sch�tzen
1515.1.1 = { training_fields = yes }
1520.5.5 = {
	base_tax = 14
	base_production = 2
	base_manpower = 1
}
1525.3.1   = {
	unrest = 6
} # Peasant Revolts
1525.9.1   = {
	unrest = 0
}
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1540.1.1   = {
	
}
1570.1.1   = {

}
1627.1.1   = {
	
} # Kramsacher Glash�tte
1646.1.1   = {
	fort_14th = yes
}
1669.1.1   = {
	art_corporation = no
	early_modern_university = yes
} # University Leopoldina
1805.12.26 = {
	owner = BAV
	controller = BAV
	add_core = BAV
    remove_core = HAB
} # Treaty of Pressburg
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
1809.4.9   = {
	controller = REB
} # Tyrolean rebellion
1810.1.19  = {
	controller = BAV
}
1814.5.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BAV
} # Treaty of Paris, ceded to the Habsburgs
