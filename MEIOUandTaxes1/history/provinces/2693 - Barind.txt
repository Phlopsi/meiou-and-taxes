# 2693 - Barind

owner = BNG
controller = BNG
culture = bengali
religion = hinduism
capital = "Gaur"
trade_goods = cotton
hre = no
base_tax = 30
base_production = 5
base_manpower = 3
citysize = 150000
add_core = BNG
discovered_by = indian
discovered_by = muslim 
discovered_by = chinese
discovered_by = steppestech

1100.1.1 = {
	marketplace = yes
	urban_infrastructure_1 = yes
	workshop = yes
	temple = yes
}

1200.1.1 = { road_network = yes }
1356.1.1  = { add_core = BNG 
	fort_14th = yes }
1474.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1511.1.1 = {
	base_tax = 35
	base_production = 10
	marketplace = no
	town_hall = no
	urban_infrastructure_1 = no
	urban_infrastructure_2 = yes
	merchant_guild = yes
}
#1529.1.1  = {
#	owner = BNG
#	revolt = { type = pretender_rebels size = 7 leader = "Sher Shah Sur"}
#	controller = REB
#} # Sur control
1530.1.1 = { 
	add_permanent_claim = MUG
	training_fields = yes
}
1530.1.2 = { add_core = TRT }
1538.1.1  = {
	revolt = {}
	controller = BNG
} # Gaur has fallen
1538.6.1  = {
	controller = MUG
} # Mughal Invasion
1539.1.1  = {
	controller = BNG
} # Surs again in control

1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1627.1.1  = { discovered_by = POR }
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1750.1.1  = {
	religion = sunni
}
1760.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
1782.1.1  = { add_core = GBR }
1810.1.1  = {
	add_core = GBR
}
