# 1184 - Urungwe

owner = LUB
controller = LUB
add_core = LUB
culture = luba
religion = animism
capital = "Urungwe"
trade_goods = copper
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
hre = no
discovered_by = central_african

1520.1.1 = { base_tax = 10 }
