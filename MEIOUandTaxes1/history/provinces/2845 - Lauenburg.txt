# 2845 - Lauenburg and Butow

owner = TEU
controller = TEU
capital = "Lauenburg"					# (Kashubian), Tuchola (Polish), Tuchel (German)
culture = kashubian					# pommeranian
religion = catholic
trade_goods = wheat
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
add_core = TEU
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = {
	local_fortification_1 = yes
}
1454.3.6   = {
	add_core = POL
} # Beginning of the "Thirteen years war"
1466.10.19 = {
	owner = PSP
	controller = PSP
	add_core = PSP
	remove_core = TEU
	rename_capital = "Lebork" 
	change_province_name = "Lebork"
} # Peace treaty, "Peace of Torun"
1478.1.1   = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PSP
} # Duchy reunited for a short period
1500.1.1 = { road_network = yes }
1519.1.1   = {
	religion = protestant
	remove_core = POL
}
1520.5.5 = {
	base_tax = 2
}
1530.1.4  = {
	bailiff = yes	
	culture = pommeranian 
}
1531.1.1   = {
	owner = PWO
	controller = PWO
	add_core = PWO
	remove_core = POM
} # Fifth Partition
1569.7.1  = {
	remove_core = POL
	add_core = PLC
} # Union of Lublin
1600.1.1   = {
	fort_14th = yes
}
1625.1.1   = {
	add_core = POM
	remove_core = PWO
} # Final reunification
1637.1.1   = {
	owner = PLC
	controller = PLC
	remove_core = POM
}
1650.1.1   = {
	
}
1657.11.6  = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # Treaty of Bromberg / Bydgoszcz
1701.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
}
1772.8.5   = {
	remove_core = PLC
} # First partition
1794.3.24  = {
	unrest = 5
} # Kosciuszko uprising
1794.11.16 = {
	unrest = 0
} # The end of the uprising
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1807.3.1   = {
	controller = FRA
} # Occupied by French troops
1807.7.9   = {
	controller = PRU
}
