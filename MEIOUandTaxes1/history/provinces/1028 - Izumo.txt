# 1028 - Izumo
# GG/LS - Japanese Civil War

owner = YMN
controller = YMN
culture = chugoku
religion = mahayana #shinbutsu
capital = "Yasugi"
trade_goods = rice
is_city = yes # citysize = 1500
hre = no
base_tax = 11
base_production = 0
base_manpower = 1
temple = yes # Izumo Shrine

discovered_by = chinese
1000.1.1 = {
	local_fortification_1 = yes
}
1356.1.1 = {
	add_core = YMN
	add_core = MRI
	add_core = OUC
}
1501.1.1 = {
	base_tax = 19
	base_production = 1
	base_manpower = 2
}
1561.1.1   = {
	owner = MRI
	controller = MRI
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
