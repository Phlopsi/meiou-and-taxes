#1117 - Gao

owner = MAL
controller = MAL
culture = songhai
religion = sunni
capital = "Gao"
base_tax = 2
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = rice

discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = HAF
discovered_by = TLE
discovered_by = FEZ

1000.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "goa_confluence"
		duration = -1
	}
}
1100.1.1 = {
	merchant_guild = yes
	urban_infrastructure_1 = yes
	workshop = yes 
}
1356.1.1  = {
	add_core = MAL
	add_core = SON
}
1433.1.1  = {
	owner = TBK
	controller = TBK
}
1438.1.1  = {
	owner = SON
	controller = SON
}	
1493.1.1  = {
	unrest = 9
} #Civil War between Sunni Baare and Muhammad Ture Sylla
1494.1.1  = {
	unrest = 0
} #Muhammad Ture Sylla establishes new dynasty
1520.1.1 = { base_production = 6 }
1529.1.1  = {
	unrest = 2
} #Musa overthrows father, becomes Askiya
1550.1.1  = {
	discovered_by = MOR
} #Era of Prosperity under Askiya Dawud
1591.3.12 = {
	owner = MOR
	controller = MOR
	add_core = MOR
} #Moroccan victory at Tondibi
1612.1.1   = {
	owner = TBK
	controller = TBK
	remove_core = MOR
} # Moroccans establish the Arma as new independent ruling class
