# 1547 - Touat

culture = berber
religion = sunni
capital = "Adrar"
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 25
native_ferocity = 4.5
native_hostileness = 9
trade_goods = unknown
discovered_by = MAM
discovered_by = HAF
discovered_by = TLE
discovered_by = FEZ
discovered_by = soudantech
discovered_by = sub_saharan
# NOT = { discovered_by = SLL }
# NOT = { discovered_by = DSL }

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	is_city = yes
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
