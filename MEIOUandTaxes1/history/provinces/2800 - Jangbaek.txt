# 2800 - Jangbaek

owner = YUA
controller = YUA
culture = uriankhai
religion = tengri_pagan_reformed
capital = "Ganggye"
base_tax = 11
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = steppestech
hre = no
500.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_medium"
		duration = -1
	}
}
1200.1.1 = { temple = yes }
1270.1.1  = {
	owner = YUA
	controller = YUA
	add_core = YUA
	add_core = MXI
}
1371.1.1  = {
	owner = KOR
	controller = KOR
}
1392.6.5  = {
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
	remove_core = YUA
}
1444.1.1 = {
	base_tax = 24
	base_manpower = 1
}
1450.1.1 = {
	religion = confucianism
	culture = korean
}
1520.5.5 = {
	base_tax = 40
	base_manpower = 2
}
1593.1.1 = {
	unrest = 5
} # Japanese invasion
1593.5.1 = {
	controller = JOS
	unrest = 0
} # Japanese invasion ends
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
