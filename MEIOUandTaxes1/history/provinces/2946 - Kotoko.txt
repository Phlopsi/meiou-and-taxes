# 2946 - Kotoko

owner = KBO
controller = KBO
culture = kanouri		
religion = animism		 
capital = "Kotoko"
base_tax = 6
base_production = 0
base_manpower = 0
citysize = 5000
trade_goods = millet
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = KBO
	add_core = YAO
}
1395.1.1   = {
	owner = YAO
	controller = YAO
}
1505.1.1 = {
	owner = KBO
	controller = KBO
	remove_core = YAO
}
1520.1.1 = {
	base_tax = 7
}
