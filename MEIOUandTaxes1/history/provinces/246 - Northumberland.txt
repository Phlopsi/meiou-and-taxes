#246 - Northumberland

owner = ENG
controller = ENG
culture = northern_e
religion = catholic
hre = no
base_tax = 3
base_production = 1
trade_goods = lumber
base_manpower = 0
is_city = yes

capital = "Newcastle"
 #Durham Cathedral

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1000.1.1   = {
	set_province_flag = has_estuary
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "northumberland_natural_harbour" 
		duration = -1 
	}
	local_fortification_1 = yes
	add_permanent_province_modifier = {
		name = "marches_of_scotland"
		duration = -1
	}
}
1001.1.1 = {
	town_hall = yes
}
1066.1.1   = {
	add_permanent_province_modifier = {
		name = "north_of_england"
		duration = -1
	}
}

1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = ENG
	add_core = NOL
}
1453.1.1   = { unrest = 5 } #Start of the War of the Roses
1461.6.1   = { unrest = 2 controller = REB } #Lancastrian Holdouts after Coronation of Edward IV
1464.6.1   = { controller = ENG } #Estimated
1467.1.1   = { unrest = 5 } #Rivalry between Edward IV & Warwick
1471.1.1   = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.4.11  = {
	remove_province_modifier = "north_of_england"
	add_permanent_province_modifier = {
		name = "council_of_north"
		duration = -1
	}
} # Council established by Edward IV and  headquartered at Sheriff Hutton Castle and Sandal Castle
1471.5.4   = { unrest = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26  = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23  = { unrest = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1520.5.5  = {
	base_tax = 5
	base_production = 1
	base_manpower = 0
}
1530.1.1 = {
	culture = english
	road_network = no
	paved_road_network = yes
	bailiff = yes
}
1560.1.1   = { fort_14th = yes }
1585.1.1   = { religion = protestant } #anglican
1603.3.24  = {
	remove_province_modifier = "marches_of_scotland"
}
1640.10.1  = { controller = SCO }
1640.10.26 = { controller = ENG }
1641.11.22 = {
	remove_province_modifier = "council_of_north"
} # Council abolishedbecause of its support for Catholic Recusants
1644.4.15  = { controller = SCO }
1646.5.5   = { controller = ENG } #End of First English Civil War
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
    	remove_core = ENG
}