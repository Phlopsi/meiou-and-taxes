# 2420 - Phetchaburi

owner = AYU
controller = AYU
add_core = AYU
culture = thai
religion = buddhism
capital = "Phetchaburi"
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1535.1.1 = { discovered_by = POR }
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
