# 1345 - Br�sa

owner = MLO    
controller = MLO
culture = lombard 
religion = catholic 
capital = "Br�sa"  
base_tax = 11
base_production = 3 
base_manpower = 1
is_city = yes 
trade_goods = livestock   
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

hre = yes 
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
	local_fortification_1 = yes
}
1250.1.1 = {
	temple = yes
	urban_infrastructure_1 = yes
	workshop = yes
	marketplace = yes
}

1356.1.1   = {
	add_core = MLO
	add_claim = VER
#	add_core = BRS
}
1426.1.1   = { controller = VEN owner = VEN add_core = VEN remove_core = MLO }
1509.6.1   = { controller = FRA } # Venice collapses
1512.1.1   = { controller = VEN } # Brescia revolts
1512.2.18  = { controller = FRA } # Sack of Brescia
1513.3.23  = { 
	controller = VEN
	bailiff = yes
	trade_goods = iron 
	weapons = yes
	hre = no 
}
1520.5.5 = {
	base_tax = 14
	base_production = 4
	base_manpower = 1
}
1559.1.1   = { remove_core = FRA }
1618.1.1  =  { hre = no }
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
	remove_core = HAB
	remove_core = VER
} # Transpadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {    ##??
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
