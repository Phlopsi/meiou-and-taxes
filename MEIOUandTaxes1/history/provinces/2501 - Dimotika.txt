# 2501 - Demotika

owner = BYZ
controller = BYZ
culture = greek
religion = orthodox
capital = "Didymoteicho"
trade_goods = fish
hre = no
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = { road_network = yes }
1350.1.1  = {
	add_core = BYZ
	add_claim = OTT
	add_local_autonomy = 15
}
1354.1.1 = {
	controller = REB
	revolt = { 
		type = pretender_rebels
		size = 1 #Near the end of his revolt
		name = "Matthew Kantakouzenos"
		leader = "Matthew Kantakouzenos"
		}
	set_province_flag = matthew_kantakouzenos
	}
1357.1.1 = { 
	revolt = {}
	controller = BYZ
	clr_province_flag = matthew_kantakouzenos
	}
1361.1.1  = {
	owner = OTT
	controller = OTT
	capital = "Demotika"
	change_province_name = "Demotika"
	add_local_autonomy = -15
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1453.5.29 = {
	remove_core = BYZ
}
1515.2.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1520.5.5 = {
	base_tax = 6
	base_production = 0
	base_manpower = 0
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }

1615.1.1  = { base_tax = 4
base_production = 4 } # The Decentralizing Effect of the Provincial System
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1689.1.1  = { controller = REB } # Karposh uprising against Ottoman rule
1690.1.1  = { controller = TUR }
1715.1.1  = {  }
1750.1.1  = { add_core = GRE }
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	controller = TUR
}
