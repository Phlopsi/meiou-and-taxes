# 2205 - Guiengola
# GG - 28/07/2008
# Mesoamerican mod v1

owner = ZAP
controller = ZAP
add_core = ZAP
culture = zapotec
religion = nahuatl
capital = "Tehuantepec" 

base_tax = 14
base_production = 1
base_manpower = 1
trade_goods = carmine
citysize = 5000


discovered_by = mesoamerican

hre = no
1000.1.1 = {
	marketplace = yes
	town_hall = yes
}
1506.1.1   = {
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	citysize = 2000
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
