# No previous file for Pennacook

culture = mahican
religion = totemism
capital = "Pennacook"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 85
native_ferocity = 3
native_hostileness = 4

1633.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1735.1.1  = {
	owner = GBR
	controller = GBR
	citysize = 250
	capital = "Ashuelot"
	culture = english
	religion = protestant
	trade_goods = fish # naval_supplies
}
1760.1.1  = { add_core = GBR citysize = 1000 }
1764.7.1  = { culture = american unrest = 6 } # Growing unrest
1776.7.4  = {
	owner = USA
	controller = USA
	add_core = USA
} # Declaration of independence
1782.11.1 = { unrest = 0 remove_core = GBR } # Preliminary articles of peace, the British recognized American independence
