# 2873 - Sherbro

culture = dyola
religion = west_african_pagan_reformed
capital = "Sherbro"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 50
native_ferocity = 4.5
native_hostileness = 9

discovered_by = sub_saharan

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = { 
		name = "sierra_leone_natural_harbour" 
		duration = -1 
		}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1456.1.1   = { discovered_by = POR } #Cadamosto
1808.1.1   = {
	owner = GBR
	controller = GBR
	add_core = GBR
	citysize = 200
		trade_goods = fish
}
