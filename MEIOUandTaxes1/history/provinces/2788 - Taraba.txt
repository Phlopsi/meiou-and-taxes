# 2788 - Taraba

owner = NRI
controller = NRI
culture = igbo		
religion = west_african_pagan_reformed		 
capital = "Taraba"
base_tax = 28
base_production = 0
base_manpower = 3
is_city = yes
trade_goods = cotton
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = IGB
}
1520.1.1 = {
	base_tax = 37
}
1807.1.1   = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
