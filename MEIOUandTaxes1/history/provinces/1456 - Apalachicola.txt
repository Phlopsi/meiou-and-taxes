# No previous file for Apalachicola

culture = creek
religion = totemism
capital = "Apalachicola"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1519.1.1  = { discovered_by = SPA } # Alvarez de Pi�eda explored the Gulf Coast
1633.1.1  = {	owner = SPA
		controller = SPA
		citysize = 150
		culture = castillian
		religion = catholic } #San Luis de Apalachee mission
#1656.1.1  = {	fort_14th = yes } #San Marcos de Apalachee presidio
1658.1.1  = { add_core = SPA }
1663.1.1  = { discovered_by = ENG } # The British claimed the region north of the Gulf of Mexico
1763.2.10 = {	discovered_by = ENG
		owner = GBR
		controller = GBR
	    culture = english
	    religion = protestant
		citysize = 1300
	    } # Part of British Georgia after the Treaty of Paris
1783.9.3  = { owner = SPA controller = SPA } # Part of Spanish West Florida
1784.1.1  = { unrest = 7 } # Controversy of the Treaty of Versaille & the Treaty of Paris, Spanish or American?
1786.3.22 = { capital = "Tallahassee" }
1800.1.1  = { citysize = 6835 }
1808.1.1  = { add_core = SPA }
1819.2.22 = {	owner = USA
		controller = USA
		add_core = USA
		remove_core = SPA
	    } # The Adams-On�s Treaty
