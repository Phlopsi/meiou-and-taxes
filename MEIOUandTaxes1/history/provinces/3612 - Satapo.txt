# No previous file for Satapo

culture = cherokee
religion = totemism
capital = "Satapo"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 15
native_ferocity = 1 
native_hostileness = 6

1785.11.2 = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
} #Treaty of Hopewell (with the Cherokee), define a new border
