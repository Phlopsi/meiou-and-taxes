# No previous file for Apartad�

culture = cariban
religion = pantheism
capital = "Apartad�"
trade_goods = unknown
hre = no
base_tax = 20
base_production = 0
base_manpower = 0
native_size = 40
native_ferocity = 1
native_hostileness = 1

1498.1.1   = {
	discovered_by = CAS
}
1520.1.1 = { base_tax = 2 }
1530.1.4  = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	add_core = SPA
	change_province_name = "Sincelejo"
	rename_capital = "San Francisco de As�s"
	culture = castillian
	religion = catholic
	is_city = yes
	trade_goods = coffee
	set_province_flag = trade_good_set
}
1700.1.1   = {
	citysize = 1750
}
1750.1.1   = {
	add_core = COL
	culture = colombian
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
