# 2796 - Omi

owner = KYO
controller = KYO
culture = kansai
religion = mahayana #shinbutsu
capital = "�tsu"
trade_goods = tea
hre = no
base_tax = 17
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_chinaware
		duration = -1
	}
}

1133.1.1 = {
	workshop = yes
	town_hall = yes
	temple = yes
	road_network = yes
}

1356.1.1 = {
	add_core = KYO
}
1501.1.1 = {
	base_tax = 27
	base_production = 5
	base_manpower = 2
}
1542.1.1   = { discovered_by = POR }
1572.1.1   = {
	owner = ODA
	controller = ODA
}
1583.1.1   = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
