# 3852 - Chemnitz

owner = MEI
controller = MEI
culture = high_saxon
religion = catholic
capital = "Chemnitz"
trade_goods = silver
hre = yes
base_tax = 8
base_production = 2
base_manpower = 1
is_city = yes
add_core = MEI
discovered_by = eastern
discovered_by = western
discovered_by = muslim


1119.1.1 = {
	constable = yes
	marketplace = yes
	workshop = yes
	town_hall = yes
	local_fortification_1 = yes
}


1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_permanent_province_modifier = {
		name = freiburg
		duration = -1
	}
}
1423.6.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Margraviate of Meissen inherits Saxony and becomes the Elector.
1485.11.11   = { 
	owner = MEI
	controller = MEI
	add_core = MEI
	remove_core = SAX
} #Treaty of Leipzig
1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 9
	base_production = 3
	base_manpower = 1
}
1520.12.10 = {
	religion = protestant
}
1547.5.19   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
} #Treaty of Wittenberg
1560.1.1  = { fort_15th = yes }
1790.8.1  = { unrest = 5 } # Peasant revolt
1791.1.1  = { unrest = 0 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
