#103 - Piemont
#turin , ivreja


owner = SAV
controller = SAV
add_core = SAV
culture = piedmontese
religion = catholic
capital = "Turin"
base_tax = 33
base_production = 3
base_manpower = 3
is_city = yes
trade_goods = wheat
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = yes

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}

1250.1.1 = {
	temple = yes
	urban_infrastructure_1 = yes
	constable = yes
	marketplace = yes
	local_fortification_1 = yes
}

1300.1.1 = { road_network = yes }

1356.1.1   = {
	add_permanent_province_modifier = {
		name = "county_of_masserano"
		duration = -1
	}
}
1404.1.1 = { small_university = yes }

1520.5.5 = {
	base_tax = 41
	base_production = 4
	base_manpower = 3
}
1530.1.2 = {
	road_network = no paved_road_network = yes 
}

1530.2.27 = {
	hre = no
}

1536.4.1   = {
	controller = FRA
	add_core = FRA
} 
1538.6.17   = {
	controller = SAV
}
1559.1.1   = {
	remove_core = FRA
}
1563.1.1  = {
	base_tax = 11
base_production = 11
	base_manpower = 7
}
1570.1.1  = {
	
}
1618.1.1  =  { hre = no }
1713.4.12 = {
	owner = SIC
	controller = SIC
	add_core = SIC
} # Treaty of Utrecht
1718.8.2  = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SIC
} # Kingdom of Piedmont-Sardinia
1760.1.1  = {
	
}
1792.9.21  = {
	controller = FRA
} # Conquered by the French
1796.4.25 = {
	owner = FRA
	add_core = FRA
} # The Republic of Alba
1814.4.11 = {
	owner = SPI
	controller = SPI
	remove_core = FRA
} # Napoleon abdicates and Victor Emmanuel is able to return to Turin
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
