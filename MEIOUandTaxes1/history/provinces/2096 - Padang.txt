# 2096 - Padang

owner = MKP
controller = MKP
culture = minang
religion = vajrayana
capital = "Padang"
trade_goods = gold #lumber
hre = no
base_tax = 7
base_production = 2
base_manpower = 1
is_city = yes
add_core = MKP
fort_14th = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian


1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "minangkabau_natural_harbour" 
		duration = -1 
		}
}
1111.1.1 = {
	harbour_infrastructure_2 = yes
	marketplace = yes
	urban_infrastructure_1 = yes
}
1501.1.1 = {
	base_tax = 8
	base_production = 4
}
1515.2.1 = { training_fields = yes }
1524.1.1 = { owner = ATJ controller = ATJ add_core = ATJ }
1650.1.1 = { religion = sunni }
1683.1.1 = { add_core = NED }
1825.1.1 = { owner = NED controller = NED unrest = 2 } # The Dutch gradually gained control
