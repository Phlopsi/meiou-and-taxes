owner = RYU
controller = RYU
culture = ryukyuan
religion = animism
capital = "Naha"
trade_goods = fish # chinaware
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
add_core = RYU

discovered_by = chinese

1000.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "okinawa_natural_harbour" 
		duration = -1 
		}
}
1356.1.1 = {
	temple = yes
	harbour_infrastructure_1 = yes
}
1501.1.1 = {
	base_tax = 5
}
1542.1.1 = { discovered_by = POR }

1609.1.1 = { controller = SMZ } # occupation de Satsuma, King Sho Nei was taken prisoner
1611.1.1 = { controller = RYU } # Sho Nei is released
1624.1.1 = { 
	owner = SMZ
	controller = SMZ
	religion = animism 
} # Annexion par les Shimazu de Satsuma
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
