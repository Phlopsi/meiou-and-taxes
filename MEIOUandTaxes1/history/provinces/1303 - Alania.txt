# 1303 - Alania

owner = WHI
controller = WHI
culture = circassian
religion = sunni
capital = "Majari"
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = gold  
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	add_permanent_province_modifier = {
		name = "caucasian_tribal_area"
		duration = -1
	}
	add_core = WHI
}
1382.1.1 = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1459.1.1 = {
	owner = AST
	controller = AST
	add_core = AST
	remove_core = GOL
}
1555.1.1 = {
	add_core = TUR
}
1774.7.21 = {
	owner = RUS
	controller = RUS
	remove_core = CIR
} # Treaty of Kuchuk-Kainarji
1777.1.1 = { fort_14th = yes }
