# 3355 - Hinojosa del Duque + Don Benito + Villanueva de la Serena

owner = CAS		#Juan II of Castille
controller = CAS
culture = castillian # culture = new_castillian
religion = catholic
hre = no
base_tax = 7
base_production = 0
trade_goods = wool
base_manpower = 0
is_city = yes
capital = "Hinojosa del Duque"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1  = {
	add_core = ENR
	add_core = CAS
	add_permanent_province_modifier = {
		name = "lordship_of_toledo"
		duration = -1
	}
	local_fortification_1 = yes
}
1369.3.23  = { 
	remove_core = ENR
}
1464.5.1   = { unrest = 3 } #Nobiliary uprising against King Enrique, Castilla goes into anarchy
1468.9.18  = { unrest = 0 } #Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved
1470.1.1   = { unrest = 3 } #Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.
1475.6.2   = { controller = POR }
1479.2.25  = { controller = CAS }
1479.9.4   = { unrest = 0 } #Peace of Alca�ovas, between Queen Isabel and King Alfonso of Portugal who had entered the war supporting her wife Juana
1500.3.3   = {
	base_tax = 7
	base_production = 1
	base_manpower = 0
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
 # culture = extremaduran
1713.4.11  = { remove_core = CAS }
1808.6.6   = { controller = REB }
1809.1.1   = { controller = SPA }
1812.7.26  = { controller = REB }
1813.12.11 = { controller = SPA }
