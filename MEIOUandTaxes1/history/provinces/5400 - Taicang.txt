# 5400 - jiangsu_area Taicang

owner = ZOU
controller = ZOU
culture = wuhan
religion = confucianism
capital = "Taicangzhou"
trade_goods = fish
hre = no
base_tax = 83
base_production = 0
base_manpower = 3
is_city = yes

discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	bailiff = yes 
	paved_road_network = yes
	temple = yes 
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1351.1.1  = {
	owner = ZOU
	controller = ZOU
    add_core = ZOU
	add_core = MNG
}	

1369.3.17 = { 
	owner = MNG
	controller = MNG
	remove_core = ZOU
}
1520.2.2 = {
	base_tax = 130
	base_production = 0
	base_manpower = 4
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
