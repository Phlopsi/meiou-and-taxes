#394 - Al Ahsa

owner = AHS
controller = AHS
culture = bahrani
religion = shiite
capital = "Al Hasa"
trade_goods = palm_date
hre = no
base_tax = 10
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = ADA
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1250.1.1 = { 
	temple = yes
	workshop = yes
}
1356.1.1 = {
	add_core = AHS
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
	local_fortification_1 = yes
}
1522.1.1 = {
	owner = BKL
	controller = BKL
	add_core = BKL
}
1640.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1725.1.1 = {
	owner = BKL
	controller = BKL
	remove_core = TUR
	add_core = NAJ
}
1795.1.1 = {
	owner = NAJ
	controller = NAJ
}
1818.9.9 = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The end of the Saudi State
