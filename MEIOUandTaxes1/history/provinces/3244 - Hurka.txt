# No previous file for Hurka

owner = MHR
controller = MHR
culture = jurchen
religion = tengri_pagan_reformed
capital = "Ningguta"
trade_goods = iron
hre = no
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese
discovered_by = steppestech



1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MHR
}
1420.1.1 = {
	owner = MRC
	controller = MRC
	add_core = MRC
	remove_core = MRC
}
1515.1.1 = { training_fields = yes }
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
}
1611.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = MHR
} # The Later Jin Khanate
1616.2.17  = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty