# 2422 - Phitnasulok

owner = SUK
controller = SUK
add_core = SUK
culture = thai
religion = buddhism
capital = "Phitsanulok"
base_tax = 2
base_production = 2
base_manpower = 0
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no


1250.1.1 = { temple = yes }
1350.1.1 = { 
	town_hall = yes
	marketplace = yes
}
1378.1.1 = {
	owner = AYU
	controller = AYU
    	add_core = AYU
	remove_core = SUK
}
1564.2.1 = { add_core = TAU } # Burmese vassal
1584.5.3 = { remove_core = TAU }
1767.4.1 = { unrest = 7 } # The fall of Ayutthaya
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
