# 333 - Mallorca

owner = ARA		
controller = ARA
culture = balearic
religion = catholic
capital = "Mallorca"
base_tax = 4
base_production = 1
base_manpower = 1
is_city = yes
trade_goods = alum

 # Mediterranean trade
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "palma_natural_harbour" 
		duration = -1 
		}
}

1350.1.1 = {
	marketplace = yes
	harbour_infrastructure_2 = yes
	town_hall = yes
	temple = yes
}

1356.1.1  = {
	add_core = ARA
	add_core = BLE
	local_fortification_1 = yes
}
1462.1.1  = {
	unrest = 2
} # Remen�a peasant revolt, in parallel with the Catalan civil war.
1472.1.1  = {
	unrest = 0 } # End of the First Remen�a revolt
1483.1.1 = { small_university = yes }
1500.3.3   = {
	base_tax = 5
	base_production = 1
	base_manpower = 1
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	bailiff = yes
	remove_core = BLE
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1521.3.16 = {
	controller = REB
} # The Germanies movement reaches the archipelago, the viceroy is deposed by the revolters
1522.3.20 = {  naval_arsenal = yes }
1523.3.8  = {
	controller = SPA
} # The royal army retakes the city of Palma
1705.10.9 = {
	controller = HAB
} # Balearic isles side with the Austrian cause
1713.4.11 = {
	controller = REB
} # The pro-Austrian forces in Mallorca and E�vissa ignore the Treaty of Utrecht.
1713.7.13  = { remove_core = ARA remove_core = BLE }
1714.9.14 = {
	controller = SPA
} # Mallorca and E�vissa surrender shortly after the fall of Barcelona
