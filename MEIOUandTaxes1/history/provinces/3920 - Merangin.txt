# 3920 - Merangin

owner = PLB
controller = PLB
culture = sumatran
religion = vajrayana		#this region began to be Islamified c1500
capital = "Merangin"
trade_goods = pepper
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
add_core = SIJ
add_core = PLB
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = {
	add_core = PLB
	add_core = MPH
	add_core = JMB
}
1377.1.1 = {
	owner = MPH
	controller = MPH
	remove_core = PLB
} # Majapahit destroys the remains of the Srivijaya
1405.1.1 = { discovered_by = MNG }
1478.1.1 = {
	owner = JMB
	controller = JMB
	remove_core = MPH
} # Destruction of Majapahit 
1500.1.1 = { religion = sunni add_core = MLC }
1501.1.1 = {
	base_tax = 2
}
1509.1.1 = { discovered_by = POR } # Diego Lopez de Sequiera
1600.1.1 = { discovered_by = NED }
1683.1.1 = { add_core = NED unrest = 1 }
1754.1.1 = { unrest = 4 } #opposition to the Dutch gets fierce
1825.1.1 = { owner = NED controller = NED unrest = 2 } # The Dutch gradually gained control
