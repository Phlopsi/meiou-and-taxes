# No previous file for Tkaronto

culture = huron
religion = totemism
capital = "Tkaronto"
trade_goods = unknown
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 85
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "toronto_natural_harbour"
		duration = -1
	}
}

1609.1.1  = { discovered_by = FRA } # Samuel de Champlain.
1629.1.1  = { discovered_by = ENG }
1674.1.1  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		citysize = 1900
	    } # Construction of Fort Frontenac
1674.1.1  = { fort_14th = yes } # Fort Frontenac
1697.1.1  = { add_core = FRA }
1700.1.1  = { citysize = 4780 }
1707.5.12 = { discovered_by = GBR } 
1750.1.1 = {	
	capital = "Fort Rouill�"
	is_city = yes
	add_core = FRA
}
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.1.1  = { 	culture = english
		religion = protestant 
	} #Loyalist migration after the revolution
1788.2.10 = { add_core = GBR }
