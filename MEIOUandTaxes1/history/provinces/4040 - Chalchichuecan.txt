# 4040 - Chalchichuecan

# owner = TOT
# controller = TOT
# add_core = TOT
culture = totonac
religion = nahuatl
capital = "Chalchichuecan"
base_tax = 4
base_production = 0
base_manpower = 0
# citysize = 1500
trade_goods = maize 
discovered_by = mesoamerican
hre = no
1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "tohancapan_natural_harbour" 
		duration = -1 
		}
}
1518.4.8   = {
	discovered_by = SPA
	capital = "San Juan de Ulua"
} # juan de grijalva
1519.4.22  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	capital = "Villa Rica de la Vera Cruz"
	#citysize = 200
	culture = castillian
	religion = catholic
	base_tax = 5
	base_production = 5
	trade_goods = gold
	naval_arsenal = yes
	marketplace = yes
	regional_capital = yes
	bailiff = yes
	is_city = yes
} # hernan cort�s disembarked
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1600.1.1   = {
	citysize = 3000
}
1608.1.1   = {
	add_core = SPA
	capital = "Veracruz"
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
