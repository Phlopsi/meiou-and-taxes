# 139 - Bosna


owner = BOS
controller = BOS
culture = serbian
religion = orthodox # with large gnostic minotiry
capital = "Doboj"
trade_goods = salt
hre = no
base_tax = 9
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1   = {
	add_core = CRO
	add_core = BOS
} # S� and Macs� B�ns�g
1444.1.1 = {
	add_core = TUR
}
1463.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The Ottoman province of Bosnia
1520.5.5 = {
	base_tax = 11
	base_production = 0
	base_manpower = 0
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1593.1.1  = {
	unrest = 3
} # Fighting began in northwestern Bosnia, sparked Habsburg-Ottoman conflict
1606.1.1  = {
	unrest = 0
} # Temporarty peace
1650.1.1  = {
	culture = bosnian
	religion = sunni
}
1670.1.1  = {
	
}
1683.1.1  = {
	unrest = 6
} # Heavy fighting & destruction in western Bosnia
1699.1.1  = {
	unrest = 0
} # Flood of Muslim refugees from Slavonia & Ottoman Hungary 
1700.1.1  = {
	unrest = 7
}
1716.12.9 = {
	controller = HAB
} # Occupied by Habsburg
1718.7.21 = {
	controller = TUR
} # Estimated
1730.1.1  = {
	
}
1737.7.1  = {
	controller = HAB
} # Occupied by Habsburg again
1739.9.18 = {
	controller = TUR
} # Treaty of Belgrade, Habsburg gave up its claim to the territory
1788.2.9  = {
	controller = HAB
} # Habsburg invasion
1791.8.4  = {
	controller = TUR
} # Peace settlement
