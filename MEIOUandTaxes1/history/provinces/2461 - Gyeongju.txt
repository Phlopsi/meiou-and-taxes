# 2461 - Gyeongju

owner = KOR
controller = KOR
add_core = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Gyeongju"
base_tax = 18
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = tea
discovered_by = chinese
discovered_by = steppestech
hre = no

987.1.1 = {
	rename_capital = "Donggyeong"
}
1000.1.1   = {
	set_province_flag = has_great_natural_harbour
	set_province_flag = has_natural_harbour
	set_province_flag = has_estuary
	set_province_flag = great_natural_place
	add_permanent_province_modifier = { 
		name = "gyeongsang_large_natural_harbor" 
		duration = -1 
	}
}
1001.1.1 = {
	harbour_infrastructure_2 = yes
	marketplace = yes
	urban_infrastructure_1 = yes
	temple = yes
}

1200.1.1 = { road_network = yes }
1308.1.1 = {
	rename_capital = "Gyerim"
}
1356.1.1  = {
	revolt = {
		type = wokou_pirates
		size = 1
	}
	controller = REB
	unrest = 3
} 
1360.1.1  = {
	revolt = { }
	controller = KOR
	unrest = 0
} # Pirates chased
1392.6.5  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
	rename_capital = "Gyeongju"
}
1444.1.1 = {
	base_tax = 44
}
1520.5.5 = {
	base_tax = 71
	base_production = 4
	base_manpower = 3
}
1592.4.24 = {
	controller = ODA
} # Japanese invasion
1593.5.18 = {
	controller = JOS
	add_core = ODA
} # The Japanese still retained a small foothold after their first invasion
1597.1.1  = {
	controller = ODA
}
1597.11.1 = {
	controller = JOS
	remove_core = ODA
}
1637.1.1  = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1  = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1  = {
	discovered_by = NED
} # Hendrick Hamel
1680.1.1  = {
	
} # Center of herbal trade in Joseon
1760.1.1  = {
	
}
