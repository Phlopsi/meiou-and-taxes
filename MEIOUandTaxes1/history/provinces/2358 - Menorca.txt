# 2358 - Minorca

owner = ARA		
controller = ARA
culture = balearic
religion = catholic
capital = "Ciutadella"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = cheese


discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

hre = no

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	add_permanent_province_modifier = { 
		name = "the_baleares_natural_harbour" 
		duration = -1 
		}
}

1356.1.1  = {
	add_core = ARA
	add_core = BLE
	local_fortification_1 = yes
}
1462.1.1  = {
	unrest = 2
} # Remen�a peasant revolt, in parallel with the Catalan civil war.
1472.1.1  = {
	unrest = 0 } # End of the First Remen�a revolt
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	bailiff = yes
	remove_core = BLE
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1521.3.16 = {
	controller = REB
} # The Germanies movement reaches the archipelago, the viceroy is deposed by the revolters
1523.3.8  = {
	controller = SPA
} # The royal army retakes the city of Palma
1708.1.1  = {
	controller = GBR
} # War of Spanish Succession
1713.4.11 = {
	owner = GBR
	capital = "Ma�"
	add_core = GBR
} # Treaty of Utrecht
1713.7.13  = { remove_core = ARA remove_core = BLE }
1756.6.29 = {
	controller = SPA
} # Seven Year War
1763.2.10 = {
	controller = GBR
} # Treaty of Paris
1782.2.5  = {
	controller = SPA
} # American War of Independance
1783.9.3  = {
	owner = SPA
} # Treaties of Versailles
1798.1.1  = {
	controller = GBR
} # War of Second Coalition
1802.3.25 = {
	controller = SPA
	remove_core = GBR
} # Treaty of Amiens
