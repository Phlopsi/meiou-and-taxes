# 3005 - Fife

owner = SCO
controller = SCO
culture = lowland_scottish
religion = catholic
hre = no
base_tax = 4
base_production = 0
trade_goods = wool
base_manpower = 0
is_city = yes
capital = "Glenrothes"
add_core = SCO
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1250.1.1 = { 
	local_fortification_1 = yes
}
1482.8.1   = { controller = ENG }
1483.3.1   = { controller = SCO }
1520.5.5  = {
	base_tax = 4
	base_production = 1
	base_manpower = 0
}
1547.10.1  = { controller = ENG } #Rough Wooing
1550.1.1   = { controller = SCO } #Scots Evict English with French Aid
1560.8.1   = {
	religion = reformed
	#reformation_center = reformed
}
1650.12.24 = { controller = ENG } #Cromwell Captures Edinburgh Castle
1652.4.21  = { controller = SCO } #Union of Scotland and the Commonwealth
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1   = {   } #Regimental Camp, Tax Assessor Estimated
