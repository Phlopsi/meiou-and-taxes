# 3355 - Kawachi

owner = HKY
controller = HKY
culture = kansai
religion = mahayana
capital = "Kawachinagano"
trade_goods = rice
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese


1356.1.1 = {
	add_core = HKY
	controller = JAP
	local_fortification_1 = yes
}
1359.1.1 = {
	controller = HKY
}
1369.1.1 = { #captured by Kusunoki Masanori until surrender ADD TAG
	controller = JAP
}
1382.1.1 = {
	owner = HKY
	controller = HKY
}
1501.1.1 = {
	base_tax = 13
	base_manpower = 1
}
1542.1.1   = { discovered_by = POR }
1568.1.1   = {
	owner = MIY
	controller = MIY
}
1572.1.1   = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
