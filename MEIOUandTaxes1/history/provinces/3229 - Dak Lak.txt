# No previous file for Dak Lak

owner = CHA
controller = CHA
add_core = CHA
culture = degar
religion = hinduism
capital = "Bu�n Ma Thu�t"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1356.1.1 = {
}
1501.1.1 = {
	base_tax = 4
}
1515.1.1 = { regimental_camp = yes }
1517.2.3 = { bailiff = yes }
1522.4.4 = { marketplace = yes }
1535.1.1 = { discovered_by = POR }
1693.1.1 = { unrest = 0 } # Plague
1698.1.1 = {
	owner = ANN
	controller = ANN
	add_core = ANN
} # Vietnamese control
1769.1.1 = { unrest = 6 } # Tay Son Rebellion
1776.1.1 = {
	unrest = 0
	owner = DAI
	controller = DAI
	add_core = DAI
} # Tay Son Dynasty conquered the Nguyen Lords
1786.1.1 = { unrest = 5 } # Unsuccessful revolt
1787.1.1 = { unrest = 0 }
1802.7.22 = {
	owner = ANN
	controller = ANN
	remove_core = DAI
} # Nguyen Phuc Anh conquered the Tay Son Dynasty
1883.8.25 = { 
	owner = FRA
	controller = FRA
}
