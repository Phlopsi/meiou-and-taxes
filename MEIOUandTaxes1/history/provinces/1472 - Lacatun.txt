# 1472 - Lacatun

# owner = MAY
# controller = MAY
# add_core = MAY
culture = tzeltal
religion = mesoamerican_religion
capital = "Bonampak" 
base_tax = 6
base_production = 0
base_manpower = 0
trade_goods = maize
hre = no

discovered_by = mesoamerican

1502.1.1   = {
	discovered_by = CAS
}
1516.1.23  = {
	discovered_by = SPA
}
1528.2.12  = {
	owner = SPA
	controller = SPA
	citysize = 200
	religion = catholic
	add_core = SPA
	is_city = yes
	
}
1553.1.1   = {
	add_core = SPA
	citysize = 5000
}
1750.1.1   = {
	add_core = MEX
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
