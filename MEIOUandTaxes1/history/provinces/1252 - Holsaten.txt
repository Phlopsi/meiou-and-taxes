# 1252 - Holstein

add_core = SHL
owner = SHL
controller = SHL
culture = old_saxon
religion = catholic
hre = yes
base_tax = 15
base_production = 1
trade_goods = wax
base_manpower = 1
is_city = yes
capital = "Kyle"
discovered_by = eastern
discovered_by = western
discovered_by = muslim


999.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "holstein_natural_harbour" 
		duration = -1 
	}
	local_fortification_1 = yes
}

1088.1.1 = { harbour_infrastructure_1 = yes }
1100.1.1 = {
	marketplace = yes
	town_hall = yes
}

1200.1.1 = { road_network = yes }
1460.1.1   = {
	add_core = DEN
}
1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 17
	base_production = 1
	base_manpower = 1
}
1523.6.21  = {
	#add_core = DAN
	remove_core = DEN
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1536.1.1   = {
	religion = protestant
}
1644.1.12  = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13  = {
	controller = SHL
} #The Peace of Br�msebro
1657.10.23 = {
	controller = SWE
} #Karl X Gustavs First Danish War-Captured by Wrangel
1658.2.26  = {
	controller = SHL
} #The Peace of Roskilde - Duchy fully independent
1710.1.1   = {
	
}
1720.7.3   = {
	remove_core = DAN
} # Treaty of Frederiksborg
1773.1.1   = {
	add_core = DAN
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9   = {
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
