#1153 - Sheba

owner = CKW
controller = CKW
add_core = CKW
culture = chokwe
religion = animism
capital = "Kalemia"
trade_goods = millet
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
hre = no
discovered_by = central_african

500.1.1 = {
	add_permanent_province_modifier = {
		name = "ivory_medium"
		duration = -1
	}
}
