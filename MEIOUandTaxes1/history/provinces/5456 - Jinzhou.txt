# 5446 jinzhou

owner = TIA
controller = TIA
culture = miao
religion = animism
capital = "Huaihua"
trade_goods = lumber
hre = no
base_tax = 14
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	owner = ZHN
	controller = ZHN
	add_core = ZHN
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CEN
	remove_core = CMN
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 20
	base_manpower = 1
}
1662.1.1 = {
	owner = CMN
	controller = CMN
	add_core = CMN
	remove_core = MNG
}# Geng Jingzhong appointed as viceroy
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1680.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = CMN
}
