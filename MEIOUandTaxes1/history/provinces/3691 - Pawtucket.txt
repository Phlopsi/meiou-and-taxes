# No previous file for Pawtucket

culture = mahican
religion = totemism
capital = "Pawtucket"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 85
native_ferocity = 3
native_hostileness = 4

1605.1.1   = { discovered_by = ENG } # George Waymouth
1606.1.1   = { discovered_by = FRA } # Samuel de Champlain
1623.1.1   = {	owner = ENG
		controller = ENG
		capital = "Casco"
		citysize = 445
		culture = english
	     	religion = reformed
			#trade_goods = naval_supplies
	     } # Settled by the British
1632.1.1   = {	citysize = 1125
			     } # Fort William & Mary
1648.1.1   = { add_core = ENG }
1650.1.1   = { citysize = 2990 }
1653.1.1   = { capital = "Portsmouth" }
1700.1.1   = { citysize = 3580 }
1707.5.12  = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1   = { citysize = 4280 }
1764.7.1   = {	culture = american
		unrest = 6
	     } # Growing unrest
1774.12.14 = { unrest = 7 } # Local mob led by John Langdon stormed the fort
1776.7.4   = {	owner = USA
		controller = USA
		add_core = USA
	     } # Declaration of independence
1782.11.1  = {	remove_core = GBR 
		unrest = 0
	     } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1   = { citysize = 6400 }
