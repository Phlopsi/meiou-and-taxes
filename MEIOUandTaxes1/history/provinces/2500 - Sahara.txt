# 2500 - Al Fula

base_tax = 6
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 2
native_hostileness = 2
culture = nuba
religion = animism
capital = "Al Fula"
trade_goods = unknown # salt
hre = no
discovered_by = ALW
discovered_by = YAO
discovered_by = ETH
discovered_by = ADA
discovered_by = DAR
discovered_by = KIT
discovered_by = MKU
discovered_by = DSL
discovered_by = SLL

1530.1.1 = { religion = sunni } #Spread of Islam leads to shift in religion affiliation of region
1550.1.1 = { discovered_by = TUR }
1600.1.1 = {
	owner = KDF
	controller = KDF
	add_core = KDF
	discovered_by = KDF
	discovered_by = SEN
	discovered_by = ETH
	is_city = yes
	trade_goods = wool
	base_tax = 6
}
1820.1.1 = {
	owner = TUR
	controller = TUR
}
