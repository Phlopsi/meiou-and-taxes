# 3364 - Suwo

owner = OUC
controller = OUC
culture = chugoku
religion = mahayana
capital = "Yamaguchi"
trade_goods = rice
hre = no
base_tax = 12
base_production = 3
base_manpower = 2
is_city = yes

discovered_by = chinese

1133.1.1 = {
	workshop = yes
	urban_infrastructure_1 = yes
	harbour_infrastructure_2 = yes
	marketplace = yes
	local_fortification_1 = yes
}
1356.1.1 = {
	add_core = OUC
}
1501.1.1 = {
	base_tax = 19
	base_production = 6
	base_manpower = 4
}
1542.1.1 = { discovered_by = POR }
1557.1.1 = {
	add_core = MRI
	owner = MRI
	controller = MRI
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
