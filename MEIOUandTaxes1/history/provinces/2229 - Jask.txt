# 2229 - Jask

owner = ORM
controller = ORM
culture = baluchi
religion = sunni
capital = "Jask"
trade_goods = fish
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = indian
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = ORM
}
1393.5.22  = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = ORM
}
1444.1.1 = {
	owner = ISF
	controller = ISF
	remove_core = TIM
	add_core = AKK
}
1447.1.1   = {
	controller = QAR
}
1447.3.11  = {
	owner = QAR
	add_core = QAR
	remove_core = TIM
} # Fars and surroundings to Qara Quyunlu
1469.11.1  = {
	controller = AKK
}
1470.1.1   = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1490.1.1   = {
	controller = REB
	revolt = { type = noble_rebels }
} # Civil War
1494.1.1   = {
	controller = AKK
	revolt = { }
} # Civil War
1500.1.1 = {
	base_tax = 2
}
1501.1.1   = {
	controller = SAM
}
1508.2.1   = {
	owner = SAM
}
1512.1.1   = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	remove_core = AKK
	training_fields = yes
	bailiff = yes
	courthouse = yes
} # Safawids "form persia"
