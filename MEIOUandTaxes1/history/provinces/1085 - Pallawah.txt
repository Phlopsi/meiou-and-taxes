# 1085 - Paredarerme

culture = aboriginal
religion = polynesian_religion
capital = "Paredarerme"
hre = no
trade_goods = unknown
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 5
native_ferocity = 0.5
native_hostileness = 0

1642.11.24 = { discovered_by = NED } # Abel Tasman
1803.1.1   = {
	owner = GBR
	controller = GBR
	capital = "Hobart"
	citysize = 100
	trade_goods = iron
	set_province_flag = trade_good_set
} # Penal colony
