# 1378 - Ferrara

owner = FER
controller = FER
culture = emilian 
religion = catholic 
hre = yes 
base_tax = 4
base_production = 3        
trade_goods = rice    
base_manpower = 0
is_city = yes       

capital = "Fr�ra"

 # Completed 1135
add_core = FER

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
	local_fortification_1 = yes
}
1000.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_delta_estuary
	add_permanent_province_modifier = {
		name = po_estuary_modifier
		duration = -1
	}
}
1111.1.1 = {
	urban_infrastructure_1 = yes
	harbour_infrastructure_1 = yes
	workshop = yes
}

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }

1391.3.4   = { small_university = yes }
1400.1.1 = {
	art_corporation = yes # School of Ferrara
}
1520.5.5 = {
	base_tax = 5
	base_production = 4
	base_manpower = 0
	fort_14th = yes 
}

1530.1.2 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
	hre = no 
}

1597.10.28   = { controller = PAP owner = PAP add_core = PAP add_core = MOD } # Annexed to the Holy See
1610.1.1   = { fort_14th = yes }
1618.1.1  =  { hre = no }
1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = HAB
} # Cispadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1802.6.26  = { remove_core = MOD }
1814.4.11  = {
	owner = PAP
	controller = PAP
	add_core = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = HAB
}
1861.2.18 = {
	owner = ITA
	controller = ITA
	add_core = ITA
	remove_core = SPI
}
