# No previous file for Ankole

owner = NKO
controller = NKO
add_core = NKO
culture = ganda
religion = animism
capital = "Ankolee" #Should really change with each monarch
trade_goods = salt
hre = no
base_tax = 10
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = central_african

1520.1.1 = { base_tax = 17 }