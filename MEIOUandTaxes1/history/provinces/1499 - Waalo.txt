#1499 - Waalo

owner = DJO
controller = DJO
culture = senegambian
religion = west_african_pagan_reformed
capital = "Diourbel"
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = DJO
}
1444.1.1 = {
	discovered_by = POR
}
1520.1.1 = {
	base_tax = 6
}
1549.1.1 = {
	owner = FLO
	controller = FLO
	add_core = FLO
}
