# 566 - Darrang

owner = ASS
controller = ASS
culture = assamese
religion = hinduism
capital = "Koliabar"
trade_goods = livestock
base_tax = 37
base_production = 0
base_manpower = 3
citysize = 18000
add_local_autonomy = 25

discovered_by = indian
discovered_by = chinese

1120.1.1 = { farm_estate = yes }
1356.1.1 = { add_core = ASS }
1450.1.1 = { citysize = 20000 }
1500.1.1 = { citysize = 21000 }
1511.1.1 = {
	base_tax = 46
	base_production = 2
}
1515.1.1 = { training_fields = yes }
1550.1.1 = { citysize = 23000 }
1600.1.1 = { citysize = 25000 }
1650.1.1 = { citysize = 27000 }
1662.3.1 = { controller = MUG } # Mirjumla entered Gargaon
1663.1.1 = { owner = MUG }
1667.1.1 = { controller = ASS } # The Mughals are defeated
1669.1.1 = { owner = ASS }	# The Mughals are defeated
1670.1.1 = {
	fort_17th = yes
}
1700.1.1 = { citysize = 30000 }
1750.1.1 = { citysize = 32000 }
1769.1.1 = {
	fort_17th = no
	fort_18th = yes
}
1770.1.1 = { unrest = 8 } # Moamoria rebellion
1780.1.1 = { unrest = 0 }
1800.1.1 = { citysize = 35000 }
