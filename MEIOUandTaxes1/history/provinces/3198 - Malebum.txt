# No previous file for Malebum

owner = NPL
controller = NPL
culture = nepali
religion = hinduism
capital = "Jumla"
trade_goods = lumber #naval_supplies
hre = no
base_tax = 24
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech
add_local_autonomy = 25

1356.1.1  = {
	add_core = NPL
}
1511.1.1 = {
	base_tax = 27
}
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
