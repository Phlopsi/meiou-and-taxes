# No previous file for Issyk-Kol

owner = MGH
controller = MGH
culture = kirgiz
religion = sunni
capital = "Kochkor"
trade_goods = wool
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = KSH
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1356.1.1 = {
	add_core = MGH
	add_core = KAS
}
1482.1.1 = {
	owner = KAS
	controller = KAS
	remove_core = MGH
}
1501.1.1 = {
	base_tax = 4
}
1515.1.1 = { training_fields = yes }
1520.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = KAS
} # Emirate of Bukhara established
1678.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
}
1755.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = ZUN
} # Part of the Manchu empire
