# 2680 - Pune

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Pune"
trade_goods = millet
hre = no
base_tax = 39
base_production = 0
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = muslim 
add_local_autonomy = 25

1000.1.1 = {
	add_permanent_province_modifier = {
		name = phaltan_state
		duration = -1
	}
}
1249.1.1 = {
	local_fortification_1 = yes
}
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = BAH
	add_core = BAS
	#fort_14th = yes
}
1490.1.1  = {
	remove_core = BAH
	controller = BAS
	owner = BAS
} # The Breakup of the Bahmani sultanate
1498.1.1  = { discovered_by = POR }
1511.1.1 = {
	base_tax = 53
}
1530.2.3 = {
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1633.7.27 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by Shah Jahan
1650.1.1  = {
	add_core = MAR #Maratha Identity
}
1657.1.1  = {
	owner = MAR
	controller = MAR
	remove_core = MUG
	fort_15th = yes
} # Inheritance of Shivaj
1680.1.1  = { fort_15th = no fort_16th = yes }
1736.1.1  = { fort_16th = no fort_17th = yes }
1818.6.3  = {
	owner = GBR
	controller = GBR
}
