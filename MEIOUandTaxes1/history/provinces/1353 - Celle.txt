# 1353 - Lauenburg

owner = LAU
controller = LAU
culture = old_saxon
religion = catholic
capital = "Lauenburg"
trade_goods = wheat
hre = yes
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
add_core = LAU

discovered_by = eastern
discovered_by = western
discovered_by = muslim



#1000.1.1 = {
#	add_permanent_province_modifier = {
#		name = "elbe_navigable_river"
#		duration = -1
#	}
#}



1133.1.1 = { 
	local_fortification_1 = yes
}
1200.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 3
	base_production = 1
}
1525.1.1 = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1805.1.1  = {
	owner = PRU
	controller = PRU
	add_core = PRU
}
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
    remove_core = WES
}
1813.10.14 = {
	owner = HAN
	controller = HAN
	remove_core = FRA
} # Westfalia is dissolved after the Battle of Leipsig
1815.5.29 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
}
1815.6.4 = {
	owner = LAU
	controller = LAU
	remove_core = PRU
}
