# 525 - Ajmer

owner = NAG
controller = NAG
culture = dhundari
religion = hinduism
capital = "Ajmer"
trade_goods = copper
hre = no
base_tax = 11
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = { town_hall = yes }
1356.1.1  = {
	add_core = NAG
	add_core = MEW
	#unrest = 10
	fort_14th = yes #lone Delhi holdout in Rajastan
}
1365.1.1  = {
	owner = MEW
	controller = MEW
}
1390.1.1  = {
	add_core = MEW
	remove_core = DLH
}
#1530.1.1  = {
#	owner = MUG
#	controller = MUG
#} # Mughal Conquest
1511.1.1 = {
	base_tax = 14
	base_production = 2
}
1515.12.17 = { training_fields = yes }
1530.1.1 = { 
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1540.1.1  = {
	owner = MAW
	controller = MAW
} # Rajput again after the fall of Delhi
1543.1.1  = {
	owner = BNG
	controller = BNG
} # Conquered by Sher Shah Sur
1553.1.1  = {
	owner = DLH
	controller = DLH
} # Split of Suri Empire
1558.1.1  = { controller = MUG }
1558.2.1  = { owner = MUG  }
1608.2.1  = { add_core = MUG }
1690.1.1  = { discovered_by = ENG }
1707.1.1  = {
	controller = MEW
} # Independent
1707.3.1  = {
	owner = MAW
	controller = MAW
	remove_core = MUG
} # Independent
1707.5.12 = { discovered_by = GBR }
1756.1.1  = {
	owner = GWA
	controller = GWA
	add_core = GWA
} # Independent
1818.6.3  = {
	owner = GBR
	controller = GBR
}
