#245 - Yorkshire

owner = ENG
controller = ENG
culture = northern_e
religion = catholic
hre = no
base_tax = 18
base_production = 2
trade_goods = wool #lead
base_manpower = 2
is_city = yes
capital = "York"
add_core = ENG
 #York Cathedral
estate = estate_nobles
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1066.1.1   = {
	add_permanent_province_modifier = {
		name = "north_of_england"
		duration = -1
	}
}
1100.1.1 = { harbour_infrastructure_1 = yes }
1101.1.1 = {
	marketplace = yes
	town_hall = yes
}
1200.1.1   = {
	set_province_flag = has_estuary
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "lincoln_natural_harbour" 
		duration = -1 
	}
	local_fortification_1 = yes
}

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1453.1.1  = { unrest = 5 } #Start of the War of the Roses
1461.4.1  = { controller = REB } #Capture of York after Battle of Towton
1461.6.1  = { unrest = 2 controller = ENG } #Coronation of Edward IV
1467.1.1  = { unrest = 5 } #Rivalry between Edward IV & Warwick
1470.3.1  = { controller = REB }
1470.10.6 = { controller = ENG } #Readeption of Henry VI
1471.1.1  = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.3.1  = { controller = REB }
1471.4.11  = {
	remove_province_modifier = "north_of_england"
	add_permanent_province_modifier = {
		name = "council_of_north"
		duration = -1
	}
} # Council established by Edward IV and  headquartered at Sheriff Hutton Castle and Sandal Castle
1471.5.4  = { unrest = 2 controller = ENG } #Murder of Henry VI & Restoration of Edward IV
1483.6.26 = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = { unrest = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1520.5.5  = {
	base_tax = 28
	base_production = 2
	base_manpower = 3
}
1530.1.1 = { culture = english road_network = no paved_road_network = yes }

1600.1.1  = { religion = protestant  fort_14th = yes  } #anglican #constable Estimated
1641.11.22 = {
	remove_province_modifier = "council_of_north"
} # Council abolishedbecause of its support for Catholic Recusants
1644.7.2  = { controller = REB } #Battle of Marston Moor
1646.5.5  = { controller = ENG } #End of First English Civil War
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
  	remove_core = ENG
}
1750.1.1  = { capital = "Leeds"  } #Tax Assessor Estimated
