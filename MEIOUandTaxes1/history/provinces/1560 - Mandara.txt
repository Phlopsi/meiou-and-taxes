# 1560 - Mandara

owner = KBO
controller = KBO
culture = kanouri		
religion = animism		 
capital = "Mandara"
base_tax = 10
base_production = 0
base_manpower = 0
citysize = 5000
trade_goods = millet
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = KBO
}
1520.1.1 = { base_tax = 14 }
