# 1417 - Eileanan Siar

owner = NOR
controller = NOR
culture = norse_gaelic
religion = catholic
hre = no
base_tax = 1

base_production = 0
trade_goods = fish
base_manpower = 0

is_city = yes
capital = "Ste�rnabhagh"
discovered_by = western
discovered_by = muslim
discovered_by = eastern



1266.1.1   = {
	owner = TLI
	controller = TLI
	add_core = TLI
}
1356.1.1 = {
	local_fortification_1 = yes
}
1493.1.1   = {
	owner = SCO
	controller = SCO
	add_core = SCO
}
1530.1.4  = {
	bailiff = yes	
}
1560.8.1   = { religion = reformed }
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1   = { fort_14th = yes }
