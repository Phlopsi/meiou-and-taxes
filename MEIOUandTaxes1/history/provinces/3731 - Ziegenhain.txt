# 3731 - Ziegenhain

owner = ZIE
controller = ZIE
capital = "Ziegenhain"
trade_goods = wool
culture = hessian
religion = catholic
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = { 
	add_core = ZIE 
	local_fortification_1 = yes
}
1450.2.14  = {
	owner = HES
	controller = HES
	add_core = HES
} # Inherited by Hesse
1470.1.1   = {
	fort_15th = yes
}
1500.1.1 = { road_network = yes }
1530.1.4  = {
	bailiff = yes	
}
1548.1.1   = {
	fort_15th = no
	fort_16th = yes
}
1567.3.31  = {
	owner = HKA
	controller = HKA
	add_core = HKA
	remove_core = HES
}
1803.2.25 = {
	owner = HES
	controller = HES
	add_core = HES
	remove_core = HKA
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.9.1   = { controller = RUS } # Occupied by Russian troops
1813.10.14 = {
	owner = HES
	controller = HES
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
1866.1.1  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES 
}
