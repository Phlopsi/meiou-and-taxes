# 2510 - sichuan_area Xichang

owner = DLI	
controller = DLI
culture = yizu
religion = animism
capital = "Yibin"
trade_goods = tin
hre = no
base_tax = 23
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech



1253.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1274.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1356.1.1 = {
	
}

1375.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = LNG
	remove_core = YUA
} 
1520.1.1 = { 
	culture = wuhan 
	religion = confucianism
}
1520.2.2 = {
	base_tax = 34
	base_manpower = 3
}
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty