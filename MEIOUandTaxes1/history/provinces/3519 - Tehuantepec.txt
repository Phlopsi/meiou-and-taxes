# 3519 - Mapachtepec

owner = XOC
controller = XOC
add_core = XOC
culture = mixean
religion = nahuatl
capital = "Mapachtepec" 

base_tax = 6
base_production = 0
base_manpower = 0
citysize = 20000
trade_goods = maize

discovered_by = mesoamerican

hre = no

1356.1.1 = {
}
1487.1.1   = {
	owner = AZT
	controller = AZT
	citysize = 10000

	trade_goods = maize
} # Conquest by Ahuitzotl, eighth tlatoani of Tenochtitlan
1506.1.1   = {
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	citysize = 2000
	add_core = SPA
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
