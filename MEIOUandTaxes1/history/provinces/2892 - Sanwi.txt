# No previous file for Sanwi

culture = akaa
religion = west_african_pagan_reformed
capital = "Sanwi"
trade_goods = unknown # palm
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
native_size = 80
native_ferocity = 4.5
native_hostileness = 9

discovered_by = sub_saharan

1456.1.1 = { discovered_by = POR } #Cadamosto
