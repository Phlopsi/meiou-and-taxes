# 2743 - Kumul

owner = YUA
controller = YUA
culture = chaghatai
religion = mahayana
capital = "Hami"
trade_goods = millet
hre = no
base_tax = 3
base_production = 1
base_manpower = 0
citysize = 12540
discovered_by = chinese
discovered_by = steppestech
1000.1.1 = {
	town_hall = yes
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = YUA
	add_core = MGH
}
1359.1.1 = {
	religion = sunni
}
#1368.1.1  = {
#	owner = OIR
#	controller = OIR
#	remove_core = YUA
#}
1389.1.1 = {
	add_core = HMI
	owner = HMI
	controller = HMI
	remove_core = YUA
}
1473.1.1 = {
	owner = MGH
	controller = MGH
}
1482.1.1 = {
	owner = HMI
	controller = HMI
}
1487.1.1 = {
	culture = uyghur
}
1488.1.1 = {
	owner = MGH
	controller = MGH
}
1492.1.1 = {
	owner = HMI
	controller = HMI
}
1493.1.1 = {
	owner = MGH
	controller = MGH
}
1495.1.1 = {
	owner = MNG
	controller = MNG
}
1513.1.1 = {
	owner = CHG
	controller = CHG
}
1515.1.1 = { training_fields = yes }
1530.1.1 = {
	owner = MGH
	controller = MGH
	add_core = MGH
	remove_core = UIG
	remove_core = CHG
}
1530.1.1 = { discovered_by = muslim }
1570.1.1 = {
	owner = KAS
	controller = KAS
	add_core = KAS
}
1682.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
}
1709.1.1 = { discovered_by = SPA }
1755.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
