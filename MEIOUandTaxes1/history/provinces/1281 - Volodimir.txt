# 1281 - Volhynia

owner = LIT
controller = LIT
add_core = LIT
add_core = GVO
capital = "Ratnije"
culture = ruthenian
religion = orthodox
trade_goods = livestock
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech


1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_claim = POL
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}

1366.9.1 = {
	owner = GVO
	controller = GVO
}

1370.11.5 = {
	owner = LIT
	controller = LIT
}

1377.1.1 = {
	owner = GVO
	controller = GVO
}

1393.1.1 = {
	remove_core = LIT
	owner = POL
	controller = POL
	add_core = POL
}
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1569.7.1  = {	
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1656.1.1 = {
	controller = RUS
}
1660.1.1 = {
	controller = PLC
}
1795.10.24  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	culture = byelorussian
} #Third Partition
1812.6.28  = { controller = FRA } # Occupied by French troops
1812.12.10 = { controller = RUS }
