# No previous file for Millioke

culture = winnebago
religion = totemism
capital = "Millioke"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 30
native_ferocity = 2
native_hostileness = 5

1634.1.1  = { discovered_by = FRA } # Jean Nicolet
1664.1.1  = { discovered_by = ENG }
1671.1.1  = {	owner = FRA
		controller = FRA
		citysize = 540
		culture = francien
		religion = catholic
		trade_goods = fur
	    } # Founding of La Baye
1696.1.1  = { add_core = FRA }
1697.1.1  = { citysize = 1090 } # The fur trade attracted more settlers
1700.1.1  = { citysize = 1988 }
1707.5.12 = { discovered_by = GBR }
1712.1.1  = { unrest = 5 } # Fox war
1714.1.1  = { unrest = 0 }
1728.1.1  = { unrest = 5 } # Second Fox war
1729.1.1  = { unrest = 0 }
1750.1.1  = { citysize = 2570 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = protestant
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.10.9 = {	owner = XXX # OJI
		controller = XXX # OJI
		# add_core = XXX # OJI
		culture = winnebago
		religion = totemism
	    } # Royal Proclamation, land set aside for natives
1800.1.1  = { citysize = 3250 }
1813.10.5 = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #The death of Tecumseh mark the end of organized native resistance 
