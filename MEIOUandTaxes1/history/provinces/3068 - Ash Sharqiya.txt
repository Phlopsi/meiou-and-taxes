# No previous file for Ash Sharqiya

owner = OMA
controller = OMA
culture = omani
religion = ibadi
capital = "Sur"
trade_goods = fish #pearls
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = muslim
discovered_by = indian

500.1.1 = {
	add_permanent_province_modifier = {
		name = "pearls_low"
		duration = -1
	}
}

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = OMA
}
1480.1.1  = { discovered_by = TUR }
1500.3.3   = {
	base_tax = 3
}
1507.6.1  = {
	discovered_by = POR
	owner = POR
	controller = POR
	add_core = POR
	naval_arsenal = yes
	marketplace = yes 
	customs_house = yes
} # Captured by the Portuguese
1533.1.1  = { add_core = POR }
1550.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # Occupied by the Ottomans
1551.1.1  = { owner = POR controller = POR }
1581.1.1  = { owner = TUR controller = TUR }
1588.1.1  = { owner = POR controller = POR }
1624.1.1  = {
	owner = OMA
	controller = OMA
	remove_core = TUR
	remove_core = POR
}
1630.1.1  = { fort_14th = yes }

1679.1.1  = { unrest = 5 } # Internal unrest upon Imam's death
1741.1.1  = {
	owner = PER
	controller = PER	
	add_core = PER
} # Invaded by Persia
1749.6.10 = {owner = OMA
	controller = OMA
	remove_core = PER
	unrest = 0
}
