# 2848 - Cherkassy

owner = KIE
controller = KIE
culture = ruthenian
religion = orthodox	
capital = "Cherkassy"
base_tax = 9
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wheat
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_permanent_claim = LIT
	add_core = KIE
}
1471.1.1 = {
	owner = LIT
	controller = LIT
	add_core = LIT
} # incorporated into Lithuania
1501.1.1 = {
	base_tax = 8
	base_production = 3
	base_manpower = 1
}

1530.1.4  = {
	bailiff = yes
	culture = ukrainian
}
1569.6.6 = {
	owner = POL
	controller = POL
	add_core = POL
	remove_core = LIT
}# Annexed to the crown of poland before Union of Lublin
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
	remove_core = LIT
} # Union of Lublin
1649.2.1   = {
	owner = ZAZ
	controller = ZAZ
	add_core = ZAZ
}
1654.1.1  = {
	add_core = RUS
} # Treaty of Pereyaslav
1667.2.9  = {
	owner = PLC
	controller = PLC
	add_core = PLC
	remove_core = ZAZ
} # Truce of Andrusovo
1793.1.23 = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Second partition of Poland
1794.3.24  = { unrest = 5 } # Kosciuszko uprising, minimize the Russian influence
1794.11.16 = { unrest = 0 }
1795.1.1   = {
	remove_core = PLC
}
