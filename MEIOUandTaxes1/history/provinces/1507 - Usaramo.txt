#1507 - Usaramo

owner = KIT
controller = KIT
culture = makua
religion = animism
capital = "Rukwa"
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = livestock
hre = no
discovered_by = central_african
discovered_by = east_african
500.1.1 = {
	add_permanent_province_modifier = {
		name = "ivory_low"
		duration = -1
	}
}
1356.1.1 = {
	add_core = KIT
}
1520.1.1 = { base_tax = 10 }
