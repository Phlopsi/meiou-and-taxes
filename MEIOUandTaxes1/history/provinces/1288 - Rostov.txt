# 1288 - Rostofa (vieux norrois) rostov
# MEIOU-GG - Turko-Mongol mod

owner = ROS
controller = ROS
culture = russian
religion = orthodox
capital = "Rostov"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1133.1.1 = { marketplace = yes }
1356.1.1  = {
	add_core = YAR
	add_core = MOS
	add_core = ROS
}
1463.1.1  = {
	owner = MOS
	controller = MOS
}
1521.1.1 = { base_tax = 9 }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
