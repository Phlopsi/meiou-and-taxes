#58 - Anhalt

owner = ANH
controller = ANH  
culture = low_saxon
religion = catholic
capital = "Dessau"
trade_goods = copper
hre = yes
base_tax = 5
base_production = 1
base_manpower = 0
is_city = yes
add_core = ANH
discovered_by = eastern
discovered_by = western
discovered_by = muslim



1000.1.1 = {
	town_hall = yes
	marketplace = yes
	local_fortification_1 = yes
}

1500.1.1 = { road_network = yes }
1529.1.1 = { religion = protestant }
1530.1.4  = {
	bailiff = yes	
}
1547.1.1 = { unrest = 2 } # Wolfgang is placed under the ban by Charles V
1552.1.1 = { unrest = 0 } # Wolfgang bought his land back


1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
