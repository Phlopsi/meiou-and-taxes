# No previous file for Rai Bareli

owner = AHM
controller = AHM
culture = avadhi
religion = hinduism
capital = "Rai Bareli"
trade_goods = rice
hre = no
base_tax = 36
base_production = 7
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1120.1.1 = { 
	urban_infrastructure_2 = yes
	marketplace = yes
	corporation_guild = yes
}

1356.1.1 = {
	#add_core = DLH
	add_core = AHM
	unrest = 6
}
1396.1.1 = {
	owner = AHM
	controller = AHM
	unrest = 0
} #guessed date for independance from Delhi Sultanate
1444.1.1 = {
	add_core = DLH
}
1486.1.1  = {
	owner = DLH
	controller = DLH
	add_core = DLH
	remove_core = AHM
} # Bahlul Lodi places his eldest surviving son Barbak Shah Lodi on the throne of Jaunpur
1511.1.1 = {
	base_tax = 46
	base_production = 9
}
1526.4.21 = {
	controller = BNG
} # Contested area with lots of different factions fighting eachother after Panipat
1528.1.1 = { road_network = yes }
#1529.1.1  = {
#	owner = BNG
#	controller = REB
#	revolt = { type = pretender_rebels }
#} # Sur control
1530.1.1   = {
	owner = MUG
	controller = MUG 
	add_core = MUG
	#revolt = { type = nationalist_rebels }
} #Lodi Pretender
1530.1.2 = { add_core = TRT }
1538.1.1  = {
	controller = BNG
	revolt = { }
} # Gaur has fallen
1538.6.1  = {
	controller = MUG
} # Mughal Invasion
1539.1.1  = {
	owner = BNG
	controller = BNG
} # Surs again in control
1553.1.1  = {
	owner = AHM
	controller = AHM
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1558.11.1 = {	controller = MUG }	#Ibrahim defeated
1558.12.1 = {	owner = MUG }	#Jaunpur annexed
1565.1.1  = { controller = REB revolt = { type = noble_rebels } }	#Revolt of Uzbek commanders
1566.6.1  = { controller = MUG revolt = { } }
1618.12.1 = { add_core = MUG }
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1724.1.1  = {
	owner = ODH
	controller = ODH
	add_core = ODH
	remove_core = MUG
} # Foundation of the Oudh dynasty
