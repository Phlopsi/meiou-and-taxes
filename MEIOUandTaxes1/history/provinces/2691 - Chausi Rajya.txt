# 2691 - Chausi Rajya

owner = NPL
controller = NPL
culture = nepali
religion = hinduism
capital = "Ghurka"
trade_goods = lumber #naval_supplies
hre = no
base_tax = 27
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = NPL
	#fort_14th = yes
}
1511.1.1 = {
	base_tax = 31
}
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
