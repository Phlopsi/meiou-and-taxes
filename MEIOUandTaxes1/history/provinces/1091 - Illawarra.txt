# 1091 - Wollongong

culture = aboriginal
religion = polynesian_religion
hre = no
capital = "Wollongong"
trade_goods = unknown #grain
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 0.5
native_hostileness = 2

1770.7.1 = {
	discovered_by = GBR
} # Cook's 1st voyage
1788.1.1 = {
	owner = GBR
	controller = GBR
	culture = english
	religion = protestant #anglican
	citysize = 350
	trade_goods = wheat
}
