# No previous file for Klallam

culture = salish
religion = totemism
capital = "Klallam"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 40
native_ferocity = 3
native_hostileness = 4

1000.1.1   = {
	set_province_flag = has_small_natural_harbour
	set_province_flag = has_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "squamish_natural_harbour" 
		duration = -1 
		}
}