# 2694 - Koch Behar

owner = KMT
controller = KMT
culture = kochrajbongshi
religion = hinduism
capital = "Kamatapur"
trade_goods = cotton
hre = no
base_tax = 20
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = chinese
discovered_by = steppestech

1120.1.1 = { town_hall = yes set_province_flag = pocket_province }
1356.1.1 = { add_core = KMT }
1511.1.1 = {
	base_tax = 24
	base_production = 2
}
1587.1.1 = { capital = "Atharokotha" }
1627.1.1 = { discovered_by = POR }
1660.1.1 = { controller = MUG }
1661.1.1 = { owner = MUG }
1680.1.1 = { controller = KMT owner = KMT } #Independent but bhutanese puppet
1714.1.1 = { capital = "Cooch Behar" }
1772.3.1 = { controller = BHU } # 1772 Bhutan annexes Koch Bihar
1772.5.1 = { owner = BHU } # 1772 Bhutan annexes Koch Bihar
1774.4.25 = {
	owner = KMT
	controller = KMT
	remove_core = BHU
} #April 25, 1774 British Vassal
