# 671 - Guanxi Jiangzhou

owner = YUA
add_core = YUA
controller = YUA
culture = zhuang
religion = animism
capital = "Chongshan"
trade_goods = rice
hre = no
base_tax = 5
base_production = 2
base_manpower = 0
is_city = yes

discovered_by = chinese
discovered_by = steppestech


985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = {
	urban_infrastructure_1 = yes
}
1279.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1320.1.1 = { remove_core = SNG }
1325.1.1 = { capital = "Nanning" }
1356.1.1 = {
	
}
1382.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
    remove_core = LNG
	remove_core = YUA
}
1521.1.1 = {
	base_tax = 8
	base_production = 3
}
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty