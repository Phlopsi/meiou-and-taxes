# 1295 - Kama

owner = PRM
controller = PRM
culture = uralic
religion = tengri_pagan_reformed
capital = "Gayny"
trade_goods = lumber
hre = no
is_city = yes

base_tax = 3
base_production = 0
base_manpower = 0


discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_medium"
		duration = -1
	}
}
1356.1.1 = {
	add_core = PRM
}
1376.1.1 = {
	religion = orthodox
} # Stefan of Perm
1505.4.1 = {
	owner = MOS
	controller = MOS
	add_core = MOS
}
1521.1.1 = { base_tax = 4 }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1579.1.1 = {
	fort_14th = yes
}
1606.1.1 = {
	unrest = 3
} # Rebellions against Russian rule
1608.1.1 = {
	unrest = 5
}
1610.1.1 = {
	unrest = 2
}
1616.1.1 = {
	unrest = 6
}
1620.1.1 = {
	unrest = 0
}
