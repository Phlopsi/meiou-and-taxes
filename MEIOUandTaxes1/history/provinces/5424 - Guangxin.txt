# 5424 Guangxin 

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Shangrao"
trade_goods = rice
hre = no
base_tax = 53
base_production = 0
base_manpower = 2
is_city = yes




discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	bailiff = yes 
	road_network = yes
	temple = yes 
}
1276.1.1 = {
	owner = CNG
	controller = CNG
	add_core = CNG
}
1351.1.1 = {
	add_core = MNG
}
1369.3.17 = { 
	owner = MNG
	controller = MNG
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 84
	base_manpower = 3
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
