# 591 - Tenasserim

owner = AYU
controller = AYU
culture = monic
religion = buddhism
capital = "Tanonsri"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1356.1.1  = {
	add_core = AYU
}
1535.1.1  = { discovered_by = POR }
1767.4.8  = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
1793.1.1  = {
	owner = BRM
	controller = BRM
	add_core = BRM
	rename_capital = "Tenasserim" 
	change_province_name = "Ma"
}
1826.2.24 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
