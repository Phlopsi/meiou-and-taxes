# No previous file for Eastern Munster

owner = BTU
controller = BTU
culture = irish
religion = catholic
hre = no
base_tax = 4
base_production = 0
trade_goods = fish
base_manpower = 0
is_city = yes
capital = "Carraig na Si�ire" # Carrick-on-Suir
discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = { name = clan_land duration = -1 }
}
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = BTU
	local_fortification_1 = yes
}
1520.5.5  = {
	base_tax = 6
	base_production = 0
	base_manpower = 0
}
1529.1.1  = { #The Butlers were loyal to Henry VIII and from here on served England
	owner = ENG
	controller = ENG
	add_core = ENG
}
1530.1.4  = {
	bailiff = yes	
}
1642.1.1  = { controller = REB } # Estimated
1642.6.7  = { owner = IRE controller = IRE } # Confederation of Kilkenny
1650.4.10 = { controller = ENG } # Battle of Macroom
1652.4.1  = { owner = ENG } # End of the Irish Confederates
1655.1.1  = { fort_14th = yes }
 # Estimated
1689.3.12 = { controller = REB } # James II Lands in Ireland
1690.9.15 = { controller = ENG }
 # marketplace Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
