# 3450 - Goroka

culture = papuan
religion = polynesian_religion
capital = "Goroka"
trade_goods = unknown #fish
hre = no
base_tax = 9
base_production = 0
base_manpower = 0
native_size = 80
native_ferocity = 4
native_hostileness = 9
discovered_by = chinese

1000.1.1   = {
	set_province_flag = papuan_natives
}
1500.1.1 = {
	base_tax = 10
}
1545.1.1 = { discovered_by = SPA }
