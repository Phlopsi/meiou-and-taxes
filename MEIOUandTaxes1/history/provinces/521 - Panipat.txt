# 521 - Panipat
#MEIOU-FB India 1337+ mod Aug 08

owner = MUL
controller = MUL
culture = kanauji
religion = hinduism
capital = "Panipat"
trade_goods = wheat
hre = no
base_tax = 27
base_production = 1
base_manpower = 2
citysize = 13800
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech


#1120.1.1 = { farm_estate = yes }
1133.1.1 = { town_hall = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
	owner = DLH
	controller = DLH
	add_core = DLH
	add_core = MUL
	add_core = PTA
}
1398.8.1  = {
	revolt = {  }
	controller = TIM
}
1443.1.1 = {
	#revolt = { type = pretender_rebels size = 4 leader = "Bahlul Lodi" }
	controller = PTA
	add_core = PTA
	owner = PTA
	remove_core = MUL
} #First revolt of Bahlul Lodi
1445.1.1 = {
	controller = DLH
	revolt = {  }
} #Bahlul Lodi returns to the Punjab
1447.1.1 = {
	#revolt = { type = pretender_rebels size = 5 leader = "Bahlul Lodi" }
	controller = PTA
} #Second revolt of Bahlul Lodi
1451.4.20 = {
	controller = DLH
	owner = DLH
	#remove_core = PTA
	revolt = {  }
} #Final triumph of the Lodi
1511.1.1 = {
	base_tax = 34
}
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
} # Battle of Panipat
1530.1.1 = { add_core = TRT }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1540.1.1 = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1550.1.1 = { citysize = 24000 }
1553.1.1 = {
	owner = DLH
	controller = DLH
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23 = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1556.10.7 = { controller = DLH }	# Hemu
1556.11.5 = { controller = MUG }	#Second battle of Panipat
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1762.1.1  = {
	owner = PUN
	controller = PUN
	add_core = PUN
} # Sikhs
1784.1.1  = {
	owner = GWA
	controller = GWA
} # The Marathas
1803.1.1  = {
	owner = GBR
	controller = GBR
}
