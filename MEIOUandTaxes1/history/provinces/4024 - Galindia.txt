# 4024 - Galindia

owner = TEU
controller = TEU
capital  = "Ortelsburg"
culture = prussian
religion = catholic
trade_goods = lumber
hre = no
base_tax = 7
base_production = 0
base_manpower = 1
is_city = yes
fort_14th   = yes
add_core = TEU
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

1444.1.1   = {
	culture = prussian
}
1453.1.1   = { add_core = POL }
1454.3.6   = { controller = REB } # Beginning of the "thirteen years war"
1466.10.19 = {
	controller = TEU
} # "Peace of Torun", became a Polish fief
1515.1.1 = { training_fields = yes }
1520.5.5 = {
	base_tax = 11
}
1525.4.10  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = TEU
	religion = protestant
	remove_core = POL
	bailiff = yes
} # Albert of Prussia became a protestant
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1618.8.28  = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # Prussia in a personal union with Brandenburg
1667.1.1   = {  }
1701.1.18  = {
	owner = PRU
	controller = PRU
	remove_core = BRA
} # Kingdom of Prussia
