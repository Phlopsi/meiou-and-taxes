#Province: Lasbela
#file name: 576 - Lasbela
#MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG - Turko-Mongol mod

culture = baluchi
religion = sunni
capital = "Bela"
trade_goods = salt
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
native_size = 10
native_ferocity = 5
native_hostileness = 5

1498.1.1 = { discovered_by = POR }
1650.1.1 = {
	controller = PER
	owner = PER
}
1666.1.1 = {
	owner = BAL
	controller = BAL
	add_core = BAL
} #Kingdom of Kalat
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
1758.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
