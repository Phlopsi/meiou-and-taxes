# Province: Banjarmasin
# file name: 640 - Banjarmasin
# from wiki: In the fourteenth century, Banjarmasin was part of the hinduism kingdom of Negara, a vassel of Majapahit.
# Pangeran Samudera converted to become a Muslim in the fifteenth century, and Banjarese people became muslims ever since

owner = BKS
controller = BKS
culture = dayak
religion = vajrayana
capital = "Banjarmasin"
trade_goods = gems			#FB this area was/is famous for precious stones
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
add_core = BKS

discovered_by = MKS
discovered_by = MPH
discovered_by = MTR
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1133.1.1 = {
	harbour_infrastructure_1 = yes
	local_fortification_1 = yes
}
1501.1.1 = {
	base_tax = 2
}
1521.1.1 = { discovered_by = POR }
1550.1.1 = { religion = sunni }
1606.1.1 = { discovered_by = NED } #Dutch trading post
1860.1.1 = { owner = NED controller = NED }
