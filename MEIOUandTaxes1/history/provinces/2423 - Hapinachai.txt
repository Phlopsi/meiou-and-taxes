# 2322 - Hapinachai
# TM-Smiles indochina-mod for meiou

owner = LNA
controller = LNA
culture = lanna
religion = buddhism
capital = "Hapinachai" 
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1292.1.1 = {
	add_core = LNA
}
1356.1.1 = {
}
1501.1.1 = {
	base_tax = 2
}
1526.1.1  = { unrest = 4 } # Political instability after King Phraya's death
1558.1.1  = { add_core = TAU unrest = 0 citysize = 2000 } # Burmese vassal
1578.1.1  = { owner = TAU controller = TAU } # Direct Burmese rule
1599.1.1  = { controller = REB }	#Shan states revolt after burmese dinasty's crisis
1615.1.1  = { controller = TAU }
1662.1.1  = {
	owner = AYU
	controller = AYU 
	rename_capital = "Phayao" 
	change_province_name = "Phayao"
} # Occupied by the Siamese
1663.1.1  = { owner = LNA controller = LNA }  #??
1700.1.1  = { citysize = 10000 }
1727.1.1  = { unrest = 4 } # Rebellion
1728.1.1  = { unrest = 0 }
1752.2.28 = {
	add_core = BRM
	remove_core = TAU
} 
1775.2.14 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	remove_core = TAU
	unrest = 0
} # Conquered by the Siamese
1805.1.1  = { fort_14th = yes }
