# No previous file for Peewaareewa

culture = illini
religion = totemism
capital = "Peewaareewa"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 55
native_ferocity = 2
native_hostileness = 5

1541.1.1  = { discovered_by = SPA } # Hernando de Soto
1673.1.1  = { discovered_by = FRA } # Jacques Marquette & Louis Jolliet
1722.1.1  = {	owner = FRA
		controller = FRA
		citysize = 250
		religion = catholic
	    	culture = francien
	    } # French settlement
1747.1.1  = { add_core = FRA citysize = 650 trade_goods = cotton }
1762.1.1  = {	owner = SPA
		controller = SPA
		culture = castillian
		remove_core = FRA
	    } # Treaty of Fontainebleau, secretely ceded to Spain
1787.1.1  = { add_core = SPA }
1800.1.1  = { citysize = 1780 }
1800.10.1 = {	owner = FRA
		controller = FRA
		add_core = FRA
	    	remove_core = SPA
	    	culture = french_colonial
	    } # Treaty of San Ildefonso
1803.4.3  = {	owner = USA
		controller = USA
		add_core = USA
		remove_core = FRA
	    } # The Louisiana purchase
