# No previous file for T�r Eoghain

owner = TYR
controller = TYR
culture = irish
religion = catholic
hre = no
base_tax = 8
base_production = 0
trade_goods = livestock
is_city = yes
base_manpower = 0
capital = "D�n Geanainn" # Dungannon
discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = { name = clan_land duration = -1 }
}

1356.1.1   = {
	add_core = TYR
	local_fortification_1 = yes
}
1520.5.5  = {
	base_tax = 10
	base_production = 0
	base_manpower = 1
}
1607.9.4   = {
	owner = ENG
	controller = ENG
	add_core = ENG
	capital = Belfast
} # Flight of the Earls
1641.10.22 = {
	controller = REB
}
1642.5.1   = {
	controller = ENG
} # Estimated
1646.6.5   = {
	controller = IRE
}
1650.6.21  = {
	controller = ENG
} # Battle of Scarrifhollis
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1798.5.23  = {
	controller = REB
} # Irish rebellion
1798.7.14  = {
	controller = GBR
} # The rebels are defeated
