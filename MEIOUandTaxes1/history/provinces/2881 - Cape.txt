# 2881 - Cape

culture = khoisan
religion = animism
capital = "iKapa"
trade_goods = unknown
base_tax = 2
native_size = 5
native_ferocity = 0.5
native_hostileness = 0
hre = no

1000.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = "cape_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1488.1.1 = { discovered_by = POR } # Bartolomeu Dias
1616.1.1 = { discovered_by = NED } # Dirk Hartog
1652.4.6 = {
	owner = NED
	controller = NED
	religion = reformed
	culture = dutch	
	citysize = 240
	capital = "Cape Town"
	trade_goods = wine
	base_tax = 2
base_production = 2
	set_province_flag = trade_good_set
} # Dutch settlement
1677.4.6 = { add_core = NED }
1700.1.1 = { citysize = 500 } # The colony expanded
