# 1032 - Harima
# GG/LS - Japanese Civil War

owner = AKM
controller = AKM
culture = kansai
religion = mahayana #shinbutsu
capital = "Himeji"
trade_goods = tea
hre = no
is_city = yes # citysize = 1500
base_tax = 18
base_production = 1
base_manpower = 2
#fort_14th = yes # Miki

discovered_by = chinese

1111.1.1 = {
	road_network = yes
	town_hall = yes
	harbour_infrastructure_1 = yes
	local_fortification_1 = yes
}
1356.1.1 = {
	add_core = AKM
	add_core = UKI
}
1501.1.1 = {
	base_tax = 31
	base_production = 2
	base_manpower = 4
}
1542.1.1 = { discovered_by = POR }

1581.1.1 = { fort_14th = yes } # Himeji Castle
1586.1.1 = {
	owner = ODA
	controller = ODA
}
1600.1.1 = {
	
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
1745.1.1 = {  }
