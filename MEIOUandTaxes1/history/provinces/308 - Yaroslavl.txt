#308 - Yaroslavl

owner = YAR
controller = YAR
culture = russian
religion = orthodox
capital = "Yaroslavl"
base_tax = 8
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no


1250.1.1 = { temple = yes }
1300.1.1 = {
	add_permanent_province_modifier = {
		name = yaroslav_confluence
		duration = -1
	}
	road_network = yes
	marketplace = yes
	town_hall = yes
}
1356.1.1  = {
	add_core = YAR
	local_fortification_1 = yes
	set_province_flag = mined_goods
	set_province_flag = iron
}
1463.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
}
 # Spasskiy Monastery
1515.1.1 = {
	base_production = 2
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = { unrest = 5 } # "Time of troubles", peasantry brought into serfdom
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1667.1.1  = { controller = REB } # Peasant uprising, Stenka Razin
1670.1.1  = { controller = RUS } # Crushed by the Tsar's army
