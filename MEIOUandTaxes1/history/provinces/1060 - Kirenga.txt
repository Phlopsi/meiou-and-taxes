# 1062 - Kirenga

owner = YUA
controller = YUA
add_core = BRT
culture = buryat
religion = tengri_pagan_reformed
capital = "Kirenga"
trade_goods = fur
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 1
native_hostileness = 3
is_city = yes

discovered_by = YEN
#discovered_by = SAK
discovered_by = steppestech

1356.1.1 = {
	add_core = YUA
}
1392.1.1 = {
	owner = BRT
	controller = BRT
	remove_core = YUA
}
1632.1.1  = { discovered_by = RUS }
1632.9.25 = {
	owner = RUS
	controller = RUS
#  	religion = orthodox
#  	culture = russian
	trade_goods = fur
}
1657.1.1 = {
	add_core = RUS
}
