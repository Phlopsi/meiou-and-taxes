# 834 - Pipil
# GG - 22/07/2008

owner = PIP
controller = PIP
add_core = PIP
culture = xinca
religion = mesoamerican_religion
capital = "Cuzcatlan"
base_tax = 12
base_production = 0
base_manpower = 2
citysize = 5000
trade_goods = maize


discovered_by = mesoamerican

hre = no

1522.1.1   = {
	discovered_by = SPA
}
1525.1.1   = {
	owner = SPA
	controller = SPA
	capital = "San Salvador"
	#citysize = 200
	culture = castillian
	religion = catholic
	base_tax = 8
	base_production = 0
	base_manpower = 1
	trade_goods = coffee
	is_city = yes
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
	}
1550.1.1   = {
	add_core = SPA
	citysize = 500
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1823.7.10  = {
	owner = CAM
	controller = CAM
	remove_core = MEX
}
