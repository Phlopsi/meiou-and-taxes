# No previous file for Bender Kassim

owner = WAR
controller = WAR
add_core = WAR
culture = somali
religion = sunni
capital = "Bosaso"
trade_goods = incense
citysize = 7000
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african

1499.1.1 = { discovered_by = POR } 
1515.2.1 = { training_fields = yes }
1526.1.1 = { owner = MJE controller = MJE add_core = MJE remove_core = WAR } #Ahmad Gran secures control over Marehan
1550.1.1 = { discovered_by = TUR }
1555.1.1 = { owner = WAR controller = WAR } #Northern part of province no longer conrolled by ADA
1730.1.1 = { owner = MJE controller = MJE }
