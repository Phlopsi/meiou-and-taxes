# No previous file for Havelland

owner = BRA
controller = BRA
add_core = BRA
culture = low_saxon
religion = catholic
hre = yes
base_tax = 12
base_production = 1
trade_goods = livestock
base_manpower = 1
is_city = yes
capital = "Brandenburg an der Havel"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1100.1.1 = {
	town_hall = yes
}

1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1520.5.5 = {
	base_tax = 15
	base_production = 0
	base_manpower = 1
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1539.1.1   = { religion = protestant }
1650.1.1   = { culture = prussian }
1701.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1806.10.27 = { controller = FRA }
1807.7.9   = { controller = PRU } # The Second treaty of Tilsit
