# 832 - Quimbaya

is_city = yes
culture = muisca
religion = pantheism
capital = "Quimbaya"
trade_goods = unknown
hre = no
base_tax = 30
base_production = 0
base_manpower = 2
native_size = 55
native_ferocity = 1
native_hostileness = 2
discovered_by = south_american

1520.1.1 = {
	base_tax = 10
	base_production = 2
}
1530.1.1   = {
	discovered_by = SPA
}
1539.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 1000
	change_province_name = "Antioqu�a"
	rename_capital = "Cartago"
	culture = castillian
	religion = catholic
	trade_goods = coffee
	set_province_flag = trade_good_set
}
1616.1.1   = {
	rename_capital = "Medellin"
	is_city = yes
}
1700.1.1   = {
	}
1750.1.1   = {
	add_core = COL
	culture = colombian
}
1800.1.1   = {
	}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
