# 782 - Manekenk

culture = chon
religion = pantheism
capital = "Tierra del Fuego"
trade_goods = unknown #fish
hre = no
base_tax = 0 # 2
base_production = 0
base_manpower = 0
native_size = 15
native_ferocity = 2
native_hostileness = 9


1356.1.1 = {
	add_permanent_province_modifier = {
		name = uncolonisable_rural_pop_02
		duration = -1
	}
}
1500.1.1 = {
	remove_province_modifier = uncolonisable_rural_pop_02
	add_permanent_province_modifier = {
		name = uncolonisable_rural_pop_01
		duration = -1
	}
#	base_tax = 1
	native_size = 1
}
1520.1.1   = {
	discovered_by = SPA
	add_core = SPA
} # Discovered by Ferdinand Magellan
1750.1.1  = {
	add_core = CHL
	add_core = LAP
	base_tax = 1
}
1818.2.12  = {
	remove_core = SPA
}
1885.1.1   = {
	owner = LAP
	controller = LAP
	citysize = 5000
	culture = platean
	religion = catholic
	trade_goods = fish
	set_province_flag = trade_good_set
}
