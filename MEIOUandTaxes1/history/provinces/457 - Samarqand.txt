# 457 - Samarqand

owner = CHG
controller = CHG
culture = chaghatai
religion = sunni
capital = "Samarqand"
trade_goods = cotton
base_tax = 8
base_production = 6
base_manpower = 1
is_city = yes

discovered_by = KSH
discovered_by = turkishtech
discovered_by = muslim
discovered_by = steppestech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_glassware
		duration = -1
	}
	local_fortification_1 = yes
	set_province_flag = silk_road_town
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "samarkand_silk_road" 
		duration = -1 
		}
}

1150.1.1 = {
	urban_infrastructure_1 = yes
	small_university = yes 
	marketplace = yes
	workshop = yes
	constable = yes
}
1201.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = CHG
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
	art_corporation = yes
}
1404.1.1 = { temple = yes }
1500.6.1    = {
	owner = SHY
	controller = SHY
	culture=uzbehk
	add_core = SHY
	remove_core = TIM
} # Shaybanids break free from the Timurids
 # The Uzbek Shaybanids brings an end to the Timurid dynasty
1501.1.1 = {
	base_tax = 11
	base_production = 5
	base_manpower = 1
}
1515.1.1 = { training_fields = yes }
1520.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
1540.1.1 = { fort_14th = yes }

1785.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
