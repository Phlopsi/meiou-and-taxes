# 6 - Skane
# MEIOU - Gigau

owner = DEN
controller = DEN
culture = danish
religion = catholic
hre = no
base_tax = 6
base_production = 1
base_manpower = 1
trade_goods = fish
is_city = yes
estate = estate_nobles
capital = "Lund"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "herring_province_big"
		duration = -1
	}
}
1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "skane_natural_harbour" 
		duration = -1 
		}
	add_permanent_province_modifier = {
		name = "herring_province_big"
		duration = -1
	}
}

1088.1.1 = {
	harbour_infrastructure_2 = yes
}
1250.1.1 = {
	temple = yes
	marketplace = yes
}

1356.1.1   = {
	owner = RSW
	controller = RSW
	add_core = RSW
	add_core = DEN
	local_fortification_1 = yes
}
1360.1.1   = {
	owner = DEN
	controller = DEN
	remove_core = RSW
}
1434.1.1   = { fort_14th = yes }

1500.1.1 = { road_network = yes }
1500.3.3 = {
	base_tax = 8
	base_production = 1
	base_manpower = 1
}
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1522.2.15 = {  shipyard = yes }
1522.3.20 = {  naval_arsenal = yes }
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1525.1.1   = {
	revolt = {
		type = nationalist_rebels
		size = 2
	}
	controller = REB
} #Prelude to 'Grevefejden'(S�ren Norrby)
1525.5.10  = {
	revolt = {  }
	controller = DAN
} #S�ren Norrby is defeated
1526.1.1 = { religion = protestant } #preaching of Hans Tausen
1529.12.17 = { merchant_guild = yes }
1534.8.15  = {
	revolt = {
		type = nationalist_rebels
		size = 2
	}
	controller = REB
} #'Grevefejden'(Christofer of Oldenburg)
1536.3.15  = {
	revolt = {  }
	controller = DAN
} #Liberated by Sweden
1536.3.15  = { religion = protestant  } #Unknown date
1556.1.1   = {
	trade_goods = wheat
}
1590.1.1   = {  }
1658.2.26  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = DAN
} #The Peace of Roskilde
1658.2.26  = {
	fort_14th = no fort_15th = yes
} #Norra och S�dra Sk�nska kavalleriregementena
1714.1.1   = { fort_15th = no fort_16th = yes }
1722.1.1  = { culture = swedish } #linguicide mostly accomplished 
