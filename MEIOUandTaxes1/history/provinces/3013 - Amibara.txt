# No previous file for Amibara

owner = ETH
controller = ETH
add_core = ETH
culture = amhara
religion = coptic
capital = "Bati"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wool

discovered_by = ALW
discovered_by = ADA
discovered_by = MKU
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
discovered_by = east_african

1486.1.1 = { unrest = 5 add_core = ADA } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1514.1.1 = {
	owner = ADA
	controller = ADA
	unrest = 6
} #Mahfuz drives Ethiopians out of Ifat region
1515.2.1 = { training_fields = yes }
1517.7.1 = {
	owner = ETH
	controller = ETH
} #Lebna Dengel occupies region during campaign that defeats Mahfuz
1527.1.1 = { unrest = 5 } #Raids by Ahmad Gran
1529.1.1 = { unrest = 7 } #Raids by Ahmad Gran
1530.1.1 = { religion = sunni controller = ADA } #Spread of Islam leads to shift in religion affiliation of region
1531.1.1 = { unrest = 7 } #Raids by Ahmad Gran
1532.1.1 = {
	owner = ADA
	controller = ADA
	unrest = 6
} #Lebna Dengel defeated by Ahmad Gran at Ayfars
1545.1.1 = {
	owner = ETH
	controller = ETH
	remove_core = ADA
} #Adal collapse in highlands in wake of Ahmad Gran's death
1549.1.1 = { unrest = 5 } #Invasion by Oromo
1550.1.1 = { discovered_by = TUR }
1553.1.1 = { unrest = 5 } #Invasion by Adal 
1558.1.1 = { unrest = 5 } #Invasion by Adal
1573.1.1 = { unrest = 5 } #Invasion by Oromo
1578.1.1 = { unrest = 5 } #Invasion by Oromo
1588.1.1 = { unrest = 5 } #Invasion by Oromo
1590.1.1 = { unrest = 0 }
