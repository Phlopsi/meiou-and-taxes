#377 - Aleppo

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "Halab"
trade_goods = wheat #linen
hre = no
base_tax = 21
base_production = 7
base_manpower = 2
is_city = yes
fort_14th = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = western
discovered_by = eastern
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}

1200.1.1 = {
	road_network = yes
	urban_infrastructure_2 = yes 
	workshop = yes
	merchant_guild = yes
}
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1500.3.3   = {
	base_tax = 25
	base_production = 6
}
1516.1.1   = { add_core = TUR }
1516.8.28  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops

1519.1.1 = { bailiff = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.5 = {
	owner = SYR
	controller = SYR
	remove_core = TUR
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1606.1.1 = { unrest = 5 } # Rebellion against the Ottomans
1607.1.1 = { unrest = 0 }
1658.1.1 = { unrest = 4 } # Revolt, Abaza Hasan Pasha
1659.1.1 = { unrest = 0 }
1775.1.1 = { unrest = 4 } # Janissary revolt
1776.1.1 = { unrest = 0 }
1780.1.1 = {  }
1784.1.1 = { unrest = 4 } # Janissary revolt
1785.1.1 = { unrest = 2 }
1787.1.1 = { unrest = 0 }
1791.1.1 = { unrest = 5 }
1792.1.1 = { unrest = 0 }
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
