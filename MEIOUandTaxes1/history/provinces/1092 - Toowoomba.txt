# 1092 - Toowoomba

culture = aboriginal
religion = polynesian_religion
capital = "Toowoomba"
trade_goods = unknown #naval_supplies
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 15
native_ferocity = 0.5
native_hostileness = 2

1000.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "bundjalung_natural_harbour" 
		duration = -1 
		}
}
1770.7.1 = {
	discovered_by = GBR
} # Cook's 1st voyage
