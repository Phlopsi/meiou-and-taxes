# 548 - Sambalpur

owner = GRJ
controller = GRJ
culture = sambalpuri
religion = adi_dharam
capital = "Sambalpur"
#trade_goods = naval_supplies
trade_goods = copper
hre = no
base_tax = 24
base_production = 0
base_manpower = 2
citysize = 7000

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = bonai_state
		duration = -1
	}
}
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = GRJ
	#fort_14th = yes
}
1450.1.1  = { citysize = 12000 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 15000 }
1511.1.1 = {
	base_tax = 30
	base_production = 1
}
1550.1.1  = { citysize = 18000 }
1600.1.1  = { citysize = 21000 }
1650.1.1  = { citysize = 25000  }
1700.1.1  = { citysize = 27000 }
1750.1.1  = { citysize = 19000 }
1752.1.1   = {
	owner = BHO
	controller = BHO
	add_core = BHO
	fort_14th = no
	fort_17th = yes
} # Marathas
1800.1.1  = { citysize = 24000 }
1803.1.1  = {
	owner = GBR
	controller = GBR
}
1806.1.1  = {
	owner = BHO
	controller = BHO
} # Restored to Nagpur
1816.1.1  = {
	owner = GBR
	controller = GBR
}
