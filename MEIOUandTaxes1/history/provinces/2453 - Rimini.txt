# 2453 - Rimini

owner = RIM
controller = RIM
culture = romagnol
religion = catholic 
hre = no 
base_tax = 1
base_production = 2        
trade_goods = fish 
base_manpower = 0
is_city = yes

capital = "R�min"

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = {
	road_network = yes
	harbour_infrastructure_1 = yes
	urban_infrastructure_1 = yes
	local_fortification_1 = yes
}
1356.1.1   = {
	add_core = RIM
}
1463.1.1   = {
	controller = PAP
	owner = PAP
	add_core = PAP
}
1503.8.28  = {
	owner = VEN
	controller = VEN
}
1509.1.1   = {
	controller = PAP
	owner = PAP
	road_network = no paved_road_network = yes 
	bailiff = yes
	fort_14th = yes 
} # Annexed to the Holy See
1805.3.17 = {
	owner = ITE
	controller = ITE
	add_core = ITE
} # Treaty of Pressburg
1814.4.11   = {
	owner = PAP
	controller = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
