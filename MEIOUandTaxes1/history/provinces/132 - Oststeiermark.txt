# 132 - Steiermark (styrie) 
# graz

owner = HAB
controller = HAB
culture = austrian
religion = catholic
capital = "Graz" 
base_tax = 9
base_production = 1
base_manpower = 1
is_city = yes
trade_goods = iron
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
}
1200.1.1 = {
	local_fortification_1 = yes
}
1250.1.1 = {
	workshop = yes
	town_hall = yes
}
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = HAB
}
1379.1.1   = { controller = STY owner = STY add_core = STY }
1462.1.1 = { temple = yes }
1490.1.1   = { controller = HAB owner = HAB remove_core = STY }
#1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1515.4.1  = { unrest = 4 } # Bund - Farmer insurrections
1515.10.1 = { unrest = 0 }
1520.5.5 = {
	base_tax = 23
	base_production = 1
	base_manpower = 1
}
1525.5.1  = { unrest = 8 } # serious Farmer insurrections 
1526.1.1  = { unrest = 0 }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1578.1.1  = { fort_14th = yes }

1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
