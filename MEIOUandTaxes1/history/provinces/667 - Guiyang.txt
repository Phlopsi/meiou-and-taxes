# 667 - guizhou_area Qixingguan

owner = YUA
controller = YUA
culture = miao
religion = animism
capital = "Guiding"
trade_goods = opium
hre = no
base_tax = 33
base_production = 1
base_manpower = 2
#citysize = 10000
fort_14th = yes

discovered_by = chinese
discovered_by = steppestech


0967.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1111.1.1 = {
	town_hall = yes
	marketplace = yes
	set_province_flag = pocket_province
}
1200.1.1 = { paved_road_network = yes }
1250.1.1 = { temple = yes }
1265.1.1 = {
	owner = YUA
	controller = YUA
	add_core = LNG
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	owner = ZHN
	controller = ZHN
	add_core = ZHN
}
1375.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = LNG
	remove_core = YUA
}
1500.1.1 = { citysize = 18000 }
1521.1.1 = {
	base_tax = 53
	base_production = 2
	base_manpower = 3
}
1550.1.1 = { citysize = 20000 }
1600.1.1 = { citysize = 24000 }
1650.1.1 = { citysize = 25000 }
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
1700.1.1 = { citysize = 26000 }
1750.1.1 = { citysize = 27000 }
1800.1.1 = { citysize = 46700 }
