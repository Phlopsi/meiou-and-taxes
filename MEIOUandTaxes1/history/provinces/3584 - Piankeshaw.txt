# No previous file for Piankeshaw

culture = miami
religion = totemism
capital = "Piankeshaw"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1659.1.1  = { discovered_by = FRA } # M�dard Chouart Des Groseilliers
1671.1.1  = { discovered_by = ENG } # Abraham Wood
1680.1.1  = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1701.8.14 = { culture = shawnee } #Vast areas left deserted after the Beaver Wars and the Iroquois withdrawal
1707.5.12 = { discovered_by = GBR }
1805.1.1  = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
	is_city = yes
} # Treaty of Grouseland
