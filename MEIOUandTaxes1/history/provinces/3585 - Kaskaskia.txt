# No previous file for Kaskaskia

culture = illini
religion = totemism
capital = "Kaskaskia"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 5
1670.1.1 = { discovered_by = FRA } # Robert Cavelier de La Salle
1679.1.1  = { discovered_by = ENG }
1680.1.1  = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1701.8.14 = { culture = illini } #Vast areas left deserted after the Beaver Wars and the Iroquois withdrawal
1703.1.1 = {
	owner = FRA
	controller = FRA
	citysize = 500
	culture = francien
	religion = catholic
	trade_goods = fur
} #Kaskaskia
1707.5.12 = { discovered_by = GBR }
1719.1.1  = { fort_14th = yes } # Fort de Chartres
1724.1.1 = { add_core = FRA }
1750.1.1 = { citysize = 2105 }
1763.2.10 = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	remove_core = FRA
} # Treaty of Paris, France gave up its claim to New France
1763.3.1 = { unrest = 6 } # Native discontent with the British rule
#1763.10.9 Royal proclamation, but Illinois country colonies in a gray zone
1763.5.1 = { unrest = 0 revolt = { type = nationalist_rebels size = 0 } controller = REB } # Pontiac's war
1764.6.1 = { revolt = {} controller = GBR } # Peace negotiations
1778.7.4  = {	owner = USA
		controller = USA
		culture = american
	    } # Americans capture Kaskaskia; Illinois country french join the revolt.
1800.1.1 = { citysize = 3670 }
1803.7.4 = { add_core = USA } #The death of Tecumseh mark the end of organized native resistance 
