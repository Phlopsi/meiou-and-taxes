# No previous file for Baydhabo

owner = AJU
controller = AJU
add_core = AJU
culture = somali
religion = sunni
capital = "Luuq"
trade_goods = millet
citysize = 1500
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african

1550.1.1 = { discovered_by = TUR }
1730.1.1 = { owner = GLE controller = GLE }
