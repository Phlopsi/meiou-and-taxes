# 1020 - Bungo
# GG/LS - Japanese Civil War

owner = OTM
controller = OTM
culture = kyushu
religion = mahayana #shinbutsu
capital = "Funai"
trade_goods = rice
hre = no
base_tax = 17
base_production = 0
base_manpower = 2
is_city = yes
 #Usa Jingu
discovered_by = chinese

1356.1.1 = {
	add_core = OTM
	local_fortification_1 = yes
}
1501.1.1 = {
	base_tax = 30
	base_production = 1
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR 
}
1578.1.1 = {
	owner = SMZ
	controller = SMZ
}
1585.1.1   = {
	religion = catholic
}
1600.1.1 = {
	
}
1615.1.1   = {
	religion = mahayana #shinbutsu
}
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
1760.1.1 = {
	
}
