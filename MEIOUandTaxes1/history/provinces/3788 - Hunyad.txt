# No previous file for Hunyad

owner = HUN
controller = HUN  
culture = transylvanian
religion = catholic
capital = "Hateg"
trade_goods = wheat
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech


1356.1.1   = {
	add_core = HUN
	add_core = TRA
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
 # Cathedral of Saint Michael
1506.1.1   = { controller = REB } # Szekely rebellion
1507.1.1   = { controller = HUN } # Estimated
1514.4.1   = { controller = REB } # Peasant rebellion against Hungary's magnates
1514.10.1  = { controller = HUN } # Estimated, the rebellions is put down by Szapolyiai

1515.2.1 = { training_fields = yes }
1520.1.1   = { fort_14th = yes } # Rural fortress Biertan (15-16th century)
1520.5.5 = {
	base_tax = 8
	base_production = 0
	base_manpower = 0
}

1526.8.30  = {
	owner = TRA
	controller = TRA
	add_permanent_claim = HAB
}
#1528.1.1   = { add_core = TUR }
1530.1.4  = {
	bailiff = yes	
}

1540.1.1   = { religion = reformed }

1599.1.1   = {
	owner = WAL
	controller = WAL
	add_core = WAL
} # Battle of Selimbar, Michael the brave gained control

1601.1.1   = {
	owner = TRA
	controller = TRA
	remove_core = WAL
} # Michael is assasinated
1604.1.1   = { owner = HAB controller = HAB }
1604.10.1  = { controller = REB } # Estimated, rebellion (Stephen Bocskai) against Austrian rule
1606.1.1   = { owner = TRA controller = TRA } # Stephen becomes prince of Transylvania
 # Reached a high point culturaly
1621.1.1   = {  } # Miko Fortress
1695.1.1 = {
	remove_core = TUR
}
1711.5.1   = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
1744.1.1   = { controller = REB } # Visarion Sarai stir up an orthodox rebellion against the Uniate church
1745.1.1   = { controller = HAB } # Estimated, Visarion Sarai is arrested and executed
1750.1.1   = {  }
1784.1.1   = { controller = REB } # Peasant rebellion against the nobles
1785.1.1   = { controller = HAB } # The leaders are arrested and executed
