# 1302 - Circassia

owner = CIR
controller = CIR
culture = circassian
religion = sunni
capital = "Maghas"
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = eastern
discovered_by = muslim
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	add_core = CIR
}
#1505.1.1 = {
#	add_core = PER
#}
1520.1.1  = {
#	owner = TUR
#	controller = TUR
#	add_core = TUR
}
1829.7.21 = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Treaty of Adrianople
