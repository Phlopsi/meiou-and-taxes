# 551 - Sistan

owner = MIH
controller = MIH
culture = baluchi
religion = sunni
capital = "Zahedan"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech


1356.1.1  = {
	add_core = MIH
	add_core = TIM
}
1384.1.1   = {
	owner = TIM
	controller = TIM
}
1444.1.1  = {
	owner = MIH
	controller = MIH
	remove_core = TIM
} # Shaybanids break free from the Timurids
1458.1.1  = {
	controller = QAR
	revolt = { }
} #Conquered by Black Sheep
1459.1.1  = {
	controller = KTD
} #Civil War
1507.1.1  = {
	controller = SHY
}
1507.7.1  = {
	owner = SHY
}
1508.1.1   = {
	owner = SAM
	controller = SAM
} # Safawid Expansion
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	#religion = shiite
	remove_core = SAM
	remove_core = QAR
	remove_core = AKK
	training_fields = yes
	bailiff = yes
	courthouse = yes
} # Safawids "form persia"
