# No previous file for Erode

owner = MAD
controller = MAD
culture = tamil
religion = hinduism
capital = "Erode"
trade_goods = tea
hre = no
base_tax = 18
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
add_local_autonomy = 25


1356.1.1  = { add_core = MAD }
1378.1.1 = { owner = VIJ controller = VIJ }
1428.1.1 = { add_core = VIJ }
1511.1.1 = {
	base_tax = 22
}
1530.1.1 = {
	#owner = MAD
	#controller = MAD
	add_core = MAD
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1565.7.1 = {
	owner = MAD
	controller = MAD
	remove_core = VIJ
} # The Vijayanagar empire collapses
1650.1.1 = { discovered_by = turkishtech }
1752.1.1 = {
	owner = MYS
	controller = MYS
	add_core = MYS
}
1799.1.1 = {
	owner = GBR
	controller = GBR
} # Fourth Anglo-Mysore War
1824.1.1 = { add_core = GBR }
