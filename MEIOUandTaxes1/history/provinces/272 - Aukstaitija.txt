# 272 - Aukstaitija

owner = LIT
controller = LIT
capital = "Vilna"
culture = lithuanian
religion = baltic_pagan_reformed
trade_goods = wheat
hre = no
base_tax = 13
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

1000.1.1 = {
	
}

1100.1.1 = {
	marketplace = yes
	town_hall = yes
	workshop = yes
}
1356.1.1   = {
	add_core = LIT
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}
1386.1.1   = {
	religion = catholic
}

1450.1.1 = { royal_palace = yes }
1508.1.1   = { fort_14th = yes }
1520.5.5 = {
	base_tax = 16
	base_production = 3
	base_manpower = 2
}
1522.1.1 = { temple = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1543.1.1   = { unrest = 3 } # Counter reformation

1569.7.1   = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1579.1.1   = { unrest = 0 } # Vilnius university

1791.6.1   = { unrest = 3} # 3rd May constitution raised opposition
1794.3.24  = { unrest = 6 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 }
1795.10.24  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	add_core = LIT
	add_core = PLC
} # Most of Lithuania became part of the Russian empire
1812.6.28  = { controller = FRA } # Occupied by French troops
1812.12.10 = { controller = RUS }
