# 3989 - Mologa

owner = YAR
controller = YAR
culture = russian
religion = orthodox
capital = "Mologa"
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = YAR
}
1463.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	fort_14th = yes
}
1521.1.1 = { base_tax = 6 }
 # Spasskiy Monastery
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = { unrest = 5 } # "Time of troubles", peasantry brought into serfdom
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1667.1.1  = { controller = REB } # Peasant uprising, Stenka Razin
1670.1.1  = { controller = RUS } # Crushed by the Tsar's army
