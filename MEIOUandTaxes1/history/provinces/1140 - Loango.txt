#1140 - Loango

owner = LOA
controller = LOA
add_core = LOA
culture = bakongo
religion = animism
capital = "Mbanza Loango"
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish

discovered_by = central_african

500.1.1 = {
	add_permanent_province_modifier = {
		name = "ivory_medium"
		duration = -1
	}
}
1200.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1472.1.1 = {
	discovered_by = POR
} # Fren�o do P�, add trade post
