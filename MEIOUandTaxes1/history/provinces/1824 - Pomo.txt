# No previous file for Pomo

culture = wintuan
religion = totemism
capital = "Pomo"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 20
native_ferocity = 3
native_hostileness = 6

1542.1.1   = { discovered_by = SPA } # Juan Rodríguez Cabrillo
1579.6.16  = { discovered_by = ENG } # Sir Francis Drake
1707.5.12  = { discovered_by = GBR }
1786.1.1   = { discovered_by = FRA } # Jean Francois la Perouse
1812.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
		citysize = 200
		trade_goods = wool
	     	culture = russian
	     	religion = orthodox
	     } # Fort Ross
