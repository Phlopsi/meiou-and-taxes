# No previous file for Uttaradit

owner = SUK
controller = SUK
add_core = SUK
culture = lanna
religion = buddhism
capital = "Uttaradit"
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
hre = no

# an indipendent kingdom
1356.1.1 = {
}
1443.1.1 = {				
	owner = LNA
	controller = LNA
    	add_core = LNA
	remove_core = SUK
} # Vassal state
1501.1.1 = {
	base_tax = 3
}
1558.1.1 = { add_core = TAU } # Burmese vassal
1578.1.1 = { owner = TAU controller = TAU } # Direct Burmese rule
1599.1.1 = { controller = REB }	#Shan states revolt after burmese dinasty's crisis
1615.1.1 = { controller = TAU }
1663.1.1  = { owner = LNA controller = LNA }
1752.2.28 = {
	add_core = BRM
	remove_core = TAU
} 
1775.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
} # Vassal state
1788.1.1 = {
	owner = SIA
	controller = SIA
} # Annexed by Siam
