# No previous file for Yamasee

culture = creek
religion = totemism
capital = "Yamasee"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 2
native_hostileness = 5

1524.1.1   = { discovered_by = POR } # Diego Gomez
1526.1.1   = { discovered_by = FRA discovered_by = SPA } # Lucas V�squez de Ayll�n
1602.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 150
	culture = castillian
	religion = catholic
} #Santa Catalina de Guale mission
1627.1.1   = {	add_core = SPA citysize = 1000 } 
1702.1.1   = {
	owner = XXX
	controller = XXX
	remove_core = SPA
	culture = creek
	religion = totemism
	citysize = 0
} #Last Georgia missions abandoned. 
1733.2.12  = {
	owner = GBR
	controller = GBR
	culture = english
	religion = protestant
	capital = "Savannah"
	is_city = yes
} # Founding of Savannah, James Oglethorpe
1758.2.12  = { add_core = GBR }
1760.1.19  = { unrest = 5 } # Cherokee war
1761.1.1   = { unrest = 0 } # Peace attempt
1764.7.1   = {
	culture = american
	unrest = 6
} # Growing unrest
1776.7.4   = {
	owner = USA
	controller = USA
	add_core = USA
} # Independence day
1778.12.29 = { controller = GBR } # Savannah under British control
1782.7.1   = { controller = USA } # The British retreat
1782.11.1  = {
	remove_core = GBR 
	unrest = 0
} # Preliminary articles of peace, the British recognized Amercian independence
1813.12.11 = { revolt = { type = nationalist_rebels size = 1 } controller = REB } # The Creek War
1814.8.9   = { revolt = {} controller = USA } # The Treaty of Fort Jackson
