# 2068 - Cuzco

owner = CZC
controller = CZC 
add_core = CZC
culture = quechuan
religion = inti
capital = "Cuzco"
trade_goods = cacao
hre = no
base_tax = 23
base_production = 2
base_manpower = 3
is_city = yes

discovered_by = south_american

1356.1.1 = {
	temple = yes
	road_network = yes
	workshop = yes
	merchant_guild = yes
	urban_infrastructure_1 =  yes
	warehouse = yes
	set_province_flag = mined_goods
	set_province_flag = salt
	set_province_flag = plantation_starting_2
}
1480.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	remove_core = CZC
	paved_road_network = yes
	bailiff = yes
	constable = yes
	marketplace = yes
}
1501.1.1 = {
	base_tax = 5
	base_production = 5
}
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
	royal_palace = yes
	training_fields = yes
}
1533.8.29   = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	citysize = 2000
	culture = castillian
	religion = catholic
} # Death of Atahualpa
1536.1.1   = {
	owner = INC
	controller = INC
} # Captured by Manco Inca
1537.1.1   = {
	owner = SPA
	controller = SPA
	unrest = 8
} # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { unrest = 5 } # Almagro is defeated & executed
1541.1.1  = { unrest = 6 } # Pizzaro is assassinated by supporters of Almagro, his brother assumed control
1548.1.1  = { unrest = 0 } # Gonzalo Pizzaro is also executed & Spain succeeds in reasserting its authority
1650.1.1   = {
	citysize = 3500
}
1700.1.1   = {
	citysize = 5000
}
1750.1.1  = {
	citysize = 7500
	add_core = PEU
	culture = peruvian
}
1800.1.1   = {
	citysize = 10000
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
