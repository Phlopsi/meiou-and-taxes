# No previous file for Pelym

#owner = PLM
#controller = PLM
culture = mansi
religion = tengri_pagan_reformed
capital = "Pelym"
trade_goods = fur
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = steppestech

#1356.1.1 = {
#	add_core = PLM
#}
1530.1.1 = {
	owner = SIB
	controller = SIB
	add_core = SIB
}
1581.1.1 = {
	discovered_by = RUS 
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
} # Yermak Timofeevic
1606.1.1 = { add_core = RUS unrest = 3 } # Rebellions against Russian rule
1608.1.1 = { unrest = 5 }
1610.1.1 = { unrest = 2 }
1616.1.1 = { unrest = 6 }
1620.1.1 = { unrest = 0 }
