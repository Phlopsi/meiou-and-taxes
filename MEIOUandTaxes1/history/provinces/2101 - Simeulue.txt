# 2101 - Sinabang

culture = batak
religion = hinduism		#later: sunni
capital = "Nias"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 10
native_ferocity = 2
native_hostileness = 5
#citysize = 2500
#add_core = SGS

discovered_by = austranesian

1624.1.1 = {
	owner = ATJ
	controller = ATJ
	add_core = ATJ
}
