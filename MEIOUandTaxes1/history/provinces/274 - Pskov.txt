# 274 - Pskov

owner = PSK
controller = PSK
culture = novgorodian
religion = orthodox
hre = no
base_tax = 4
base_production = 3
trade_goods = livestock
base_manpower = 1
is_city = yes
capital = "Pskov"
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = steppestech

1100.1.1 = {
	marketplace = yes
	urban_infrastructure_1 = yes
	corporation_guild = yes
	add_permanent_province_modifier = {
		name = pskov_confluence
		duration = -1
	}
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
}


1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1  = { add_core = PSK }
1470.1.1  = { unrest = 5 } # Struggles between the boyars and smerds
1490.1.1  = { unrest = 0 }
1510.1.24 = {
	owner = MOS
	controller = MOS
	add_core = MOS
   	remove_core = PSK
	culture = russian
} # Vasili III put an end to the Pskov republic, still remained a strategic fortress

1515.1.1 = {
	base_production = 2
}
1521.1.1 = { base_tax = 7 }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
 
1598.1.1  = { unrest = 8 } # "Time of troubles"
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1773.1.1  = { unrest = 5 } # Peasant uprising, Pugachev
1774.1.1  = { unrest = 0 }
