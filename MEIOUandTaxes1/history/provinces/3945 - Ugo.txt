# 3945 - Dewa-Ugo

owner = ANO
controller = ANO
culture = tohoku
religion = mahayana
capital = "Kubota"
trade_goods = rice
hre = no
base_tax = 20
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = chinese
1000.1.1 = {
	harbour_infrastructure_1 = yes
}
1356.1.1 = {
	add_core = ANO
}
1367.1.1 = {
	remove_core = DTE
}
1501.1.1 = {
	base_tax = 33
	base_production = 1
	base_manpower = 3
}
1542.1.1   = { discovered_by = POR }
1630.1.1   = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
