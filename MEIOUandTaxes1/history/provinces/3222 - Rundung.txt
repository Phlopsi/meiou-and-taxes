# No previous file for Rundung

owner = NST
controller = NST
culture = malayan
religion = buddhism
capital = "Rundung"
trade_goods = fish
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = austranesian

1356.1.1 = {
	add_core = AYU
	add_core = NST
}
1410.1.1 = {
	culture = dambru
}
1467.4.8 = {
	owner = AYU
	controller = AYU
    	add_core = AYU
	remove_core = NST
	unrest = 0
	rename_capital = "Ranong" 
	change_province_name = "Ranong"
}
1564.2.1 = { add_core = TAU } # Burmese vassal
1584.5.3 = { remove_core = TAU }
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
