# 3544 - Sayula

owner = XAL
controller = XAL
add_core = XAL
culture = tecos
religion = nahuatl
capital = "Tzaulan" 

base_tax = 10
base_production = 0
base_manpower = 0
citysize = 5000
trade_goods = maize 


hre = no

discovered_by = mesoamerican

1530.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	religion = catholic
	marketplace = yes
	bailiff = yes
	courthouse = yes
}
1540.1.1   = {
	discovered_by = SPA
} # Francisco V�zquez de Coronado y Luj�n
1546.9.8   = {
	owner = SPA
	controller = SPA
	capital = "Sayula"
	citysize = 2200
	culture = castillian
	religion = catholic
	base_tax = 2
base_production = 2
	trade_goods = silver
	}
1571.1.1   = {
	add_core = SPA
	citysize = 1000
}
1600.1.1   = {
	citysize = 2000
}
1650.1.1   = {
	citysize = 5000
}
1700.1.1   = {
	citysize = 10000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 20000
}
1800.1.1   = {
	citysize = 50000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
