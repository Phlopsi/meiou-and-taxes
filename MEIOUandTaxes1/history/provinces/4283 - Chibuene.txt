# No previous file for Chibuene

owner = SED
controller = SED
capital = "Chibuene"
culture = nguni
religion = animism
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
hre = no
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = SED
}
1530.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	is_city = yes
	trade_goods = slaves
	naval_arsenal = yes
	customs_house = yes 
	marketplace = yes
}
