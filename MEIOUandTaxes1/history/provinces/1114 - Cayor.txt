# 1114- Cayor

owner = DJO
controller = DJO
culture = senegambian
religion = west_african_pagan_reformed
capital = "Tivaouane"
base_tax = 10
base_production = 1
base_manpower = 1
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1100.1.1 = {
	marketplace = yes
	town_hall = yes
}
1356.1.1 = {
	add_core = CAY
	add_core = DJO
}
1445.1.1 = {
	discovered_by = POR
}
1520.1.1 = {
	base_tax = 11
}
1549.1.1 = {
	owner = CAY
	controller = CAY
}
