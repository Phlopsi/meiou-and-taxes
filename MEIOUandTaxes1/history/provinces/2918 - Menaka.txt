# 2918 - Menaka

owner = SON
controller = SON
add_core = SON
culture = songhai
religion = sunni
capital = "Wallam"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wool
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1493.1.1  = { unrest = 9 } #Civil War between Sunni Baare and Muhammad Ture Sylla
1494.1.1  = { unrest = 0 } #Muhammad Ture Sylla establishes new dynasty
1529.1.1  = { unrest = 2 } #Musa overthrows father, becomes Askiya
