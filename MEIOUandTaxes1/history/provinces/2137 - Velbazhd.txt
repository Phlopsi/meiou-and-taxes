# 2137 - Velbazhd

owner = BYZ
controller = BYZ
culture = bulgarian
religion = orthodox
capital = "Velbazhd"
trade_goods = wine
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = { road_network = yes }
1350.1.1  = {
	owner = VBZ
	controller = VBZ
	add_core = VBZ
	add_core = BUL
	add_local_autonomy = 15
	add_permanent_claim = SER
}
1371.2.17 = {
	owner = OTT
	controller = OTT
	add_core = OTT
	add_local_autonomy = -15
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1444.1.1 = {
	remove_core = SER
}
1515.2.1 = { training_fields = yes }
1520.5.5 = {
	base_tax = 3
	base_production = 0
	base_manpower = 0
}
1530.1.4  = {
	bailiff = yes	
}
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }

1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1689.1.1  = { controller = REB } # Karposh uprising against Ottoman rule
1690.1.1  = { controller = TUR }
1715.1.1  = {  }
1750.1.1  = { add_core = GRE }
1821.3.1  = { controller = REB }
1829.1.1  = { controller = TUR }
