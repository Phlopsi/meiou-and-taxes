# 331 - Konya

owner = KAR
controller = KAR
culture = turkish
religion = sunni
capital = "Konya"
trade_goods = salt
hre = no
base_tax = 7
base_production = 4
base_manpower = 1
is_city = yes
small_university = yes

discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_leather
		duration = -1
	}
}

1088.1.1 = {
	urban_infrastructure_1 = yes	
	marketplace = yes
	corporation_guild = yes
}
1200.1.1 = {
	local_fortification_1 = yes
}
1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = KAR
	set_province_flag = turkish_name
}
1444.1.1 = {
	add_core = TUR
}
1483.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1492.1.1 = { remove_core = KAR } ###
1500.3.3   = {
	base_tax = 10
	base_production = 2
	}
1509.1.1 = { controller = REB } # Civil war
1513.1.1 = { controller = TUR }
1515.1.1 = { training_fields = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}

1658.1.1 = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1 = { controller = TUR }
