# 688 - Huguang Xiangcheng

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Xiangyang"
trade_goods = tea
hre = no
base_tax = 9
base_production = 0
base_manpower = 0
is_city = yes


discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = { paved_road_network = yes }
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1  = {
	owner = CYU
	controller = CYU
    add_core = CYU
}	
1360.1.1 = {
	owner = DAA
	controller = DAA
	add_core = DAA
}
1365.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
}
1368.1.1 = {
	remove_core = DAA
	remove_core = TIA
}
1521.1.1 = {
	base_tax = 14
}
1643.11.1  = {
	owner = DXI
	controller = DXI
	add_core = DXI
}
1646.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = DXI
} # The Qing Dynasty
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
