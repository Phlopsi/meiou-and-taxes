# No previous file for Galam

culture = fulani
religion = west_african_pagan_reformed
capital = "Galam"
trade_goods = unknown
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 70
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan

1490.1.1 = {
	owner = FLO
	controller = FLO
	add_core = FLO
	base_tax = 3
	is_city = yes
	trade_goods = millet
}
