# No previous file for Singhbhum

owner = NGP
controller = NGP
culture = nagpuri
religion = adi_dharam
capital = "Chaibasa"
trade_goods = rice
hre = no
base_tax = 30
base_production = 0
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech

1356.1.1  = {
	add_core = NGP
}
1511.1.1 = {
	base_tax = 38
}
1530.2.3 = {
	add_permanent_claim = MUG
}
1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1627.1.1  = { discovered_by = POR }
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1760.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
