# No previous file for Roponowini

culture = cariban
religion = pantheism
capital = "Iaoyi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 20
native_ferocity = 2
native_hostileness = 8

1750.1.1   = {
	owner = FRA
	controller = FRA
	add_core = FRA
	citysize = 422
	culture = francien
	religion = catholic
	trade_goods = sugar
	change_province_name = "Guyane"
	rename_capital = "Camopi"
} # The French monarchy is restored
1809.1.1   = {
	controller = POR
}
1814.4.11  = {
	controller = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
