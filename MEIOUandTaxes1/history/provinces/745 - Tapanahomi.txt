# 745 - Tapanahomi

culture = guajiro
religion = pantheism
capital = "Tapanahomi"
trade_goods = unknown
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
native_size = 55
native_ferocity = 2
native_hostileness = 8

1498.1.1  = { discovered_by = CAS } # Columbus
1516.1.23 = { discovered_by = SPA }
1525.1.1 = {
	base_tax = 1
}
1630.1.1  = {
	owner = NED
	controller = NED
	culture = dutch
	religion = reformed
	citysize = 500
	trade_goods = sugar
	change_province_name = "Paramaribo"
	rename_capital = "Paramaribo"
	set_province_flag = trade_good_set
	}
1650.1.1  = {
	owner = ENG
	controller = ENG
}
1666.1.1  = {
	owner = NED
	controller = NED
}
1670.1.1  = {
	add_core = NED
	unrest = 6
} # Violent conflicts between native tribes & the Dutch
1700.1.1  = { citysize = 3680 culture = amerikaner }
1750.1.1  = { citysize = 8400 unrest = 0 }
1763.1.1  = { unrest = 4 } # Slave rebellion
1770.1.1  = { unrest = 5 } # Slave rebellion
1772.1.1  = { unrest = 0 }
1799.8.1  = { controller = GBR } # Occupied by British troops
1800.1.1  = { citysize = 16870 }
1801.2.9  = { controller = NED }
# 1975.1.1 - Independance
