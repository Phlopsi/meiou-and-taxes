
owner = TRI
controller = TRI
culture = ripuarianfranconian
religion = catholic
trade_goods = wine
capital = "Trier"
base_tax = 8
base_production = 1
base_manpower = 0
is_city = yes

add_core = TRI

hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = {
	temple = yes
	town_hall = yes
	workshop = yes
	local_fortification_1 = yes
}
1300.1.1 = { road_network = yes }

1520.5.5 = {
	base_tax = 9
	base_production = 1
	base_manpower = 1
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1560.1.1   = { small_university = yes fort_14th = yes } # University of Trier is now handled by the Jesuites who bring a higher quality in education
 # The whole country of Trier receives one law.
1690.1.1   = { base_manpower = 1.0  } # Trier is repeatedly victim of French agression and population declines.

1792.10.4  = { controller = FRA } # Occupied by French troops
1797.10.17 = {
	owner = FRA
	add_core = FRA
} # The Treaty of Campo Formio
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1814.4.6   = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Napoleon abdicates
