# 1552 - Tunkuru

owner = NRI
controller = NRI
culture = igbo		
religion = west_african_pagan_reformed		 
capital = "Tunkuru"
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = IGB
}
1520.1.1 = {
	base_tax = 10
}
1807.1.1   = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
