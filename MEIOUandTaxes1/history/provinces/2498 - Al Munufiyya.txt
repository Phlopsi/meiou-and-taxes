# 2498 - Al Munufiyya

owner = MAM
controller = MAM
add_core = MAM
culture = egyptian
religion = sunni
capital = "Al Munufiyya"
trade_goods = wheat
base_tax = 38
base_production = 3
base_manpower = 2
is_city = yes

discovered_by = ALW
discovered_by = MKU
discovered_by = CIR
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
discovered_by = eastern


1133.1.1 = {
	urban_infrastructure_1 = yes
	workshop = yes
}
1200.1.1 = {
	local_fortification_1 = yes
}
1300.1.1 = { road_network = yes }
#1356.1.1   = { add_core = EGY }
1500.3.3 = {
	base_tax = 27
	base_production = 9
	urban_infrastructure_1 = no
	urban_infrastructure_3 = yes
	marketplace = yes
	}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.1   = {
	#owner = MAM
	#controller = MAM
	add_core = MAM
	#remove_core = TUR
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = MAM
	controller = MAM
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1796.1.1   = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Revolts against the Ottomans
1798.8.10  = {
	revolt = {}
	controller = FRA
} # Occupied by the French
1802.10.1  = {
	controller = TUR
} # Turkish rule is restored but a few troublesome years follow
1805.1.1 = {
	owner = EGY
	controller = EGY
}
1811.6.1   = {
	owner = TUR
	controller = TUR
} # Order is restored
