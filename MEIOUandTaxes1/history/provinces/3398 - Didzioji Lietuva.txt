# 3398 - Did�ioji Lietuva

owner = LIT
controller = LIT
capital = "Breslauja"
culture = lithuanian
religion = baltic_pagan_reformed
trade_goods = wax
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

1356.1.1   = {
	add_core = LIT
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}
1386.1.1   = {
	religion = catholic
}

1508.1.1   = { fort_14th = yes }
1520.5.5 = {
	base_tax = 10
}
1530.1.4  = {
	bailiff = yes	
}
1543.1.1   = { unrest = 3 } # Counter reformation

1569.7.1   = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1579.1.1   = { small_university = yes unrest = 0 } # Vilnius university

1791.6.1   = { unrest = 3} # 3rd May constitution raised opposition
1794.3.24  = { unrest = 6 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 }
1795.10.24  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	add_core = LIT
	add_core = PLC
} # Most of Lithuania became part of the Russian empire
1812.6.28  = { controller = FRA } # Occupied by French troops
1812.12.10 = { controller = RUS }
