# 2308 - Beira Baixa

owner = POR
controller = POR
culture = portugese
religion = catholic
capital = "Castelo Branco"
trade_goods = olive
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
add_core = POR
estate = estate_nobles # estate order militaro
discovered_by = western
discovered_by = turkishtech
discovered_by = muslim
discovered_by = eastern

1249.1.1 = {
	local_fortification_1 = yes
}
1372.5.5   = { unrest = 1 } # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5   = { unrest = 0 } # Civil unrest repressed.
1420.1.1 = {
	base_tax = 4
}
1500.3.3 = {
	base_tax = 6
	base_production = 1
}
1515.1.1 = { training_fields = yes 
	bailiff = yes 
	marketplace = yes
}
1580.8.25  = { controller = SPA }
1580.8.26  = { controller = POR }
1640.1.1   = { unrest = 8 } # Revolt headed by John of Bragan�a
1640.12.1  = { unrest = 0 }


1710.1.1   = {  }
1807.11.30 = { controller = FRA } # Occupied by France
1808.8.30  = { controller = POR }
1810.7.25  = { controller = FRA } # Invaded after the battle of C�a
1811.1.1   = { controller = POR } # The Napoleonic army retreats from Portugal
