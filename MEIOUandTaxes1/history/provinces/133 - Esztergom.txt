#133 - Esztergom

owner = HUN
controller = HUN
culture = hungarian
religion = catholic
capital = "Kaposvár"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1200.1.1 = {
	local_fortification_1 = yes
}
1250.1.1 = { temple = yes }

1356.1.1  = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1506.1.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Szekely rebellion
1507.1.1  = { revolt = {  } controller = HUN } # Estimated
1514.4.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Peasant rebellion against Hungary's magnates
1515.1.1  = { revolt = {  } controller = HUN } # Estimated, put down by the Voyevode of Transylvania, Szapolyiai

1520.5.5 = {
	base_tax = 7
	base_production = 0
	base_manpower = 0
}
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1530.1.1 = { add_claim = TUR }
1540.1.1  = { religion = reformed }
1541.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = HAB
} # End of Ottoman-Habsburg War
1678.1.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Kuruc rebellion
1684.1.1  = { revolt = {  } controller = TUR }
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1703.8.1  = { controller = TRA } # Kuruc rebellion, led by Francisc Rakoczy
1711.1.1  = { controller = HAB } # The treaty of Szatmar
