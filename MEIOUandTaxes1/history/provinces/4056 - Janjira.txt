# 4056 - Janjira

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Janjira"
trade_goods = millet
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = indian
discovered_by = muslim

1356.1.1  = {
	add_core = BIJ
	add_core = BAH
}
#1380.1.1  = {
#	controller = VIJ
#	owner = VIJ
#} # Taken by Harihara II
#1469.1.1  = {
#	controller = BAH
#} # Taken by Bahmanis
#1470.1.1  = {
#	owner = BAH
#} # Taken by Bahmanis
1489.1.1  = { 
	add_permanent_province_modifier = {
		name = habshi_commander
		duration = -1
	}
	capital = "Janjira"
	add_core = JAJ
	controller = JAJ
	owner = JAJ
	fort_15th = yes 
} #Ceded in treaty to Mughals by Bijapur but never actually carried out. De facto under Sidhi, Maratha and Bijapur control without a clear stronger party or claim.
1490.1.1  = {
#	owner = BIJ
#	controller = BIJ
	remove_core = BIJ
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1502.1.1  = { discovered_by = POR }
1511.1.1 = {
	base_tax = 8
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	
}
1571.1.1  = { 
	fort_15th = no
	fort_16th = yes 
} #Piram Khan, demolished the old fort and built an impregnable structure in 22 acres between A. D. 1567-1571. The fort was called Jazeere Mahroob Jazeera.
1596.2.1  = { discovered_by = NED } # Cornelis de Houtman
1600.1.1  = {discovered_by = turkishtech discovered_by = ENG discovered_by = FRA }
1659.11.30 = {
	owner = MAR
	controller = MAR
	capital = "Dabhol"
	add_core = MAR
} # battle of Pratapgarh
1686.1.1  = { controller = MUG } # Aurangzeb
1703.1.1  = { controller = MAR }
1707.5.12 = { discovered_by = GBR }
1818.6.3  = {
	owner = GBR
	controller = GBR
}
