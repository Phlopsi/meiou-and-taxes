# 504 - Thurr

owner = SND
controller = SND
culture = sindhi
religion = sunni 
capital = "Amerkot"
trade_goods = rice
hre = no
base_tax = 11
base_production = 1
base_manpower = 0
citysize = 9287

discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1120.1.1 = { farm_estate = yes }
1133.1.1 = { town_hall = yes }
#1180.1.1 = { post_system = yes }
1356.1.1  = {
	add_core = SND
	add_permanent_province_modifier = {
		name = dhata_state
		duration = -1
	}
	local_fortification_1 = yes
}
1362.1.1 = {
	owner = DLH
	controller = DLH
}
1388.1.1 = {
	owner = SND
	controller = SND
}
1450.1.1 = { citysize = 14000 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 17000 }
1511.1.1 = {
	base_tax = 14
}
1520.7.1 = {
	controller = KAB
} # Arghuns
1521.1.1 = {
	controller = SND
	add_permanent_claim = MUG
} # Arghuns + tag change
1528.1.1 = { road_network = yes }
1544.1.1 = {
	owner = BNG
	controller = BNG
} # Sur Expansionism
1545.1.1 = {
	owner = SND
	controller = SND
} # Sher Shah dies
1550.1.1 = { citysize = 19000 }
1591.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1600.1.1 = { discovered_by = TUR	citysize = 23000 }
1650.1.1 = { citysize = 21000 }
1700.1.1 = { 	citysize = 18000 }
1739.1.1 = {
	owner = SND 
	controller = SND
	remove_core = MUG
}
1750.1.1 = { citysize = 16000 }
1800.1.1 = { citysize = 14000 }
1839.1.1 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
