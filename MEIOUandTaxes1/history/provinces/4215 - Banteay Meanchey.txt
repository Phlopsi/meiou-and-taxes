# No previous file for Banteay Meanchey

owner = KHM
controller = KHM
culture = khmer
religion = buddhism
capital = "Banteay Meanchey"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no


1250.1.1 = { temple = yes } 
1356.1.1 = {
	#controller = AYU
	add_core = KHM
	add_core = AYU
	fort_14th = yes
}
1360.1.1 = {
	controller = KHM
}
1767.4.8 = { 
	add_core = SIA
	remove_core = AYU
}
1811.1.1 = { controller = REB } # The Siamese-Cambodian Rebellion
1812.1.1 = { controller = KHM }
1867.1.1 = { 			# agreement with France
	owner = SIA
	controller = SIA
	rename_capital = "Sa Kaeo" 
	change_province_name = "Sa Kaeo"
}
1907.1.1 = { 			
	owner = FRA
	controller = FRA
}
