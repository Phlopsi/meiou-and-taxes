# 3298 - Sighnaq

owner = BLU
controller = BLU
culture = chaghatai
religion = sunni
capital = "Sighnaq"
trade_goods = wool
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = BLU
}
1382.1.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = BLU
}
1444.1.1 = {
	add_core = SHY
}
1446.1.1 = {
	owner = SHY
	controller = SHY
	culture=uzbehk
	add_core = SHY
	remove_core = GOL
}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1502.1.1 = {
	owner = SHY
	controller = SHY
	remove_core = TIM
}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = KZH
	remove_core = SHY
}
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
   	remove_core = BUK
}
