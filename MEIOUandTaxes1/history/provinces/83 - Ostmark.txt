# 83 - Ostmark 
# Wien St-polten

owner = HAB
controller = HAB
add_core = HAB
culture = austrian
religion = catholic
capital = "Wien" 
base_tax = 23
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = wine 
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

1200.1.1 = {
	set_province_flag = plantation_starting_2
	marketplace = yes
	town_hall = yes
	constable = yes
	workshop = yes
	local_fortification_1 = yes
}
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1365.1.1 = { small_university = yes }
1469.1.1 = {  }
 # Stephansdom becomes Cathedral
1485.1.1  = {	owner = HUN
		controller = HUN
		add_core = HUN
	    } #Mathias Corvinus
1490.1.1  = { owner = HAB controller = HAB remove_core = HUN }
#1500.1.1 = { road_network = yes }


1515.1.1 = { training_fields = yes }

1520.5.5 = {
	base_tax = 46
	base_production = 3
	base_manpower = 4
}
1530.1.1  = { fort_15th = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}


1599.1.1  = { fort_15th = no fort_17th = yes  }
1620.1.1  = {  }
1650.1.1  = { fort_17th = no fort_18th = yes }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
