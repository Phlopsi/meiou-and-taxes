# 2908 - Hombori

owner = LIP
controller = LIP
culture = songhai
religion = west_african_pagan_reformed
capital = "Djiba"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = livestock
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = LIP
}
1430.1.1   = {
	owner = SON
	controller = SON
	add_core = SON
}
1591.4.12  = {
	owner = LIP
	controller = LIP
	remove_core = SON
} # Battle of Tondibi
