# 2986 - Inhassoro

culture = nguni
religion = sunni
capital = "Inhassoro"
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
native_size = 15
native_ferocity = 6
native_hostileness = 6
trade_goods = unknown
discovered_by = central_african
discovered_by = east_african

1498.1.6   = {
	discovered_by = POR
} #Vasco Da Gama
1530.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	is_city = yes
	trade_goods = slaves
	naval_arsenal = yes
	customs_house = yes 
	marketplace = yes
}
