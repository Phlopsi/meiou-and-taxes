# 323 - Ankara

owner = OTT
controller = OTT
culture = turkish
religion = sunni
capital = "Ankara"
base_tax = 10
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = wool #cloth
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1200.1.1 = {
	local_fortification_1 = yes
}
1250.1.1 = {
	temple = yes
	marketplace = yes
	urban_infrastructure_1 = yes
	workshop = yes
	constable = yes
}
1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = OTT
	set_province_flag = turkish_name
	add_permanent_province_modifier = {
		name = "beylik_of_ankara"
		duration = -1
	}
}

1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1429.1.1 = {
	remove_province_modifier = beylik_of_ankara
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = KAR } ###
1500.3.3   = {
	base_tax = 11
}
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR  }
1515.1.1 = { training_fields = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR  } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1720.1.1  = {  }
