# 343 - Fez

owner = FEZ
controller = FEZ
culture = rifain
religion = sunni
capital = "Fez"
base_tax = 6
base_production = 12
base_manpower = 1
is_city = yes
trade_goods = olive # leather #famous for leather goods
	# Kariouyine mosque
small_university = yes # university AL Quaraouiyine 
fort_14th = yes 
discovered_by = TUA
discovered_by = CNA
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_leather
		duration = -1
	}
}

1000.1.1 = {
	
}

1200.1.1 = { merchant_guild = yes }
1250.1.1 = { temple = yes }
1350.1.1 = {
	urban_infrastructure_2 = yes
	corporation_guild = yes
	constable = yes
	
}
1356.1.1 = {
	add_core = FEZ
}
1500.3.3 = {
	base_tax = 4
	base_production = 13
	base_manpower = 1
	}
1530.1.1 = {
	add_core = MOR
}
1548.1.1 = { fort_14th = yes }
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1659.1.1 = { unrest = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { unrest = 3 }
1666.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
	remove_core = MOR
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	remove_core = TFL
}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
1750.1.1 = {  }
1790.1.1 = {
	owner = FEZ
	controller = FEZ
}
1795.1.1 = {
	owner = MOR
	controller = MOR
}
