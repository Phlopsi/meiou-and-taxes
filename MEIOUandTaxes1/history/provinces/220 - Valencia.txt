# 220  - Val�ncia

owner = ARA		#Alfons V of Aragon
controller = ARA
add_core = ARA
culture = valencian
religion = catholic
capital = "Val�ncia" 
base_tax = 10
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = rice
# Val�ncia was the center of peninsular trade with the Mediterranean, surpassing Barcelona
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
estate = estate_burghers
hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
	set_province_flag = farm_estate_starting_2
}

1000.1.1   = {
	local_fortification_1 = yes
}
1100.1.1 = { merchant_guild = yes }
1350.1.1 = {
	road_network = yes
	harbour_infrastructure_2 = yes
	workshop = yes
	urban_infrastructure_1 = yes
}

1356.1.1   = { set_province_flag = spanish_name }
1459.1.1 = { temple = yes fort_14th = yes }
1499.1.1 = { small_university = yes }

1500.3.3   = {
	base_tax = 11
	base_production = 5
	base_manpower = 2
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1519.7.1   = { unrest = 2 } # Revolt of the Germanies, which is at first moderate and appeased by the King
1521.5.1   = { controller = REB } # The revolt radicalizes and the Agermanats organize an army 
1521.9.11  = { controller = SPA unrest = 0 } # The Agermanats are soundly defeated in Oriola, Val�ncia capitulates alongside most revolted towns.
1522.3.20 = {  naval_arsenal = yes }
1609.9.22  = { unrest = 2 } # Decree for the expulsion of the morisques in Valencia. Morisque mutiny in the Alicante harbour.
1609.11.1  = { unrest = 0 } # The morisque mutiny is finally controlled. (economic consequences should be added)

1705.12.16 = { controller = HAB } # Val�ncia joins the Austrian side in the War of the Spanish Succession
1707.5.8   = { controller = SPA } # Val�ncia falls to the Borbonic troops
1713.7.13  = { remove_core = ARA }
1808.6.6   = { controller = REB }
1812.1.1   = { controller = SPA }
1813.3.19  = { controller = REB }
1813.12.11 = { controller = SPA }
