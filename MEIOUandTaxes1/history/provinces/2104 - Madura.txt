# Province : Madura
# file name : 2104 - Madura
# MEIOU-FB Indonesia mod v3 - for IN+JV

owner = MPH
controller = MPH
culture = balinese
religion = hinduism
capital = "Madura"
trade_goods = salt
hre = no
base_tax = 13
base_production = 2
base_manpower = 1
citysize = 25000
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1111.1.1 = {
	harbour_infrastructure_1 = yes
	urban_infrastructure_1 = yes
}

1356.1.1  = {
	add_core = MPH
	add_core = BSM
}
1400.1.1  = { citysize = 28890 }
1478.1.1 = {
	owner = BSM
	controller = BSM
}
1501.1.1 = {
	base_tax = 16
	base_production = 3
}
1512.1.1 = { discovered_by = POR citysize = 34000 }
1597.1.1 = { discovered_by = NED citysize = 38000 }	#FB
1625.1.1 = {
	owner = MTR
	controller = MTR
	remove_core = MPH
	unrest = 2
} # Mataram conquered Surabaya after 12+ years of struggle
1650.1.1 = { citysize = 45200 add_core = MTR unrest = 1 }
1657.1.1 = { unrest = 5 } # Amangkurat's murderous regime becomes increasingly unpopular
1676.10.15 = { controller = REB } #rebels defeat Mataram army at Gogodog
1677.7.13 = {
	controller = MTR
	unrest = 2
} # Amangkurat's death
#after 1680 MTR had little real control in this province
1700.1.1 = { citysize = 47877 }
1717.1.1 = { controller = REB } #Surabaya rebellion/2nd war of Javanese Succession
1721.1.1 = { controller = MTR unrest = 1 } 
1743.11.1 = { 
	owner = NED
	controller = NED
	unrest = 1
}
1750.1.1 = { citysize = 48404 }
1800.1.1 = { citysize = 51240 add_core = NED unrest = 0 }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
