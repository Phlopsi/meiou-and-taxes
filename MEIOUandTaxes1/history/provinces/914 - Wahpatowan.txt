# No previous file for Wahpatowan

culture = dakota
religion = totemism
capital = "Wahpatowan"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 2
native_hostileness = 5

1670.1.1  = { discovered_by = FRA } # Robert Cavelier de La Salle
1808.1.1 = {
	owner = USA
	controller = USA
	citysize = 350
	trade_goods = wheat
	religion = protestant
	culture = american
 } #Fort Madison
