# No previous file for Phongsali

owner = LXA
controller = LXA
add_core = LXA
culture = khmu
religion = animism
capital = "Phongsali"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no


1501.1.1 = {
	base_tax = 2
}
1707.1.1 = {
	owner = LUA
	controller = LUA
	add_core = LUA
	remove_core = LXA
} # Declared independent, Lan Xang was partitioned into three kingdoms; Vientiane, Champasak & Luang Prabang

#1870s 'haw marauders' invasion
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
 