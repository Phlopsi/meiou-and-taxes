# 3730 - Waldeck

owner = WDK
controller = WDK
capital = "Waldeck"
trade_goods = wool
culture = hessian
religion = catholic
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = { 
	add_core = WDK 
	local_fortification_1 = yes
}
1500.1.1 = { road_network = yes }
1525.1.1  = { religion = protestant }
1530.1.4  = {
	bailiff = yes	
}
1620.1.1   = {
	capital = "Arolsen"
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
