# No previous file for Selis

culture = salish
religion = totemism
capital = "Selis"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 4

1808.1.1 = {	owner = GBR
		controller = GBR
		citysize = 350
		trade_goods = fur
		religion = protestant
		culture = american } #Kootenae house
