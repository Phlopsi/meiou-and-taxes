# No previous file for Metekel

owner = DAM
controller = DAM
culture = amhara 
religion = jewish
capital = "Mankush"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = millet

discovered_by = DAR
discovered_by = ALW
discovered_by = SLL
discovered_by = MKU
discovered_by = muslim
discovered_by = east_african

1356.1.1 = {
	add_core = ETH
	add_core = DAM
}
1486.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1515.2.1 = { training_fields = yes }
1530.1.1 = {
	owner = ETH
	controller = ETH
}
1550.1.1 = { discovered_by = TUR }
1583.1.1 = { unrest = 6 } #Shaykh Ajib expelled from Sennar, Abdallabi discontent grows
1584.1.1 = { unrest = 0 } #Dakin and Ajib reach agreement to end conflict
1612.1.1 = { unrest = 5 } #Funj destroy Ajib at Karkoj
1613.1.1 = { unrest = 0 } #Funj restore autonomy to the Abdallabi
1627.1.1 = {
	owner = ETH
	controller = ETH
	add_core = ETH
} # Beta Israel comes to an end in 1627 during the reign of emperor Susenyos of Ethiopia
1706.1.1 = { unrest = 3 } #Badi III faces revolt over development of slave army at aristocrats expense
1709.1.1 = { unrest = 0 } #Badi III faces revolt over development of slave army at aristocrats expense
1718.1.1 = { unrest = 6 } #Unsa III comes into conflict with aristocrats
1720.1.1 = { unrest = 0 } #Unsa III deposed, new Funj dynasty set up by aristocrats
