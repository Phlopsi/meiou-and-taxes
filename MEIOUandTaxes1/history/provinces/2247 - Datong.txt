#2247 - shanxi_area Datong

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Datong"
trade_goods = wheat
hre = no
base_tax = 32
base_production = 1
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = { 
		name = great_wall_ruins
		duration = -1
	}
}

1133.1.1 = { town_hall = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1351.1.1  = {
	owner = QIN
	controller = QIN
    add_core = QIN
}		
1368.1.1  = { 
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 49
	base_production = 3
	base_manpower = 3
}
1530.1.1 = { fort_14th = no fort_15th = yes }
1548.1.1 = {
	remove_province_modifier = great_wall_ruins
	add_permanent_province_modifier = {
		name = great_wall_full
		duration = -1
	}
}
1643.11.1  = { 
	owner = DSH
	controller = DSH
	add_core = DSH
}
#1644.1.1 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty

1644.4.29 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = DSH
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
