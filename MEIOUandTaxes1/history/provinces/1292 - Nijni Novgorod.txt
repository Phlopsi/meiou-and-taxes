# 1292 - Nijni-Novgorod
# MEIOU-GG - Turko-Mongol mod

owner = NZH
controller = NZH
culture = russian
religion = orthodox
capital = "Nizhny Novgorod"
base_tax = 12
base_production = 1
base_manpower = 1
is_city = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

500.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "nijni_novgorod_confluence"
		duration = -1
	}
}
600.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_medium"
		duration = -1
	}
}
1000.1.1 = {
	road_network = yes
	town_hall = yes
	marketplace = yes
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = NZH
}
1382.1.1  = {
	add_core = GOL
}
1392.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NZH
}
1444.1.1 = {
	remove_core = GOL
}
1480.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1511.1.1   = { fort_14th = yes  }
1521.1.1 = { base_tax = 15 }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1   = { unrest = 5 } # "Time of troubles"
1610.9.27  = { add_core = POL } # Polish-Lituanian occupation
1612.1.1   = { remove_core = POL } # Knyaz Dmitry Pozharsky expelles the Polish troops.
1613.1.1   = { unrest = 0 } # Order returned, Romanov dynasty
 # Archangel cathedral
1670.1.1   = { unrest = 8 } # Stepan Razin
1671.1.1   = { unrest = 0 } # Razin is captured
1721.10.22 = {  } # Governmental reforms and the absolutism
1773.1.1   = { unrest = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14  = { unrest = 0 } # Pugachev is captured
