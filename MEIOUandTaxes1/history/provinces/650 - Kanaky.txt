# 650 - Kanaky

culture = papuan
religion = polynesian_religion
capital = "Kanaky"
trade_goods = unknown # spices
hre = no
base_tax = 3
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 25
native_ferocity = 0.5
native_hostileness = 1

1774.1.1 = {
	discovered_by = GBR
	capital = "New Caledonia"
} # Discovered by James Cook
