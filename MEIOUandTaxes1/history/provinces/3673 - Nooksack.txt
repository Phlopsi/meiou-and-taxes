# No previous file for Nooksack

culture = shuswap
religion = totemism
capital = "Nooksack"
trade_goods = unknown
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 85
native_ferocity = 3
native_hostileness = 4

1000.1.1   = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = { 
		name = "kwakiutl_natural_harbour" 
		duration = -1 
		}
	}