#1139 - Doala

culture = bakongo
religion = animism
capital = "Doala"
trade_goods = unknown # millet
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
native_size = 80
native_ferocity = 4.5
native_hostileness = 9
discovered_by = sub_saharan

1520.1.1 = { base_tax = 7 }
