# No previous file for Shenyang

owner = YUA
controller = YUA
culture = korean
religion = mahayana
capital = "Shenyang"
trade_goods = wheat
hre = no
base_tax = 15
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech
fort_14th = yes

1200.1.1 = {
	paved_road_network = yes
	urban_infrastructure_1 = yes
}

1235.1.1  = {
	add_core = YUA
}
1308.1.1 = {#Shen viceroy
	owner = CSE
	controller = CSE
	add_core = CSE
}
1370.1.1 = {
	culture = hanyu
	religion = confucianism
}
1376.1.1 = {
	owner = MXI
	controller = MXI
	add_core = MXI
	remove_core = CSE
}
1381.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = MXI
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 26
	base_production = 0
	base_manpower = 2
}
#1530.1.1 = { fort_14th = no fort_15th = yes }
1621.4.29 = {
	owner = JIN
	controller = JIN
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
