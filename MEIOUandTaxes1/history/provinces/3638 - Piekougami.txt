# No previous file for Piekougami

culture = innu
religion = totemism
capital = "Piekougami"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1652.1.1 = {
		discovered_by = FRA
		owner = FRA
		controller = FRA
		citysize = 250
		trade_goods = fur
		culture = francien
		religion = catholic } #Fort Métabéchouane
1677.1.1 = { add_core = FRA }
1707.5.12  = { discovered_by = GBR }
1760.1.1  = { controller = GBR } # The government of New France capitulate at Montreal
1763.2.10 = {	owner = GBR
		remove_core = FRA
	    	culture = french_colonial
	    } # Treaty of Paris
1788.2.10  = { add_core = GBR }
1800.1.1  = { citysize = 8000 }
