# 3366 - Awaji

owner = HKW
controller = HKW
culture = kansai
religion = mahayana
capital = "Sumoto"
trade_goods = rice
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = HKW
}
1450.1.1 = {
	owner = MIY
	controller = MIY
	add_core = MIY
}
1501.1.1 = {
	base_tax = 10
}
1542.1.1 = { discovered_by = POR }
1550.1.1 = {
	owner = CSK
	controller = CSK
	add_core = CSK
}
1585.1.1 = {
	owner = ODA
	controller = ODA
} # Hashiba (later Toyotomi) Hideyoshi invaded Shikoku with a force of 100,000 men, Chosokabe Motochika keeps only Tosa
1600.9.15  = {
	owner = TGW
	controller = TGW
} # Battle of Sekigahara
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
