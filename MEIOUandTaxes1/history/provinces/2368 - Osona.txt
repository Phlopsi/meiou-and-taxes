# 2368 - Vic

owner = ARA
controller = ARA
add_core = ARA
culture = catalan
religion = catholic
capital = "Osona"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wool
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

1250.1.1 = { 
	temple = yes 
	local_fortification_1 = yes
}
1472.10.16 = { controller = ARA unrest = 0 } #The Generalitat capitulates to King Joan, the civil war ends
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	bailiff = yes
	add_core = CAT
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
 
1640.6.7   = {
	controller = REB
	unrest = 5
	add_core = CAT
} # Guerra dels Segadors 
1641.1.26  = {
	unrest = 0
	owner = CAT
	controller = CAT
} # Proclamation of the Republic of Catalonia
1641.2.27  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Death of Pau Claris i Casademunt, proclamation of Louis XIII de France as count of Barcelona.
1652.10.11 = {
	owner = SPA
	controller = SPA
	remove_core = FRA
} 

1668.4.4   = { unrest = 5 } # Peasant revolt of the "barretines"
1669.12.2  = { unrest = 5 } # End of the revolt
1689.1.1   = { controller = REB } # Peasant revolt against new taxes to fund the war against France
1689.11.30 = { controller = SPA } # The peasants retreat from Barcelona
 # Recovery of the Catalan trade and flourishment of textil industry thanks to exports to America
1697.8.10  = { controller = FRA	} #
1697.9.20  = { controller = SPA } #Peace of Ryswick

1705.10.9  = { controller = HAB } # Catalonia recognises Archduke Carlos as their sovereign, joining the Anglo-Austrian cause in the War of Spanish Succession. The loyalist troops in Barcelona are defeated.
1713.4.11  = { controller = REB	} # Catalan authorities don't recognise the Peace of Utrecht, and face war alone against the Franco-Spanish army.
1713.7.13  = { remove_core = ARA }
1714.9.11  = { controller = SPA } 

1813.8.31  = { controller = REB }
1813.12.11 = { controller = SPA }
