# 1448 - Vier Waldstätten
# schwitz uri unterwald luzern

owner = SWI
controller = SWI
add_core = SWI	
culture = high_alemanisch
religion = catholic
capital = "Luzern"
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = cheese
discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = yes

1200.1.1 = { road_network = yes }
1356.1.1 = {
	local_fortification_1 = yes
	set_province_flag = freeholders_control_province
}
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1520.5.5 = {
	base_tax = 7
	base_production = 0
	base_manpower = 0
}
1530.1.4  = {
	bailiff = yes	
}
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1798.3.5  = { controller = FRA } # French occupation
1798.4.12 = { controller = SWI } # The establishment of the Helvetic Republic
1798.4.15 = { controller = REB } # The Nidwalden Revolt
1798.9.1  = { controller = SWI } # The revolt is supressed
1802.6.1  = { controller = REB } # Swiss rebellion
1802.9.18 = { controller = SWI }
