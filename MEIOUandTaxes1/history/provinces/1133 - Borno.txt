# 1133 - Borno

owner = KBO
controller = KBO
culture = kanouri		
religion = sunni		 
capital = "Borno"
base_tax = 7
base_production = 2
base_manpower = 0
is_city = yes
trade_goods = millet
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1200.1.1 = {
	town_hall = yes
	marketplace = yes 
	workshop = yes
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = KBO
}
1520.1.1 = {
	base_tax = 9
}
