# 513 - Tonga

culture = polynesian
religion = polynesian_religion
capital = "Tonga"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 10
native_ferocity = 2
native_hostileness = 9

1356.1.1  = {
	add_permanent_province_modifier = { name = remote_island duration = -1 }
}
1616.1.1 = {
	discovered_by = NED
} # Discovered by Willem Schouten
1773.1.1 = {
	discovered_by = GBR
} # Visited by James Cook
