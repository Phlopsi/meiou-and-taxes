# 5473 xuchang

owner = SNG
controller = SNG
culture = hanyu
religion = confucianism
capital = "Xuchang" 
trade_goods = wheat
hre = no
base_tax = 22
base_production = 2
base_manpower = 1
is_city = yes




discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	town_hall = yes
	paved_road_network = yes
	temple = yes 
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}

1351.1.1  = {
	owner = SNG
	controller = SNG
    add_core = SNG
}		

1369.3.17 = { 
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = SNG
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 31
	base_production = 6
	base_manpower = 2
	town_hall = no
	urban_infrastructure_2 = yes
	workshop = yes
	marketplace = yes
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
