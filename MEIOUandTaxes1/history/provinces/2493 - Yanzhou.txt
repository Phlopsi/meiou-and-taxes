# 2493 - Yanzhou

owner = CMN
controller = CMN
culture = wuhan
religion = confucianism
capital = "Jiande"
trade_goods = lumber #naval_supplies # bamboo
hre = no
base_tax = 52
base_production = 0
base_manpower = 3
is_city = yes


discovered_by = chinese
discovered_by = steppestech

#1111.1.1 = { post_system = yes }
1200.1.1 = { paved_road_network = yes }
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
	bailiff = yes constable = yes
}

1351.1.1  = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	add_core = MNG
}
1366.1.1  = {
	owner = MNG
	controller = MNG
	remove_core = ZOU
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 83
	base_manpower = 4
}
1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1645.5.27 = { unrest = 0 } # The rebellion is defeated
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1644.1.1 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
1745.1.1  = {  }
