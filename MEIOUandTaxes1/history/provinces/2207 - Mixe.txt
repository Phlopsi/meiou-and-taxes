# 2207 - Mixe

# owner = MAY
# controller = MAY
# add_core = MAY
culture = mixean
religion = nahuatl
capital = "Totontepec" 

base_tax = 7
base_production = 0
#base_manpower = 1.0
base_manpower = 0
# citysize = 3000
trade_goods = cotton

# 
discovered_by = mesoamerican

hre = no

1450.1.1   = {
	owner = AZT
	controller = AZT
	citysize = 1000
	is_city = yes
} # Conquest by Ahuitzotl, eighth tlatoani of Tenochtitlan
1521.8.13  = {
	discovered_by = SPA
}
1528.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	base_tax = 4
base_production = 4
#	base_manpower = 1.5
	base_manpower = 3.0
	citysize = 1500
	trade_goods = maize
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
	}
1535.3.1   = {
	capital = "San Crist�bal de los Llanos"
}
1536.7.7   = {
	capital = "Ciudad Real de Chiapa"
}
1553.1.1   = {
	add_core = SPA
}
1750.1.1   = {
	add_core = MEX
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1829.7.27  = {
	capital = "Ciudad de San Crist�bal"
}
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
