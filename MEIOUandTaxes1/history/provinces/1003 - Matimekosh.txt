# No previous file for Matimekosh

culture = innu
religion = totemism
capital = "Matimekosh"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1 
native_hostileness = 6
