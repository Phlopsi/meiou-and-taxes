# No previous file for Khulna

owner = BNG
controller = BNG
culture = bengali
religion = hinduism
capital = "Khulna"
trade_goods = opium
hre = no
base_tax = 40
base_production = 0
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1356.1.1  = {
	add_core = BNG
}
1500.1.1  = { discovered_by = POR }
1511.1.1 = {
	base_tax = 52
}
1530.1.1 = { 
	add_permanent_claim = MUG
	bailiff = yes	
}
1530.1.2 = { add_core = TRT }
1550.1.1  = {
	religion = sunni
} #Land reclamation
1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1698.1.1  = {
	owner = ENG
	controller = ENG
	remove_core = MUG
} # The area is bought by the East India Company
1707.5.12 = { owner = GBR controller = GBR }
#
1748.1.1  = { add_core = GBR }
1756.1.1  = { controller = BNG } # Captured by Siraj-ud-daullah
1757.6.23 = { controller = GBR } # Battle of Plassey, a new Nawab is installed by the British
