# 2757 - Marburg

owner = HES
controller = HES
add_core = HES
capital = "Marburg"
trade_goods = wool
culture = hessian
religion = catholic
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = {
	local_fortification_1 = yes
}
1500.1.1   = { fort_14th = yes }
1501.1.1 = { road_network = yes }
1527.1.1 = { small_university = yes }
1536.1.1   = { religion = protestant }
1567.3.31  = {
	owner = HKA
	controller = HKA
	add_core = HKA
	remove_core = HES
}
1803.2.25  = {
	owner = HES
	controller = HES
	add_core = HES
	remove_core = HKA
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.9.1   = { controller = RUS } # Occupied by Russian troops
1813.10.14 = {
	owner = HES
	controller = HES
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
1866.1.1  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES 
}
