# 2681 - Bidar

owner = BAH
controller = BAH
culture = kannada
religion = hinduism
capital = "Bidar"
trade_goods = livestock #cloth	#silk
hre = no
base_tax = 33
base_production = 2
base_manpower = 3
is_city = yes
#fort_14th = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1100.1.1 = {
	town_hall = yes
	corporation_guild = yes
}
1249.1.1 = {
	local_fortification_1 = yes
}

1356.1.1  = { add_core = BAH }
1432.1.1 = {
	#fort_14th = no
	#fort_15th = yes
	road_network = yes
}
1498.1.1  = { discovered_by = POR }
1511.1.1 = {
	base_tax = 46
	base_production = 2
}
1618.1.1  = {
	controller = BIJ
} # captured by Bijapur
1619.1.1  = {
	owner = BIJ
} # captured by Bijapur
1685.1.1  = {
	controller = MUG
}
1686.1.1  = {
	owner = MUG
}
1712.1.1  = { add_core = HYD }	#Viceroyalty of the Deccan
1724.1.1  = {
	owner = HYD
	controller = HYD
} # Asif Jah declared himself Nizam-al-Mulk
1760.1.1  = {
	owner = MAR
	controller = MAR
} # Battle of Udgir
1798.1.1  = {
	owner = HYD
	controller = HYD
}
