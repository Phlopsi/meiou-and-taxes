# 4096 - Hajjah

owner = YRA
controller = YRA
culture = yemeni
religion = shiite
capital = "Hajjah"
trade_goods = wool
hre = no
base_tax = 21
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = ADA
discovered_by = MKU
discovered_by = KIL
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = YRA
}
1465.1.1   = {
	trade_goods = coffee
}
1500.3.3   = {
	base_tax = 23
	base_production = 0
	base_manpower = 1
}
1516.1.1   = { add_core = TUR }
1517.4.13  = { 
	owner = TUR
	controller = TUR
	remove_core = MAM
} # Conquered by Ottoman troops
1524.1.1 = { discovered_by = POR }
1530.1.1   = {
	owner = YRA
	controller = YRA
	add_core = YRA
	remove_core = TUR
}
1530.8.1 = {
	add_claim = TUR
}
1567.1.1 = { unrest = 4 } # Revolt against the Ottomans
1570.1.1 = { unrest = 0 }
1597.9.1 = { unrest = 5 } # Qasimi state, revolt against the Ottomans
1602.1.1 = { unrest = 0 }
1636.1.1 = {
	owner = YEM
	controller = YEM
	remove_core = TUR
}