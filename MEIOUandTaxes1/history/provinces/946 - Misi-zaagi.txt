# No previous file for Misi-zaagi

culture = odawa
religion = totemism
capital = "Misi-zaagi"
trade_goods = unknown
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 85
native_ferocity = 1 
native_hostileness = 6

1611.1.1  = {	discovered_by = FRA
		discovered_by = ENG
	    } # �tienne Brul�, Henry Hudson
1668.1.1  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		citysize = 100
	    }
1693.1.1  = { add_core = FRA }
1707.5.12 = { discovered_by = GBR }
1763.2.10 = {
		owner = GBR
		controller = GBR
		remove_core = FRA
		citysize = 1200
		religion = protestant
		culture = english
		trade_goods = fur
	    } # Treaty of Paris
1788.2.10 = { add_core = GBR }
1800.1.1  = { citysize = 2590 }
