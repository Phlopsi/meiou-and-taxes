# No previous file for Potawatami

culture = potawatomi
religion = totemism
capital = "Potawatomi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 4
native_hostileness = 8

450.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "detroit_natural_harbour"
		duration = -1
	}
}
1659.1.1 = { discovered_by = FRA } # M�dard Chouart Des Groseilliers
1670.1.1 = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1701.8.4 = {	owner = FRA
	controller = FRA
	# remove_core = IRO
	culture = francien
	religion = catholic
	citysize = 534
	fort_14th = yes } # Great Peace of Montreal and Fort Detroit
1707.5.12 = { discovered_by = GBR }
1726.1.1 = { add_core = FRA }
1750.1.1 = { citysize = 940 }
1763.2.10 = {	owner = GBR
	controller = GBR
	culture = english
	religion = protestant
	remove_core = FRA
} # The British seized control
#1763.10.9 Royal proclamation, but British retain Detroit
#1783.9.3 = {	owner = POT
#	controller = POT
#	add_core = POT
#	add_core = USA
#	is_city = yes
#	culture = anishinabe
#	religion = totemism
#}# Treaty of Paris, GBR abandons but support natives against USA.
1800.1.1 = { citysize = 1400 }
1807.11.17 = {	owner = USA
	controller = USA
	is_city = yes
	culture = american
	religion = protestant } #Treaty of Detroit cedes much of the region
