# 1119 - Bambuk

culture = dyola
religion = west_african_pagan_reformed
capital = "Beafada"
trade_goods = unknown
base_tax = 9
base_production = 0
base_manpower = 0
native_size = 45
native_ferocity = 6
native_hostileness = 7

discovered_by = sub_saharan

1530.1.1 = {
	owner = MNE
	controller = MNE
	add_core = MNE
	is_city = yes
	trade_goods = slaves
}
