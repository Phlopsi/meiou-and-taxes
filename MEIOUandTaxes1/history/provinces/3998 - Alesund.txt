# 3998 - �lesund

owner = NOR
controller = NOR
add_core = NOR
culture = norwegian
religion = catholic
hre = no
base_tax = 2
base_production = 0
trade_goods = fish
base_manpower = 0
is_city = yes
capital = "�lesund"
discovered_by = eastern
discovered_by = western

1200.1.1 = { set_province_flag = freeholders_control_province }
1500.3.3 = {
	base_tax = 3
	base_production = 0
	base_manpower = 0
}
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
}
1531.11.1 = {
	revolt = { type = nationalist_rebels size = 0 }
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = {  }
	controller = DAN
}
1536.1.1   = { religion = protestant} #Unknown date
1564.3.3   = { controller = SWE }#Nordic Seven-years War
1564.5.21  = {
	controller = DAN
}#Nordic Seven-years War
1658.2.26  = {
	owner = SWE
	controller = SWE
	add_core = SWE
} #The Peace of Roskilde
1658.12.1  = {
	controller = DAN
}#Karl X Gustavs Second Danish War
1660.5.27  = {
	owner = DAN
} #The Peace of Copenhagen
1814.1.14  = {
	owner = SWE
	revolt = { type = nationalist_rebels size = 0 }
	controller = REB
	remove_core = DAN
} # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {  } owner = NOR controller = NOR }
