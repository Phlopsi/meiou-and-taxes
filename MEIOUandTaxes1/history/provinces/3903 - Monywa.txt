# 3903 - Monywa

owner = SAG
controller = SAG
culture = burmese
religion = buddhism
capital = "Monywa"
base_tax = 15
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = opium
discovered_by = chinese
discovered_by = indian
hre = no

1313.2.7 = {
	owner = PIN
	controller = PIN
	add_core = PIN
	add_core = SAG
}
1315.5.15 = {
	owner = SAG
	controller = SAG
}

1365.2.26 = {
	add_core = AVA
	owner = AVA
	controller = AVA
}
1367.9.1 = {
	owner = SAG
	controller = SAG
}
1368.1.1 = {
	owner = AVA
	controller = AVA
	remove_core = SAG
	remove_core = PIN
}
1501.1.1 = {
	base_tax = 20
}
1510.1.16 = {
	owner = MYA
	controller = MYA
}
1527.1.16 = {
	owner = MYA
	controller = MYA
	add_core= MYA
}
1555.1.1 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = MYA
}
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
	remove_core = AVA
}
1885.1.1 = {		
	owner = GBR
	controller = GBR
}
