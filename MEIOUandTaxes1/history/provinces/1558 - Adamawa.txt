#1558 - Adamawa

owner = KUB
controller = KUB
add_core = KUB
culture = kuba
religion = animism
capital = "Adamawa"
trade_goods = copper
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
hre = no
discovered_by = central_african

500.1.1 = {
	add_permanent_province_modifier = {
		name = "ivory_large"
		duration = -1
	}
}
1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}

1520.1.1 = { base_tax = 10 }
