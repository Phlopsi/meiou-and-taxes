# 2734 - Sre Ambel

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Sre Ambel"
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = muslim
discovered_by = indian
hre = no

1501.1.1 = {
	base_tax = 3
}
1867.1.1 = { 			
	owner = FRA
	controller = FRA
}
