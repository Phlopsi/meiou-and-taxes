# No previous file for Nuu-chah-nulth

culture = salish
religion = totemism
capital = "Nuu-chah-nulth"
trade_goods = unknown
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
native_size = 55
native_ferocity = 3
native_hostileness = 4
