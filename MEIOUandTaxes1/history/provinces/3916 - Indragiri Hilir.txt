# 3916 - Indragiri Hilir

owner = SIJ
controller = SIJ
add_core = SIJ
culture = malayan
religion = vajrayana
capital = "Tembilahan"
trade_goods = fish
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = indian
discovered_by = chinese
discovered_by = austranesian

1133.1.1 = {
	harbour_infrastructure_1 = yes
}
1480.1.1   = {
	owner = MLC
	controller = MLC
	add_core = ATJ
}
1500.1.1   = { add_core = MLC }
1511.9.10  = {
	owner = SIJ
	controller = SIJ
	unrest = 2
} # Malacca falls to the Portuguese
1550.1.1   = { religion = sunni }
1624.1.1   = {
	owner = ATJ
	controller = ATJ
}
1683.1.1   = { add_core = NED }
1725.1.1   = {
	owner = SIJ
	controller = SIJ
}
1825.1.1   = {
	owner = NED
	controller = NED
	unrest = 2
} # The Dutch gradually gained control
