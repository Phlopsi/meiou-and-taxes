# 2951 - Butuna

owner = ALW
controller = ALW
culture = nubian 
religion = coptic
capital = "Atbarah"
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wool

discovered_by = DSL
discovered_by = SLL
discovered_by = ALW
discovered_by = YAO
discovered_by = ETH
discovered_by = ADA
discovered_by = DAR
discovered_by = KIT
discovered_by = MKU
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = ALW
}
1503.1.1 = {
	owner = SEN
	controller = SEN
	add_core = SEN
	culture = funj
	discovered_by = SEN
	discovered_by = ETH
}
1515.2.1 = { training_fields = yes }
1530.1.1 = { religion = sunni } #Spread of Islam leads to shift in religion affiliation of region
1550.1.1 = { discovered_by = TUR }
1583.1.1 = { unrest = 6 } #Shaykh Ajib expelled from Sennar, Abdallabi discontent grows
1584.1.1 = { unrest = 0 } #Dakin and Ajib reach agreement to end conflict
1612.1.1 = { unrest = 5 } #Funj destroy Ajib at Karkoj
1613.1.1 = { unrest = 0 } #Funj restore autonomy to the Abdallabi
1706.1.1 = { unrest = 3 } #Badi III faces revolt over development of slave army at aristocrats expense
1709.1.1 = { unrest = 0 } #Badi III faces revolt over development of slave army at aristocrats expense
1718.1.1 = { unrest = 6 } #Unsa III comes into conflict with aristocrats
1720.1.1 = { unrest = 0 } #Unsa III deposed, new Funj dynasty set up by aristocrats
1820.1.1 = {
	owner = TUR
	controller = TUR
}
