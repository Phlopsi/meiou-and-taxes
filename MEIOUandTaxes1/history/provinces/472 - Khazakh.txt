# 472 - Khazakh

owner = BLU
controller = BLU
culture = tartar
religion = sunni
hre = no
base_tax = 6 
trade_goods = salt
base_production = 0
base_manpower = 0
capital = "Argyn"
is_city = yes
discovered_by = SIB
discovered_by = steppestech
discovered_by = turkishtech

#1111.1.1 = { post_system = yes }
1356.1.1 = {
	add_core = BLU
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1428.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
}
1465.1.1 = {
	owner = KZH
	controller = KZH
	add_core = KZH
	culture = khazak
	remove_core = SHY
}
1501.1.1 = {
	base_tax = 7
}
1550.1.1  = {
	discovered_by = TUR
}
1622.1.1 = {
	discovered_by = RUS
}
1740.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1 = { owner = KZH controller = KZH }
