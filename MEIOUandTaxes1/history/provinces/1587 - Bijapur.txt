# 1587 - Bijapur

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Bijapur"
trade_goods = carmine #services
hre = no
base_tax = 24
base_production = 3
base_manpower = 2
is_city = yes
temple =  yes

discovered_by = indian
discovered_by = muslim

1100.1.1 = { 
	marketplace = yes
	workshop = yes
}

1115.1.1 = { constable = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = BIJ
	add_core = BAH
	#fort_14th = yes
}
1490.1.1  = {
	owner = BIJ
	controller = BIJ
	remove_core = BAH
	weapons = yes
	road_network = yes
} # The Breakup of the Bahmani sultanate
1511.1.1 = {
	base_tax = 29
	base_production = 6
}
1515.12.17 = { training_fields = yes }
1566.1.1  = { 
	fort_14th = no fort_16th = yes
}
1600.1.1  = { discovered_by = turkishtech }
1650.1.1  = {  add_core = MAR }
1687.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
	
} # Conquered by the mughal emperor Aurangzeb
1700.1.1  = {  }
1707.5.12 = { discovered_by = GBR }
1712.1.1  = { add_core = HYD }
1724.1.1  = {
	owner = HYD
	controller = HYD
}
1760.1.1  = {
	owner = MAR
	controller = MAR
	remove_core = MUG
} # battle of Kharda
1810.1.1  = {
	add_core = MAR
}
