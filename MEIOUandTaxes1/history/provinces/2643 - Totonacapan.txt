# 2643 - Totonecapan

owner = TOT
controller = TOT
add_core = TOT
culture = totonac
religion = nahuatl
capital = "Cempohuallan" 

base_tax = 9
base_production = 1
base_manpower = 1
citysize = 2500
trade_goods = cotton 


hre = no

discovered_by = mesoamerican

1200.1.1 = { town_hall = yes }
1356.1.1  = {
	add_core = MAY
}
1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
	road_network = yes
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	citysize = 1250
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 2000
}
1650.1.1   = {
	citysize = 5000
}
1700.1.1   = {
	citysize = 10000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 15000
}
1800.1.1   = {
	citysize = 25000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
