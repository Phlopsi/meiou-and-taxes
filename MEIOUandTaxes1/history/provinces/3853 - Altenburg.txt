# No previous file for Altenburg

owner = MEI
controller = MEI
culture = high_saxon
religion = catholic
capital = "Altenburg"
trade_goods = wheat
hre = yes
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = {
	owner = MEI
	controller = MEI
	add_core = MEI
	local_fortification_1 = yes
}
1423.6.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Margraviate of Meissen inherits Saxony and becomes the Elector.
1500.1.1 = { road_network = yes }
1530.1.4  = {
	bailiff = yes	
}
1539.1.1   = {
	religion = protestant
}
1572.6.11   = { 
	owner = SWR
	controller = SWR
	add_core = SWR
	remove_core = SAX
}#Division of Erfurt
1602.1.1   = { 
	owner = SWT
	controller = SWT
	add_core = SWT
	remove_core = SWR
}
1640.1.1   = {  }
1672.1.1   = { 
	owner = SGO
	controller = SGO
	add_core = SGO
	remove_core = SWT
}
1700.1.1   = {  }
1790.8.1   = { unrest = 5 } # Peasant revolt
1791.1.1   = { unrest = 0 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1825.1.1   = { 
	owner = SWT
	controller = SWT
	add_core = SWT
	remove_core = SGO
}
