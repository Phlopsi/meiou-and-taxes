# No previous file for Quechuan

culture = apache
religion = totemism
capital = "Quechuan"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 8

1541.1.1 = { discovered_by = SPA } # Francisco V�squez de Coronado
1731.1.1 = {
	owner = SPA
	controller = SPA
	# remove_core = PIM
	citysize = 250
	religion = catholic
	culture = castillian
} # First permanent Spanish settlers
1751.1.1 = { unrest = 5 }
1752.1.1 = { unrest = 0 }
1756.1.1 = { add_core = SPA citysize = 1450 }
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
