# 1150 - Luangwa

owner = LDU
controller = LDU
culture = nyasa
religion = animism
capital = "Chipata"
base_tax = 4 #Theorical
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = gems
hre = no
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = LDU
}

