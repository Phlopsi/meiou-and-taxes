# 2687 - Bagelkhand

owner = BGL
controller = BGL
culture = bagheli
religion = hinduism
capital = "Bandhogarh"
trade_goods = rice
hre = no
base_tax = 22
base_production = 1
base_manpower = 1
citysize = 12000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1120.1.1 = { town_hall = yes }
1249.1.1 = {
	local_fortification_1 = yes
}

1356.1.1  = {
	add_core = BGL
	#fort_14th = yes
}
1412.1.1  = {
	controller = AHM
} # Jaunpur
1413.1.1  = {
	controller = BGL
} # Jaunpur
1450.1.1  = { citysize = 13000 }
1494.1.1  = {
	controller = DLH
} # Delhi
1495.1.1  = {
	controller = BGL
} # Delhi
1500.1.1  = { citysize = 15000 }
1511.1.1 = {
	base_tax = 29
}
1530.1.1 = { 
	add_permanent_claim = MUG
}
1550.1.1  = { citysize = 17000 }
1562.1.1  = {
	controller = MUG
}
1563.1.1  = {
	controller = BGL
}
1595.1.1  = {
	controller = MUG
}
1597.1.1  = {
	controller = BGL
	capital = "Rewa"
}
1600.1.1  = { citysize = 14000 }
1650.1.1  = { add_core = BHO citysize = 19000 }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 22000 }
1707.5.12 = { discovered_by = GBR }
1741.1.1  = { controller = MAR }	#Maratha expansion
1743.1.1  = {
	owner = BHO
	controller = BHO
} # The Marathan Empire
1750.1.1  = { citysize = 30000 }
1800.1.1  = { citysize = 39000 }
1809.1.1  = {
	owner = BGL
	controller = BGL
} # British dependency
