# 1306 - Miass

owner = BLU
controller = BLU
culture = bashkir
religion = sunni
capital = "Myass"
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = lumber
discovered_by = steppestech
hre = no

1200.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = iron
}
1356.1.1 = {
	add_core = BLU
	add_core = BSH
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1428.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = GOL
}
1468.1.1 = {
	owner = SIB
	controller = SIB
	add_core = SIB
}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1530.1.1 = {
	owner = KZH
	controller = KZH
	add_core = KZH
}
1552.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
}
1600.1.1   = {
#	culture = russian
#	religion = orthodox
	remove_core = KAZ
}
