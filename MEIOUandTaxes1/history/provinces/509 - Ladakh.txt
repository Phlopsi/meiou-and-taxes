# Province : Ladakh
# MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG - Mongol mod

owner = LDK
controller = LDK
culture = ladakhi
religion = vajrayana
capital = "Leh"
trade_goods = wool #carpet
hre = no
base_tax = 2
base_production = 1
base_manpower = 0
citysize = 14572
#fort_14th = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_carpet
		duration = -1
	}
}
1000.1.1 = {
	workshop = yes
	town_hall = yes
}
#1120.1.1 = { textile = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = LDK
}
1400.1.1  = { citysize = 16480 }
1450.1.1 = {
	citysize = 5000
#	owner = KSH
#	controller = KSH
} #Shah Mirs
1500.1.1  = { citysize = 19570  }
1511.1.1 = {
	base_tax = 2
	base_production = 2
}
1550.1.1  = { citysize = 22870 }
1570.1.1 = {
	owner = LDK
	controller = LDK
}
1600.1.1  = { citysize = 25480 }
1650.1.1  = { citysize = 28122 }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 31805 }
1707.5.12 = { discovered_by = GBR }
1750.1.1  = { citysize = 35760  }
1800.1.1  = { citysize = 39400 }
1834.1.1  = {
	owner = PUN
	controller = PUN
}
1849.3.30 = {
	owner = GBR
	controller = GBR
} # End of the Second Anglo-Sikh War

