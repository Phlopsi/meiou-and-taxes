# 2900 - Lobi

owner = MAL
controller = MAL
culture = mossi
religion = west_african_pagan_reformed
capital = "Lobi"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = gold #lobi goldfield
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = MAL
}
1520.1.1 = {
	base_tax = 9
}
1600.1.1  = {
	owner = KNG
	controller = KNG
	add_core = KNG
	remove_core = MAL
}
