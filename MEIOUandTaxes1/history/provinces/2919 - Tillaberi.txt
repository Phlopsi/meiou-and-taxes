# 2919 - Tillabéri

owner = SON
controller = SON
culture = songhai
religion = sunni
capital = "Say"
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1  = { add_core = SON }
1493.1.1  = { unrest = 9 } #Civil War between Sunni Baare and Muhammad Ture Sylla
1494.1.1  = { unrest = 0 } #Muhammad Ture Sylla establishes new dynasty
1529.1.1  = { unrest = 2 } #Musa overthrows father, becomes Askiya
