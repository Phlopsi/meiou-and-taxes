# 691 - anhui_area Tunxi

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Shexian"
trade_goods = lumber
hre = no
base_tax = 66
base_production = 0
base_manpower = 3
is_city = yes
discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
	bailiff = yes constable = yes
}
1200.1.1 = { paved_road_network = yes }
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	add_core = MNG
	
}
1356.1.1 = {
	owner = MNG
	controller = MNG
#	remove_core = YUA # Red Turbans
}
1521.1.1 = {
	base_tax = 104
	base_manpower = 4
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
