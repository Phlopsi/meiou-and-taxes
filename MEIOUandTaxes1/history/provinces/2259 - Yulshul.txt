# 2259 - Yansu

owner = MDO
controller = MDO
culture = tibetan
religion = mahayana
capital = "Gy�gu" # "Jyekundo" "Yushu"
trade_goods = wool
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1356.1.1 = {
	add_core = MDO
}
1637.6.1  = {
	controller = KSD
	owner = KSD
	add_core = KSD
}
1717.1.1 = {
	owner = ZUN
	controller = ZUN
} #Zunghar invasion of Tibet
1720.1.1 = {
	owner = KSD
	controller = KSD
	add_core = KSD
}
1721.1.1 = { discovered_by = SPA }
1724.1.1  = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Qinghai conquered by Qing
1755.10.4 = { unrest = 7 } # Rebellion aginst the Chinese
1757.10.4 = { unrest = 0 }
