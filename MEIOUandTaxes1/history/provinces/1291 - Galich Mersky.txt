# 1291 - Galich-Merskiy

owner = GLC
controller = GLC
capital = "Merya"
culture = russian
religion = orthodox
trade_goods = lumber
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = NOR
discovered_by = SWE
discovered_by = RSW
discovered_by = eastern
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_low"
		duration = -1
	}
}
1356.1.1  = {
	add_core = GLC
	add_core = MOS
}
1364.1.1  = {
	owner = MOS
	controller = MOS
}
1433.1.1   = {
	revolt = { 
		type = pretender_rebels
		size = 2
		name = "Yuri Dmitrievich"
#		dynasty = "Rurikovich" #Does not work
	}
	controller = REB
}
1434.5.6   = {
	revolt = { }
	controller = MOS
}
1438.1.1  = {
	discovered_by = KAZ
}
1450.1.1  = {
	discovered_by = SIB
}
1453.1.1  = {
	discovered_by = western
	discovered_by = eastern
}
1521.1.1 = { base_tax = 7 }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
