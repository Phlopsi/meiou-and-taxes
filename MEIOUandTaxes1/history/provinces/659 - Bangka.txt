# 659 - Bangka
# Note: if MEIOU ever has a start date prior to 1280 then this province should be part of the
# Srivijaya Empire.

owner = PLB
controller = PLB
culture = malayan
religion = vajrayana
capital = "Riau"
trade_goods = copper
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian


1356.1.1 = {
	add_core = MPH
	add_core = PLB
}
1377.1.1 = {
	owner = MPH
	controller = MPH
} # Majapahit destroys the remains of the Srivijaya
1405.1.1 = { discovered_by = MNG }
1478.1.1 = {
	owner = PLB
	controller = PLB
	remove_core = MPH
} # Destruction of Majapahit 
1501.1.1 = {
	base_tax = 3
}
1509.1.1 = { discovered_by = POR }
1550.1.1 = { religion = sunni }
1587.1.1 = {
	owner = MTR
	controller = MTR
}
1682.1.1 = {
	owner = PLB
	controller = PLB
}
1684.1.1 = { add_core = NED }
1812.1.1 = { owner = GBR controller = GBR unrest = 2 } # The Dutch gradually gained control
1825.1.1 = { owner = NED controller = NED } # The Dutch gradually gained control
