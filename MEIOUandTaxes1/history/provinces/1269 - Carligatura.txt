# 1269 - Moldova iasi

owner = MOL
controller = MOL  
add_core = MOL
culture = moldovian
religion =  orthodox
capital = "Iasi"
base_tax = 6
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = livestock
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

hre = no

1100.1.1 = {
	town_hall = yes
}
1250.1.1 = { road_network = yes }
#1498.1.1  = {
#	add_core = TUR
#} # Bayezid II forces Stephen the Great to accept Ottoman suzereignty.
1520.5.5 = {
	base_tax = 9
	base_production = 2
	base_manpower = 1
}
1530.1.4  = {
	bailiff = yes	
}
1564.1.1  = {
	capital = "Iasi"
}
1660.1.1  = {
	
}
