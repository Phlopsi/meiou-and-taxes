# No previous file for Nakaw�

culture = ojibwa
religion = totemism
capital = "Nakawe"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 55
native_ferocity = 2
native_hostileness = 5

1732.1.1  = { discovered_by = FRA } # Pierre Gaultier de Varennes
1732.1.1 = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		citysize = 150 } #Fort St Charles
1757.1.1 = {	add_core = FRA }
1763.2.10 = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		remove_core = FRA
		citysize = 400
		culture = english
	    	religion = protestant
			trade_goods = fur
	    } # Treaty of Paris
1788.2.10 = { add_core = GBR }
