# 58 - Unterfranken

capital = "Aschaffenburg"
culture = eastfranconian
religion = catholic
trade_goods = wine
owner = MAI
base_tax = 9
base_production = 0
base_manpower = 1
is_city = yes
controller = MAI
add_core = MAI
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = { 
	temple = yes 
	local_fortification_1 = yes
}
 # The Kaiserdom in Speyer is on of the oldest and greatest in the HRE, built over the 11th and 12th century

 # The Reichskammergericht (1495-1806) is the highest court in the HRE situated in Worms and after 1527 Speyer

1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 11
	base_production = 0
	base_manpower = 1
}
1530.1.4  = {
	bailiff = yes	
}
1546.4.19  = { religion = protestant } # #Friedrich II converts the coutnry to protestant

1620.1.1   = { fort_14th = yes  }
1689.8.1   = {
	base_tax = 5
	base_production = 5
	fort_15th = no #	controller = FRA
} # French troops burn, pillage and destroy in the succession wars.
1700.1.1   = { fort_14th = yes }
1750.1.1   = {  }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = HES
	controller = HES
	add_core = HES
} # Congress of Vienna
