# 3844 - Kerki

owner = CHG
controller = CHG
culture = chaghatai
religion = sunni
capital = "Kerki"
trade_goods = wool
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = CHG
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1500.6.1    = {
	owner = SHY
	controller = SHY
	culture=uzbehk
	add_core = SHY
	remove_core = TIM
} # Shaybanids break free from the Timurids
1501.1.1 = {
	base_tax = 3
}
1520.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
1785.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
