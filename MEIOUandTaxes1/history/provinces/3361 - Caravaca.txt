# 3361 - Caravaca + Cehegín + Moratalla

owner = CAS		#Juan II of Castille
controller = CAS
culture = andalucian # culture = murcian
religion = catholic 
hre = no
base_tax = 3
base_production = 1
trade_goods = livestock
base_manpower = 0
is_city = yes
capital = "Caravaca de la Cruz"
estate = estate_nobles # estate order militaro
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1133.1.1 = { town_hall = yes }
1356.1.1  = {
	owner = ENR	
	controller = ENR
	add_core = ENR
	add_core = CAS
	set_province_flag = spanish_name
	add_permanent_province_modifier = {
		name = "lordship_of_murcia"
		duration = -1
	}
	local_fortification_1 = yes
}
1369.3.23  = { 
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes
	bailiff = yes 
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille

1713.4.11  = {
	remove_core = CAS
}
1808.6.6   = {
	controller = REB
}
1811.1.1   = {
	controller = SPA
}
1812.10.1  = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
