# 727 - Hailanpao

owner = MYR
controller = MYR
culture = daur
religion = tengri_pagan_reformed
capital = "Aytyun"
trade_goods = fur
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = steppestech
discovered_by = chinese

500.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "blagoveshchensk_confluence"
		duration = -1
	}
}
1356.1.1 = {
	add_core = MYR
}
1515.1.1 = { training_fields = yes }
1616.2.17  = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1628.1.1 = {
   	owner = MCH
   	controller = MCH
	add_core = MCH
} # The Later Jin Khanate
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1643.1.1  = { discovered_by = RUS } # Vasily Poyarkov
1858.1.1 = {
   	owner = RUS
   	controller = RUS
#  	religion = orthodox
#  	culture = russian
} # Treaty of Aigun
