# 2148 - jiangsu_area Xuzhou

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Xuzhou"
trade_goods = cotton
hre = no
base_tax = 21
base_production = 4
base_manpower = 1
is_city = yes

discovered_by = chinese
discovered_by = steppestech

1200.1.1 = {
	paved_road_network = yes
	urban_infrastructure_1 = yes
	workshop = yes
	constable = yes
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1351.1.1  = {
	owner = SNG
	controller = SNG
    add_core = SNG
}			
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1369.3.17 = { 
	marketplace = yes
	constable = no
	courthouse = yes
}
1520.2.2 = {
	base_tax = 33
	base_production = 6
	base_manpower = 2
}
1540.1.1  = { fort_14th = yes }

1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1640.1.1  = { controller = REB } # Financhial crisis
1641.1.1  = { controller = MNG } 
1644.1.1 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
1645.5.27 = { unrest = 0 } # The rebellion is defeated
1740.1.1  = {  }
