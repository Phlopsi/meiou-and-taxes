# 2686 - Mahakoshal

owner = GHR
controller = GHR
culture = gondi
religion = hinduism
capital = "Garha"
trade_goods = livestock
hre = no
base_tax = 19
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1356.1.1  = {
	add_core = GHR
}
1439.1.1  = { controller = MLW }
1441.1.1  = { controller = GHR }
1511.1.1 = {
	base_tax = 25
}
1530.1.1 = { 
	add_permanent_claim = MUG
}
1564.2.1  = { controller = MUG } #Conquered by Mughals
1564.7.1  = {
	owner = MUG
} #Annexed by Mughals
1614.7.1  = { add_core = MUG }
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1741.1.1  = { controller = MAR }	#Maratha expansion
1743.1.1  = {
	owner = BHO
	controller = BHO
	add_core = BHO
	remove_core = MUG
} # The Marathan Empire
1818.6.3 = {
	owner = GBR
	controller = GBR
}
