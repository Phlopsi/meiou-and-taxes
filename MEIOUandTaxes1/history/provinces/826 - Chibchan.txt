# - Chibchan

culture = muisca
religion = pantheism
capital = "Choco"
trade_goods = unknown
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
native_size = 20
native_ferocity = 1
native_hostileness = 2
discovered_by = south_american

1502.1.1  = { discovered_by = CAS } # Christopher Columbus
1516.1.23 = { discovered_by = SPA }
1525.1.1  = {
	owner = SPA
	controller = SPA
	capital = "Santa Mar�a la Antigua del Dari�n"
	culture = castillian
	religion = catholic
	citysize = 1250
	trade_goods = coffee
	base_tax = 1
	set_province_flag = trade_good_set
	add_core = SPA
} # Spanish conquest, Francisco Pizarro
1550.1.1  = { citysize = 1488 }
1600.1.1  = { citysize = 1900 }
1650.1.1  = { citysize = 2580 }
1690.1.1  = { capital = "Quibd�" }
1700.1.1  = { citysize = 2854 }
1750.1.1   = {
	add_core = COL
	culture = colombian
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
