# No previous file for Caslavsko

owner = BOH
controller = BOH
add_core = BOH
culture = czech
religion = catholic
capital = "Kutn� Hora" 
base_tax = 6
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = silver

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes
1000.1.1 = {
	town_hall = yes
}
1298.1.1  = {
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
	add_permanent_province_modifier = {
		name = kutna_hora
		duration = -1
	}
	local_fortification_1 = yes
}

1457.1.1  = { unrest = 5  } # George of Podiebrand had to secure recognition from the German and Catholic towns
1464.1.1  = { unrest = 1 } # The Catholic nobility still undermines the crown.
1471.1.1  = { unrest = 0 }
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1520.5.5 = {
	base_tax = 11
	base_production = 1
	base_manpower = 0
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.3 = {
	road_network = no paved_road_network = yes 
}

1576.1.1  = { religion = reformed  }

1618.4.23 = { revolt = { type = religious_rebels size = 2 } controller = REB } # Defenstration of Prague
1619.3.1  = {
	revolt = { }
	owner = PAL
	controller = PAL
	add_core = PAL
}
1620.11.8 = {
	owner = HAB
	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
 # ... and let us start this session by executing the most inconvenient nobles....
1627.1.1 = { religion = catholic } # Order from Ferdinand II to reconvert to Catholicism, many Protestant leave the country  
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
