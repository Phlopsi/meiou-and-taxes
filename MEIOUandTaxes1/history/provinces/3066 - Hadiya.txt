# 3066 - Hadiya

owner = SID
controller = SID
culture = sidamo 
religion = coptic
capital = "Yirgalem"
base_tax = 12
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = livestock

discovered_by = SLL
discovered_by = ETH
discovered_by = ADA
discovered_by = ALW
discovered_by = MKU
discovered_by = SID
discovered_by = muslim
discovered_by = east_african

1356.1.1 = {
	add_core = SID
}
