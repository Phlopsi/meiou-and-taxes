# 668 - guizhou_area Yinyan

owner = YUA
controller = YUA
culture = miao
religion = animism
capital = "Puding"
trade_goods = rice
hre = no
base_tax = 16
base_production = 0
base_manpower = 0
citysize = 10000

discovered_by = chinese
discovered_by = steppestech


967.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1241.1.1 = {
	owner = SHU
	controller = SHU
}
1265.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1274.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1320.1.1 = {
	remove_core = SNG
}
1356.1.1 = {
}
1369.3.17 = { 
	marketplace = yes
	courthouse = yes
	road_network = yes
}
1382.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = LNG
	remove_core = YUA
}
1500.1.1 = { citysize = 18000 }
1521.1.1 = {
	base_tax = 20
	base_production = 4
	base_manpower = 1
}
1550.1.1 = { citysize = 20000 }
1600.1.1 = { citysize = 24000 }
1650.1.1 = { citysize = 25000 }
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
1700.1.1 = { citysize = 26000 }
1750.1.1 = { citysize = 27000 }
1800.1.1 = { citysize = 46700 }
