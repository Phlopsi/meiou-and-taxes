# 5427 yihuang

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Chongren"
trade_goods = rice
hre = no
base_tax = 62
base_production = 0
base_manpower = 2
is_city = yes




discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	bailiff = yes 
	road_network = yes
	temple = yes 
}
1276.1.1 = {
	owner = CNG
	controller = CNG
	add_core = CNG
}
	

1369.3.17 = { 
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 95
	base_manpower = 5
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
