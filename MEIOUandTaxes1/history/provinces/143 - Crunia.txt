# 143 - Coru�a

owner = CAS		#Juan II of Castille
controller = CAS
culture = galician
religion = catholic
hre = no
base_tax = 10
base_production = 0
trade_goods = tin
is_city = yes # citysize = 1500
base_manpower = 1
capital = "Crunia" 
fort_14th = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

900.1.1   = {
	harbour_infrastructure_1 = yes
}
1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "galicia_natural_harbour" 
		duration = -1 
		}
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
}

1356.1.1   = {
	owner = ENR	
	controller = ENR
	add_core = ENR
	add_core = CAS
	add_core = GAL
	add_permanent_province_modifier = {
		name = "kingdom_of_leon"
		duration = -1
	}
	local_fortification_1 = yes
}
1369.3.23  = { 
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1467.1.1   = { unrest = 4 } # Second war of the "irmandi�os"
1470.1.1   = { unrest = 0 } # End of the Second war of the "irmandi�os"
1475.6.2   = { controller = POR }
1476.3.2   = { controller = CAS }
1500.3.3   = {
	base_tax = 10
	base_production = 1
	base_manpower = 1
}

1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	bailiff = yes
	military_harbour_2 = yes
	remove_core = GAL
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1522.3.20 = { road_network = no paved_road_network = yes }

1713.4.11  = { remove_core = GAL }
1808.6.6   = { revolt = { type = anti_tax_rebels size = 0 } controller = REB }
1809.1.1   = { revolt = {  } controller = SPA }
1812.10.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB }
1813.12.11 = { revolt = {  } controller = SPA }
