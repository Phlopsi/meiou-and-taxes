# 699 - Shantong Qingzhou

owner = QII
controller = QII
culture = hanyu
religion = confucianism
capital = "Zibo"
trade_goods = cotton
hre = no
base_tax = 46
base_production = 2
base_manpower = 2
is_city = yes
fort_14th = yes
discovered_by = chinese
discovered_by = steppestech

1200.1.1 = {
	urban_infrastructure_1 = yes
}
1271.12.18  = {#Proclamation of Yuan dynasty
	owner = YUA
	controller = YUA
	add_core = YUA
}
1351.1.1  = {
	owner = QII
	controller = QII
	add_core = QII # without this the game log gets spammed with trying to create an advisor per Captain Gars
}
1362.11.1  = {
	owner = YUA
	controller = YUA
}
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = QII
	remove_core = YUA
}
1521.1.1 = {
	base_tax = 73
	base_production = 2
	base_manpower = 3
}
1640.1.1 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
