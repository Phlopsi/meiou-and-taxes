# No previous file for Jalpaiguri

owner = KMT
controller = KMT
culture = kochrajbongshi
religion = hinduism
capital = "Bodoganj"
trade_goods = cotton
hre = no
base_tax = 21
base_production = 0
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = chinese
discovered_by = steppestech

1120.1.1 = { plantations = yes }
1200.1.1 = { road_network = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1 = { add_core = KMT
	#fort_14th = yes 
}
1511.1.1 = {
	base_tax = 26
}
1660.1.1 = { controller = MUG }
1661.1.1 = { owner = MUG }
1680.1.1 = { controller = KMT owner = KMT } #Independent but bhutanese puppet
1772.3.1 = { controller = BHU } # 1772 Bhutan annexes Koch Bihar
1772.5.1 = { owner = BHU } # 1772 Bhutan annexes Koch Bihar
1774.4.25 = {
	owner = KMT
	controller = KMT
	remove_core = BHU
} #April 25, 1774 British Vassal
