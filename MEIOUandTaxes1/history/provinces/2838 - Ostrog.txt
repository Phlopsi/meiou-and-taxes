# 2838 - Ostrog

owner = LIT
controller = LIT
add_core = LIT
add_core = GVO
culture = ruthenian
religion = orthodox
hre = no
base_tax = 10
base_production = 0
trade_goods = carmine
base_manpower = 0
is_city = yes
capital = "Ostrog"

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_claim = POL
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}
1366.9.1 = {
	owner = LIT
	controller = LIT
}
1377.1.1 = {
	owner = GVO
	controller = GVO
}
1387.1.1 = {
	remove_claim = POL
	owner = LIT
	controller = LIT
}

1480.1.1  = {
	fort_14th = yes
}
1501.1.1 = {
	base_tax = 11
}
1530.1.4  = {
	bailiff = yes	
}
1569.5.26  = {
	owner = POL
	controller = POL
	add_core = POL
} # Annexed to the crown of poland before Union of Lublin
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
	remove_core = LIT
} # Union of Lublin
1793.1.23  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	culture = byelorussian
} #Third Partition
