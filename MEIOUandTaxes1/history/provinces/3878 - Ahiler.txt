# No previous file for Ahiler

owner = OTT
controller = OTT
culture = turkish
religion = sunni
capital = "Kirikkale"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = OTT
	set_province_flag = turkish_name
	#add_permanent_province_modifier = {
	#	name = "ahis_fraternity"
	#	duration = -1
	#}# Ahis is a fraternity independent until conquest by Murat I in 1362 -politically irrelevant after 1354-impspy
}

1362.1.1 = {
	remove_province_modifier = ahis_fraternity
}

1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = KAR } ###
1500.3.3   = {
	base_tax = 8
}
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR  }
1515.1.1 = { training_fields = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR  } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
