# 3340 - Iwaki (Lower East Mutsu)

owner = HKY
controller = HKY
culture = tohoku
religion = mahayana
capital = "Inodaira"
trade_goods = rice
hre = no
base_tax = 11
base_production = 0
base_manpower = 1
is_city = yes
#medieval_university = yes
discovered_by = chinese

1356.1.1 = {
	add_core = HKY #given to Hatakeyama during nanbokucho to fight mutsu daimyo
}
1501.1.1 = {
	base_tax = 19
}
1542.1.1   = { discovered_by = POR }
1584.1.1 = { add_claim = DTE } #Obama castle taken by Date
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}