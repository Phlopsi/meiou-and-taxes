# 661 - yunnan_area Longyang

owner = DLI
controller = DLI
culture = baizu
religion = mahayana
capital = "Baoshan"
trade_goods = tea
hre = no
base_tax = 11
base_production = 0
base_manpower = 0
is_city = yes
add_core = DLI
discovered_by = chinese


1200.1.1 = { paved_road_network = yes }
1253.1.1 = {
	owner = YUA
	controller = YUA
}
1274.1.1 = {
	add_core = YUA
} #creation of yunan province
1330.1.1 = {
	owner = DLI		#mong mao
	controller = DLI	#mong mao
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1369.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
}
1383.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1520.1.1 = {
	base_tax = 18
}
1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
