# 2696 - Dakka

owner = BNG
controller = BNG
culture = bengali
religion = sunni
capital = "Dakka"
trade_goods = tea #cloth
hre = no
base_tax = 47
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1000.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "dakka_confluence"
		duration = -1
	}
}

1100.1.1 = { marketplace = yes }
1115.1.1 = {
	constable = yes
	workshop = yes
}

1200.1.1 = { road_network = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1  = {
	add_core = BNG
	#fort_14th = yes
}
1483.1.1 = { temple = yes }
1500.1.1  = {
	discovered_by = POR
}
1511.1.1 = {
	base_tax = 98
	base_production = 7
	base_manpower = 4
}
1530.1.1 = { 
	add_permanent_claim = MUG
}
1530.1.2 = { add_core = TRT }

1570.1.1  = {
#	base_tax = 9
#base_production = 9
	religion = sunni
} #Land reclamation
1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1627.1.1  = { discovered_by = POR }
1660.1.1 = { 
	fort_14th = no
	fort_16th = yes }
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1760.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
1810.1.1  = {
	add_core = GBR
}
