# 2291 - Echigo
# GG/LS - Japanese Civil War

owner = USG
controller = USG
culture = koshi
religion = mahayana #shinbutsu
capital = "Kasugayama"
trade_goods = hemp
hre = no
base_tax = 41
base_production = 2
base_manpower = 3
is_city = yes

discovered_by = chinese

1111.1.1 = {
	marketplace = yes
	temple = yes
	harbour_infrastructure_2 = yes
	local_fortification_1 = yes
}
1356.1.1 = {
	add_core = USG
}
1501.1.1 = {
	base_tax = 71
	base_production = 5
	base_manpower = 4
}
1542.1.1   = { discovered_by = POR }
1600.9.15  = {
	owner = DTE
	controller = DTE
} # Battle of Sekigahara
1630.1.1   = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
