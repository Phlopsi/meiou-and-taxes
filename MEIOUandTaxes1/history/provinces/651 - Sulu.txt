owner = SUL
controller = SUL
culture = tausug
religion = polynesian_religion	#FB first evidence of Islam in Sulu is a tombstone dated 1310
capital = "Astana Putih"
trade_goods = fish
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
citysize = 4500
add_core = SUL
discovered_by = chinese
discovered_by = austranesian

500.1.1 = {
	add_permanent_province_modifier = {
		name = "pearls_low"
		duration = -1
	}
}
1380.1.1 = { temple = yes }
1450.1.1 = {
	religion = sunni
}
1500.1.1 = { citysize = 4975 }
1521.1.1 = { discovered_by = SPA } # Ferdinand Magellan 
1550.1.1 = { citysize = 5575 }
1600.1.1 = { citysize = 6260 }
1638.1.1 = {	owner = SPA
		controller = SPA
			   } # Spanish occupation
1646.1.1 = {	owner = SUL
		controller = SUL
	   }
1650.1.1 = { citysize = 6810 }
1700.1.1 = { citysize = 7422 }
1750.1.1 = { citysize = 8340 }
1800.1.1 = { citysize = 8800 }
