# 2241 - Aristano (former Giudicato of Arborea)

owner = ARB
controller = ARB
culture = sardinian 
religion = catholic 
hre = no					#AdL: not part of the HRE 
base_tax = 4
base_production = 0
trade_goods = iron
base_manpower = 0
is_city = yes
capital = "Aristanis" 
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech


1088.1.1 = { harbour_infrastructure_1 = yes }
1326.1.1  = { add_core = ARB }
1356.1.1  = { add_claim = ARA }
1420.8.17  = {
	owner = ARA
	controller = ARA
	remove_core = ARB
	add_core = SAR
}
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # Unification of Spain 
1520.5.5 = {
	base_tax = 5
	base_production = 0
	base_manpower = 0
}
1530.1.1 = {
	owner = SAR
	controller = SAR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}

1713.4.12 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1718.8.2  = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SAR
	remove_core = HAB
} # House of Savoy becomes Kings of Sardinia
1796.1.1  = { controller = FRA } # French invasion
1796.4.16 = { controller = SPI } # Peace between Sardinia and France
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	add_core = SAR
}
