# 1231 - Chuuk

culture = polynesian
religion = polynesian_religion
capital = "Chuuk"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 5
native_ferocity = 0.5
native_hostileness = 2

1356.1.1  = {
	add_permanent_province_modifier = { name = remote_island duration = -1 }
}
1526.1.1 = {
	discovered_by = SPA
} # Discovered by Toribio Alonso de Salazar
1527.1.1 = {
	discovered_by = POR
} # Visited by Diego da Rocha
1686.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	citysize = 100
	capital = "Carolinas"
	trade_goods = fish
	set_province_flag = trade_good_set
}
