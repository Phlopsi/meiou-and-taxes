# 2146 - Maranoa

culture = aboriginal
religion = polynesian_religion
trade_goods = unknown #grain
capital = "Maranoa"
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 0.5
native_hostileness = 1
