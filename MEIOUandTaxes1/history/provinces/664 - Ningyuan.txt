# 664 - sichuan_area Dongqu

owner = SHU
controller = SHU
culture = yizu
religion = animism
capital = "Jianchang"
trade_goods = livestock
hre = no
base_tax = 15
base_production = 0
base_manpower = 0
is_city = yes
add_core = SHU
discovered_by = chinese
discovered_by = steppestech

967.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
	add_core = YUA
}
1200.1.1 = { paved_road_network = yes }
1241.1.1 = {
	owner = SHU
	controller = SHU
}
1259.1.1 = {
	owner = YUA
	controller = YUA
	remove_core = SNG
}
1274.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1354.1.1 =  {
	owner = XIA
	controller = XIA
	add_core = XIA
}
1356.1.1 = {
	remove_core = LNG # Red Turbans
}
1369.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
}
1371.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = SHU
	remove_core = XIA
}
1521.1.1 = {
	base_tax = 23
}
1644.3.19  = {
	owner = DXI
	controller = DXI
	add_core = DXI
}
1646.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = DXI
	remove_core = MNG
} # The Qing Dynasty
