# No previous file for Powys

owner = ENG
controller = ENG
culture = welsh
religion = catholic
hre = no
base_tax = 2
base_production = 0
trade_goods = wool
base_manpower = 0
is_city = yes
capital = "Y Drenewydd" # Newtown
discovered_by = western
discovered_by = muslim
discovered_by = eastern

#1100.1.1 = { bailiff = yes }
1284.3.3  = {
	add_permanent_province_modifier = {
		name = "marches_of_wales"
		duration = -1
	}
} # Statute of Rhuddlan
1356.1.1  = {
	add_core = ENG
	add_core = WLS
	local_fortification_1 = yes
}
1453.1.1  = { unrest = 5 } #Start of the War of the Roses
1461.6.1  = { unrest = 2 controller = REB } #Lancastrian Holdouts after Coronation of Edward IV
1467.1.1  = { unrest = 5 } #Rivalry between Edward IV & Warwick
1468.6.1  = { controller = ENG } #Estimated
1471.1.1  = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.3.1  = { controller = REB }
1471.5.4  = { unrest = 2 controller = ENG } #Murder of Henry VI & Restoration of Edward IV
1472.1.1  = {
	remove_province_modifier = "marches_of_wales"
	add_permanent_province_modifier = {
		name = "council_of_wales"
		duration = -1
	}
} # Council of Wales and the Marches, exact date uncertain
1483.6.26 = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
	bailiff = yes
} #Battle of Bosworth Field & the End of the War of the Roses
1520.5.5  = {
	base_tax = 4
	base_production = 0
	base_manpower = 0
}	

1542.3.25 = {
	remove_province_modifier = "council_of_wales"
	add_permanent_province_modifier = {
		name = "laws_in_wales"
		duration = -1
	}
} # The Laws in Wales Act, exact date uncertain
1600.1.1  = { fort_14th = yes }
 #marketplace Estimated
1689.7.25 = {
	remove_province_modifier = "laws_in_wales"
} # Council abolished following the Glorious Revolution of 1688
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
    	remove_core = ENG
}
1750.1.1  = { religion = reformed }
