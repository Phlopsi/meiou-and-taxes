#Province: Qu'aiti
#file name: 389 - Qu'aiti
#MEIOU-FB Arabia mod

owner = YEM
controller = YEM
culture = hadhrami
religion = sunni
capital = "Al Mukalla"
trade_goods = incense
hre = no
base_tax = 13
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = ADA
discovered_by = muslim
discovered_by = indian
discovered_by = turkishtech
discovered_by = east_african

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = YEM
	add_core = HAD
}
1480.1.1 = { discovered_by = TUR }
1488.1.1 = { discovered_by = POR } # P�ro da Covilh�
1500.3.3   = {
	base_tax = 15
}
1530.1.1 = { owner = HAD controller = HAD }
1547.1.1 = { controller = POR } # Occupied by Portugal

