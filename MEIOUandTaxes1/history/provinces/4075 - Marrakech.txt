# 4075 - Marrakech

owner = MOR
controller = MOR
culture = fassi
religion = sunni
capital = "Marrakech"
base_tax = 11
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = olive #sugar
estate = estate_nobles
fort_14th = yes
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_leather
		duration = -1
	}
}
1100.1.1 = { merchant_guild = yes }

1250.1.1 = {
	temple = yes
	urban_infrastructure_2 = yes
	workshop = yes
	local_fortification_1 = yes
}
 
1356.1.1 = {
	add_core = MOR
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1500.3.3 = {
	base_tax = 9
	base_production = 6
	base_manpower = 2
	}
1519.1.1 = { bailiff = yes }
1530.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
	trade_goods = sugar
}
1554.1.1   = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1613.1.1 = { unrest = 0 }
1659.1.1 = { unrest = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { unrest = 3}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
