# 3390 - Milas

owner = AYD
controller = AYD
culture = greek
religion = sunni
capital = "Milas"
trade_goods = alum
hre = no
base_tax = 8
base_production = 1
base_manpower = 1
is_city = yes

# Isabey Mosque

discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

add_permanent_claim = BYZ

1000.1.1 = {
	town_hall = yes
	temple = yes
}
1088.1.1 = { harbour_infrastructure_1 = yes }
1356.1.1  = {
	add_core = AYD
	set_province_flag = turkish_name
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1402.1.1  = {
	owner = AYD
	controller = AYD
} # Tamerlane defeat the Ottomans in Angora (Ankara)
1426.1.1  = {
	owner = TUR
	controller = TUR
}
1456.1.1  = {
	culture = turkish
	religion =  sunni
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = AYD } ###
1500.3.3   = {
	base_tax = 9
}
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1640.1.1  = {   } # One of the most important port-cities during the 17th-19th centuries
1740.1.1  = {  }
