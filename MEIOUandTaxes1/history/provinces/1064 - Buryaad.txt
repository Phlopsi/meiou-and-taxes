# 1064 - Kuznetsk

culture = evenki
religion = tengri_pagan_reformed
capital = "Ilimsk"
trade_goods = unknown
base_tax = 1
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 10
native_ferocity = 1
native_hostileness = 3

#discovered_by = SAK
discovered_by = YEN
discovered_by = YUA

1610.1.1 = { discovered_by = RUS }
1610.1.1 = {
	owner = RUS
	controller = RUS
#  	religion = orthodox
#  	culture = russian
	citysize = 500
	trade_goods = fur
	set_province_flag = trade_good_set
} # Part of Russia
1781.1.1 = {
	add_core = RUS
   	citysize = 1188
}
1800.1.1 = { citysize = 2680 }
