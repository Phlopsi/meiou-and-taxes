# 783 - Aonikenk

culture = mapuche
religion = pantheism
capital = "Aonikenk"
trade_goods = unknown #fish
hre = no
base_tax = 0 # 10
base_production = 0
base_manpower = 0
native_size = 65
native_ferocity = 2
native_hostileness = 9

1356.1.1 = {
	add_permanent_province_modifier = {
		name = uncolonisable_rural_pop_10
		duration = -1
	}
}
1500.1.1 = {
	remove_province_modifier = uncolonisable_rural_pop_10
	add_permanent_province_modifier = {
		name = uncolonisable_rural_pop_01
		duration = -1
	}
#	base_tax = 1
	native_size = 5
}
1520.1.1   = {
	discovered_by = SPA
	add_core = SPA
} # Discovered by Ferdinand Magellan
1750.1.1  = {
	add_core = CHL
	remove_core = SPA
	base_tax=  1
}
1890.1.1   = {
	owner = CHL
	controller = CHL
	culture = platean
	religion = catholic
	citysize = 200
	trade_goods = fish
	set_province_flag = trade_good_set
}
