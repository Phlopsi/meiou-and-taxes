# 287 - Kouban
# MEIOU-GG - Turko-Mongol mod


owner = WHI
controller = WHI
culture = crimean
religion = sunni
capital = "Kuban"

base_tax = 3
base_production = 0
base_manpower = 0
citysize = 4007
trade_goods = wool


discovered_by = eastern
discovered_by = muslim
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech

hre = no

1356.1.1 = {
	add_core = WHI
	add_core = CRI
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1441.1.1 = {
	owner = CRI
	controller = CRI
	remove_core = GOL
}
#1475.1.1 = {
#	add_core = TUR
#}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1774.7.21 = {
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
} # Annexed by Catherine II
