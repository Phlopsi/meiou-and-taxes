# 2543 - Al Fayym

owner = MAM
controller = MAM
add_core = MAM
culture = egyptian
religion = sunni
capital = "Al Fayym"
trade_goods = wool #linen
hre = no
base_tax = 18
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = ADA
discovered_by = ALW
discovered_by = MKU
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
discovered_by = eastern

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1100.1.1 = {
	town_hall = yes
	workshop = yes
}
#1356.1.1   = { add_core = EGY }
1500.3.3 = {
	base_tax = 16
	base_production = 1
	}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.1   = {
	#owner = MAM
	#controller = MAM
	add_core = MAM
	#remove_core = TUR
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = MAM
	controller = MAM
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1796.1.1   = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Revolts against the Ottomans
1798.8.10  = {
	revolt = {}
	controller = FRA
} # Occupied by the French
1805.1.1 = {
	owner = EGY
	controller = EGY
}
1811.6.1 = {
	owner = TUR
	controller = TUR
} # Order is restored
