# 2564 - Massa

owner = MAS
controller = MAS
culture = emilian
religion = catholic 
hre = yes 
base_tax = 6
base_production = 0       
trade_goods = lumber
base_manpower = 0
is_city = yes

capital = "Massa"
# 12th century
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
	set_province_flag = mined_goods
	set_province_flag = marble
}
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = MAS
	add_permanent_province_modifier = {
		name = "county_of_fosdinovo"
		duration = -1
	}
	local_fortification_1 = yes
}
1520.5.5 = {
	base_tax = 7
	base_production = 0
	base_manpower = 0
}
1530.1.2 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}
1530.2.27 = {
	hre = no
	fort_14th = yes 
}
1618.1.1  = { hre = no }
1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = HAB
} # Cispadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1814.4.11 = { owner = MOD controller = MOD add_core = MOD remove_core = ITE }
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
