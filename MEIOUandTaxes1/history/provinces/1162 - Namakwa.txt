# 1162 - Namakwa

culture = khoisan
religion = animism
capital = "Karoo"
trade_goods = unknown
base_tax = 1
base_production = 0
base_manpower = 0
hre = no
native_size = 5
native_ferocity = 0.5
native_hostileness = 0
discovered_by = KON

1488.1.1 = { discovered_by = POR } # Bartolomeu Dias
