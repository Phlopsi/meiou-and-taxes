# 535 - Thiruvithaamkoor

owner = TRV
controller = TRV
culture = malayalam
religion = hinduism
capital = "Padmanabhapuram"
trade_goods = pepper
hre = no
base_tax = 19
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim

1100.1.1 = {
	town_hall = yes
}
1249.1.1 = {
	local_fortification_1 = yes
}

1356.1.1  = { 
	add_core = TRV
	#fort_14th = yes
}
1405.1.1  = { discovered_by = chinese }
1498.5.20 = { discovered_by = POR }
1511.1.1 = {
	base_tax = 23
	base_production = 2
}
1550.1.1  = { capital = "Padmanabhapuram" }
1565.7.1  = {
	owner = MAD
	controller = MAD
	add_core = MAD
	unrest = 4
}
1570.1.1  = { unrest = 0 }
1600.1.1  = { discovered_by = ENG discovered_by = FRA discovered_by = NED }

1662.1.1  = {
	controller = NED
	owner = NED
}
1729.1.1  = {
	owner = TRV
	controller = TRV
}
1798.1.1  = { capital = "Thiruvananthapuram" }
