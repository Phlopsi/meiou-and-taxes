# 1067 - Khoongorai

owner = YEN
controller = YEN
culture = old_kirgiz
religion = tengri_pagan_reformed
capital = "Kem-Kemchik"
trade_goods = fur
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
discovered_by = steppestech
discovered_by = YUA

1356.1.1  = {
	add_core = YEN
}
1604.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
#  	religion = orthodox
#  	culture = russian
	rename_capital = "Krasnoyarsk" 
	change_province_name = "Krasnoyarsk"
}
1629.1.1 = {
	add_core = RUS
}
