# 3379 - Schwaben / Burgau

culture = schwabisch
owner = HAB
controller = HAB
add_core = HAB
capital = "Burgau"
religion = catholic
trade_goods = wheat
base_tax = 15
base_production = 1
base_manpower = 2
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { 
	town_hall = yes 
	local_fortification_1 = yes
	set_province_flag = freeholders_control_province
}

1450.1.1 = {
	owner = BAX
	controller= BAX
}
1451.1.1 = {
	owner = HAB
	controller= HAB
}
1485.1.1 = {
	owner = AUH
	controller= AUH
}
1486.1.1 = {
	owner = BAX
	controller= BAX
}
1492.1.1 = {
	owner = HAB
	controller= HAB
}
1498.1.1 = {
	owner = AUH
	controller= AUH
}
1500.1.1 = { road_network = yes fort_14th = yes }
1520.5.5 = {
	base_tax = 18
	base_production = 1
	base_manpower = 2
}
#1534.1.1  = {
#	religion = protestant
#} # House Würtemberg reconquers the coutnry and installs the protestant religion
1559.1.1 = {
	owner = HAB
	controller= HAB
}
1733.1.1  = {
	unrest = 2
} # Karl Alexander becomes Duke of Würtemberg. He is a catholic with a jewish  advisor, which is very much resented by the protestant nobility.
1737.3.12 = {
	unrest = 0
} # Death of the Duke, execution of the advisor after a set up process.
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
