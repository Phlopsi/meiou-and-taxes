# 1170 - Swazi

culture = nguni
religion = animism
capital = "Massingir"
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
native_size = 15
native_ferocity = 6
native_hostileness = 6
trade_goods = unknown

discovered_by = BUT
discovered_by = ZIM
discovered_by = MNK
discovered_by = SED

1498.1.6   = {
	discovered_by = POR
} #Vasco Da Gama
1790.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
	trade_goods = gold
}

