
owner = FRA
controller = FRA 
culture = flemish
religion = catholic
capital = "Cal�s"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
fort_14th = yes
trade_goods = fish
 # Exception to the rule, trade junction & established here
 # 'The Brightest Jewel in the English Crown'
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no


1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "strait_of_calais_strait" 
		duration = -1 
		}
}
1250.1.1 = { harbour_infrastructure_2 = yes }


1300.1.1 = { road_network = yes }
1347.8.4   = {
	owner = ENG
	controller = ENG
	add_permanent_claim = FRA
	add_core = ENG
	add_core = FLA
	culture = english
	rename_capital = "Calais"
	change_province_name = "Pale of Calais"
} # England conquer and expeled french inhabitants after siege
1453.1.1   = { add_core = BUR }
1459.1.1   = { unrest = 7 } # War of the Roses in England
1459.11.1  = { controller = REB unrest = 3 } # Yorkist control over Calais
1461.6.1   = { controller = ENG unrest = 0 } # Edward VI crowned King of England
1477.1.5   = { remove_core = BUR } # Charles the Bold dies
1500.1.1 = { road_network = yes }
1519.1.1 = { bailiff = yes }
1522.3.20 = {  naval_arsenal = yes }
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1558.1.7   = { controller = FRA } # France captures the weakened Calais
1559.4.3   = {
	owner = FRA
	add_core = FRA
	remove_core = ENG
	culture = picard
	rename_capital = "Calais"
	change_province_name = "Calaisis"
} # Peace of Cateau-Cambr�sis, France annexes Calais from England
1560.1.1   = { fort_14th = yes } # Very late, English neglected Calais' forts the last few decades
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1596.1.1   = { controller = SPA } # The Spanish take hold of Calais
1598.1.1   = { controller = FRA } # Treaty of Vervins, Calais back to France

1640.1.1   = { fort_14th = no fort_15th = yes }
1670.1.1   = { fort_15th = no fort_16th = yes }
1680.1.1   = { fort_16th = no fort_17th = yes } # Vauban's 'pointed' fort in Calais

