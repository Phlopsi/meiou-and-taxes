# No previous file for Hararghe

owner = ETH
controller = ETH
add_core = ETH
culture = somali 
religion = sunni
capital = "Harar"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wool
discovered_by = ADA
discovered_by = muslim
discovered_by = east_african
hre = no

1100.1.1 = { marketplace = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1486.1.1 = { unrest = 5 add_core = ADA } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1514.1.1 = {
	owner = ADA
	controller = ADA
	unrest = 6
} #Mahfuz drives Ethiopians out of Ifat region
1515.2.1 = { training_fields = yes }
#1517.7.1 = {
#	owner = ETH
#	controller = ETH
#} #Lebna Dengel occupies region during campaign that defeats Mahfuz
#1527.1.1 = { unrest = 5 } #Raids by Ahmad Gran
#1529.1.1 = { unrest = 7 } #Raids by Ahmad Gran
1530.1.1 = { religion = sunni } #Spread of Islam leads to shift in religion affiliation of region
#1531.1.1 = { unrest = 7 } #Raids by Ahmad Gran
1532.1.1 = {
	owner = ADA
	controller = ADA
#	unrest = 6
} #Lebna Dengel defeated by Ahmad Gran at Ayfars
1545.1.1 = {
	owner = ETH
	controller = ETH
	remove_core = ADA
} #Adal collapse in highlands in wake of Ahmad Gran's death
1549.1.1 = { unrest = 5 } #Invasion by Oromo
1550.1.1 = { discovered_by = TUR }
1553.1.1 = { unrest = 5 } #Invasion by Adal 
1558.1.1 = {
	owner = ADA
	controller = ADA
	unrest = 5
} #Invasion by Adal
1564.1.1 = {
	citysize = 0
	native_size = 50
	native_ferocity = 4.5
	native_hostileness = 9 
	owner = XXX
	controller = XXX
	trade_goods = unknown
	base_tax = 1
base_production = 1
#	base_manpower = 0.5
	base_manpower = 1.0
} #Oromo invasion of Adal breaks control over northern Somali
