# 2787 - Osun

owner = OYO
controller = OYO
culture = yorumba
religion = west_african_pagan_reformed
capital = "Osun"
base_tax = 13
base_production = 0
base_manpower = 1
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = OYO
}
1519.1.1 = {
	base_tax = 15
}
1520.1.1 = { add_core = BEN unrest = 4 }
1525.1.1 = { unrest = 0 }
1535.1.1 = { capital = Ijebu }
1585.1.1 = { unrest = 4 } #Nupe Raids
1590.1.1 = { unrest = 0 }
