# 149 - Thessalia

owner = TSL
controller = TSL
culture = greek
religion = orthodox
capital = "L�rissa"
trade_goods = olive
hre = no
base_tax = 5
base_production = 1
base_manpower = 0
is_city = yes

discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1100.1.1 = {
	marketplace = yes
	town_hall = yes
}
1249.1.1 = {
	local_fortification_1 = yes
}
1300.1.1 = { road_network = yes }
1356.1.1   = {
	controller = EPI
	add_core = TSL
	add_core = BYZ
	#add_claim = EPI # claim set already by the starting war
	add_claim = SER
} # Epirus revolt
1373.1.1   = {
	owner = BYZ
	controller = BYZ
	add_local_autonomy = 15
}
1394.1.1   = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_local_autonomy = -15
}
1444.1.1 = {
	remove_core = SER
}
1453.5.29  = {
	remove_core = BYZ
	remove_core = SER
}
1459.1.1   = {
	religion = sunni
}

1481.6.1   = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26  = { controller = TUR } # Jem escapes to Rhodes
1515.2.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1520.5.5 = {
	base_tax = 7
	base_production = 1
	base_manpower = 0
}
1522.3.20 = {  naval_arsenal = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1555.1.1   = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1   = { unrest = 0 }
 # Selimiye mosque
1621.1.1   = { unrest = 6 } # Osman II's reforms against the Janissaries
1622.5.20  = { unrest = 7 } # Osman II is murdered
1622.6.1   = { controller = TUR unrest = 0 } # Mustafa I, estimated
1623.1.1   = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1   = { controller = TUR } # Murad tries to quell the corruption
1718.1.1   = { unrest = 3 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1   = { unrest = 0 }
1730.1.1   = {  }
1750.1.1   = { add_core = GRE } # Great fire (1745), earthquake in 1751, city declined
1795.1.1   = { unrest = 6 } # Reforms by Sultan Selim III, tried to replace th Janissary corps
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	controller = TUR
}
