# 5417 yanzhou

owner = MNG
controller = ZOU
culture = wuhan
religion = confucianism
capital = "Dongyang"
trade_goods = rice
hre = no
base_tax = 38
base_production = 0
base_manpower = 2
is_city = yes




discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	bailiff = yes 
	road_network = yes
	fort_14th = yes
	temple = yes 
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1351.1.1  = {
	owner = MNG
    add_core = MNG
	add_core = ZOU
}	

1369.3.17 = { 
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = ZOU
}
1520.2.2 = {
	base_tax = 58
	base_manpower = 4
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
