# 2236 - Puttalam

owner = JAF
controller = JAF
culture = sinhala
religion = buddhism
capital = "Puttalam"
trade_goods = ebony
hre = no

base_tax = 10
base_production = 0
base_manpower = 0
citysize = 4308
add_core = JAF
discovered_by = muslim
discovered_by = indian

500.1.1 = {
	add_permanent_province_modifier = {
		name = "pearls_medium"
		duration = -1
	}
}

1000.1.1 = {
	harbour_infrastructure_1 = yes
	temple = yes
}
1450.1.1 = {
	owner = KTH
	controller = KTH
}
1500.1.1 = { citysize = 4877 }
1505.1.1 = { discovered_by = POR } # Francisco de Almeida
1511.1.1 = {
	base_tax = 13
}
1517.1.1 = { owner = POR controller = POR
	add_core = POR
}
1522.3.20 = { naval_arsenal = yes marketplace = yes customs_house = yes }
1542.1.1 = { add_core = POR }
1550.1.1 = { citysize = 5412 }
1600.1.1 = { citysize = 6320 }
1650.1.1 = { citysize = 7564 }
1660.1.1 = { owner = NED controller = NED remove_core = POR } # Dutch control
1676.1.1 = { fort_17th = yes }
1685.1.1 = { add_core = NED }
1700.1.1 = { citysize = 9050}
1750.1.1 = { citysize = 10870 }
1799.8.1 = { controller = GBR } # Occupied by England
1800.1.1 = { citysize = 12332 }
1802.3.25 = {
	owner = GBR
	add_core = GBR
	remove_core = NED
} # Treaty of Amiens
