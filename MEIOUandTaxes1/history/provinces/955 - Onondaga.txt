# No previous file for Onondaga

culture = iroquois
religion = totemism
capital = "Onondaga"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 2
native_hostileness = 5
450.1.1 = {
	#add_permanent_province_modifier = {
	#	name = "saint_lawrence_entry_south"
	#	duration = -1
	#}
}
1615.1.1  = { discovered_by = FRA } # �tienne Br�l� 
1629.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1784.10.22  = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
	is_city = yes
} #Second Treaty of Fort Stanwix, Iroquois confederacy no longer dominant
1809.10.22 = { add_core = USA }
