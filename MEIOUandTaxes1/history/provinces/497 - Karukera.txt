# 497 - Karuk�ra

culture = carib
religion = pantheism
capital = "Karuk�ra"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 25
native_ferocity = 2
native_hostileness = 9

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "guadeloupe_natural_harbour" 
		duration = -1 
		}
}

1493.11.14 = {
	discovered_by = CAS
} # Christopher Columbus, never showed much interest for the island
1516.1.23  = {
	discovered_by = SPA
}
1635.1.1   = {
	owner = FRA
	controller = FRA 
	citysize = 389
	religion = catholic
	culture = french_colonial
	capital = "Guadeloupe"
	trade_goods = cacao
	set_province_flag = trade_good_set
} # Most of the Caribs were wiped out by the French
1643.1.1   = {
	capital = "Haute-Terre"
	citysize = 1210
}
1650.1.1   = {
	citysize = 2242
}
1660.1.1   = {
	add_core = FRA
}
1700.1.1   = {
	citysize = 2637
}
1710.1.1   = {
	
}
1750.1.1   = {
	citysize = 3103
}
1793.1.1   = {
	controller = REB
} # Slave rebellion
1794.4.21  = {
	controller = GBR
} # Seized by the British
1794.6.2   = {
	controller = FRA
} # French control is restored
1800.1.1   = {
	citysize = 3650
}
1813.2.5   = {
	controller = GBR
} # Occupied by British troop again
1813.3.3   = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = FRA
} # Ceded to Sweden
1814.5.30  = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = SWE
} # The Treaty of Paris
