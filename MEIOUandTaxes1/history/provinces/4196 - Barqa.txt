# No previous file for Barqa

owner = YUA
controller = YUA
culture = evenki
religion = tengri_pagan_reformed
capital = "Barqa"
trade_goods = fur
hre = no
is_city = yes
base_tax = 2
base_production = 0
base_manpower = 0
discovered_by = steppestech

1356.1.1 = {
	add_core = YUA
}
1392.1.1 = {
	owner = BRT
	controller = BRT
	add_core = BRT
	remove_core = YUA
}
1653.1.1 = { discovered_by = RUS } # Pyotr Beketov
1656.1.1 = {
	owner = RUS
	controller = RUS
#  	religion = orthodox
#  	culture = russian
}
1691.1.1 = { add_core = RUS }
1740.1.1 = {
	culture = buryat
}

