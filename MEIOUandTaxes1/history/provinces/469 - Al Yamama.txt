# 469 - Ar Riyad

owner = BTA
controller = BTA
culture = najdi
religion = sunni
capital = "Hajr Al Yamama"
trade_goods = palm_date
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = turkishtech

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = BTA
}
1744.1.1 = {
	religion = wahhabi
	owner = NAJ
	controller = NAJ
	add_core = NAJ
}
1818.9.9 = {
	owner = TUR
	controller = TUR
} # The end of the Saudi State
