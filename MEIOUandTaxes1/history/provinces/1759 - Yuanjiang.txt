# 1759 - yunnan_area Simao

owner = DLI
controller = DLI
add_core = DLI
culture = baizu
religion = mahayana
capital = "Fenghua"
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = tea

discovered_by = chinese
discovered_by = steppestech

1200.1.1 = { paved_road_network = yes }
1250.1.1 = { temple = yes }
1253.1.1 = { owner = YUA controller = YUA }
1274.1.1 = { add_core = YUA } 			#creation of yunan province
1350.1.1 = { owner = DLI controller = DLI }
1383.1.1 = { 
	owner = MNG 
	controller = MNG 
	add_core = MNG
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 11

}
1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
