# 5451 Baoning

owner = QIN
controller = XIA
culture = wuhan
religion = confucianism
capital = "Baoning"
trade_goods = carmine
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes




discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	bailiff = yes 
	paved_road_network = yes
	temple = yes 
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1351.1.1  = {
	owner = XIA
	controller = XIA
    add_core = QIN
	add_core = XIA
}		

1369.3.17 = { 
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = QIN
	remove_core = XIA
	remove_core = YUA
}
1520.2.2 = {
	base_tax = 12
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
