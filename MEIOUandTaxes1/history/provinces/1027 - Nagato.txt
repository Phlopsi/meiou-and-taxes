# 1027 - Nagato
# GG/LS - Japanese Civil War

owner = OUC
controller = OUC
culture = chugoku
religion = mahayana #shinbutsu
capital = "Shimonoseki"
trade_goods = rice #chinaware
hre = no
base_tax = 10
base_production = 0
base_manpower = 1
is_city = yes

discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_chinaware
		duration = -1
	}
}
1111.1.1 = {
	workshop = yes
	temple = yes
	local_fortification_1 = yes
}
1356.1.1 = {
	add_core = OUC
}
1501.1.1 = {
	base_tax = 18
}
1542.1.1 = { discovered_by = POR }
1557.1.1 = {
	owner = MRI
	controller = MRI
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
