# Bergenhus
# MEIOU - Gigau

owner = NOR
controller = NOR
add_core = NOR
#add_core = VES
culture = norwegian
religion = catholic
hre = no
base_tax = 3
base_production = 1
trade_goods = fish
base_manpower = 0
is_city = yes
capital = "Bergen"
discovered_by = western
discovered_by = eastern
estate = estate_burghers

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "herring_province_medium"
		duration = -1
	}
}

1088.1.1 = {
	harbour_infrastructure_2 = yes
	marketplace = yes
	constable = yes
}

1100.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "bergenshus_natural_harbour" 
		duration = -1 
		}
}


1250.1.1 = {
	temple = yes
	local_fortification_1 = yes
}
1400.1.1 = {
	fort_14th = yes
}
1500.3.3 = {
	base_tax = 5
	base_production = 1
	base_manpower = 1
}
1515.1.1 = { training_fields = yes }

1522.3.20 = {  naval_arsenal = yes }

1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
}
1526.1.1 = { religion = protestant } #preaching of Hans Tausen
1529.12.17 = { merchant_guild = yes }
1531.11.1 = {
	revolt = { type = nationalist_rebels size = 0 }
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = {  }
	controller = DAN
}
1536.1.1  = { religion = protestant} #Unknown date
1641.1.1  = {
	
}
1814.1.14  = {
	owner = SWE
	revolt = { type = nationalist_rebels size = 2 } controller = REB
	remove_core = DAN
} # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {  } owner = NOR controller = NOR }
