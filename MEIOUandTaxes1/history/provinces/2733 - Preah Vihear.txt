# 2733 - Preah Vihear
# TM-Smiles indochina-mod for meiou

owner = KHM
controller = KHM
culture = khmer
religion = buddhism
capital = "Preah Vihear"  # or Koh Ker

base_tax = 2
base_production = 0
base_manpower = 0
citysize = 6000
trade_goods = gems #rubis



discovered_by = chinese
discovered_by = indian

hre = no

1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = KHM
}
1811.1.1 = { controller = REB } # The Siamese-Cambodian Rebellion
1812.1.1 = { controller = KHM }
1867.1.1 = { 			# agreement with France
	owner = SIA
	controller = SIA
}
1904.1.1 = { 			
	owner = FRA
	controller = FRA
}
