# 3406 - Ucayali

culture = jivaro
religion = pantheism
capital = "Ucayali"
trade_goods = unknown
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
native_size = 5
native_ferocity = 1
native_hostileness = 2
discovered_by = south_american

1501.1.1 = {
	base_tax = 1
	native_size = 5
}
1542.1.1   = {
	discovered_by = SPA
}
1650.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	is_city = yes
	trade_goods = lumber
	culture = castillian
	religion = catholic
}
1750.1.1  = {
	add_core = PEU
	culture = peruvian
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
