# 654 - Visayas

culture = cebuano
religion = polynesian_religion
capital = "Mairete"
trade_goods = unknown
hre = no
base_tax = 4
base_production = 0
base_manpower = 0
native_size = 35
native_ferocity = 2
native_hostileness = 9
discovered_by = chinese
discovered_by = austranesian

1000.1.1   = {
	set_province_flag = cebuano_natives
}
1501.1.1 = {
	base_tax = 6
	native_size = 50
}
1521.1.1  = { discovered_by = SPA } # Ferdinand Magellan 
1565.4.27 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	capital = "San Miguel"
	religion = catholic
	citysize = 350
	trade_goods = clove
	set_province_flag = trade_good_set
}
1600.1.1 = { citysize = 648 }
1650.1.1 = { citysize = 1400 }
1700.1.1 = { citysize = 2555 }
1750.1.1 = { citysize = 4850 }
1800.1.1 = { citysize = 9600 }
