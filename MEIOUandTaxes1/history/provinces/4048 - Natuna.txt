# 4048 - Natuna

owner = MLC
controller = MLC
culture = malayan
religion = vajrayana
capital = "Ranai"
trade_goods = fish
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
citysize = 2234
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = {
	add_core = MLC
	add_core = SIJ
	add_core = PLB
}
1509.1.1 = { discovered_by = POR } # Diego Lopez de Sequiera
1511.9.10 = {
	owner = JOH
	controller = JOH
	add_core = JOH
} # Malacca falls to the Portuguese
1550.1.1 = { religion = sunni }
