# 1409 - Cantabria

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = castillian
religion = catholic
hre = no
base_tax = 8
base_production = 0
trade_goods = lumber # naval_supplies
base_manpower = 1
is_city = yes
capital = "Santander"

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = {
	temple = yes
	harbour_infrastructure_1 = yes
	set_province_flag = freeholders_control_province
}


1300.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "cantabria_natural_harbour" 
		duration = -1 
	}
	local_fortification_1 = yes
}

1356.1.1  = { 
	add_core = ENR
}
1369.3.23  = { 
	remove_core = ENR
}
1475.6.2   = { controller = POR }
1476.3.2   = { controller = CAS }
1500.3.3   = {
	base_tax = 9
	base_production = 0
	base_manpower = 1
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	bailiff = yes
	military_harbour_1 = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1522.3.20 = {  naval_arsenal = yes }
1713.4.11  = { remove_core = CAS }
 #Audiencia de Asturias
