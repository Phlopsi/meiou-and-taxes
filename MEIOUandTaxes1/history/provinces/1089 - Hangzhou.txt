# 1089 - Hangzhou

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Qiantang"
trade_goods = lumber #naval_supplies # bamboo
hre = no
base_tax = 24
base_production = 43
base_manpower = 4
is_city = yes
fort_14th = yes


discovered_by = chinese
discovered_by = steppestech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
}

1000.1.1   = {
	set_province_flag = has_estuary
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = "hangzhou_natural_harbour"
		duration = -1
	}
	constable = yes
}
1100.1.1 = { 
	temple = yes
	paved_road_network = yes
}
1200.1.1 = {
	merchant_guild = yes
	corporation_guild = yes
	harbour_infrastructure_2 = yes
	urban_infrastructure_3 = yes
	bureaucracy_2 = yes
	art_corporation = yes # Hangzhou School of Painting
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1351.1.1  = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	add_core = MNG
}
1366.1.1  = {
	owner = MNG
	controller = MNG
	remove_core = ZOU
}
1520.2.2 = {
	base_tax = 80
	base_production = 26
	base_manpower = 6
}
1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1645.5.27 = { unrest = 0 } # The rebellion is defeated
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1644.1.1 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
1745.1.1  = {  }
