# No previous file for Zlatibor

owner = SER
controller = SER
culture = serbian
religion = orthodox
capital = "Uzice"
trade_goods = silver
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = SER
	add_local_autonomy = 15
#	add_core = HUN
}
1515.2.1 = { training_fields = yes }
1520.5.5 = {
	base_tax = 3
	base_production = 0
	base_manpower = 0
}
1521.8.28 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_local_autonomy = -15
}
1530.1.4  = {
	bailiff = yes	
}
1595.1.1  = { fort_14th = yes } # Estimated

1688.11.7 = { unrest = 7 } # Serb rebellion under D. Brankovic

1717.8.22 = {
	owner = HAB
	controller = HAB
	
} # Austrian occupation
1739.9.18 = {
	owner = TUR
	controller = TUR
} # Treaty of Beograd, the city is given back
1750.1.1  = {  }
1788.3.1  = { controller = HAB } # Occupied by Austrian forces
1791.1.1  = { controller = TUR }
1806.1.8  = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} # The first Serbian uprising
1813.1.1  = {
	revolt = { }
	controller = TUR
}
1815.4.23 = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} # The second Serbian uprising
1817.1.1  = {
	revolt = { }
	owner = SER
	controller = SER
} # Serbia released as a vassal
