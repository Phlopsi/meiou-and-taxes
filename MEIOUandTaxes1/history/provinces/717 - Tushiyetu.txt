#717 - Lop

owner = MGH
controller = MGH
culture = chaghatai
religion = mahayana
capital = "Lop"
trade_goods = wheat
hre = no
base_tax = 3
base_production = 1
base_manpower = 0
citysize = 13487
discovered_by = chinese
discovered_by = steppestech

1111.1.1 = {
	marketplace = yes
	town_hall = yes
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MGH
	add_core = KAS
}
1359.1.1 = {
	religion = sunni
}
1487.1.1 = {
	owner = UIG
	controller = UIG
	culture=uyghur
	add_core = UIG
	remove_core = MGH
	remove_core = KAS
}	
1515.1.1 = { training_fields = yes }
1529.1.1 = { discovered_by = muslim }
1530.1.1 = {
	owner = MGH
	controller = MGH
	add_core = MGH
	remove_core = UIG
}
1678.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
}
1755.1.1 = {
	owner = KAS
	controller = KAS
	remove_core = ZUN
}
1759.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Part of the Manchu empire
