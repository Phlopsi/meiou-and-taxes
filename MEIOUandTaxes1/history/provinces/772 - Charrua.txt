# 772 - Charrua

culture = charruan
religion = pantheism
capital = "Charrua"
trade_goods = unknown
hre = no
base_tax = 4
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 1
native_hostileness = 5	

1500.1.1 = {
	base_tax = 1
}
1516.1.23  = {
	discovered_by = SPA
	#add_core = SPA
}
#1634.1.1   = {
#	owner = SPA
#	controller = SPA
#	citysize = 580
#	culture = castillian
#	religion = catholic	    
#	capital = "Villa Soriano"
#	trade_goods = livestock
#}
1676.1.1   = {
	discovered_by = POR
	owner = POR
	controller = POR
	#capital = "Montevideo"
	citysize = 580
	culture = portugese
	religion = catholic
	trade_goods = livestock
	set_province_flag = trade_good_set
}
1700.1.1   = {
	citysize = 2000
}
1735.1.1   = {
	owner = SPA
	controller = SPA
}
1750.1.1   = {
	citysize = 5000
	add_core = URU
	culture = platean
}
1800.1.1   = {
	citysize = 10000
}
1811.5.18  = {
	owner = URU
	controller = URU
	add_core = BRZ
	add_core = LAP
}
1822.7.9   = {
	owner = BRZ
	controller = BRZ
	remove_core = SPA
}
1825.8.25  = {
	owner = URU
	controller = URU
} # Beginning of the Argentinian-Brazilian War
1828.1.1   = {
	remove_core = BRZ
	remove_core = LAP
} # End of the Argentinian-Brazilian War
