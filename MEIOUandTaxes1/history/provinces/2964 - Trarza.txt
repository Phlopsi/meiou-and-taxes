# No previous file for Trarza

culture = senegambian
religion = west_african_pagan_reformed
capital = "Awlil"
trade_goods = unknown
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
native_size = 70
native_ferocity = 4.5
native_hostileness = 9

discovered_by = TBK
discovered_by = TUA
discovered_by = sub_saharan

1437.1.1   = { discovered_by = POR } #Cadamosto
1490.1.1   = {
	owner = FLO
	controller = FLO
	add_core = FLO
	is_city = yes
	trade_goods = slaves
}
