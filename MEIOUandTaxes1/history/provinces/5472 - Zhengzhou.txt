# 5472 Xinxiang

owner = SNG
controller = SNG
culture = hanyu
religion = confucianism
capital = "Xinxiang" 
trade_goods = wheat
hre = no
base_tax = 23
base_production = 4
base_manpower = 2
is_city = yes

discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	constable = yes 
	paved_road_network = yes
	temple = yes 
}
1200.1.1 = {
	urban_infrastructure_1 = yes
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}

1351.1.1  = {
	owner = SNG
	controller = SNG
    add_core = SNG
	add_core = CYU
}		

1369.3.17 = { 
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = SNG
	remove_core = YUA
	small_university = yes # Songyang Academy (One of the 4 Great Academies )
}
1520.2.2 = {
	base_tax = 38
	base_production = 5
	base_manpower = 3
}
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty

1662.1.1 = { remove_core = MNG }
