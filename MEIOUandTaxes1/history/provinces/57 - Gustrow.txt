# 57 - G�strow

owner = WRL
controller = WRL
culture = pommeranian
religion = catholic
hre = yes
base_tax = 8
base_production = 0
trade_goods = wheat#linen
base_manpower = 0
is_city = yes
capital = "G�strow"
discovered_by = eastern
discovered_by = western
discovered_by = muslim
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
	local_fortification_1 = yes
}
1356.1.1   = {
	add_core = WRL
}
1436.9.8 = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 9
}
1525.1.1   = {
	fort_14th = yes
}
1530.1.1   = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1650.1.1   = {
	fort_14th = no
	fort_15th = yes
}
1695.1.1   = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1750.1.1   = {
	fort_15th = no
	fort_16th = yes
} #add customs house to reflect rapid economic growth
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
