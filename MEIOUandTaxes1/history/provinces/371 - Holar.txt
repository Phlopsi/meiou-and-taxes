# East Iceland
# MEIOU - Gigau

owner = ICE
controller = ICE
add_core = ICE
culture = norse
religion = catholic
hre = no
base_tax = 2
base_production = 0
trade_goods = fish
base_manpower = 0
is_city = yes
capital = "Akureyri"
discovered_by = western

1262.1.1   = {
	owner = NOR
	controller = NOR
	add_core = NOR 
	remove_core = ICE
	set_province_flag = freeholders_control_province
}
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = NOR
}
1530.1.4  = {
	bailiff = yes	
}
1540.1.1 = { religion = protestant }
1752.1.1 = { trade_goods = wool } # Wool becomes more important.
1814.5.17 = { owner = DEN controller = DEN add_core = DEN remove_core = DAN }
