# Uusimaa
# MEIOU - Gigau

owner = SWE
controller = SWE
culture = finnish
religion = catholic
hre = no
base_tax = 2
base_production = 0
trade_goods = fish
base_manpower = 0
is_city = yes
capital = "Porvoo"
discovered_by = western
discovered_by = eastern


1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = { 
		name = "nyland_natural_harbour" 
		duration = -1 
		}
}

1356.1.1   = {
	owner = RSW
	controller = RSW
	add_core = RSW
	add_core = SWE
}
1360.1.1   = {
	owner = SWE
	controller = SWE
	remove_core = RSW
}
1485.1.1 = { temple = yes }
1522.2.15 = {  shipyard = yes }
1527.6.1  = { religion = protestant }
1550.1.1  = { trade_goods = wheat } #Estimated Date
1598.8.1  = { controller = PLC } #Sigismund tries to reconquer his crown
1599.8.4  = { controller = SWE } #Duke Karl get it back
1710.6.10  = { controller = RUS } #The Great Nordic War-Captured Viborg
1721.8.30  = { owner = RUS add_core = RUS remove_core = SWE } #The Peace of Nystad
1808.3.1   = { controller = RUS } # Overran by Russian troops
1809.3.29  = {
	owner = RUS
	add_core = RUS
	remove_core = SWE
} # Treaty of Fredrikshamn
1809.9.17 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
}
1812.1.1  = {
	capital = "Helsinki"
}
