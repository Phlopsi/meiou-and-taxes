# 2187 - Westfalen
# GG - 22/07/2008

owner = KOL
controller = KOL
culture = westphalian
religion = catholic
capital = "Arnsberg"
trade_goods = livestock
hre = yes
base_tax = 10
base_production = 0
base_manpower = 0
is_city = yes
add_core = KOL
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1111.1.1 = { 
	local_fortification_1 = yes
}

1500.1.1 = { road_network = yes }
1520.5.5 = {
	base_tax = 11
	base_production = 0
	base_manpower = 1
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1802.1.1 = {
	owner = HES
	controller = HES
}
1803.4.27 = {
	add_core = HES
} #Reichsdeputationshauptschluss
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES
}#Congress of Vienna
