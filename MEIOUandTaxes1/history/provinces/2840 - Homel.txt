# 2840 - Homel

owner = SMO
controller = SMO     
culture = ruthenian
religion = orthodox 
hre = no
base_tax = 5
base_production = 0
trade_goods = livestock
base_manpower = 0
is_city = yes
capital = "Homel"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim


1356.1.1   = {
	
	add_core = SMO
	add_permanent_claim = LIT
	add_core = PLT
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
	local_fortification_1 = yes
}
1404.1.1   = {
	owner = LIT
	controller = LIT
	add_core = LIT
}
1501.1.1 = {
	base_tax = 6
}
1530.1.4  = {
	bailiff = yes	
}
1567.1.1  = { fort_14th = yes }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1772.8.5  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = LIT
	remove_core = PLC
	culture = byelorussian
} # First partition of Poland
1779.1.1  = {  } # Almost entirely rebuilt.
 # Became a large trade center.
