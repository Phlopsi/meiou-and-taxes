# 512 - Marwar

owner = MAW
controller = MAW
culture = marwari
religion = hinduism
capital = "Mandore"
trade_goods = wool
hre = no
base_tax = 13
base_production = 2
base_manpower = 2
citysize = 22000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1100.1.1 = {
	marketplace = yes
	town_hall = yes
	workshop = yes
}
1115.1.1 = { constable = yes }
#1120.1.1 = { textile = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = MAW
	fort_14th = yes
	local_fortification_1 = yes
}

1438.1.1 = {
	owner = MEW
	controller = MEW
	unrest = 5
} #Conquered by Mewar after Rathore, Sisodiya cooperation breaks down in plots and murder.
1444.1.1 = {
	revolt = { size = 1 type = nationalist_rebels leader = "Jodha Rathore" }
} #During the period between 1438 and 1459 Jodha made several attempts to retake his ancestral lands.
1459.1.1 = {
	capital = "Jodphur"
	fort_14th = no
	fort_15th = yes
	controller = MAW
	owner = MAW
	unrest = 0
	revolt = { }
} #Rathores restored.
1511.1.1 = {
	base_tax = 15
	base_production = 4
}
1515.12.17 = { training_fields = yes }
1530.1.1 = { 
	add_permanent_claim = MUG
}
1530.3.17 = {
	road_network = yes
}
1650.1.1  = {  }
1670.1.1 = { fort_15th = no fort_17th = yes }
1679.8.1 = { controller = MUG } # The city is pillaged
1681.1.1 = {
	owner = MUG
	add_core = MUG
	revolt_risk = 5
}
1690.1.1  = { discovered_by = ENG }
1707.3.15 = { controller = MEW } # Jodphur is recaptured & the Mughals are driven out
1707.4.1 = {
	controller = MAW
	owner = MAW
	revolt_risk = 0
} # Marwar independent
1707.5.12 = { discovered_by = GBR }
