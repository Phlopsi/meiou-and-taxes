# No previous file for Farah

owner = MIH
controller = MIH
culture = persian
religion = sunni
capital = "Farah"
trade_goods = livestock
hre = no
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = indian
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = MIH
	add_core = KTD
	add_core = TIM
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
1384.1.1   = {
	owner = TIM
	controller = TIM
}
1444.1.1  = {
	owner = KTD
	controller = KTD
	remove_core = TIM
	remove_core = KAB
	add_core = DUR
} # Shaybanids break free from the Timurids
1501.1.1 = {
	base_tax = 9
}
1507.1.1  = {
	controller = SHY
}
1507.7.1  = {
	owner = SHY
}
1510.1.1  = {
	controller = SAM
}
1511.1.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	#religion = shiite
	bailiff = yes
	courthouse = yes
} # Safawids "form persia"
1515.1.1 = { training_fields = yes }
1530.1.1 = { add_claim = BUK }
1677.1.1 = { discovered_by = FRA }
1690.1.1  = { discovered_by = ENG }
1704.1.1  = { unrest = 5 } # Afghanian revolts
1707.5.12 = { discovered_by = GBR }
1708.1.1  = { unrest = 8 } # Mir Wais rebelled against Persian rule
1709.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels size = 3 }
} # Widespread tribal uprisings
1711.1.1  = {
	owner = KAB
	controller = KAB
	revolt = { }
}
1738.1.1  = {
	owner = PER
	controller = PER
} # Nader Shah absorbs Afghanistan
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
