# 219 - Ciudad Real + Daimiel + Almagro + Valdepe�as + Almodovar del Campo

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = castillian # culture = new_castillian
religion = catholic
hre = no
base_tax = 9
base_production = 1
trade_goods = gold 
base_manpower = 1
is_city = yes
capital = "Ciudad Real"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
# estate order militaro

1000.1.1 = {
	town_hall = yes
	workshop = yes
	local_fortification_1 = yes
}
1200.1.1 = { road_network = yes }
1356.1.1   = {
	set_province_flag = spanish_name
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_toledo"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
}
1464.5.1  = { unrest = 3 } #Nobiliary uprising against King Enrique, Castilla goes into anarchy
1468.9.18 = { unrest = 0 } #Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved
1470.1.1  = { unrest = 3 } #Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.
1479.9.4  = { unrest = 0 } #Peace of Alca�ovas, between Queen Isabel and King Alfonso of Portugal who had entered the war supporting her wife Juana

1500.3.3   = {
	base_tax = 9
	base_production = 3
	base_manpower = 1
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1520.5.1  = { unrest = 5 } # War of the Comunidades
1522.2.3  = { unrest = 0 } # After the defeat of the comuneros at Villalar, Toledo was still able to resist for 9 months.

1713.4.11 = { remove_core = CAS }
