# 3740 - Schonburg

owner = SBG
controller = SBG
culture = high_saxon
religion = catholic
capital = "Sch�nburg"
trade_goods = wheat
hre = yes
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
add_core = SBG
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = {
	local_fortification_1 = yes
}
1500.1.1 = { road_network = yes }
1530.1.4  = {
	bailiff = yes	
}
1540.1.1   = {
	religion = protestant
}
1740.1.1   = {
	owner = SAX
	controller = SAX
	add_core = SAX
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
