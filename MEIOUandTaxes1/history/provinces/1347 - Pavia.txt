# 1347 - Pavia

owner = MLO
controller = MLO
culture = lombard
religion = catholic
capital = "Pavia"
base_tax = 17
base_production = 4
base_manpower = 1
is_city = yes
trade_goods = rice

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1111.1.1 = {
	urban_infrastructure_2 = yes
	workshop = yes 
	marketplace = yes
	local_fortification_1 = yes
}

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = MLO
#	add_core = PAV
	add_core = MFT owner = MFT controller = MFT
}
1361.1.1 = { small_university = yes }

1453.1.1   = { add_core = FRA }
1499.8.10  = { controller = FRA } # Louis XII of France invades...
1504.1.31  = { owner = FRA } # ...and seizes Milan
1512.1.1   = { controller = MLO owner = MLO }  # Massimiliano Sforza restored by the Swiss
1515.1.1   = { controller = FRA } # Francis I of France invades...
1516.8.13  = { owner = FRA } # ...and seizes Milan, Treaty of Noyon
1520.5.5 = {
	base_tax = 20
	base_production = 6
	base_manpower = 2
}
1525.2.24  = { controller = SPA }              # The Spanish invades...
1526.1.14  = { controller = MLO owner = MLO }  # ...and restores the Sforzas
1526.5.22  = { controller = HAB } # Milan joined the League of Cognac, and is invaded by the Emperor
1529.8.3  = {
	controller = MLO
	remove_core = FRA
	bailiff = yes
} # Treaty of Cambrai restores the Sforzas and includes renounciation of French claims

1530.2.27 = {
	hre = no
}
1535.1.1  = {
	owner = SPA
	controller = SPA
   	add_core = SPA
}  # Annexed to Spain after the last Sforza died

1618.1.1  =  { hre = no }
1700.1.1  = {  }
1714.9.7  = {
	owner = HAB
 	controller = HAB
 	add_core = HAB
 	remove_core = SPA
}  # Treaty of Baden
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
	remove_core = HAB
} # Transpadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1814.4.20 =  {
	controller = REB
} # The Milan insurrection
1814.4.26 =  { controller = HAB } # The Milan insurrection ends
1860.3.20 = {    #??
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
