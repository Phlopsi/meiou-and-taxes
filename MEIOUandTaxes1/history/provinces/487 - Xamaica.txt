# 487 - Xamaica

culture = arawak
religion = pantheism
capital = "Xamaica"
trade_goods = unknown
hre = no
base_tax = 4
base_production = 0
#base_manpower = 0.5
base_manpower = 0
native_size = 25 
native_ferocity = 2
native_hostileness = 6

1000.1.1   = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = { 
		name = "jamaica_large_natural_harbor" 
		duration = -1 
		}
}
1494.1.1  = {
	discovered_by = CAS
	owner = CAS
	controller = CAS
	culture = castillian
	religion = catholic
	citysize = 350
	capital = "Seville"
	trade_goods = sugar
	set_province_flag = trade_good_set
} # Christopher Columbus
1500.1.1  = {
	citysize = 1670
} 
1516.1.23 = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
}
1519.1.1  = {
	add_core = SPA 
	citysize = 2670
}
1525.1.1 = {
	base_tax = 1
}
1534.1.1 = {
	capital = "Villa de la Vega"
}
1550.1.1  = {
	citysize = 10200
} 
1600.1.1  = {
	citysize = 12000
}
1650.1.1  = {
	citysize = 7300
} # Earhtquakes
1655.1.1  = {
	owner = ENG
	controller = ENG
	culture = english
	religion = protestant #anglican
	capital = "Port Royal"
	remove_core = SPA
} # General Venables seized the island
1680.1.1  = {
	add_core = ENG
}
1685.1.1  = {
	
} # During the British rule, Jamaica became the worlds largest sugar exporting nation
1693.1.1  = {
	capital = "Kingston" }
1700.1.1  = {
	citysize = 12610
} # African slave labor
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1  = {
	citysize = 19550
}
1760.1.1  = {
	unrest = 4
} # Slave rebellion
1761.1.1  = {
	unrest = 0
}
1795.1.1  = {
	unrest = 4
} # Slave rebellion
1796.1.1  = {
	unrest = 0
}
1800.1.1  = {
	citysize = 23000
}
