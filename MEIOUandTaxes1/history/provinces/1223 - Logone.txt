# 1223 - Mandjia

base_tax = 13
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 2
native_hostileness = 2
culture = kanouri
religion = animism
capital = "Bossangoa"
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1505.1.1 = {
	owner = KBO
	controller = KBO
	add_core = KBO
	is_city = yes
	base_tax = 13
}
