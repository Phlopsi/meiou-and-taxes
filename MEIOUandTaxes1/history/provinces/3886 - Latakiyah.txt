# No previous file for Latakiyah

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "Latakiyah"
trade_goods = cotton
hre = no
base_tax = 10
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1249.1.1 = {
	local_fortification_1 = yes
}
1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MAM
	add_core = SYR
	harbour_infrastructure_1 = yes
	town_hall = yes
}
1500.3.3   = {
	base_tax = 12
}
1516.1.1   = { add_core = TUR }
1516.8.28  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = SYR
	controller = SYR
	remove_core = TUR
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1750.1.1 = {  }
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
