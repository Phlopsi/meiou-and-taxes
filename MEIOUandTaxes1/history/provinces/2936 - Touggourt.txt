# 2936 - Touggourt

owner = TOG
controller = TOG
culture = berber
religion = sunni
capital = "Touggourt"
trade_goods = pepper
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = muslim
discovered_by = soudantech
discovered_by = turkishtech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = TOG
}
1530.1.1 = {
	add_permanent_claim = ALG
}
1671.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
} # Virtually independent of the Ottoman empire
