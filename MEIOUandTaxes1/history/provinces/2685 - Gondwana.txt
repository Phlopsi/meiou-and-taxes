# 2685 - Middle Gondwana

owner = GHR
controller = GHR
culture = gondi
religion = hinduism
capital = "Kherla"
trade_goods = livestock
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = steppestech
discovered_by = turkishtech

1120.1.1 = { farm_estate = yes }
1249.1.1 = {
	local_fortification_1 = yes
}
1356.1.1 = {
	#add_core = BRR
	add_core = GHR
	add_core = DGA
	#fort_14th = yes
}
#1410.1.1 = {
#	controller = BAH
#} # Invaded by Bahmanis
#1411.1.1 = {
#	owner = BAH
#} # Invaded by Bahmanis
#1417.1.1 = {
#	controller = MLW
#} # Invaded by Malwa
#1418.1.1 = {
#	owner = MLW
#} # Invaded by Malwa
1450.1.1 = { citysize = 11000 }
#1467.1.1 = {
#	owner = BAH
#	controller = BAH
#} # Invaded by Bahmanis
#1468.1.1 = {
#	owner = MLW
#	controller = MLW
#} # Invaded by Malwa
1498.1.1 = { discovered_by = POR }
1511.1.1 = {
	base_tax = 9
	base_production = 1
	town_hall = yes
}
1530.1.1 = { 
	add_permanent_claim = MUG
}
1564.5.1 = { controller = MUG }
1564.7.1 = { controller = DGA }
1580.1.1 = {
	owner = DGA
	controller = DGA
	capital = Deogarh
} # Independence and new dominant state
1724.1.1 = {
	owner = BHO
	controller = BHO
	add_core = BHO
}	#Forms kingdom of Nagpur
#1743.1.1 = {	   } # Marathas take control over Nagpur kingdom
#1743 Take over by the Maratha Bhonsle dynasty
1818.6.3 = {
	owner = GBR
	controller = GBR
}
