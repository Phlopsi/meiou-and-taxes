name = "Red Turban Rebellion"
war_goal = {
	type = unify_china
	casus_belli = cb_chinese_civil_war
	province = 2125 #zhenjiang
}

1356.1.1   = {
	add_attacker = ZOU
	add_defender = MNG
}
1375.1.1   = {
	rem_attacker = ZOU
	rem_defender = MNG
}