name = "Orsini-Uros War"
war_goal = {
	type = take_claim
	casus_belli = cb_conquest
	province = 149 # Thessaly
}

1356.1.1   = {
	add_attacker = EPI
	add_defender = TSL
	add_defender = ALB
}
1392.1.1   = {
	rem_attacker = EPI
	rem_defender = TSL
	rem_defender = ALB
}