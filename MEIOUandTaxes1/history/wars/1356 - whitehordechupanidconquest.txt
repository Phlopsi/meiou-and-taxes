name = "White Horde War to Usurp the Chupanid Throne"
war_goal = {
	type = take_capital_throne
	casus_belli = cb_restore_personal_union
	tag = CHU
}
1356.7.1 = {
	add_attacker = WHI
	add_attacker = SHI
	add_defender = CHU
}
1357.8.1 = {
	rem_attacker = WHI
	rem_attacker = SHI
	rem_defender = CHU
}