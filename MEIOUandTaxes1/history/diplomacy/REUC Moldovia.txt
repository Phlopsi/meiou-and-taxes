# Ottoman vassal
vassal = {
	first = TUR
	second = MOL
	start_date = 1512.1.1
	end_date = 1600.1.1
}

# Anti-Ottoman rebellion
alliance = {
	first = RUS
	second = MOL
	start_date = 1710.1.1
	end_date = 1711.12.30
}

# Ottoman rule tightened
vassal = {
	first = TUR
	second = MOL
	start_date = 1712.1.1
	end_date = 1837.1.1
}
