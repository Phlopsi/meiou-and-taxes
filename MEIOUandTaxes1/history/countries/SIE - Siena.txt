#SIE - Siena.txt
#2011-jan-15 FB Ghibelline Alliances

government = oligarchic_republic government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = tuscan
religion = catholic
capital = 117	# Siena
historical_friend = VEN
historical_friend = PIS

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1287.1.1 = {
	monarch = {
		name = "I Noveschi"
		DIP = 4
		MIL = 3
		ADM = 6
	}
} #The Noveschi or the IX were a mercantile-banking oligarchy that ruled the Italian city-state of Siena from 1287 to 1355 A.D. They oversaw the period of Siena's greatest stability and prosperity in the Medieval era

#1548/49 city devastated by black death

1355.1.1 = {
	monarch = {
		name = "I Dodici"
		DIP = 3
		MIL = 3
		ADM = 5
	}
}

1386.1.1 = {
	monarch = {
		name = "I Dieci"
		DIP = 3
		MIL = 3
		ADM = 4
	}
}

1388.1.1 = {
	monarch = {
		name = "I Undici"
		DIP = 3
		MIL = 3
		ADM = 4
	}
}

1399.1.1 = {
	monarch = {
		name = "Giovanni Galeazzo"
		DIP = 4
		MIL = 3
		ADM = 3
	}
}

1402.1.1 = {
	monarch = {
		name = "Giovanni Maria"
		DIP = 2
		MIL = 3
		ADM = 4
	}
}

1404.1.1 = {

    government = siena_republic

	monarch = {
		name = "dei Nobili"	
		adm = 2
		dip = 1
		mil = 3
	}
}

1444.1.1 = {

	monarch = {
		name = "del Popolo"
		adm = 3
		dip = 2
		mil = 1
	}
}

1487.7.22 = {

    government = signoria_monarchy

	monarch = {
		name = "Pandolfo"
		dynasty = "Petrucci"
		dip = 4
		mil = 4
		adm = 2
	}
}
		
1502.2.2 = {
	monarch = {
		name = "Cesare"
		dynasty = "Borgia"
		dip = 2
		mil = 6
		adm = 4
	}
}

1503.3.29 = {
	monarch = {
		name = "Pandolfo"
		dynasty = "Petrucci"
		dip = 4
		mil = 4
		adm = 2
	}
}

1512.5.21 = {
	monarch = {
		name = "Borghese"
		dynasty = "Petrucci"
		dip = 0
		mil = 0
		adm = 2
	}
}

1515.3.6 = {
	monarch = {
		name = "Raffaele"
		dynasty = "Petrucci"
		dip = 1
		mil = 1
		adm = 1
	}
}

1522.10.26 = {
	monarch = {
		name = "Francesco"
		dynasty = "Petrucci"
		dip = 1
		mil = 1
		adm = 1
	}
}

1523.6.11 = {
	monarch = {
		name = "Fabio"
		dynasty = "Petrucci"
		dip = 1
		mil = 1
		adm = 1
	}
}

# To the Habsburgs (Spain), (18th feb 1525)
