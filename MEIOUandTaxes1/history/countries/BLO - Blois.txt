# BLO - Blois
# MEIOU-GG - Hundred Year War
# 2009-dec-19 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = breton
religion = catholic
technology_group = western
capital = 1388	# Nantes

historical_friend = FRA

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}
1341.4.30 = {
#	set_country_flag = brittany_war_succession
	monarch = {
		name = "Charles"
		dynasty = "de Blois"
		ADM = 3
		DIP = 3
		MIL = 5
	}
	heir = {
		name = "Jeanne"
		monarch_name = "Jeanne de Penthi�vre"
		dynasty = "de Blois"
		birth_Date = 1340.1.1
		death_date = 1384.9.10
		claim = 95
		ADM = 5
		DIP = 3
		MIL = 3
	}
	define_ruler_to_general = {
		fire = 2
		shock = 2
		manuever = 1
		siege = 1
	}
}

1364.9.29  = {
	monarch = {
		name = "Jeanne de Penthi�vre"
		dynasty = "de Blois"
		ADM = 5
		DIP = 3
		MIL = 3
	}
}

1365.1.1   = {
	capital = 357
}

#1365.4.12  = {
#	clr_country_flag = brittany_war_succession
#}

1384.9.10  = {
	monarch = {
		name = "Jean Ier de Ch�tillon"
		dynasty = "de Ch�tillon"
		ADM = 3
		DIP = 3
		MIL = 5
	}
}

1589.8.3   = {
	government = administrative_monarchy remove_country_modifier = title_3 add_country_modifier = { name = title_3 duration = -1 }
}

1661.3.9   = {
	government = absolute_monarchy remove_country_modifier = title_3 add_country_modifier = { name = title_3 duration = -1 }
}
