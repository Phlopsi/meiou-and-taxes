# HEN - Henneberg

government = feudal_monarchy
government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = high_saxon
religion = catholic
capital = 3735

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1245.1.1 = {
	monarch = {
		name = "Herman"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1262.1.1 = {
	monarch = {
		name = "Berthold III"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1284.1.1 = {
	monarch = {
		name = "Berthold IV"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1340.1.1 = {
	monarch = {
		name = "Johann"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1347.1.1 = {
	monarch = {
		name = "Heinrich V"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1405.1.1 = {
	monarch = {
		name = "Wilhelm II"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1444.1.8 = {
	monarch = {
		name = "Wilhelm III"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1495.5.25 = {
	monarch = {
		name = "Wilhelm IV"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1511.5.27 = {
	heir = {
		name = "Georg Ernst"
		monarch_name = "Georg Ernst I"
		dynasty = "von Henneberg"
		birth_date = 1511.5.27
		death_date = 1836.1.1
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
1530.1.2 = {
	government = administrative_monarchy
}

1540.1.1 = {
	religion = protestant
}

1559.1.24 = {
	monarch = {
		name = "Georg Ernst I"
		dynasty = "von Henneberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}



# 1660 - merged into Hesse-Kassel
