# GOT - Gotland

government = merchant_republic government_rank = 1
mercantilism = 10
religion = catholic
primary_culture = gutnish
technology_group = western
capital = 25	#Gotland

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}
