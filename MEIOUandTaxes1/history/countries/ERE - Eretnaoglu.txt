# ERE - Eretnaoglu

government = ottoman_government government_rank = 1
mercantilism = 0.0
primary_culture = mongol
religion = sunni
technology_group = muslim
#unit_type = muslim #dharper
capital = 1333 # Kayseri
historical_rival = OTT
historical_rival = TUR
historical_rival = MAM
historical_rival = JAI
historical_rival = KAR
historical_rival = SHI
add_accepted_culture = yorouk

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 50
}
1327.1.1 = {
	# add_country_modifier = { name = turkish_beylik duration = -1 }
	monarch = {
		name = "Eretna"
		dynasty = "Eretnid"
		ADM = 4
		DIP = 4
		MIL = 6
	}
}

1352.1.1 = {
	monarch = {
		name = "Mehmed"
		dynasty = "Eretnid"
		ADM = 4
		DIP = 4
		MIL = 4
	}
	add_country_modifier = { name = obstacle_traditional_military duration = -1 }
	add_country_modifier = { name = obstacle_succession duration = -1 }
	add_country_modifier = { name = obstacle_shifting_loyalties duration = -1 }
}

1365.1.1 = {
	monarch = {
		name = "Mehmed II"
		dynasty = "Eretnid"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1380.1.1 = {
	monarch = {
		name = "Burhaneddin"
		dynasty = "Eretnid"
		ADM = 4
		DIP = 3
		MIL = 1
	}
}

# 1381.1.1 - Fall to the Ottomans
