# ERF - Erfurt

government = imperial_city government_rank = 1
mercantilism = 0.0
primary_culture = thuringian
religion = catholic
technology_group = western
capital = 3741
fixed_capital = 3741

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}


1349.1.1 = {
	monarch = {
		name = "Balthasar"
		DIP = 4
		ADM = 1
		MIL = 3
	}
}
