# PIL - Pilten

government = theocratic_government government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = baltendeutsche
religion = catholic
capital = 1432

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 1 }
	add_absolutism = -100
	add_absolutism = 40
}

1425.1.1 = {
	monarch = {
		name = "Johann Tiergart"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1457.1.1 = {
	monarch = {
		name = "Paul II. Einwald"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1473.1.1 = {
	monarch = {
		name = "Martin Lewitz"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1500.1.1 = {
	monarch = {
		name = "Michael Sculteti"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1501.1.1 = {
	monarch = {
		name = "Heinrich II. Basedow"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1524.1.1 = {
	monarch = {
		name = "Hermann II. Ronneberg"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1540.1.1 = {
	monarch = {
		name = "Johann IV. von Münchhausen"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1560.1.1 = {
	monarch = {
		name = "Magnus Herzog von Holstein"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1617.1.1 = {
	monarch = {
		name = "Zygmunt III Wasa"
		ADM = 2
		DIP = 3
		MIL = 3
	}
}

1632.4.30 = {
	monarch = {
		name = "Wladyslaw IV Wasa"
		ADM = 5
		DIP = 6
		MIL = 5
	}
}

1648.5.20 = {
	monarch = {
		name = "Jan II Kazimierz"
		ADM = 1
		DIP = 4
		MIL = 2
	}
}

1656.1.1 = {
	monarch = {
		name = "Jakob"
		ADM = 4
		DIP = 3
		MIL = 6
	}
}

1681.12.30 = {
	monarch = {
		name = "Friedrich Kasimir"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1698.1.23 = {
	monarch = {
		name = "Friedrich Wilhelm"
		ADM = 2
		DIP = 5
		MIL = 4
	}
}

1711.1.3 = {
	monarch = {
		name = "Anna Ivanova"
		ADM = 3
		DIP = 3
		MIL = 3
		female = yes
	}
}

1717.1.1 = {
	monarch = {
		name = "Stanislaw I Leszczynski"
		ADM = 6
		DIP = 3
		MIL = 4
	}
}

1733.1.30 = {
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
		mercantilism = 10
	} # The Polish Succession of 1733

1733.2.1 = {
	monarch = {
		name = "August III Sas"
		ADM = 2
		DIP = 4
		MIL = 1
	}
}

1740.1.30 = {
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
} # Friedrich Augustus Wettin backs down!

1764.2.1 = {
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
	mercantilism = 10
	} # Ending the Stagnation

1764.9.6 = {
		#set_variable = { which = "centralization_decentralization" value = 2 }
		add_absolutism = -100
		add_absolutism = 30
	mercantilism = 10
		# innovative_narrowminded = -3
} # Stanislaus Poniatowski-'The Last King

1764.9.8 = {
	monarch = {
		name = "Stanislaw II Poniatowski"
		ADM = 3
		DIP = 5
		MIL = 2
	}
}

# 1795 : annexation by Russia