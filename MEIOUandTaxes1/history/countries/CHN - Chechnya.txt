# CHN - Chechnya

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
# isolationist_expansionist = -5
primary_culture = chechen
religion = animism
technology_group = muslim
capital = 1009
historical_neutral = GAZ
historical_neutral = AST
historical_neutral = PER

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}
1500.1.1 = {
	religion = sunni
}