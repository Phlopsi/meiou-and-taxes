# FER - Ferrara

government = signoria_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = emilian
capital = 1378		# Ferrara

#Ghibelline Alliances:
# historical_friend = PIS
# historical_friend = SIE
# historical_friend = URB

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

# Duces of Ferrara
1247.1.1 = {
	monarch = {
		name = "Obizz� II"
		dynasty = "d'Este"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1293.2.13 = {
	monarch = {
		name = "Azz� VIII"
		dynasty = "d'Este"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1308.1.31 = {
	monarch = {
		name = "Aldobrandin� II"
		dynasty = "d'Este"
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1327.1.1 = {
	monarch = {
		name = "Obizz� III"
		dynasty = "d'Este"
		ADM = 4
		DIP = 3
		MIL = 5
	}
}

1352.3.20 = {
	monarch = {
		name = "Aldobrandin� III"
		dynasty = "d'Este"
		ADM = 3
		DIP = 5
		MIL = 3
	}
	heir = {
		name = "Niccol�"
		monarch_name ="Niccol� II"
		dynasty = "d'Este"
		birth_date = 1338.1.1
		death_date = 1358.3.26
		claim = 95
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1361.1.1 = {
	monarch = {
		name = "Niccol� II"
		dynasty = "d'Este"
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1388.3.26 = {
	monarch = {
		name = "Albert�"
		dynasty = "d'Este"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1393.7.30 = {
	monarch = {
		name = "Niccol� III"
		dynasty = "d'Este"
		ADM = 2
		DIP = 4
		MIL = 3
	}
}

1441.12.26 = {
	monarch = {
		name = "Leonello"
		dynasty = "d'Este"
		ADM = 2
		DIP = 4
		MIL = 3
	}
}

1450.10.2 = {
	monarch = {
		name = "Borso"
		dynasty = "d'Este"
		ADM = 4
		DIP = 5
		MIL = 3
	}
	government = feudal_monarchy
	remove_country_modifier = title_2 add_country_modifier = { name = title_3 duration = -1 }
}

1471.8.21 = {
	monarch = {
		name = "Ercole"
		dynasty = "d'Este"
		ADM = 6
		DIP = 4
		MIL = 4
	}
}

1505.6.16 = {
	monarch = {
		name = "Alfonso"
		dynasty = "d'Este"
		ADM = 6
		DIP = 5
		MIL = 4
	}
}
1530.1.2 = {
	government = administrative_monarchy
}

1534.10.30 = {
	government = despotic_monarchy
}

1534.10.30 = {
	monarch = {
		name = "Ercole II"
		dynasty = "d'Este"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}

1559.10.4 = {
	monarch = {
		name = "Alfonso II"
		dynasty = "d'Este"
		ADM = 5
		DIP = 3
		MIL = 2
	}
}
