# NUP - Kingdom of Nupe

government = tribal_republic government_rank = 1
mercantilism = 0.0
technology_group = sub_saharan
primary_culture = yorumba
religion = sunni
capital = 1551

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1354.1.1 = {
	monarch = {
		name = "Oduduwa"
		dynasty = "Oduduwa"
		DIP = 4
		ADM = 4
		MIL = 2
	}
}
