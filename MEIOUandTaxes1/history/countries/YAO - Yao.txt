# YAO - Sultanate of Yao

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = soudantech
religion = sunni
primary_culture = chadic
# capital = 1225 # not colonised
capital = 2945 # Yao

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1356.1.1 = {
	monarch = {
		name = "Ali Gaji"
		dynasty = "Sefuwa"
		DIP = 3
		ADM = 2
		MIL = 2
	}
}
