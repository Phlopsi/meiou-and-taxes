# SLL - Shilluk

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = soudantech
religion = animism
primary_culture = shilluk
capital = 3030

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1356.1.1 = {
	monarch = {
		name = "Duwadh"
		dynasty = "Nyikang"
		ADM = 1
		DIP = 1
		MIL = 1
	}
} # Invention

1600.1.1 = {
	monarch = {
		name = "Odak"
		dynasty = "Ochollo"
		ADM = 1
		DIP = 1
		MIL = 1
	}
} # Invention
