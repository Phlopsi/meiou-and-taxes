# XAL - Xalisco

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = tecos
religion = nahuatl
technology_group = mesoamerican
capital = 856 # ZApotlan

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1356.1.1   = {
	monarch = {
		name = "Monarch"
		dynasty = "Xalisco"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
