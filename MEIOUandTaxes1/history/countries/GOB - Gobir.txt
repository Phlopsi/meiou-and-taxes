# GOB - Gobir

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = haussa
religion = sunni
technology_group = sub_saharan
capital = 2924 # Gobir

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1350.1.1 = {
	monarch = {
		name = "Muhammad"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}
