# SUW - Suwa
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = kanto
religion = mahayana
technology_group = chinese
capital = 3974 # Suwa

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1336.1.1 = {
	monarch = {
		name = "Tadayori" 
		dynasty = "Suwa"
		ADM = 4
		DIP = 3
		MIL = 4
		}
}

1350.1.1 = {
	heir = {
		name = "Nobuari"
		monarch_name = "Nobuari"
		dynasty = "Suwa"
		birth_date = 1350.1.1
		death_date = 1392.1.1
		claim = 70
		ADM = 5
		DIP = 2
		MIL = 3
	}
}
