
government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = vezu
religion = animism
technology_group = malagasy_tech
capital = 1227

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1350.1.1 = {
	monarch = {
		name = "Tribal Elders"
		dynasty = "Betanimena"
		ADM = 3
		DIP = 3
		MIL = 3
		regent = yes
	}
}
1712.1.1 = {
	monarch = {
		name = "Ramaromango"
		dynasty = "Betanimena"
		DIP = 4
		ADM = 6
		MIL = 5
	}
}

1750.8.1 = {
	monarch = {
		name = "Zanahary*"
		dynasty = "Betanimena"
		DIP = 2
		ADM = 4
		MIL = 6
	}
}

1767.5.1 = {
	monarch = {
		name = "Iavy"
		dynasty = "Betanimena"
		DIP = 1
		ADM = 3
		MIL = 2
	}
}

1791.1.1 = {
	monarch = {
		name = "Zakavola"
		dynasty = "Betanimena"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1803.3.1 = {
	monarch = {
		name = "Anarchy"
		dynasty = "Betanimena"
		DIP = 0
		ADM = 0
		MIL = 0
	}
}

1811.2.18 = {
	monarch = {
		name = "Jean Ren�"
		dynasty = "Betanimena"
		DIP = 5
		ADM = 6
		MIL = 5
	}
}

1826.1.1 = {
	monarch = {
		name = "Kisiharo*"
		dynasty = "Betanimena"
		DIP = 5
		ADM = 6
		MIL = 5
	}
}
