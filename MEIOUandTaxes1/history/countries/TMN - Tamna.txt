# TMN - Tamna

government = chinese_monarchy_2 government_rank = 1
mercantilism = 0.0
primary_culture = korean
religion = mahayana
technology_group = chinese
capital = 2276

historical_rival = KOR
historical_rival = AKG
historical_rival = JAP

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = -2 }
	add_absolutism = -100
	add_absolutism = 70
}

1352.1.1 = {
	monarch = {
		name = "Bong-ye"
		dynasty = "Go"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
