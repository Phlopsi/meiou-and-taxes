# LUC - Lucca
# 2010-jan-21 - FB - HT3 changes

government = oligarchic_republic
mercantilism = 5
# land_naval = -2			##1
# isolationist_expansionist = -2	##0
technology_group = western
primary_culture = tuscan
religion = catholic
capital = 115
historical_friend = GEN
historical_friend = MLO

1000.1.1  = {
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1399.1.1 = {
	monarch = {
		name = "Paolo"
        dynasty = "Giunigi"
		dip = 4
		mil = 5
		adm = 3
	}
	government = signoria_monarchy government_rank = 1
	add_country_modifier = { name = title_2 duration = -1 }
}

1430.8.15 = {
        monarch = {
                name = "Pietro Cenami"
                adm = 4
                dip = 4
                mil = 3
        }
		government = oligarchic_republic
}
1436.1.1 = {
        monarch = {
                name = "Lorenzo Buonvisi"
                adm = 3
                dip = 3
                mil = 3
        }
}
1460.1.1 = {
        monarch = {
                name = "Filippo Gentili"
                adm = 3
                dip = 3
                mil = 3
        }
}
1491.1.1 = {
        monarch = {
                name = "Francesco Cenami"
                adm = 3
                dip = 3
                mil = 3
        }
}
1494.1.1 = {
        monarch = {
                name = "Benedetto Buonvisi"
                adm = 3
                dip = 3
                mil = 3
        }
}

1556.1.1 = { government = noble_republic }

1805.1.1 = { government = enlightened_despotism remove_country_modifier = title_2 add_country_modifier = { name = title_3 duration = -1 } }

1805.1.1 = {
        monarch = {
        name = "Elisa"
		dynasty = "Bonaparte"
        adm = 4
		dip = 3
		mil = 1
        female = yes
	}
}

1815.1.1 = {
        monarch = {
        name = "Maria Luisa"
		dynasty = "de Bourbon"
        adm = 3
		dip = 3
		mil = 0
        female = yes
	}
}
