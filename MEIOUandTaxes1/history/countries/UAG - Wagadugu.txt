#MEIOU-GG Governemnt changes

government = tribal_republic government_rank = 1
mercantilism = 0.0
primary_culture = mali
religion = sunni
technology_group = sub_saharan
capital = 1495

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

#Kings of Mali
1350.1.1 = {
	monarch = {
		name = "City council"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}
