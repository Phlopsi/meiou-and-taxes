# KBI - Kabi Kingdom
# 2010-jan-16 - FB - HT3 changes

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = yorumba		
religion = sunni		 
technology_group = sub_saharan
capital = 1135

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}
