# VOR - Vorotinsk

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 313

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1356.1.1 = {
	monarch = {
		name = "Mikhael"
		dynasty = "Vorotinsky"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
