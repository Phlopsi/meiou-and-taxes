# SYG - Sarigyogir

government = steppe_horde government_rank = 1
primary_culture = old_uyghur
religion = mahayana
technology_group = steppestech
capital = 2258

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}
1356.1.1 = {
	monarch = {
		name = "Council of Elders"
		ADM = 2
		DIP = 2
		MIL = 2
		regent = yes
	}
}

1400.1.1 = {
	monarch = {
		name = "Bayan Temur"
		adm = 1
		dip = 2
		mil = 1
	}
}