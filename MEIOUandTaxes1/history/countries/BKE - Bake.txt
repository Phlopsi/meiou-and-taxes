# BKE - Bak� Kingdom

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = senegambian
religion = west_african_pagan_reformed
technology_group = sub_saharan
capital = 1121

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}
