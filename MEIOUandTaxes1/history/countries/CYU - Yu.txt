government = chinese_monarchy_2 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = hanyu
capital = 702

historical_rival = QIN
historical_rival = ZHE

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
	add_country_modifier = { name = mongol_xingsheng duration = -1 }	
}
1330.1.1 = {
	monarch = {
		name = "Chagan Tem�r"
		dynasty = "Bayad"
		ADM = 4
		DIP = 3
		MIL = 6
	}
	define_ruler_to_general = {
		fire = 5
		shock = 5
		manuever = 4	
		siege = 3 
	}
	add_ruler_modifier = { name = "brilliant_strategist" }
	add_ruler_modifier = { name = "tough_soldier" }	
}
1351.1.1  = {
#	set_country_flag = lost_mandate_of_heaven
}
1356.1.1 = {
	heir = {
		name = "K�ke Tem�r"
		monarch_name = "K�ke Tem�r"
		dynasty = "Bayad"
		birth_date = 1330.1.1
		death_date = 1375.1.1
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 6
	}
}
1362.6.6 = {
	monarch = {
		name = "K�ke Tem�r"
		dynasty = "Bayad"
		ADM = 2
		DIP = 4
		MIL = 6
	}
}