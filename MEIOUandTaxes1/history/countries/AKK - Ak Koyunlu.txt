# AKK - Ak Koyunlu
# 2009-dec-19 - FB - HT3 changes

government = altaic_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = yorouk
add_accepted_culture = azerbadjani 
add_accepted_culture = kurdish
add_accepted_culture = shami
add_accepted_culture = iraqi
religion = sunni
technology_group = muslim
capital = 418
historical_rival = QAR
historical_friend = TRE

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
}
1340.1.1 = {
	monarch = {
		name = "Tur Ali"
		dynasty = "Osman"
		ADM = 3
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Kutlu"
		monarch_name = "Kutlu Beg"
		dynasty = "Osman"
		birth_date = 1330.1.1
		death_date = 1378.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1356.1.1 = {
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1360.1.1 = {
	monarch = {
		name = "Kutlu Beg"
		dynasty = "Osman"
		ADM = 3
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Qara"
		monarch_name = "Qara Yoluq"
		dynasty = "Osman"
		birth_date = 1356.1.1
		death_date = 1435.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1396.1.1 = {
	monarch = {
		name = "Qara Yoluq"
		dynasty = "Osman"
		ADM = 3
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "'Ali Djalal"
		monarch_name = "'Ali Djalal"
		dynasty = "Osman"
		birth_date = 1396.1.1
		death_date = 1444.1.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1435.1.1 = {
	monarch = {
		name = "'Ali Djalal"
		dynasty = "Osman"
		ADM = 1
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Jahangir Mu'izz"
		monarch_name = "Jahangir Mu'izz"
		dynasty = "Osman"
		birth_date = 1424.1.1
		death_date = 1453.1.1
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1444.1.1 = {
	monarch = {
		name = "Jahangir Mu'izz"
		dynasty = "Osman"
		ADM = 3
		DIP = 4
		MIL = 3
	}
	heir = {
		name = "Usan Hasan"
		monarch_name = "Usan Hasan"
		dynasty = "Osman"
		birth_date = 1433.1.1
		death_date = 1478.1.6
		claim = 95
		ADM = 5
		DIP = 5
		MIL = 6
	}
	define_heir_to_general = {
		fire = 4
		shock = 5
		manuever = 5
		siege = 2 
	}
}

1453.1.1 = {
	monarch = {
		name = "Usun Hasan"
		dynasty = "Osman"
		ADM = 5
		DIP = 5
		MIL = 6
	}
	define_ruler_to_general = { 
		fire = 4	
		shock = 5
		manuever = 5 
		siege = 0 
	}
	heir = {
		name = "Khal�l"
		monarch_name = "Khal�l"
		dynasty = "Osman"
		birth_date = 1453.1.1
		death_date = 1478.7.16
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1469.1.1 = { capital = 414 } # Tabriz

1478.1.6 = {
	monarch = {
		name = "Khal�l"
		dynasty = "Osman"
		ADM = 3
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Ya'q�b"
		monarch_name = "Ya'q�b"
		dynasty = "Osman"
		birth_date = 1458.1.1
		death_date = 1490.12.25
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1478.7.16 = {
	monarch = {
		name = "Ya'q�b"
		dynasty = "Osman"
		ADM = 2
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Baisunqur"
		monarch_name = "Baisunqur"
		dynasty = "Osman"
		birth_date = 1470.1.1
		death_date = 1492.5.23
		claim = 95
		ADM = 5
		DIP = 2
		MIL = 2
	}
}

1490.12.25 = {
	monarch = {
		name = "Baisunqur"
		dynasty = "Osman"
		ADM = 5
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Rustam"
		monarch_name = "Rustam"
		dynasty = "Osman"
		birth_date = 1470.1.1
		death_date = 1496.2.8
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1492.5.23 = {
	monarch = {
		name = "Rustam"
		dynasty = "Osman"
		ADM = 1
		DIP = 3
		MIL = 2
	}
	heir = {
		name = "Ahmad"
		monarch_name = "Ahmad"
		dynasty = "Osman"
		birth_date = 1475.1.1
		death_date = 1497.1.1
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1496.2.8 = {
	monarch = {
		name = "Ahmad"
		dynasty = "Osman"
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Murad"
		monarch_name = "Murad"
		dynasty = "Osman"
		birth_date = 1475.1.1
		death_date = 1498.1.1
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 1
	}
}

1497.1.1 = {
	monarch = {
		name = "Murad"
		dynasty = "Osman"
		ADM = 1
		DIP = 4
		MIL = 1
	}
	heir = {
		name = "Alwand"
		monarch_name = "Alwand"
		dynasty = "Osman"
		birth_date = 1475.1.1
		death_date = 1499.5.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1498.1.1 = {
	monarch = {
		name = "Alwand"
		dynasty = "Osman"
		ADM = 1
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Muhammed M�rz�"
		monarch_name = "Muhammed M�rz�"
		dynasty = "Osman"
		birth_date = 1475.1.1
		death_date = 1500.6.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1499.5.1 = {
	monarch = {
		name = "Muhammed M�rz�"
		dynasty = "Osman"
		ADM = 1
		DIP = 1
		MIL = 1
	}
	heir = {
		name = "Murad"
		monarch_name = "Murad"
		dynasty = "Osman"
		birth_date = 1475.1.1
		death_date = 1508.1.1
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 1
	}
}

1500.6.1 = {
	monarch = {
		name = "Murad"
		dynasty = "Osman"
		ADM = 1
		DIP = 4
		MIL = 1
	}
}

#Safavid Conquest
