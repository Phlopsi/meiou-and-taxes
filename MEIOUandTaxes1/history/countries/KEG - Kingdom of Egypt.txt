# KEG - Latin Egypt
# 2010-jan-16 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
# innovative_narrowminded = -3
mercantilism = 0.0
primary_culture = frankish
religion = catholic
technology_group = western
capital = 358

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}
