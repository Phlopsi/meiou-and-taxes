# SBA - Shiba clan

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = koshi
religion = mahayana
technology_group = chinese
capital = 2284		# Echizen

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1330.1.1 = { 
	monarch = {
		name = "Takatsune"
		dynasty = "Shiba"
		ADM = 3
		DIP = 3
		MIL = 4
		}
	}

1350.1.1 = {
	heir = {
		name = "Yoshimasa"
		monarch_name = "Yoshimasa"
		dynasty = "Shiba"
		birth_date = 1350.1.1
		death_date = 1410.6.9
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 3
	}
}	
	
1367.8.9 = { 
	monarch = {
		name = "Yoshimasa"
		dynasty = "Shiba"
		ADM = 4
		DIP = 4
		MIL = 3
		}
	}
	
1385.1.1 = {
	heir = {
		name = "Yoshishige"
		monarch_name = "Yoshishige"
		dynasty = "Shiba"
		birth_date = 1371.1.1
		death_date = 1418.9.10
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1410.6.9 = {
	monarch = {
		name = "Yoshishige" 
		dynasty = "Shiba"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1410.6.9 = {
	heir = {
		name = "Yoshiatsu"
		monarch_name = "Yoshiatsu"
		dynasty = "Shiba"
		birth_date = 1397.1.1
		death_date = 1434.1.11
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1418.9.10 = {
	monarch = {
		name = "Yoshiatsu" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1434.1.11 = {
	monarch = {
		name = "Yoshisato" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1435.1.1 = {
	heir = {
		name = "Yoshitake"
		monarch_name = "Yoshitake"
		dynasty = "Shiba"
		birth_date = 1435.1.1
		death_date = 1452.10.3
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1436.1.1 = {
	monarch = {
		name = "Yoshitake" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1452.10.1 = {
	monarch = {
		name = "Yoshitoshi" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 1
		MIL = 2
		}
}

1461.1.1 = {
	monarch = {
		name = "Yoshikado" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 1
		MIL = 2
		}
}

1461.1.1 = {
	heir = {
		name = "Yoshihiro"
		monarch_name = "Yoshihiro"
		dynasty = "Shiba"
		birth_date = 1457.1.1
		death_date = 1514.5.21
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1485.1.1 = {
	monarch = {
		name = "Yoshihiro" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1485.1.1 = {
	heir = {
		name = "Yoshitatsu"
		monarch_name = "Yoshitatsu"
		dynasty = "Shiba"
		birth_date = 1480.1.1
		death_date = 1521.1.1
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1514.5.21 = {
	monarch = {
		name = "Yoshitastu" 
		dynasty = "Shiba"
		ADM = 2
		DIP = 2
		MIL = 3
		}
}

1514.5.21 = {
	heir = {
		name = "Yoshimune"
		monarch_name = "Yoshimune"
		dynasty = "Shiba"
		birth_date = 1513.1.1
		death_date = 1554.8.10
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1515.1.1 = {
	monarch = {
		name = "Yoshimune" 
		dynasty = "Shiba"
		ADM = 1
		DIP = 1
		MIL = 1
		}
}

1540.1.1 = {
	heir = {
		name = "Yoshikane"
		monarch_name = "Yoshikane"
		dynasty = "Shiba"
		birth_date = 1540.1.1
		death_date = 1600.9.23
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1554.8.10 = {
	monarch = {
		name = "Yoshikane" 
		dynasty = "Shiba"
		ADM = 1
		DIP = 1
		MIL = 1
		}
}