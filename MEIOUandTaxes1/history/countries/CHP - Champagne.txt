# CHP - Duchy  of Champagne

government = feudal_monarchy government_rank = 1
# centralization_decentralization = -5
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = francien
capital = 2354	# Troyes

historical_friend = FRA

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = -5 }
	add_absolutism = -100
	add_absolutism = 100
}

1515.1.1 = {
	government = despotic_monarchy remove_country_modifier = title_3 add_country_modifier = { name = title_3 duration = -1 }
}

1589.8.3 = {
	government = administrative_monarchy remove_country_modifier = title_3 add_country_modifier = { name = title_3 duration = -1 }
}

1661.3.9 = {
	government = absolute_monarchy remove_country_modifier = title_3 add_country_modifier = { name = title_3 duration = -1 }
}
