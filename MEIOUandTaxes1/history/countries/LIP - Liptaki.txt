# LIP - Liptaki

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = mossi
religion = west_african_pagan_reformed
technology_group = sub_saharan
capital = 2903

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1333.1.1 = {
	monarch = {
		name = "Saydu"
		dynasty = "Brahima"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}
