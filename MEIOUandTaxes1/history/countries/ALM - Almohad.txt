# ALM - Almohad
# 2013-aug-07 - GG - EUIV changes

government = despotic_monarchy
government_rank = 5
# aristocracy_plutocracy = 4
# centralization_decentralization = -3
# innovative_narrowminded = -2
mercantilism = 0.1 # mercantilism_freetrade = -4
# offensive_defensive = 5
# land_naval = -3
# quality_quantity = 2
# serfdom_freesubjects = -2
primary_culture = berber # maure
religion = sunni
add_accepted_culture = andalucian
technology_group = muslim
capital = 347 # Marrakech

1266.1.1 = {
	monarch = {
		name = "Idris II"
		dynasty = "al-Muwahhidun"
		ADM = 4
		DIP = 6
		MIL = 4
	}
}

1356.1.1 = {
	set_variable = { which = "centralization_decentralization" value = -3 }
}
