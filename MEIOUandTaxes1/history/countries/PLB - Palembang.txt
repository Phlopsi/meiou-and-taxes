#PLB - Palembang
#Palembang was the capital of the Srivijaya empire 600-1280
#It was a centre of Sanskrit culture in the C7th to C12th and 
#is still a major city today
#FB-TO DO Palembang a vassal of Majapahit from c1625 to c1659
#need rulers from c1420 to 1659.1.1

government = eastern_monarchy government_rank = 1
mercantilism = 0.0
technology_group = austranesian		#FB was:chinese
primary_culture = malayan
religion = buddhism
capital = 3921 # Palembang

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

#1337.1.1 = {
#	monarch = {
#		name = "Ananggavarman"
#		ADM = 3
#		DIP = 3
#		MIL = 3
#	}
#}
#
#1374.1.1 = {
#	monarch = {
#		name = "Adityawarman"
#		ADM = 3
#		DIP = 3
#		MIL = 3
#	}
#}
1356.1.1 = {
	monarch = {
		name = "Regency Council"
		ADM = 3
		DIP = 3
		MIL = 3
		regent = yes
	}
}

1360.1.1 = {
	monarch = {
		name = "Parameswara"
		dynasty = "Svirijaya"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1478.1.1 = {
	religion = sunni
	remove_country_modifier = title_4
	clr_country_flag = title_4
 add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	monarch = {
		name = "Raden Fatah"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1518.1.1 = {
	monarch = {
		name = "Pati Unus"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1521.1.1 = {
	monarch = {
		name = "Pangeran Trenggono"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1546.1.1 = {
	monarch = {
		name = "Pangeran Seda Ing"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

# 1587 - Loyal to Mataram

# 1658.1.1 dutch established on the coast

1662.1.1 = {
	monarch = {
		name = "Abdul Rahman"
		ADM = 5
		DIP = 5
		MIL = 3
	}
}

1706.1.1 = {
	monarch = {
		name = "Mahmud Rahman"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}#invented ruler

1724.1.1 = {
	monarch = {
		name = "Mahmud Badaruddin"
		ADM = 5
		DIP = 4
		MIL = 3
	}
}

1757.1.1 = {
	monarch = {
		name = "Ahmad Tajuddin"
		ADM = 5
		DIP = 3
		MIL = 3
	}
}#ruled to 1774

#rulers below this taken from D&I
1798.1.1 = {
	monarch = {
		name = "Mahmud Badruddin bin Muhammad"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
1812.1.1 = {
	monarch = {
		name = "Ahmad Najmuddin bin Muhammad"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
1813.5.1 = {
	monarch = {
		name = "Ahmad Najmuddin II bin Muhammad"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
1821.1.1 = {
	set_country_flag = civil_war
	monarch = {
		name = "Mahmud Badruddin II bin Muhammad"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
