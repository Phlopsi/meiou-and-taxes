# KAA - Kingdom of Kaabu
# 2010-jan-16 - FB - HT3 changes

government = tribal_monarchy government_rank = 1
mercantilism = 10
primary_culture = senegambian
religion = west_african_pagan_reformed
technology_group = sub_saharan
capital = 1114 # Cayor

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}


1530.1.1 = {
	monarch = {
		name = "Sama Koli"
		dynasty = "Jeenung"
		dip = 2
		adm = 1
		mil = 3
	}
}