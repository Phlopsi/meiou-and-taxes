# OTM - �tomo clan
# LS/GG - Japanese Civil War
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
# aristocracy_plutocracy = -4
# centralization_decentralization = 2
# innovative_narrowminded = 2
mercantilism = 0.0 # mercantilism_freetrade = -5
# offensive_defensive = -1
# land_naval = 0
# quality_quantity = 4
# serfdom_freesubjects = -5
# isolationist_expansionist = -3
# secularism_theocracy = 0
primary_culture = kyushu
religion = mahayana
technology_group = chinese
capital = 1018		# Bungo

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1311.1.1 = {
	monarch = {
		name = "Sadamune"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1321.1.1 = {
	heir = {
		name = "Ujiyasu"
		monarch_name = "Ujiyasu"
		dynasty = "�tomo"
		birth_date = 1321.1.1
		death_date = 1362.11.19
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1334.1.9 = {
	monarch = {
		name = "Ujiyasu"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1362.11.19 = {
	monarch = {
		name = "Ujitoki"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1368.4.8 = {
	monarch = {
		name = "Chikayo"
		dynasty = "�tomo"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1386.1.1 = {
	heir = {
		name = "Chikaaki"
		monarch_name = "Chikaaki"
		dynasty = "�tomo"
		birth_date = 1385.1.1
		death_date = 1423.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
	
1401.1.1 = { 
	monarch = {
		name = "Chikaaki"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 3
		}
	}
	
1401.1.1 = {
	heir = {
		name = "Mochinao"
		monarch_name = "Mochinao"
		dynasty = "�tomo"
		birth_date = 1401.1.1
		death_date = 1445.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
	
1423.1.1 = { 
	monarch = {
		name = "Mochinao"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 3
		}
	}
	
1432.11.27 = {
	heir = {
		name = "Chikatsuna"
		monarch_name = "Chikatsuna"
		dynasty = "�tomo"
		birth_date = 1430.1.1
		death_date = 1459.1.1
		claim = 40
		ADM = 2
		DIP = 2
		MIL = 1
	}
}
	
1445.1.1 = { 
	monarch = {
		name = "Chikatsuna"
		dynasty = "�tomo"
		ADM = 2
		DIP = 2
		MIL = 1
		}
	}
	
1450.1.1 = {
	heir = {
		name = "Chikataka"
		monarch_name = "Chikataka"
		dynasty = "Shimazu"
		birth_date = 1430.1.1
		death_date = 1565.1.1
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
	
1453.1.1 = { 
	monarch = {
		name = "Chikataka"
		dynasty = "�tomo"
		ADM = 2
		DIP = 2
		MIL = 2
		}
	}
	
1453.1.1 = {
	heir = {
		name = "Chikashige"
		monarch_name = "Chikashige"
		dynasty = "�tomo"
		birth_date = 1411.1.1
		death_date = 1493.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
	
1461.1.1 = { 
	monarch = {
		name = "Chikashige"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 3
		}
	}
	
1461.1.1 = {
	heir = {
		name = "Masachika"
		monarch_name = "Masachika"
		dynasty = "�tomo"
		birth_date = 1444.1.1
		death_date = 1496.7.20
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
	
1473.1.1 = { 
	monarch = {
		name = "Masachika"
		dynasty = "�tomo"
		ADM = 2
		DIP = 2
		MIL = 2
		}
	}
	
1484.1.1 = {
	heir = {
		name = "Yoshisuke"
		monarch_name = "Yoshisuke"
		dynasty = "�tomo"
		birth_date = 1484.1.1
		death_date = 1496.5.27
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
	
1489.1.1 = { 
	monarch = {
		name = "Yoshisuke"
		dynasty = "�tomo"
		ADM = 1
		DIP = 1
		MIL = 1
		}
	}

1496.7.7 = {
	# innovative_narrowminded = 2
}
	
1496.7.7 = { 
	monarch = {
		name = "Chikaharu"
		dynasty = "�tomo"
		ADM = 3
		DIP = 3
		MIL = 4
		}
	}
	
1496.7.7 = {
	heir = {
		name = "Yoshinaga"
		monarch_name = "Yoshinaga"
		dynasty = "�tomo"
		birth_date = 1478.1.1
		death_date = 1518.1.1
		claim = 80
		ADM = 4
		DIP = 3
		MIL = 4
	}
}

1501.1.1 = { 
	monarch = {
		name = "Yoshinaga"
		dynasty = "�tomo"
		ADM = 4
		DIP = 3
		MIL = 4
		}
	}
	
1502.1.1 = {
	heir = {
		name = "Yoshiaki"
		monarch_name = "Yoshiaki"
		dynasty = "�tomo"
		birth_date = 1502.1.1
		death_date = 1550.2.28
		claim = 90
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1518.1.1 = { 
	monarch = {
		name = "Yoshiaki"
		dynasty = "�tomo"
		ADM = 3
		DIP = 2
		MIL = 3
		}
	}
	
1530.1.31 = {
	heir = {
		name = "Yoshishige"
		monarch_name = "Yoshishige"
		dynasty = "�tomo"
		birth_date = 1530.1.31
		death_date = 1587.6.11
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1550.2.28 = { 
	monarch = {
		name = "Yoshishige"
		dynasty = "�tomo"
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1558.1.1 = {
	heir = {
		name = "Yoshimune"
		monarch_name = "Yoshimune"
		dynasty = "�tomo"
		birth_date = 1558.1.1
		death_date = 1610.9.2
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1587.6.11 = { 
	monarch = {
		name = "Yoshimune"
		dynasty = "�tomo"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
	