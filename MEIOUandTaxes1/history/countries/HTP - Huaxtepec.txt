# HTP - Huaxtepec

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = nahuatl_c
religion = nahuatl
technology_group = mesoamerican
capital = 2408 # Huaxtepec

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}
