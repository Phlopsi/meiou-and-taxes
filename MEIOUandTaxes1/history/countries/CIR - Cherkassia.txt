# CIR - Cherkassia

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
# isolationist_expansionist = -5
primary_culture = circassian
religion = sunni
technology_group = muslim
capital = 1302
historical_neutral = GAZ
historical_neutral = AST
historical_neutral = PER

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1333.1.1 = {
	monarch = {
		name = "Verzacht"
		dynasty = "Zichia"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1471.1.1 = {
	monarch = {
		name = "Petrezok"
		dynasty = "Zichia"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}