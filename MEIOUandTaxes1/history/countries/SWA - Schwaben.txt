# SWA - Swabia

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = schwabisch
religion = catholic
technology_group = western
capital = 77

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1050.1.1 = {
	monarch = {
		name = "Henry III"
		dynasty = "Salian"
		DIP = 3
		ADM = 3
		MIL = 1
	}
}

1534.1.1 = {
	religion = protestant
	government = constitutional_monarchy remove_country_modifier = title_3 add_country_modifier = { name = title_6 duration = -1 }}
