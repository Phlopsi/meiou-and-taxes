# MRC - Oroch

government = altaic_monarchy government_rank = 1
mercantilism = 0.0
technology_group = steppestech
primary_culture = jurchen
religion = tengri_pagan_reformed
capital = 3246	# Khanka

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1356.1.1 = {
	monarch = {
		name = "Council of Elders"
		ADM = 2
		DIP = 2
		MIL = 2
		regent = yes
	}
}