# KUR - Kurland
# 2010-jan-21 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = baltendeutsche
religion = catholic
capital = 39	# Mitau

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1400.1.1 = {
	monarch = {
		name = "The Grandmasters"
		ADM = 2
		DIP = 3
		MIL = 3
	}
}

1522.1.1 = {
	religion = protestant
	}

1569.11.25 = {
	heir = {
		name = "Friedrich"
		monarch_name = "Friedrich"
		dynasty = "Kettler"
		birth_date = 1569.11.25
		death_date = 1642.8.17
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1587.5.18 = {
	monarch = {
		name = "Friedrich"
		dynasty = "Kettler"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1610.10.28 = {
	heir = {
		name = "Jakob"
		monarch_name = "Jakob"
		dynasty = "Kettler"
		birth_date = 1610.10.28
		death_date = 1682.1.1
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 6
	}
}

1641.8.18 = {
	monarch = {
		name = "Jakob"
		dynasty = "Kettler"
		ADM = 4
		DIP = 3
		MIL = 6
	}
}

1650.7.6 = {
	heir = {
		name = "Friedrich Kasimir"
		monarch_name = "Friedrich Kasimir"
		dynasty = "Kettler"
		birth_date = 1650.7.6
		death_date = 1698.1.22
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1681.12.30 = {
	monarch = {
		name = "Friedrich Kasimir"
		dynasty = "Kettler"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1692.7.19 = {
	heir = {
		name = "Friedrich Wilhelm"
		monarch_name = "Freidrich Wilhelm"
		dynasty = "Kettler"
		birth_date = 1692.7.19
		death_date = 1711.1.21
		claim = 95
		ADM = 2
		DIP = 5
		MIL = 4
	}
}

1698.1.23 = {
	monarch = {
		name = "Friedrich Wilhelm"
		dynasty = "Kettler"
		ADM = 2
		DIP = 5
		MIL = 4
	}
}

1711.1.3 = {
	monarch = {
		name = "Anna Ivanova"
		dynasty = "Romanov"
		ADM = 3
		DIP = 3
		MIL = 3
		female = yes
	}
}

1730.1.1 = {
	monarch = {
		name = "Ferdinand"
		dynasty = "Kettler"
		ADM = 1
		DIP = 3
		MIL = 3
	}
}

1737.5.5 = {
	monarch = {
		name = "Ernst Johann"
		dynasty = "von Biron"
		ADM = 1
		DIP = 3
		MIL = 1
	}
}

1758.11.17 = {
	monarch = {
		name = "Karl Christian Joseph"
		dynasty = "von Wettin"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1763.2.20 = {
	monarch = {
		name = "Ernst Johann"
		dynasty = "von Biron"
		ADM = 1
		DIP = 3
		MIL = 1
	}
}

1763.2.20 = {
	heir = {
		name = "Peter"
		monarch_name = "Peter"
		dynasty = "von Biron"
		birth_date = 1724.2.15
		death_date = 1800.1.13
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 4
	}
}
		
1769.11.25 = {
	monarch = {
		name = "Peter"
		dynasty = "von Biron"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}
