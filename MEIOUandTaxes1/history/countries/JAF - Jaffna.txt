# Country: Jaffna
# file name: JAF - Jaffna
# MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG Religion changes
# 2010-jan-16 - FB - HT3 changes

government = indian_monarchy government_rank = 1		#FB was eastern_monarchy_2
mercantilism = 0.0
technology_group = indian
religion = hinduism
primary_culture = tamil
capital = 572	# Jaffna
fixed_capital = 572 # 

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1356.1.1 = {
	monarch = {
		name = "Virodaya"
		dynasty = "Cinkaiariyan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1380.1.1 = {
	monarch = {
		name = "Jeyaveera"
		dynasty = "Cinkaiariyan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1410.1.1 = {
	monarch = {
		name = "Gunaveera"
		dynasty = "Cinkaiariyan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1440.1.1 = {
	monarch = {
		name = "Kanakasooriya"
		dynasty = "Cinkaiariyan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1450.1.1 = {
	monarch = {
		name = "Sapumal"
		dynasty = "Cinkaiariyan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1467.1.1 = {
	monarch = {
		name = "Kanakasooriya II"
		dynasty = "Cinkaiariyan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1478.1.1 = {
	monarch = {
		name = "Singai"
		dynasty = "Pararasasegaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1519.1.1 = {
	monarch = {
		name = "Cankili"
		dynasty = "Pandaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1561.1.1 = {
	monarch = {
		name = "Puviraja"
		dynasty = "Pandaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1565.1.1 = {
	monarch = {
		name = "Periyapillai"
		dynasty = "Pandaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1582.1.1 = {
	monarch = {
		name = "Puviraja"
		dynasty = "Pandaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1591.1.1 = {
	monarch = {
		name = "Ethirimana Cinkam"
		dynasty = "Pandaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1616.1.1 = {
	monarch = {
		name = "Cankili II"
		dynasty = "Pandaram"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1619.1.1 = {
	monarch = {
		name = "Phelipe"
		dynasty = "de Oliveira"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

# 1619 - Part of the Portuguese Empire