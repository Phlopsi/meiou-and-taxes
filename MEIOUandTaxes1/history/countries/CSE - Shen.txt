
government = chinese_monarchy_2 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = mahayana
primary_culture = korean
capital = 4035

historical_rival = KOR

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 50
}
1307.1.1 = {
	monarch = {
		name = "Jung-ang"
		dynasty = "Wang"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1316.1.1 = {
	monarch = {
		name = "Go"
		dynasty = "Wang"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1345.1.1 = {
	monarch = {
		name = "Heun"
		dynasty = "Wang"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1348.1.1 = {
	monarch = {
		name = "Jeo"
		dynasty = "Wang"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1354.1.1 = {
	monarch = {
		name = "Toghtua Bukha"
		dynasty = "Wang"
		ADM = 4
		DIP = 4
		MIL = 5
	}
}
