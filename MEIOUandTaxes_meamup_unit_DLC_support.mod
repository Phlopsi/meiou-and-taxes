name="M&T El Dorado Content DLC Support"
path="mod/MEIOUandTaxes_meamup_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesEDC.jpg"
supported_version="1.21.*.*"
