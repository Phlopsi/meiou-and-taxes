# No previous file for Pequtt�og

owner = PEQ
controller = PEQ
add_core = PEQ
is_city = yes
culture = pequot
religion = totemism
capital = "Pequtt�og"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 3
native_hostileness = 4

1614.1.1  = {  } # Adriaen Block
1636.1.1  = {	
		owner = ENG
		controller = ENG
		capital = "Providence"
		citysize = 1100
		religion = reformed
		culture = english
		trade_goods = fish
	    } # British settlers from Massachusetts
1650.1.1  = { citysize = 2480 }
1661.1.1  = { add_core = ENG }
1687.10.1 = { unrest = 4 } # The unwelcomed arrival of Edmund Andros
1689.1.1  = { unrest = 0 } # The colonials overthrow the new President of New England after James II is overthrown
1700.1.1  = { citysize = 3400 }
1707.5.12  = {
	
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1  = { citysize = 4985 }
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = { unrest = 0 remove_core = GBR } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1  = { citysize = 5523 }
