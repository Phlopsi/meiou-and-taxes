# No previous file for Chaui

owner = PAW
controller = PAW
add_core = PAW
is_city = yes
culture = pawnee
religion = totemism
capital = "Chaui"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 4
native_hostileness = 6
