# No previous file for Winnipeg

culture = cree
religion = totemism
capital = "Winnipeg"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 5

1664.1.1 = {  }
1710.1.1  = {
	owner = ASI
	controller = ASI
	add_core = ASI
	is_city = yes
	trade_goods = fur
	culture = nakota
} #Horses cause waves of migration on the Great Plains
1732.1.1 = {  } # Pierre Gaultier de Varennes
1734.1.1 = {
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
	capital = "Fort Maurepas"
	citysize = 100
}
1763.2.10 = {
	
	owner = GBR
	controller = GBR
	citysize = 450
	culture = english
	religion = protestant
	trade_goods = fur
}# Treaty of Paris
1788.2.10 = { add_core = GBR }
