# No previous file for Adirondack

culture = iroquois
religion = totemism
capital = "Adirondack"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 40
native_ferocity = 2
native_hostileness = 5

1615.1.1  = {  } # �tienne Br�l� 
1629.1.1  = {  }
1650.1.1  = {
	owner = IRO
	controller = IRO
	add_core = IRO
	is_city = yes
	trade_goods = naval_supplies
} #Extent of the Iroquois circa the Beaver Wars
1707.5.12 = {  }
1784.10.22  = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
	is_city = yes
} #Second Treaty of Fort Stanwix, Iroquois confederacy no longer dominant
1809.10.22 = { add_core = USA }
