# No previous file for Sauk

culture = potawatomi
religion = totemism
capital = "Sauk"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 55
native_ferocity = 4
native_hostileness = 8

1609.1.1 = {  }
1611.1.1 = {  }
1650.1.1 = {
	owner = OTW
	controller = OTW
	add_core = OTW
	is_city = yes
	trade_goods = fur
}
1680.1.1  = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1690.1.1  = { culture = potawatomi } #Retaken by French/Allies in Beaver Wars
1707.5.12 = {  }
1813.10.5 = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
	is_city = yes
} #The death of Tecumseh mark the end of organized native resistance 
