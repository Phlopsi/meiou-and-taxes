# No previous file for Altamaha

owner = CRE
controller = CRE
add_core = CRE
is_city = yes
culture = creek
religion = totemism
capital = "Altamaha"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1524.1.1   = {  } # Diego Gomez
1650.1.1 = { trade_goods = cotton } #Formation of the Creek confederacy
1736.1.1 = {	owner = GBR 
		controller = GBR
		citysize = 500
		culture = english
		religion = protestant } #Settlement of Fort Augusta
1761.1.1 = {	add_core = GBR }
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
		culture = american
	    } # Declaration of independence
1782.11.1 = { remove_core = GBR unrest = 0 } # Preliminary articles of peace, the British recognized American independence	
