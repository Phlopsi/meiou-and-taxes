# No previous file for Teyas

culture = apache
religion = totemism
capital = "Teyas"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 8

1760.1.1  = {	owner = COM
		controller = COM
		add_core = COM
		is_city = yes } #Great Plain tribes spread over vast territories
