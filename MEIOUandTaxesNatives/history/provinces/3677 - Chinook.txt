# No previous file for Chinook

#owner = CNK
#controller = CNK
#add_core = CNK
#is_city = yes
culture = chinook
religion = totemism
capital = "Chinook"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 4

1543.1.1 = {  }
1778.1.1 = {  }
1811.1.1 = {
	owner = USA
	controller = USA
	citysize = 350
	religion = protestant
	culture = american 
} #Fort Astoria
1814.12.24 = {
	owner = GBR
	controller = GBR
	citysize = 200
	culture = english
	religion = protestant
} # British control after the War of 1812
