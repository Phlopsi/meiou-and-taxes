# No previous file for Ongniaahratonon

culture = neutral
religion = totemism
capital = "Ongniaahratonon"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 1 
native_hostileness = 6

1615.1.1  = {  } # �tienne Br�l�
1629.1.1  = {  }
1650.1.1  = { 	owner = HUR
		controller = HUR
		add_core = HUR
		is_city = yes	
		culture = huron } #Fleeing from the Beaver Wars
1656.1.1  = { 	owner = IRO
		controller = IRO
		citysize = 100
		culture = iroquois } #Driven off by Iroquois raids
1679.1.1  = {   }
1701.8.4  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
	    }  #Great Peace of Montreal
1726.1.1  = { add_core = FRA }
1750.1.1  = { citysize = 5900 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.1.1  = { 	culture = english
		religion = protestant 
	} #Loyalist migration after the revolution
1788.2.10 = { add_core = GBR }
1800.1.1  = { citysize = 6350 }
