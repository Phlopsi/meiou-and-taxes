# No previous file for Muh-hek

add_core = MAH
owner = MAH
controller = MAH
is_city = yes
culture = mahican
religion = totemism
capital = "Muh-hek"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 2
native_hostileness = 5

1615.1.1  = {  } # �tienne Br�l� 
1629.1.1  = {  }
1707.5.12 = {  }
1784.10.22  = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
} #Second Treaty of Fort Stanwix, Iroquois confederacy no longer dominant
1809.10.22 = { add_core = USA }
