# No previous file for Ihantowan

#owner = ARP
#controller = ARP
#add_core = ARP
#is_city = yes
culture = lakota
religion = totemism
capital = "Ihantowan"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 55
native_ferocity = 3
native_hostileness = 4

1732.1.1 = {  } # La Vérendrye
1760.1.1  = {
	owner = ASI
	controller = ASI
	add_core = ASI
	is_city = yes
	trade_goods = fur
	culture = nakota
} #Horses cause waves of migration on the Great Plains
1800.1.1  = { citysize = 1580 }
1808.1.1 = {	owner = GBR
		controller = GBR
		citysize = 500
		capital = "Grandes Fourches"
	    	culture = english
	    	religion = protestant
	    } #French fur traders from Canada establish trading posts (part of Hudson Bay territory at the time, Hudson Bay watershed)
1818.10.20 = {	owner = USA
		controller = USA
		add_core = USA
		culture = american
		religion = protestant
} #ceded in the treaty of 1818, not renamed Grand Forks until 1870
