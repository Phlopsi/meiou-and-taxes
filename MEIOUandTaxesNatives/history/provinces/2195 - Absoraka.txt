# No previous file for Absoraka

add_core = COM
owner = COM
controller = COM
is_city = yes
culture = nakota
religion = totemism
capital = "Absoraka"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 4

1710.1.1  = {	owner = XXX
		controller = XXX
		remove_core = COM } #Horses cause waves of migration on the Great Plains
1810.1.1  = {	owner = BLA
		controller = BLA
		add_core = BLA
		culture = blackfoot
		is_city = yes } #Lakota pushes west across the Missouri
