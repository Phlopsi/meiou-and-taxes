name="M&T Thirty Years' War Majors DLC Support"
path="mod/MEIOUandTaxes_catmaj_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesTYW.jpg"
supported_version="1.21.*.*"
