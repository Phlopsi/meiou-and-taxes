name="M&T Mandate of Heaven Content DLC Support"
path="mod/MEIOUandTaxes_mandateofheaven_content_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesMoH.jpg"
supported_version="1.21.*.*"
