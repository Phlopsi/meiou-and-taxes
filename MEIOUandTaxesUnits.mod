name="MEIOU and Taxes Units Module v2.0"
path="mod/MEIOUandTaxesUnits"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesUS.jpg"
supported_version="1.21.*.*"
