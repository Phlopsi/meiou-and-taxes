name="M&T French and English Colonial Unit DLC Support"
path="mod/MEIOUandTaxes_cupb_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.00"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesFIW.jpg"
supported_version="1.21.*.*"
